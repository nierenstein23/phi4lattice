#ifndef CLASSPECFUNC_H
#define CLASSPECFUNC_H

#include <specfunc.h>
#include <boost/serialization/base_object.hpp>

class ClassicalSpectralFunction : public SpectralFunction {
    public:
        ClassicalSpectralFunction(){};
        ClassicalSpectralFunction(const int theSize, const float theTimestep) : SpectralFunction(theSize, theTimestep) {};

        void createFFT();

        void readTData(const DataSeries &data, double tau=0.0);
        void readTData(std::vector<Tfloat> *phi, std::vector<Tfloat> *pi, paramset params);
        void readTData(std::vector<Tcomplex> *phi, std::vector<Tcomplex> *pi, paramset params);
//        void readTData(std::vector<Tfloat> *field, paramset params);

    private:
        friend class boost::serialization::access;
        template<class Archive> void serialize(Archive & ar, const unsigned int version) {

            ar & boost::serialization::base_object<SpectralFunction>(*this);
        }
};

#endif
