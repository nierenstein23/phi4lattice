#ifndef MHRW_H
#define MHRW_H

#include <global.h>
#include <map>
#include <vector>
#include <datafile.h>
#include <iostream>

// can easily switch precision
typedef long double Tprec;

class MultiHistogram {
    public: 
        void readData(std::vector<std::pair<Tfloat, std::vector<Tfloat> > >& energies);
        void Reweigh();
        std::vector< std::vector<Tprec> > getWeights(Tprec T) const;


        // execution & precision control
        int MaxIterations = 64, MinElements=512, Divisor=4;
        Tprec maxDeviation = std::pow(10.0,-12);

    private:
        std::vector<Tprec> weights;
        std::vector<size_t> lengths;
        std::vector<std::pair<Tfloat, std::vector<Tfloat> > > energies;

        Tprec Emean;
        size_t totalPoints;

        bool updateDeviation(Tprec **Jacobian, std::vector<Tprec>& function, int DataIncrement) const;
        void enforceNorm(Tprec **Jacobian, std::vector<Tprec>& function) const;
        void inverseWeighting(Tprec **Jacobian, std::vector<Tprec>& function) const;
        std::vector<Tprec> SolveLinearEquation(Tprec ** A, int nvar);

        int NumberOfDataFiles() const { return energies.size(); };
        int NumberOfVariables() const { return NumberOfDataFiles(); };
        int NumberOfEquations() const { return NumberOfDataFiles() +1; };
};

#endif
