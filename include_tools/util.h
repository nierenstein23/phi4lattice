#ifndef UTIL_H
#define UTIL_H
#include <cmath>
#include <global.h>
#include <functional>

#include <algorithm>
#include <numeric>
#include <vector>

#include <iostream>

/**
 * @Synopsis  Find a divisor greater than a minium
 *
 * @Param number should have a divisor
 * @Param minDivisor minimum divisor
 *
 * @Returns	a divisor greater than minDivisor
 */
size_t greaterDivisor(size_t number, Tfloat minDivisor) {
	for (int i=static_cast<int>(std::ceil(minDivisor));i<number/2+1;i++) {
		if (number%i == 0) return i;
	}
	return number;
}


Tfloat autoCorrTime(std::vector<Tfloat> values) {
    Tfloat tAutoCorr = 0.0;
    // first, normalize
    Tfloat mean = std::accumulate(values.begin(), values.end(), 0.0)/values.size();
    std::transform(values.begin(), values.end(), values.begin(), std::bind2nd(std::minus<Tfloat>(), mean));

    // second, find variance
    Tfloat variance = std::inner_product(values.begin(), values.end(), values.begin(), 0.0)/(values.size() -1);

    // iterate until negative slope or reached full measuring time
    size_t i=1;
    
    while (i < values.size() - 1) {
        //
        // inner product with shift i
        Tfloat pAi = std::inner_product(values.begin() + i, values.end(), values.begin(), 0.0)/(variance*(values.size()-i));

        if (pAi < 0) break;
        tAutoCorr += pAi;
        i++;
    }

    return tAutoCorr;
};


/**
 * @Synopsis  Resample a dataset into bins and jackknife.
 *              If binsize is not a divisor of the total size, 
 *              the rest is ignored
 *
 * @Param values	the dataset
 * @Param binmean	target for the calculated mean
 * @Param binvar	target for the variance
 * @Param binsize	size of the bins 
 * @Param size		size of the dataset
 */
void binning(Tfloat *values, Tfloat &binmean, Tfloat &binvar, size_t binsize, size_t size) {
    int nbins=size/binsize;
    std::vector<Tfloat> binvalues(nbins, 0.0), jackknifes(nbins);

    binmean = binvar = 0.0;
	
    for (size_t i=0;i<nbins;i++)
	for (size_t j=0;j<binsize;j++)
	    binvalues[i] += values[i*binsize + j]/binsize;
	
    binmean = std::accumulate(binvalues.begin(), binvalues.end(), 0.0)/nbins;
    std::transform(binvalues.begin(), binvalues.end(), jackknifes.begin(), [=](Tfloat x){ return (binmean - x)/(nbins-1); });

    binvar = (nbins-1.0)/nbins*std::inner_product(jackknifes.begin(), jackknifes.end(), jackknifes.begin(), 0.0);
}


/**
 * @Synopsis  overload of binning(pointer, ..., size) with vector syntax
 *
 * @Param values
 * @Param binmean
 * @Param binvar
 * @Param binsize
 */
void binning(std::vector<Tfloat> values, Tfloat &binmean, Tfloat &binvar, size_t binsize) {
    size_t size = values.size();
    int nbins = size/binsize;

    std::vector<double> binvalues(nbins), jackknifes(nbins);

    for (int i=0;i<nbins;i++)
        binvalues[i] = std::accumulate(values.begin() + i*binsize, values.begin() + (i+1)*binsize, 0.0)/binsize;

    binmean = std::accumulate(binvalues.begin(), binvalues.end(), 0.0)/nbins;

    std::transform(binvalues.begin(), binvalues.end(), jackknifes.begin(), [binmean,nbins](double b){ return (binmean - b)/(nbins-1); });

    binvar = std::inner_product(jackknifes.begin(), jackknifes.end(), jackknifes.begin(), 0.0);
}


/**
 * @Synopsis  Calculate the variance naively on the CPU.
 *
 * @Param values	the dataset 
 * @Param mean		its mean
 * @Param size		its size
 *
 * @Returns	the variance
 */
Tfloat variance(Tfloat *values, Tfloat mean, size_t size) {
	Tfloat result=0;

	for (size_t i=0;i<size;i++) {
		result += SQR(values[i]-mean);
	}

	return result/size;
}

/**
 * @Synopsis  linearize vector of vectors into one vector
 *
 * @tparam T value type in vector
 * @Param in vector of vectors
 *
 * @Returns linearized input vector
 */
template<class T> std::vector<T> linearize(std::vector< std::vector<T> > in) {
    size_t totalSize=0;
    for (int i=0;i<in.size();i++)
        totalSize += in[i].size();

    std::vector<T> out;
    out.reserve(totalSize);
    for (int i=0;i<in.size();i++)
        out.insert(out.end(), in[i].begin(), in[i].end());

    return out;
};
 #endif
