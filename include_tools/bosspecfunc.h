#ifndef BOSSPECFUNC_H
#define BOSSPECFUNC_H

#include <specfunc.h>
#include <boost/serialization/base_object.hpp>

class BosonicSpectralFunction : public SpectralFunction {
    public:
        BosonicSpectralFunction(const int theSize, const float theTimestep) : SpectralFunction(theSize, theTimestep) {};

        void createFFT();

        void readTData(const DataSeries &data, double tau=0.0);
        void readTData(std::vector<Tfloat> *obs, paramset params);
//        void readTData(std::vector<Tcomplex> *obs, paramset params);

    private:
        Tfloat T;

        friend class boost::serialization::access;
        template<class Archive> void serialize(Archive & ar, const unsigned int version) {

            ar & boost::serialization::base_object<SpectralFunction>(*this);
            ar & T;
        }
};

#endif
