#ifndef SPECFUNC_H
#define SPECFUNC_H

#include <algorithm>
#include <boost/serialization/access.hpp>
//#include <boost/serialization/split_member.hpp>
#include <boost/serialization/vector.hpp>
#include <fftw3.h>
#include <datafile.h>

#include <type_traits>

#include <exception>

/**
 * @Synopsis A data encapsulation class for spectral functions
 */
class SpectralFunction {
    public:
        SpectralFunction(){};
        SpectralFunction(const int theSize, const float theTimestep);
//      ~SpectralFunction();

        float getMaxTime() const;
        float getTimestep() const;
        float getFreqStep() const;
        int getTSize() const;
        int getFSize() const;

        float getSFatTimeStep(const int step) const;
        float getSFatFreqStep(const int step) const;

        float getSFRealPart(const int step) const;
        float getSFImagPart(const int step) const;

        virtual void readTData(const DataSeries &data, double tau=0.0) { throw std::runtime_error("Function not bound!"); };
        virtual void createFFT() { throw std::runtime_error("Function not bound!"); };

    private:
        int size;
        float timestep;

    protected:
        std::vector<float> SFTime; 
        std::vector<float> SFFreq;
        bool hasData;
        bool hasFFT;

        friend class boost::serialization::access;
        template<class Archive> void serialize(Archive &ar, const unsigned int version) {
            ar & size & timestep & hasData & hasFFT;
            ar & SFTime & SFFreq;
        }
};
#endif
