#ifndef CPU_LATTICE_HCU
#define CPU_LATTICE_HCU

#include <lattice.h>
#include <cpu_rvectarray.hpp>

class CPULattice : virtual public iLattice {
    private:
        void doBackupPhi() { d_phi_backup.copyFrom(d_phi); };
        void doRestorePhi() { d_phi.copyFrom(d_phi_backup); };

        void doStepPhi(const Tfloat dt) { d_phi.saxpy(dt, d_pi); zMomentumIsCurrent = EzMomentumIsCurrent = false; };

        void doUpdateStressTensor();

        void dampPi(const Tfloat damping) { d_pi.saxpy(damping, d_pi); }
        void generateNoiseVector(Tfloat amplitude);

        void doSeedTaus(const long newseed);

    protected:
        CPUrvectArray d_phi, d_pi, d_phi_backup;
        Tfloat *zBuffer, *vecbuf;

        Tfloat *buffer_d, *tmp_d;

        fft_plan plan, kplan, ikplan;
        ffTcomplex *pik, *fft_result, *kfield, *vkfield;

        ffTfloat *pibuf;

    public:
        CPULattice(size_t size, ushort components);
        virtual ~CPULattice();

};

class CPU3DLattice : public CPULattice, virtual public iLattice3D {
    private:
        void doUpdateZMomentum(const bool phi);
        void doUpdateEMomentum();

        void stepPiRelax(const Tfloat dt);
        void stepPiDiff(const Tfloat dt);

        void addGradNoiseToPi();

        Tfloat doGetGradPhiSq() const;
        Tfloat doGetGradSqPhiSq() const { return 0.; };
        Tfloat doGetGradSqPhiQrt() const { return 0.; };

    public:
        CPU3DLattice(size_t lx, size_t ly, size_t lz, unsigned short components);

        CPU3DLattice(size_t l, ushort components) : CPU3DLattice(l, l, l, components) {}
        ~CPU3DLattice();
};

class CPU2DLattice : public iLattice2D, public CPULattice {
    private:
        void doUpdateZMomentum(const bool phi);
        void doUpdateEMomentum();

        void stepPiRelax(const Tfloat dt);
        void stepPiDiff(const Tfloat dt);

        Tfloat doGetGradPhiSq() const;
        Tfloat doGetGradSqPhiSq() const;
        Tfloat doGetGradSqPhiQrt() const;
        //Tfloat doGetSliceCorrelation(const size_t offset) const;
        Tfloat dogetinvlappisq() const;

        void addGradNoiseToPi();

    public:
        CPU2DLattice(size_t lx, size_t ly, unsigned short components);
        CPU2DLattice(size_t l, ushort components) : CPU2DLattice(l, l, components) {}
        ~CPU2DLattice();
};

#endif
