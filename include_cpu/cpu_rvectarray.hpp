#ifndef CPU_RVECTARRAY_HPP
#define CPU_RVECTARRAY_HPP

#include <rvectarray.h>
#include <stdexcept>

#include <sstream>

class CPUrvectArray : public rvectArray {
    private:
        Tfloat *buffer;

        Tfloat ompReduce(const Tfloat * const array, size_t size) const;

        void init();

        void doCopyFrom(const rvectArray &src);
        void doWriteFieldsToBuffer(Tfloat * const buffer) const;
        void doReadFieldsFromBuffer(const Tfloat * const buffer);

        Tfloat doSumComponents() const;
        Tfloat doSumJthComponent(const unsigned short j) const;
        Tfloat doSumSquares() const;
        Tfloat doSumQuartes() const;

        void doAddConst(const Tfloat a);
        void doSaxpy(const Tfloat a, const rvectArray &x);
        void doSetGaussianRandom(const Tfloat average, const Tfloat sigma, taus_state *rnd_state);
        void doRandomizeAndDamp(const Tfloat noiseAmp, const Tfloat damping, taus_state *rnd_state);

    public:
        CPUrvectArray(const size_t NumberOfElements, const unsigned short NumberOfComponents) 
            : rvectArray(NumberOfElements, NumberOfComponents) { init(); };
        CPUrvectArray(const rvectArray &src);
        CPUrvectArray(const CPUrvectArray &src);

        ~CPUrvectArray();

        void saxpy(const Tfloat a, const CPUrvectArray &x);
};

void divideIdx(const int i, const int imax, const size_t size, size_t &lowerIndex, size_t &diff);
#endif
