#include <global.h>
#include <lattice_index.h>

#include <tausworthe.h>
#include <lattice.h>


void stepMomentumZplane(Tfloat *Pi, Tfloat *Phi, 
        Tfloat dt, Tfloat mu0sq, Tfloat g0, Tfloat a, Tfloat J,
        const lattice3DIndex * const d_indexer, const int z);

void stepMomentumConsZplane(Tfloat *Pi, Tfloat *Phi, 
        Tfloat dt, Tfloat mu0sq, Tfloat g0, Tfloat a, Tfloat J,
        const lattice3DIndex * const d_indexer, const int z);

void stepMomentumYline(Tfloat *Pi, Tfloat *Phi, 
        Tfloat dt, Tfloat mu0sq, Tfloat g0, Tfloat a, Tfloat J,
        const lattice2DIndex * const d_indexer, const int y);

void stepMomentumConsYline(Tfloat *Pi, Tfloat *Phi, 
        Tfloat dt, Tfloat mu0sq, Tfloat g0, Tfloat a, Tfloat J,
        const lattice2DIndex * const d_indexer, const int y);

Tfloat squareGradientZplane(Tfloat *phi, const lattice3DIndex * const d_indexer, const int z);
Tfloat squareGradientYline(Tfloat *phi, const lattice2DIndex * const d_indexer, const int y);
Tfloat squareGradSqYline(Tfloat *phi, const lattice2DIndex * const d_indexer, const int y);
Tfloat squareGradQrtYline(Tfloat *phi, const lattice2DIndex * const d_indexer, const int y);

void addGradientZplane(Tfloat * const target, const Tfloat * const v, const lattice3DIndex * const d_indexer, const int z);
void addGradientYline(Tfloat * const target, const Tfloat * const v, const lattice2DIndex * const d_indexer, const int y);

Tfloat sliceCorrelationPlane(size_t offset, Tfloat *phi, const lattice3DIndex * const d_indexer, const int z);
Tfloat lineCorrelation(size_t offset, Tfloat *phi, const lattice2DIndex * const d_indexer, const int y);

void fillArrayWithGaussian(Tfloat * const target, const Tfloat mu, const Tfloat sigma, const size_t size, taus_state * const rnd_state);

void invKsqrLine(ffTcomplex * const field, bool abs, const lattice2DIndex * const d_indexer, const int y);
void invKsqrPlane(ffTcomplex * const field, bool abs, const lattice3DIndex * const d_indexer, const int z);
