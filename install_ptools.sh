#!/bin/bash

pyfiles=(txtbin.py evaluator.py pevaluator.py dyncombinator.py dynevaluator.py finitepevaluator.py emodevaluator.py py{,p,ep}direval.sh xivaluator.py)

BIN_DIR=~/bin/
PYTHON_RESOURCE_DIR=~/.python.d

for file in ${pyfiles[@]}; do
	install -Dm 755 tools_python/$file $BIN_DIR/$file
done

install -Dm 644 tools_python/utils.py $PYTHON_RESOURCE_DIR/utils.py

for file in tools_python/phi4/*.py; do 
	install -Dm 644 "$file" $PYTHON_RESOURCE_DIR/phi4/$(basename "$file")
done

install -Dm644 tools_python/utils.py $PYTHON_RESOURCE_DIR/utils.py

# shell utils
shfiles=(fitscript.sh wfit.sh)
for file in ${shfiles[@]}; do
	install -Dm 755 ./shell_tools/$file $BIN_DIR/$file
done
