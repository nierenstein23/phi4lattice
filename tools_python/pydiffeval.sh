#!/bin/bash
if [ -z ${TTIME+x} ]; then TTIME=0; fi
if [ -z ${NUM_THREADS+x} ]; then NUM_THREADS="4"; fi

if [ -z ${INFIX+x} ]; then INFIX="Conservative"; fi


# scan root dir for result directories
for dir in $@;
do
	# remove trailing slash and preceding path
	dir=${dir%/}
	name=${dir##*/}

	tmpdir=$(mktemp -d)
	trap "pkill -P $$; rm -r $tmpdir; exit -1" SIGINT

	pcount=0;
	for binfile in $dir/OrderParameter${INFIX}ID*;
	do
		OMP_NUM_THREADS=1 diffevaluator.py -T $TTIME -i $binfile -O $tmpdir &#--maxTime 32767 &
		if [ "$(( ++pcount ))" -ge "$NUM_THREADS" ]; then wait; pcount=0; fi
	done
	wait

	fname=${name}_diff.dat
	suffix=".npz"
	echo "combination to ${fname}, using suffix $suffix"
	diffcombinator.py -i $tmpdir -o ${fname} -s $suffix

	rm -r $tmpdir
done
