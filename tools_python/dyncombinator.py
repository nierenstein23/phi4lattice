#!/usr/bin/env python

import argparse
import glob
import numpy as np

parser = argparse.ArgumentParser()

# reproduce cmdline arguments from c++ version
parser.add_argument("-i", "--indir", help="read from this directory", default="tmp")
parser.add_argument("-s", "--suffix", help="use this suffix (include dot!)", default=".npz")
parser.add_argument("-o", "--OutputFile", default="combined_output.dat", help="Give output file")
parser.add_argument("-n", "--nBins", type=int, help="max number of bins, DEPRECATED", default=0)
parser.add_argument("-t", "--nTbin", type=int, help="time binning, DEPRECATED", default=1)
parser.add_argument("-d", "--debugplot", help="create plot for debugging purposes", action="store_true")
parser.add_argument("-v", "--verbosity", help="verbosity level", action="store_true")
parser.add_argument("-a", "--absolute", help="output absolute", action="store_true")

args = parser.parse_args()

npz_filenames = glob.glob("{}/*{}".format(args.indir, args.suffix))
npz_files = []

for fname in npz_filenames:
    npz_files.append(np.load(fname))

# datafile format: timestep, specfunc_fft, specfunc
timestep = npz_files[0]['arr_0']

sf_data = []
sf_fft_data = []
for nfile in npz_files:
    sf_data.append(nfile['arr_2'])
    sf_fft_data.append(nfile['arr_1'])

mean_specfunc = sum(sf_data)/len(sf_data)
mean_specfunc_fft = sum(sf_fft_data)/len(sf_fft_data)

var_specfunc = sum([np.abs(specfunc - mean_specfunc)**2 for specfunc in sf_data])/(len(sf_data)*(len(sf_data)-1))
var_specfunc_fft = sum([np.abs(specfunc - mean_specfunc_fft)**2 for specfunc in sf_fft_data])/(len(sf_data)*(len(sf_data)-1))


# ============= output to file  ============#

iInt = len(mean_specfunc)
t = [timestep*i for i in range(0, iInt,1)]
w = np.fft.rfftfreq(2*len(mean_specfunc_fft)-1)/timestep
with open(args.OutputFile, 'w') as of:
    print('# combination of {} files'.format(len(npz_files)), file=of)

    if args.absolute:
        for i in range(len(w)):
            print(w[i], np.abs(mean_specfunc_fft)[i]*timestep, var_specfunc_fft[i]*timestep, file=of)

    else:
        for i in range(len(w)):
            print(w[i], -mean_specfunc_fft.imag[i]*timestep, var_specfunc_fft[i]*timestep, file=of)

    print("\n\n", file=of)

    for i in range(len(t)):
        print(t[i], mean_specfunc[i], var_specfunc[i], file=of)


# ============= plot for debugging ============#
if not args.debugplot: exit()

import matplotlib.pyplot as plt

plt.title('debugplot')

#plt.subplot(211)
tmax = 100

plt.subplot(211)
plt.plot(t[0:tmax], mean_specfunc[:tmax], label='spectral function')
plt.plot(t[:5], t[:5], label='linear function')
plt.legend()

plt.subplot(212)
plt.plot(w, mean_specfunc_fft.imag, label='imag of spectral function')
plt.plot(w, mean_specfunc_fft.real, label='real of spectral function')
plt.legend()

plt.show()
