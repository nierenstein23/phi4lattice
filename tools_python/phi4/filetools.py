#!/usr/bin/env python

from os.path import basename, dirname
import re
import numpy
from functools import reduce

filetypes = ("Configuration", "Energy", "FiniteP", "Info", "Moments", "OrderParameter")

def listFiles(opfile):
    """List files belonging to this OrderParameter file.
    Provides a convenient way to access all relevant files via their prefix. 

    input:
        OrderParameter file

    output:
        dictionary of files belonging to the same simulation sequence as the input file
    """

    bname = basename(opfile)

    IDfix = bname[bname.find("ID"):]
    ID = re.sub("[^0-9]", "", IDfix)

    prefix = bname[:bname.find("ID")]
    mode = prefix.replace("OrderParameter",'')
    
    filelist = dict()
    for filetype in filetypes:
        if (filetype == "Info"):
            infix = ""
            suffix = ".txt"
        elif (filetype == "Configuration"):
            infix = ""
            suffix = ""
        else:
            infix = mode
            suffix = ""
            
        filelist[filetype] = "{}/{}".format(dirname(opfile), filetype+infix+"ID"+ID+suffix)

    return filelist


def extractInfo(info_file):
    """Extract info from Info*txt file.

    input: 
        Info*txt file

    output:
        dictionary containing relevant data from Info*txt file
    """

    with open(info_file) as f:
        info_dict = dict(numpy.loadtxt(f, dtype={'names': ('specifier', 'value'), 'formats': ('object', 'object')}, delimiter="=", comments='%'))
    
        # strip leading number signs
        for key in list(info_dict.keys()):
            info_dict[key[1:] if key.startswith('#') else key] = info_dict.pop(key)

    return info_dict

def getVolumeFromInfo(info_dict):
    """Calculate the volume from a dictionary containing info variables"""

    volume = 1

    key_pattern = re.compile('N[xyz]')
    for key in [n_key for n_key in info_dict.keys() if key_pattern.match(n_key)]:
        volume *= int(info_dict[key])

    return volume

def vol(line, d=2):
    return (line[0])**d

def chi(line):
    return vol(line)*(line[6] - line[10]**2)/line[1]

def chi_err(line):
    return vol(line)*np.sqrt((line[7] + 4*line[10]**2*line[11]))/line[1]
