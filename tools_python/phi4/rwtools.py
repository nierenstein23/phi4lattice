#!/usr/bin/env python

import numpy

def getWeights(energy, order_parameter_J, beta, beta_prime, deltaJ):
    weights = [numpy.exp(-volume*((beta_prime - beta)*x[0] + beta*deltaJ*x[1])) for x in zip(energy, order_parameter_J)]
    return weights/sum(weights)*len(weights)
