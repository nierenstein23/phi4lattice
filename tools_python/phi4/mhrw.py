#!/usr/bin/env python

from statistics import mean
import numpy

class energyFile:
    """
    Class definition to provide a type to collect in a dictionary.
    Could in theory be replaced by a simple 'pass'.
    """
    def __init__(self, ID, T, energies):
        self.ID = ID
        self.T = T
        self.energies = energies

class MultiHistogram:
    MaxIterations   = 64
    MinElements     = 512
    Divisor         = 4
    targetDeviation = 10e-12

    def __init__(self, energyFileList):
        # fill dict and length vector
        self.eFileList = energyFileList
        self.lengths = [len(efile.energies) for efile in energyFileList if isinstance(efile, energyFile)]

        # calculate the mean in two steps; otherwise, precision is nearly guaranteed to run out
        Emean = mean([mean(efile.energies) for efile in energyFileList])

    def reweigh(self):

        NumberOfDataFiles = len(self.eFileList)
        NumberOfVariables = NumberOfDataFiles
        NumberOfEquations = NumberOfDataFiles+1

        if NumberOfDataFiles == 1:
            self.weights = [1.0]

        self.weights = [1/NumberOfDataFiles for i in NumberOfDataFiles]
        fWeights = [-numpy.log(NumberOfDataFiles) for i in NumberOfDataFiles]

       DataIncrement = max(2**(numpy.floor(numpy.log2(min(self.lengths)/self.MinElements))), 1)
       OldDeviation = GlobalDeviation = 10*targetDeviation
       justDecreased = True
       counter = 0

       while (GlobalDeviation > self.targetDeviation and counter < self.MaxIterations) or DataIncrement > 1:
           counter += 1

           # TODO: decrease DataIncrement

           function, Jacobian = __updateDeviation(DataIncrement)
           __enforceNorm(function, Jacobian)

           OldDeviation = GlobalDeviation
           GlobalDeviation = max(np.abs(function))
           
           Coeffs = numpy.zeros(NumberOfEquations, NumberOfEquations)



    def __updateDeviation(self, DataIncrement):
        """
        Update the deviation from the "true" weights.
        Only use every DataIncrement-th data point.
        Returns the deviation function and its Jacobian.
        """
        function = [-1*w for w in self.weights]
        function.append(0)

        NoVar = len(self.weights)
        Jacobian = numpy.zeros((NoVar+1),NoVar)
        Jacobian[:NoVar,:NoVar] = -numpy.identity(NoVar)

        for eFile in self.eFileList:
            for energy in eFile.energies[::DataIncrement]:
                dE = energy - Emean

                for k in range(len(self.eFileList)):
                    eFilek = self.eFileList[k]:
                    Denominator = .0

                    for j in range(len(self.eFileList)):
                        eFilej = self.eFileList[j]
                        Denominator += self.lengths[j]/(DataIncrement*self.weights[j]) \
                                        * numpy.exp(-(1/eFilej.T - 1/eFilek.T)*dE)

                    function[k] += 1.0/Denominator

                    for l in range(len(self.eFileList)):
                        eFilel = self.eFileList[l]
                        Jacobian += self.lengths[l]/(DataIncrement*(Denominator*self.weights[l](**2) \
                                    * numpy.exp(-(1/eFilel.T - 1/eFilek.T)*dE)

        return function, Jacobian

    def __enforceNorm(self, function, Jacobian):
        Norm = sum(self.weights)
        function[-1] = numpy.log(Norm)

        for l in range(len(self.weights)):
            Jacobian[-1,l] = 1./Norm



    def __inverseWeighting(self, function, Jacobian):
    # TODO: continue
