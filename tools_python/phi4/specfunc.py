#!/usr/bin/env python

import numpy as np
from numpy.fft import rfft,irfft,rfftfreq,ifftshift

def specfunc(pi, phi, temperature, cutIdx):
    if (len(pi)!=len(phi)): raise ValueError("lengths of pi and phi have to match!") 
    N=len(pi)
    Ncut=N-cutIdx
    Np=N-Ncut # = cutIdx

    # shift
    phi = phi - np.mean(phi)
    pi = pi - np.mean(pi)

    # unnormalized, full correlation (!= <x(t)x(0)>
    piphicorr = (np.correlate(pi,phi,mode='full')).real
    corr = (piphicorr - piphicorr[::-1])/2
    
    # normalize to get <x(t)x(0)>
    norm_half = [-temperature*abs(N-x) for x in range(Np)]
    norm = np.concatenate((norm_half[::-1], norm_half[1:]))

    ncorr = np.divide(corr[Ncut:-Ncut], norm).real
    specfunc_half = ncorr[Np-1:]
    specfunc_fft = rfft(ifftshift(ncorr),norm=None).imag*(0.5j) # factor 1/2 since using both forward and backward part

    return specfunc_fft, specfunc_half


def auto_cfunc(x, norm, cutIdx, symm=1):
    N=len(x)
    Ncut=N-cutIdx
    Np=N-Ncut # = cutIdx


    xcorr = np.correlate(x,x,mode='full')
    corr = (xcorr + xcorr[::-1])/2

    norm_half = [abs(N-x) for x in range(Np)]
    norm = np.concatenate((norm_half[::-1], norm_half[1:]))

    ncorr = np.divide(corr[Ncut:-Ncut], norm).real

    fftc = rfft(ifftshift(ncorr),norm='ortho')

    return fftc.real*(-1j), ncorr[Np-1:]
