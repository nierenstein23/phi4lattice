#!/usr/bin/env python

import argparse
import numpy as np
from phi4.filetools import *
from utils import *
from os.path import basename, dirname
import statistics

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--infile", help="input file to use")
parser.add_argument("-T", "--thermTime", help="thermalization cutoff", type=int)
parser.add_argument("-t", "--deltaTs", help="temperature shifts for reweighting", type=float, nargs='*', default=[.0])
parser.add_argument("-j", "--deltaJs", help="field shifts for reweighting", type=float, nargs='*', default=[.0])
parser.add_argument("-b", "--nbins", help="number of bins", type=int)
parser.add_argument("-s", "--sparsefactor", help="sparsification", type=int, default=10)
parser.add_argument("-d", "--debugplot", help="create plot for debugging purposes", action="store_true")

args = parser.parse_args()
files = listFiles(args.infile)

info_dict = extractInfo(files["Info"])

volume = getVolumeFromInfo(info_dict)
Nc = int(info_dict['Nc'])

indexArray = [1 + 2*i for i in range(Nc)]

op_raw = np.loadtxt(files["OrderParameter"])[:,indexArray]
f1_raw = np.loadtxt(files["FiniteP"])[:,[1,2]]

# thermalization cutoff:
# if thermalization time is specified, cut it off
# else if number of bins is specified, cut first bin
# else calculate number of bins via autocorrelation time and cut first bin

if args.thermTime:  # override anything else
    cutoff = args.thermTime
else:
    cutoff = 0

if args.nbins:
    nbins = args.nbins
    if not args.thermTime:
        cutoff = args.nbins
else:
    act = (autocorrelationTime(op_raw[cutoff:,0], sparsefactor=args.sparsefactor)+1)//1
    nbins = int(len(op_raw)//act)
    if not args.thermTime:
        cutoff = int(act)

op_raw = op_raw[cutoff:,:] 
f1_raw = f1_raw[cutoff:,:]


order_parameter = numpy.apply_along_axis(lambda x: np.sqrt(sum(np.square(x))), 1, op_raw)
f1 = numpy.apply_along_axis(lambda x: sum(np.square(x)), 1, f1_raw)
energy = np.loadtxt(files["Energy"])[cutoff:,1]

# reweighting loop
for deltaT in args.deltaTs:
    for deltaJ in args.deltaJs:
        T = float(info_dict["T"]) 
        T_prime = float(info_dict["T"]) + deltaT
        # shift beta
        beta = 1./T
        beta_prime = 1./T_prime

        weights = [np.exp(-volume*((beta_prime - beta)*x[0] + beta*deltaJ*x[1])) for x in zip(energy, op_raw[:,0])]
        weights = weights/sum(weights)*len(weights)

        rwop = np.multiply(order_parameter, weights)
        rwop2 = np.multiply(np.square(order_parameter), weights)
        rwop4 = np.multiply(np.square(np.square(order_parameter)), weights)

        rwopm, rwopv = stat(binning(rwop, numberOfBins=nbins))
        rwphi2m, rwphi2v = stat(binning(rwop2, numberOfBins=nbins))
        rwphi4m, rwphi4v = stat(binning(rwop4, numberOfBins=nbins))

        rwopr = np.multiply(op_raw[:,0], weights)
        rwoprm, rwoprv = stat(binning(rwopr, numberOfBins=nbins))

        rwopr2 = np.multiply([(O-rwoprm)**2 for O in op_raw[:,0]], weights)
        #rwbphi2, rwbphi2v = stat(binned_function(rwopr, statistics.pvariance, numberOfBins=nbins))
        rwbphi2, rwbphi2v = stat(binned_function(rwopr2, numberOfBins=nbins))

        def binder_func(x): 
            return 1 - statistics.mean(np.square(np.square(x)))/(3*statistics.mean(np.square(x))**2)

        rwbinder, rwbinderv = stat(binned_function(rwopr, binder_func, numberOfBins=nbins))

        rwf1m, rwf1v = stat(binned_function(np.multiply(f1, weights), numberOfBins=nbins))

        print(f"{info_dict['Nx']} {T_prime:.6e} {float(info_dict['J']) + deltaJ:.6e} {rwopm:.6e} {rwopv/nbins:.6e} {len(op_raw)//nbins:.6e} {rwphi2m:.6e} {rwphi2v/nbins:.6e} {rwphi4m:.6e} {rwphi4v/nbins:.6e} {rwoprm:.6e} {rwoprv/nbins:.6e} {rwbphi2:.6e} {rwbphi2v/nbins:.6e} {rwbinder:.6e} {rwbinderv/nbins:.6e} {rwf1m:.6e} {rwf1v/nbins:.6e}")

if not args.debugplot: exit()
import matplotlib.pyplot as plt
