#!/usr/bin/env python

import argparse
import numpy as np

from utils import *

parser = argparse.ArgumentParser()
parser.add_argument("-n", "--nBins", help="number of bins", type=int)
parser.add_argument("files", help="files to bin", nargs='+')
parser.add_argument("-d", "--debugplot", help="create plot for debugging purposes", action="store_true")
parser.add_argument("-a", "--abs", help="take absolute before binning", action="store_true")
parser.add_argument("-x", "--xcolumn", help="column containing abscissa", type=int, default=0)
parser.add_argument("-y", "--ycolumn", help="column containing observable", type=int, default=1)
sqmode = parser.add_mutually_exclusive_group()
sqmode.add_argument("--square", help="analyse x:(($y)^2 + $(y+1)^2)", action="store_true")
sqmode.add_argument("--sqrt", help="analyse x:sqrt(($y)^2 + $(y+1)^2)", action="store_true")

args = parser.parse_args()

nBins = args.nBins

t_column = args.xcolumn
data_column = args.ycolumn

data = []
for fname in args.files:
    if args.square:
        raw_t = np.loadtxt(fname)[:,t_column]
        raw_d = [sum(np.square(x)) for x in np.loadtxt(fname)[:,[data_column, data_column+1]]]
        raw_data = np.array([[raw_t[i], raw_d[i]] for i in range(len(raw_t))])

    elif args.sqrt:
        raw_t = np.loadtxt(fname)[:,t_column]
        raw_d = [(sum(np.square(x)))**.5 for x in np.loadtxt(fname)[:,[data_column, data_column+1]]]
        raw_data = np.array([[raw_t[i], raw_d[i]] for i in range(len(raw_t))])

    else:
        raw_data = np.loadtxt(fname)[:,[t_column,data_column]]

    t_data = binning(raw_data[:,0], numberOfBins=nBins) if args.nBins else raw_data[:,0]
    o_data = binning(raw_data[:,1], numberOfBins=nBins) if args.nBins else raw_data[:,1]

    data.append(abs(o_data) if args.abs else o_data)

nfiles = len(data)

bin_data = []
bin_error = []

for i in range(len(t_data)):
    m, v = stat([d[i] for d in data])
    err = np.sqrt(v/nfiles)

    print(t_data[i], m, err)

    if args.debugplot:
        bin_data.append(m)
        bin_error.append(err)


if not args.debugplot: exit()
import matplotlib.pyplot as plt

plt.subplot(211)
for n in range(4):
    plt.plot(t_data, data[n], label=f'data[{n}]')
plt.legend()

plt.subplot(212)
plt.errorbar(t_data, bin_data, yerr=bin_error)
plt.show()

