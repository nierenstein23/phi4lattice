#!/usr/bin/env python

import numpy
import statistics
from scipy.stats import binned_statistic
from itertools import accumulate
import sys

def smooth(x,window_len=11,window='hanning'):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.

    SOURCE: https://scipy-cookbook.readthedocs.io/items/SignalSmooth.html
    """

    if x.ndim != 1:
        raise ValueError("smooth only accepts 1 dimension arrays.")

    if x.size < window_len:
        raise ValueError("Input vector needs to be bigger than window size.")


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError("Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")


    s=numpy.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=numpy.ones(window_len,'d')
    else:
        w=eval('numpy.'+window+'(window_len)')

    y=numpy.convolve(w/w.sum(),s,mode='valid')
    return y


def stat(x):
    """do some basic statistics on x.

    input:
        x: array-like sequence of data

    output:
        (mean, variance) in a list"""

    m = statistics.mean(x)
    return m, statistics.variance(x, m)


def binning(x, numberOfBins=10):
    """Resum the data into bins.

    input:
        x: array-like sequence of data
        numberOfBins: length of the resulting array. Defaults to 10.

    output:
        an array containing the average of the data in the bins."""

    if (numberOfBins >= len(x)):
        return x
    else:
        array, edges, numbers = binned_statistic(range(len(x)), x, bins=numberOfBins)
        return array

def autocorrelationTime(x, fast=False, sparsefactor=1):
    x_sparse = x[::sparsefactor]
    z_sparse = x_sparse - statistics.mean(x_sparse)

    ac_raw = numpy.correlate(z_sparse, z_sparse, mode='full')[len(z_sparse)-1:]
    autocorrelation = ac_raw/ac_raw[0]

    if not fast:    # integrate
        act = 2*sparsefactor*max(list(accumulate(autocorrelation)))
    else:           # triangulate
        act = sparsefactor*next(x[0] for x in enumerate(autocorrelation) if x[1] < 0)

    return act

def binned_pvariance(x, numberOfBins=10):
    if (numberOfBins >= len(x)):
        return statistics.pvariance(x)
    else:
        array, edges, numbers = binned_statistic(range(len(x)), x, statistic=statistics.pvariance, bins=numberOfBins)
        return array

def binned_function(x, function=statistics.mean, numberOfBins=10):
    if (numberOfBins >= len(x)):
        return function(x)
    else:
        array, edges, numbers = binned_statistic(range(len(x)), x, statistic=function, bins=numberOfBins)
        return array

def jknf(x):
    N = len(x)
    jknf_x = []

    for i in range(N):
        jknf_x.append(statistics.mean([y[1] for y in enumerate(x) if y[0] != i]))

    return stat(jknf_x)
    

def jackknife(x, numberOfBins=10):
    M = numberOfBins
    N = len(x)
    b = N//M

    knifed_x = []
    knifed_x.append(sum(x[b:M*b])/((M-1)*b))
    for m in range(1,M-1):
        xtmp = numpy.concatenate((x[:m*b],x[(m+1)*b:]))
        knifed_x.append(sum(xtmp)/((M-1)*b))

    knifed_x.append(sum(x[0:(M-1)*b])/((M-1)*b))

    xm = sum(knifed_x)/M
    xv = (M-1)/M * sum([(x - xm)**2 for x in knifed_x])

    return xm, xv

def weighted_avg(samples_w_errors):
    """calculate the weighted average of pairs (x,x_err) in samples_w_errors"""

    sum_x = sum([x/x_err for x,x_err in samples_w_errors])
    ierr_x = sum([1/x_err for x,x_err in samples_w_errors])


    return sum_x/ierr_x, len(samples_w_errors)**.5/ierr_x
