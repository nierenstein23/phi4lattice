#!/usr/bin/env python

import argparse
from os.path import basename,dirname
import numpy as np
import time
import phi4.specfunc as phi4sf
import phi4.filetools as phi4ft


parser = argparse.ArgumentParser()

# reproduce cmdline arguments from c++ version
parser.add_argument("file", help="read from this finite-p file")
parser.add_argument("-i", "--infile", help="compatibility option", action="store_true")
parser.add_argument("-m", "--maxTime", type=float, help="maximum time")
parser.add_argument("-T", "--thermTime", type=float, help="thermalization time cutoff", default=0)
parser.add_argument("-o", "--OutputFile", help="Give output file")
parser.add_argument("-O", "--OutputDir", help="Output directory; location where to save the FT", default="tmp")
parser.add_argument("-p", "--poffset", type=int, default=[0], help="momentum offset", nargs='+')
parser.add_argument("-d", "--debugplot", help="create plot for debugging purposes", action="store_true")
parser.add_argument("-v", "--verbosity", help="verbosity level", action="store_true")
parser.add_argument("-t", "--timers", help="time stuff", action="store_true")

args = parser.parse_args()

# construct file names

finitep_file = args.file
bname = basename(finitep_file)

ID = bname[bname.find("ID"):]
info_file = "{}/{}".format(dirname(finitep_file), "Info"+ID+".txt")

# load info array
with open(info_file) as f:
    info_dict = dict(np.loadtxt(f, dtype={'names': ('specifier', 'value'), 'formats': ('object', 'object')}, delimiter="=", comments='#N1='))

    # strip leading number signs
    for key in list(info_dict.keys()):
        info_dict[key[1:] if key.startswith('#') else key] = info_dict.pop(key)


starttime = time.time()

temperature = float(info_dict['T'])
vol = phi4ft.getVolumeFromInfo(info_dict)

with open(finitep_file) as pf:
    p_data = np.loadtxt(pf)
    t = p_data[:,0]

    cutoff = max(args.thermTime, 0.0)
    cutIdx = next(x[0] for x in enumerate(t) if x[1] > cutoff)
    
    p_data = p_data[cutIdx:]

    endtime = time.time()
    if args.timers: print("Reading files from disk took {} seconds".format(endtime-starttime))
    starttime = time.time()

    for poffset in args.poffset:
        # find out which p-modes were stored
        pmax = len(p_data[1,:])//4
        if (pmax <= poffset): 
            print("pmax < poffset, stopping.")
            break;
    
        t = p_data[:,0]-1
        phi_p = p_data[:,1 + 2*poffset] + 1j*p_data[:,2 + 2*poffset]
        pi_p = p_data[:,2*pmax + 1 + 2*poffset] + 1j*p_data[:,2*pmax + 2 + 2*poffset]
    
    
        iInt = len(t)
        timestep = t[1]-t[0]

        if (args.maxTime): 
            maxTime = min(args.maxTime, t[-1])
            maxIdx = next(x[0] for x in enumerate(t) if x[1] > maxTime)
        else:
            maxTime = t[iInt//2]
            maxIdx = iInt//2


        specfunc_fft, specfunc = phi4sf.specfunc(pi_p, phi_p, temperature/(vol), cutIdx=maxIdx)

        endtime = time.time()
        if args.timers: print("Correlating stuff took {} seconds".format(endtime-starttime))
        starttime = time.time()
        
        sf_archive = "{}/{}_P{}.{}".format(args.OutputDir,ID,poffset+1,"npz")
        np.savez(sf_archive, timestep, specfunc_fft, specfunc)
        
        endtime = time.time()
        if args.timers: print("Writing archive took {} seconds".format(endtime-starttime))

if not args.debugplot: exit()
import matplotlib.pyplot as plt


plt.title('debug plot')
plt.subplot(311)
plt.plot(np.real(phi_p)[:200], label='phi')
plt.plot(np.real(np.cumsum(pi_p))[:200], label='Integral over pi')

plt.xlabel('t')
plt.ylabel('phi')
plt.legend()

print (specfunc[0], t[0])
plt.subplot(312)
tmax = 25
#plt.plot(-1*specfunc, label='real part of spectral function')
plt.plot(t[:tmax], specfunc[:tmax], label='spectral function')
plt.plot([x for x in range(5)], label='linear function')

plt.xlabel('t')
plt.ylabel('corr')
plt.legend()

w = np.fft.rfftfreq(2*len(specfunc_fft)-1)
plt.subplot(313)
plt.plot(w, np.real(specfunc_fft), label='real part of spectral function')
plt.plot(w, np.imag(specfunc_fft), label='imag part of spectral function')

plt.xlabel('w')
plt.ylabel('corr')

plt.legend()
plt.show()
