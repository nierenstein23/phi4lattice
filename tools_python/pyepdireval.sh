#!/bin/bash
if [ -z ${TTIME+x} ]; then TTIME=0; fi
if [ -z ${POFFSET+x} ]; then POFFSET="0"; fi
if [ -z ${NUM_THREADS+x} ]; then NUM_THREADS="4"; fi

if [ -z ${INFIX+x} ]; then INFIX="Conservative"; fi


# scan root dir for result directories
for dir in $@;
do
	# remove trailing slash and preceding path
	dir=${dir%/}
	name=${dir##*/}

	tmpdir=$(mktemp -d)
	trap "pkill -P $$; rm -r $tmpdir; exit -1" SIGINT

	pcount=0;
	for binfile in $dir/EModesP${INFIX}ID*;
	do
		OMP_NUM_THREADS=1 emodevaluator.py -T $TTIME -i $binfile -O $tmpdir -p $POFFSET &#--maxTime 32767 &
		if [ "$(( ++pcount ))" -ge "$NUM_THREADS" ]; then wait; pcount=0; fi
	done
	wait

	for P in $POFFSET; do
		P=$(( P+1 ))
		fname=${name}_EP$P.dat
		suffix="_P${P}.npz"
		echo "combination to ${fname}, using suffix $suffix"
		dyncombinator.py -i $tmpdir -o ${fname} -s $suffix
	done

	rm -r $tmpdir
done
