#!/usr/bin/env python

import sys
import argparse
from os.path import basename, dirname
import numpy as np
from utils import binning,stat

parser = argparse.ArgumentParser()

# reproduce cmdline arguments from c++ version
#parser.add_argument("-p", "--finite-p-infile", help="compatibility option", action="store_true")
parser.add_argument("file", help="read from this (order parameter) file")
parser.add_argument("-o", "--outdir", help="output directory")
parser.add_argument("-T", "--thermTime", type=float, help="thermalization time", default=0.0)
parser.add_argument("-b", "--bins", help="data points per bin", default=100, type=int)
parser.add_argument("-m", "--maxp", help="maximum p", type=int)
parser.add_argument("-v", "--verbosity", help="verbosity level", action="store_true")
parser.add_argument("-d", "--debugplot", help="create plot for debugging purposes", action="store_true")

args = parser.parse_args()

DEBUG = args.debugplot
def dprint(*args):
    if DEBUG:
        print('DEBUG:\t', *args, file=sys.stderr)

fp_file = args.file
bname = basename(fp_file)

ID = bname[bname.find("ID"):]
info_file = "{}/{}".format(dirname(fp_file), "Info"+ID+".txt")

dprint(f'Accessing {fp_file} and {info_file}')

# load info array
with open(info_file) as f:
    info_dict = dict(np.loadtxt(f, dtype={'names': ('specifier', 'value'), 'formats': ('object', 'object')}, delimiter="=", comments='%'))

    # strip leading number signs
    for key in list(info_dict.keys()):
        info_dict[key[1:] if key.startswith('#') else key] = info_dict.pop(key)


# load data
with open(fp_file) as ff:
    fp_data = np.loadtxt(ff)
    t = fp_data[:,0]


N = int(info_dict['Nx'])
if args.maxp: 
    maxp = args.maxp
else:
    maxp = N//2

dprint(f'set maxp = {maxp}')

cutoff = max(args.thermTime, 0)
cutIdx = next(x[0] for x in enumerate(t) if x[1] > cutoff)

dprint(f'set cutoff = {cutoff} ==> cutIdx = {cutIdx}')

fp_data = fp_data[cutIdx:]
t = fp_data[:,0]

n = len(fp_data)
bins = args.bins

pcorrs = []
for p in range(1, maxp):
    phip2 = np.square(fp_data[:,1+2*(p-1)]) + np.square(fp_data[:,2+2*(p-1)])

    dprint(f'max(phip2) = {max(phip2)}')
    dprint(f'min(phip2) = {min(phip2)}')
    dprint(f'np.mean(phip2) = {np.mean(phip2)}')
    dprint(f'np.var(phip2) = {np.var(phip2)}')

    mean, var = stat(binning(phip2, args.bins))
    pcorrs.append((2*np.sin(np.pi*(p)/N), mean, var/bins))

for p in range(len(pcorrs)):
    print(N, float(info_dict['T']), float(info_dict['J']) , bins, 0, pcorrs[p][0], pcorrs[p][1], pcorrs[p][2]) 


if not args.debugplot: exit()

import matplotlib.pyplot as plt

#plt.subplot(211)
p=1
plt.plot(t, np.square(fp_data[:,1+2*(p-1)]) + np.square(fp_data[:,2+2*(p-1)]), label=f'data@p={p}')
plt.plot(t, [pcorrs[p][1] for x in t], label=f'phi^2(p={p})')
plt.legend()

#plo = plt.subplot(212)
#plo.set_xscale('log')
#plo.set_yscale('log')
#
#plt.errorbar([x[0] for x in pcorrs], [x[1] for x in pcorrs], yerr=[2*x[2]**.5 for x in pcorrs])
plt.show()
