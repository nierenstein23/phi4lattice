#!/usr/bin/env python

import argparse
from os.path import basename,dirname
import numpy as np
from phi4 import specfunc, filetools as ft

parser = argparse.ArgumentParser()

# reproduce cmdline arguments from c++ version
parser.add_argument("-i", "--infile", help="compatibility option", action="store_true")
parser.add_argument("file", help="read from this (order parameter) file")
parser.add_argument("-o", "--OutputFile", help="Give output file")
parser.add_argument("-O", "--OutputDir", help="Output directory; location where to save the FT", default="tmp")
parser.add_argument("-m", "--maxTime", type=float, help="maximum time")
parser.add_argument("-T", "--thermTime", type=float, help="thermalization time cutoff", default=0)
parser.add_argument("-v", "--verbosity", help="verbosity level", action="store_true")
parser.add_argument("-d", "--debugplot", help="create plot for debugging purposes", action="store_true")

args = parser.parse_args()

# construct file names

op_file = args.file
bname = basename(op_file)

ID = bname[bname.find("ID"):]
info_file = "{}/{}".format(dirname(op_file), "Info"+ID+".txt")
# for later
#finitep_file = "{}/{}".format(dirname(op_file), "FiniteP"+bname[len("OrderParameter"):])

# load info array
info_dict = ft.extractInfo(info_file)

with open(op_file) as of:
    op_data = np.loadtxt(of)
    t = op_data[:,0]

    cutoff = max(args.thermTime, 0.0)
    cutIdx = next(x[0] for x in enumerate(t) if x[1] > cutoff)

    op_data = op_data[cutIdx:]
    t = op_data[:,0]
    phi = op_data[:,1]

timestep = t[1]-t[0]

if (args.maxTime): 
    maxTime = min(args.maxTime, t[-1])
else:
    maxTime = t[len(t)//2]

maxIdx = next(x[0] for x in enumerate(t) if x[1] > maxTime)

temp = float(info_dict['T'])
N = int(info_dict['Nx'])
vol = ft.getVolumeFromInfo(info_dict)
specfunc_fft, specfunc = specfunc.specfunc(phi, phi, temp/vol, cutIdx=maxIdx, symm=1)

sf_archive = "{}/{}".format(args.OutputDir,ID+".npz")
np.savez(sf_archive, timestep, specfunc_fft, specfunc)

if not args.debugplot: exit()
import matplotlib.pyplot as plt

plt.title('debug plot')
plt.subplot(211)
plt.xlabel('t')
plt.ylabel('corr')
plt.semilogx(t[:len(specfunc)], specfunc, label='spectral function')
plt.legend()

plt.subplot(212)
w = np.fft.rfftfreq(2*len(specfunc_fft)-1)/timestep
plt.semilogx(w, specfunc_fft.real, label='real')
plt.semilogx(w, specfunc_fft.imag, label='imag')
plt.legend()


plt.show()
