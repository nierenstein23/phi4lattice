#ifndef _TAUSWORTHE_H_
#define _TAUSWORTHE_H_

#include <cstdlib>
#include <iostream>
#include <cmath>

#include <global.h>

struct myuint4 {
	unsigned int x;
	unsigned int y;
	unsigned int z;
	unsigned int w;
};

typedef myuint4 taus_state;

CHOST inline std::ostream& operator<<( std::ostream& s, taus_state z );

/* internal function */
inline C_HD  unsigned taus_step( unsigned &z, int S1, int S2, int S3, unsigned M)
{
	unsigned b=(((z<<S1)^z)>>S2);
	return z=(((z &M)<<S3)^b);
}

/* internal function */

inline C_HD  unsigned LCG_step( unsigned &z, unsigned A, unsigned C)
{
	return z=(A*z+C);
}

/**
 * Generates the next random number using a hybrid Tausworthe PRNG.
 *
 * This is based on an article in GPU Gems 3, see
 * http://http.developer.nvidia.com/GPUGems3/gpugems3_ch37.html
 * for details.
 *
 * The generated random numbers are evenly distributed. The state is modified.
 * Before the first call the state should be initializes using some random RNG.
 * The initial values for the first the x, y and z components of the state should
 * be > 128.
 */

inline C_HD double taus_rnd( taus_state* state )
{
    double tmp = 2.328306436538696e-10f*( taus_step( state->x, 13, 19, 12, 4294967294ul)^
	                            taus_step( state->y, 2, 25, 4,   4294967288ul)^
	                            taus_step( state->z, 3, 11, 17,  4294967280ul)^
	                            LCG_step(  state->w, 1664525,    1013904223ul) );
    // do not return 0.0
    if (tmp == 0.0) tmp = 1.0;
    return tmp;
}

/* A utility function to seed the taus state using the standard rand48()-Function. */

inline taus_state taus_seed( )
{
	taus_state state;
	
	while( ( state.x = lrand48() ) <= 128 ) {};
	while( ( state.y = lrand48() ) <= 128 ) {};
	while( ( state.z = lrand48() ) <= 128 ) {};
	state.w = lrand48();

	return state;
}

CHOST inline std::ostream& operator<<( std::ostream& s, taus_state z )
{
	return s << '(' << z.x << ',' << z.y << ',' << z.z << ',' << z.w << ')';
}

C_HD inline float gaussianfloat( taus_state * const state )
{
    double radius, rnd;
    const double phi = 2.0 * M_PI * taus_rnd( state );

    rnd = taus_rnd( state );
    radius = sqrt( -2.0 * log(  rnd ) );

    return (radius * cos(phi));
}

C_HD inline double gaussiandouble( taus_state * const state )
{
    double radius, rnd;
    const double phi = 2.0 * M_PI * taus_rnd( state );

    rnd = taus_rnd( state );
    radius = sqrt( -2.0 * log(  rnd ) );

    return (radius * cos(phi));
}


C_HD inline Tfloat generateGaussian(Tfloat mean, Tfloat sigma, taus_state *state) {
    Tfloat rnd, rad, phi;

    // get a random number that is NOT exactly 0
    rnd = taus_rnd( state );
    // get a random number
    rad = sigma * SQRT( -2.0 * LOG(rnd));
    phi = 2.0 * M_PI * (Tfloat)taus_rnd(state); 

    return mean + rad * COS(phi);
}


CHOST inline bool do_metropolis_check( const double deltaS, float *accept, float *count, taus_state * const state )
{
    bool is_accepted;
    if ( deltaS < 0 ) { 
        *accept+=1.0; 
        *count+=1.0;
        is_accepted = true;
    } else {
        const double rnd = taus_rnd( state );
        
        if (exp(-1.0*deltaS ) > rnd) {
            *accept+=1.0;
            *count+=1.0;
            is_accepted = true;
        } else {
            *count+=1.0;
            is_accepted = false;
        }
    }
  
    return is_accepted;
}

#endif /* _TAUSWORTHE_H_ */

