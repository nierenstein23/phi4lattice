#ifndef LATTICE_H
#define LATTICE_H

//#include <ilattice.h>
#include <global.h>
#include <rvectarray.h>
#include <lattice_index.h>

#include <fftw3.h>
#ifdef DOUBLE_PREC
typedef double ffTfloat;
typedef fftw_complex ffTcomplex;
typedef fftw_plan fft_plan;
#define fft_plan_r2c fftw_plan_dft_r2c_1d
#define fft_plan_c2r fftw_plan_dft_c2r_1d
//#define fft_plan_2d fftw_plan_dft_2d
#define fft_plan_r2c_2d fftw_plan_dft_r2c_2d
#define fft_plan_c2r_2d fftw_plan_dft_c2r_2d
#define fft_plan_r2c_3d fftw_plan_dft_r2c_3d
#define fft_plan_c2r_3d fftw_plan_dft_c2r_3d
#define fft_execute fftw_execute
#else
typedef float ffTfloat;
typedef fftwf_complex ffTcomplex;
typedef fftwf_plan fft_plan;
#define fft_plan_r2c fftwf_plan_dft_r2c_1d
#define fft_plan_c2r fftwf_plan_dft_c2r_1d
//#define fft_plan_2d fftwf_plan_dft_2d
#define fft_plan_r2c_2d fftwf_plan_dft_r2c_2d
#define fft_plan_c2r_2d fftwf_plan_dft_c2r_2d
#define fft_plan_r2c_3d fftwf_plan_dft_r2c_3d
#define fft_plan_c2r_3d fftwf_plan_dft_c2r_3d
#define fft_execute fftwf_execute
#endif


#include <datafile.h>
#include <fstream>
#include <boost/log/trivial.hpp>

class iLattice {
    private:
        // use relaxational dynamics by default
        dynmode dmode {relax};

        virtual void doUpdateZMomentum(const bool phi) = 0;
        virtual void doUpdateStressTensor() = 0;

        virtual void doCopyFrom(const iLattice &src);
        virtual void doSeedTaus(const long seed) = 0;

        virtual void doBackupPhi() = 0;
        virtual void doRestorePhi() = 0;

        // this fallback should generally produce correct results, but is not as efficient, as it does not know the HW base
        virtual void doStepPhi(const Tfloat dt) { phi->saxpy(dt, *pi); }

        virtual void stepPiRelax(const Tfloat dt) = 0;
        virtual void stepPiDiff(const Tfloat dt) = 0;

        virtual void stepPiRelaxLangevin(const Tfloat T, const Tfloat dt);
        virtual void stepPiDiffLangevin(const Tfloat T, const Tfloat dt);

        virtual std::vector<Tcomplex> getFieldP(const int p, const bool on_phi);

        virtual Tfloat doGetSliceCorrelation(const size_t offset);

        // these ones have to be aware of lattice borders
        virtual Tfloat doGetGradPhiSq() const = 0;
        virtual Tfloat doGetGradSqPhiSq() const = 0;
        virtual Tfloat doGetGradSqPhiQrt() const = 0;

        virtual Tfloat dogetinvlappisq() const { return 0.; };

        virtual void dampPi(const Tfloat damping) = 0;
        virtual void generateNoiseVector(const Tfloat amplitude) = 0;
        virtual void addGradNoiseToPi() = 0;

        virtual const latticeIndex & getDims() const = 0;

        virtual void doDumpInfo(const Tfloat T, const std::string outputLocation) const = 0;

        virtual void doSavePhiPretty(std::ofstream &outStream) const = 0;
        virtual void doSavePiPretty(std::ofstream &outStream) const = 0;

    protected:
        long seed;

        latticeIndex *index;
        rvectArray *phi, *pi;
        taus_state *rnd_state;

        ushort dim;

        bool zMomentumIsCurrent {false}, EzMomentumIsCurrent {false};
        std::vector<Tcomplex> *PhizMomentum, *PizMomentum, EzMomentum, TkBuffer;
        std::vector<Tfloat> *zCorrelation;

        std::vector<Tcomplex> *stressTensor;

        void convertPtoC();

        // iLattice3D is a friend of rvectArrays and can extract the pointers.
        // This is useful for momentum updates, since one needs both rvectarrays.
        Tfloat* getPhiPointer() const { return phi->fields; }
        Tfloat* getPiPointer() const { return pi->fields; }

        // physical parameters
        Tfloat lambda, kappa, gamma;
        Tfloat la, lmu0sq, lg0, J;

        void init();

    public:
        iLattice() = default;
        virtual ~iLattice();

        // utility methods ====================================================================
        void copyFrom(const iLattice &src);
        void seedTaus(const long newseed) { doSeedTaus(newseed); }

        void backupPhi() { doBackupPhi(); }
        void restorePhi() { doRestorePhi(); }

        void randomizePi(const Tfloat T);
        void randomizePhi(const Tfloat V) { setGaussPhi(0,SSQRT(V)); if(dmode==diff) phi->addConst(-getSumPhi()/Dims().getPoints()); }

        void setGaussPi(const Tfloat m, const Tfloat v) { pi->setGaussianRandom(m, v, rnd_state); }
        void setGaussPhi(const Tfloat m, const Tfloat v) { phi->setGaussianRandom(m, v, rnd_state); }

        // physical/observable methods ========================================================
        void stepPhi(const Tfloat dt) { doStepPhi(dt); }
        void stepPi(const Tfloat dt);
        void stepPiLangevin(const Tfloat T, const Tfloat dt);

        std::vector<Tcomplex> getPhiP(const int px) { return getFieldP(px, true); }
        std::vector<Tcomplex> getPiP(const int px) { return getFieldP(px, false); }
        Tcomplex getEP(const int px);
        //Tcomplex getStress(const short component, const int px);
        std::vector<Tcomplex>& getStressTensor(const short c);
        Tfloat getSliceCorrelation(const size_t offset) { return doGetSliceCorrelation(offset); }
        Tfloat getGradPhiSq() const { return doGetGradPhiSq(); }
        Tfloat getGradSqPhiSq() const { return doGetGradSqPhiSq(); }
        Tfloat getGradSqPhiQrt() const { return doGetGradSqPhiQrt(); }

        // these can be directly taken from the rvectArray instances
        Tfloat getPhiSq() const { return phi->sumSquares(); }
        Tfloat getPhiQuart() const { return phi->sumQuartes(); }
        Tfloat getPiSq() const { return pi->sumSquares(); }

        Tfloat getPhiJ(const ushort j) const { return phi->sumJthComponent(j); }
        Tfloat getPiJ(const ushort j) const { return pi->sumJthComponent(j); }

        Tfloat getSumPhi() const { return phi->sumComponents(); }
        Tfloat getSumPi() const { return pi->sumComponents(); }

        Tfloat getinvlappisqr() const { return dogetinvlappisq(); };

        // parameter methods ==================================================================
        const latticeIndex & Dims() const { return getDims(); }

        void generateLagrangeParameters(const Tfloat a, const Tfloat mu0sq, const Tfloat g0);
        long getSeed() const { return seed; }

        Tfloat getlatticeSpacing() const { return la; }
        Tfloat getMu0Sq() const { return lmu0sq; }
        Tfloat getg0() const { return lg0; }

        Tfloat getSource() const { return J; }
        void setSource(const Tfloat newJ) { J = newJ; }

        Tfloat getLambda() const { return lambda; }
        Tfloat getKappa() const { return kappa; }
        Tfloat getGamma() const { return gamma; }
        void setLambda(const Tfloat newLambda) { lambda = newLambda; }
        void setKappa(const Tfloat newKappa) { kappa = newKappa; }
        void setGamma(const Tfloat newGamma) { gamma = newGamma; }

        Tfloat getFieldFactor() const { return SSQRT(2*kappa)/la; }

        void dumpInfo(const Tfloat T, const std::string outputLocation) const { doDumpInfo(T, outputLocation); }

        void loadFields(std::ifstream &inStream);
        void saveFields(std::ofstream &outStream) const;

        void savePhiPretty(std::ofstream &outStream) const { doSavePhiPretty(outStream); }
        void savePiPretty(std::ofstream &outStream) const { doSavePiPretty(outStream); }

        ushort getDim() const { return dim; };
        void setDynMode(const dynmode dm) { dmode = dm; };
        dynmode getDynMode() const { return dmode; };
};

/**
 * @Synopsis abstract interface class for field lattices.
 *
 * This class does much of the work. Its derived classes only need 
 * to provide specific definitions for handling the different array
 * types. However, they may override some functions that thereby 
 * can be accelerated (e.g. by using a global GPU buffer).
 */
class iLattice3D : virtual public iLattice {
    protected:
        lattice3DIndex indexer;

        void doCopyFrom(const iLattice &src);

        const latticeIndex & getDims() const { return indexer; };
        void doDumpInfo(const Tfloat T, const std::string outputLocation) const;
        void doSavePhiPretty(std::ofstream &outStream) const;
        void doSavePiPretty(std::ofstream &outStream) const { outStream.flush(); };

    public:
        iLattice3D(const size_t lx, const size_t ly, const size_t lz, const ushort comp);
        iLattice3D(const size_t l, const ushort comp) : iLattice3D(l, l, l, comp) {};
        virtual ~iLattice3D() { BOOST_LOG_TRIVIAL(debug) << "iLattice3D destructor";};
};


class iLattice2D : virtual public iLattice {
    protected:
        lattice2DIndex indexer;

        void doCopyFrom(const iLattice &src);

        const latticeIndex & getDims() const { return indexer; };
        void doDumpInfo(const Tfloat T, const std::string outputLocation) const;
        void doSavePhiPretty(std::ofstream &outStream) const;
        void doSavePiPretty(std::ofstream &outStream) const;

    public:
        iLattice2D(const size_t lx, const size_t ly, const ushort comp);
        iLattice2D(const size_t l, const ushort comp) : iLattice2D(l, l, comp) {};
        virtual ~iLattice2D() { BOOST_LOG_TRIVIAL(debug) << "iLattice2D destructor";};
};

#endif
