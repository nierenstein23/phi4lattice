#ifndef RVECTARRAY_HPP
#define RVECTARRAY_HPP

#include <global.h>

#include <stddef.h>
#include <tausworthe.h>

#include <vector>
#include <complex>


class rvectArray {
        private:
                // this class needs to know how many elements are stored, as well as the number of components per site
                size_t elements;
                ushort components;
        
                virtual void doCopyFrom( const rvectArray &src) = 0;
                virtual void doWriteFieldsToBuffer(Tfloat * const buffer) const = 0;
                virtual void doReadFieldsFromBuffer(const Tfloat * const buffer) = 0;

                virtual Tfloat doSumComponents() const = 0;
                virtual Tfloat doSumJthComponent(const ushort j) const = 0;
                virtual Tfloat doSumSquares() const = 0;
                virtual Tfloat doSumQuartes() const = 0;

                virtual void doAddConst(const Tfloat a) = 0;
                virtual void doSaxpy(const Tfloat a, const rvectArray &x) = 0;
                virtual void doSetGaussianRandom(const Tfloat average, const Tfloat sigma, taus_state * const rnd_state) = 0;
                virtual void doRandomizeAndDamp(const Tfloat noiseAmp, const Tfloat damping, taus_state * const rnd_state) = 0;

        protected:
                friend class iLattice;
                Tfloat *fields;

        public:
                /**
                 * @Synopsis  Standard constructor does very few things, since derived classes handle *fields
                 *
                 * @Param NumberOfElements
                 * @Param NumberOfComponents
                 */
                rvectArray(const size_t NumberOfElements, const unsigned short NumberOfComponents) 
                        : elements(NumberOfElements), components(NumberOfComponents) {}

                virtual ~rvectArray() = default;

                void copyFrom( const rvectArray &src) { doCopyFrom(src); }
                void writeFieldsToBuffer(Tfloat * const buffer) const { doWriteFieldsToBuffer(buffer); }
                void readFieldsFromBuffer(const Tfloat * const buffer) { doReadFieldsFromBuffer(buffer); }

                Tfloat sumComponents() { return doSumComponents(); }
                Tfloat sumJthComponent(const ushort j) { return doSumJthComponent(j); }
                Tfloat sumSquares() { return doSumSquares(); }
                Tfloat sumQuartes() { return doSumQuartes(); }

                void addConst(const Tfloat a) { doAddConst(a); }
                void saxpy(const Tfloat a, const rvectArray &x) { doSaxpy(a, x); }
                void setGaussianRandom(const Tfloat average, const Tfloat sigma, taus_state * const rnd_state) { doSetGaussianRandom(average, sigma, rnd_state); }
                void randomizeAndDamp(const Tfloat noiseAmp, const Tfloat damping, taus_state * const rnd_state) { doRandomizeAndDamp(noiseAmp, damping, rnd_state); }

                size_t NoElements() const { return elements; }
                ushort NoComponents() const { return components; }
                size_t NoFloats() const { return NoElements()*NoComponents(); }
                
};

#endif
