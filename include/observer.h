#ifndef OBSERVER_H
#define OBSERVER_H
#include <lattice.h>
#include <datafile.h>
#include <boost/filesystem.hpp>

#include <boost/serialization/utility.hpp>

namespace fs = boost::filesystem;

enum updateMode { FULL, EUCL, CLAS, LANG };

class Observer {
    public:
        Observer(iLattice *lat, bool ntxtout=false, bool npout=false);
        ~Observer();

        void update(updateMode mode=FULL);
        void output(Tfloat time);
        void reset();
        void backup();

        void setEP(bool newEP) { doEP = newEP; };

        void setPostfix(std::string newPostfix);
        void init (std::string outputLoc, std::string newPostfix);
        void setParams(Tfloat T);
        void finalize();

        Tfloat getEnergy() const;
        Tfloat getEPot() const;
        Tfloat getEKin() const;

        void setMaxP(unsigned int newmaxp) { if (newmaxp != 0) maxp = newmaxp; }
        unsigned int getMaxP() { return maxp; };

    private:
        iLattice *lattice;

        dataset theData, previousData;
        DataSeries binData;

        bool doEP;
        unsigned int maxp;

        // momentum fun
        fs::ofstream PFile, EPFile, BinPFile, TFile;
        std::map<Tfloat, std::vector<Tcomplex> > PhiPt, PiPt, bPhiPt, bPiPt;
        std::map<Tfloat, Tcomplex > EPt, bEPt;
        std::vector< std::map< Tfloat, std::vector<Tcomplex > > > PhiP, PiP;

        //std::vector<Tfloat> zCorr, bzCorr;

        std::string outputLocation;
        std::string postfix; 

        bool txtout, pbinout, hasbackup;
        fs::ofstream OPFile, EnergyFile, MomFile, BinFile, CorrFile;

        void generatePhiPi();
        void generateMoments(bool nan=false);
        void generateEnergies(bool nan=false);
        void generateCorrels(bool nan=false);
        void generateP(bool nan=false);
        void generateEP(bool nan=false);

        std::string OPString() const;
        std::string EnergyString() const;
        std::string MomString() const;
        std::string PString() const;
        std::string EPString() const;
        std::string CorrString() const;

        void writeToTFile(Tfloat time);
};

#endif
