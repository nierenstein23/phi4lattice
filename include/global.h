#ifndef GLOBAL_H
#define GLOBAL_H

#ifdef DOUBLE_PREC
typedef double Tfloat;
#else
typedef float Tfloat;
#endif

#include<complex>
typedef unsigned short ushort;
typedef unsigned int uint;
typedef std::complex<Tfloat> Tcomplex;

// get M_PI
#define _USE_MATH_DEFINES
#include <math.h>

// autocompleter workaround
#ifdef __clang__
#define __syncthreads()
#endif

// include execution space decorators only if compiled for device
#ifdef __CUDACC__
#define C_HD __host__ __device__
#define CHOST __host__
#define CDEV __device__
#else 
#define C_HD
#define CHOST
#define CDEV
#endif

// rudimentarily detect SMT
#ifdef __AVX512F__		// we are on a phi node, 
#define __SMT__ 4		// which has quad hyperthreading
#else 
#define __SMT__ 2		// other stuff has double HT
#endif

// use the correct intrinsics dependent on floattype!
#ifndef DOUBLE_PREC

#define COS(x) cosf(x)
#define SIN(x) sinf(x)
#define LOG(x) logf(x)
#define SQRT(x) sqrtf(x)
#define FMA(x,y,z) fmaf(x,y,z)

#else

#define COS(x) cos(x)
#define SIN(x) sin(x)
#define LOG(x) log(x)
#define SQRT(x) sqrt(x)
#define FMA(x,y,z) fma(x,y,z)

#endif

#define DELTA(a,b) (((a)==(b))?(1):(0)) 
#define SQR(x) ((x)*(x))
#define CUB(x) ((x)*SQR(x))
#define QRT(x) (SQR(x)*SQR(x))
#define SSQRT(x) std::sqrt(x)
#define EXP(x) std::exp(x)

#define CGX_BLOCKSIZE 128
#define NCMAX 16

/*
 * Stress tensor has 8 (9) (potentially) interesting components in 2+1D (3+1D):
 *      T00: energy density
 *      T10, T20: longitudinal, transversal energy flux
 *      T01, T02: longitudinal, transversal momentum (same as energy flux in Model A/C)
 *      T11, T22: longitudinal, transversal pressure
 *      T12 (, T23): longitudinal (, transversal) shear stress
 *
 *      Output will happen in this order
 */
//#define STRESS_TENSOR_NC 9

struct Tfloat2 { Tfloat x; Tfloat y; };

// relaxational (Glauber-like) or diffusive (Kawasaki-like) dynamics
enum dynmode { relax, diff};

enum hwtype { CPU, GPU };
struct hardware {
    hwtype type;
    std::string selector;
    ushort dev;
};


#endif
