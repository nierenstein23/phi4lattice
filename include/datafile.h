#ifndef DATA_FILE_H
#define DATA_FILE_H

#include <global.h>
#include <vector>
#include <utility>
#include <map>

#include <fstream>

#include <boost/serialization/access.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/version.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/split_free.hpp>

enum dataField { PHI, PI, PHISQ, PISQ, GPHISQ, PHIQ, C1, C2, E, EK, EP, C3, C4 }; // added C3 and C4 later (obviously); positioned at the end to mitigate serialization compatibility problems

/**
 * @Synopsis  datapoint containing equal-time observables
 */
struct dataset {
    std::vector<Tfloat> PhiAvg;
    std::vector<Tfloat> PiAvg;

    std::map<dataField, Tfloat> datamap;

    Tfloat operator [] (dataField field) const { const Tfloat val=this->datamap.at(field); return val; }
    Tfloat& operator [] (dataField field) { return this->datamap[field]; }
};

/**
 * @Synopsis  Parameter set containing the environment parameters of the simulation
 */
struct paramset {
    int Nx, Ny, Nz;
    ushort components;

    Tfloat MSqr, lambda;
    Tfloat T, J, g;

    void printToFile(std::ofstream& outStream) {
        outStream
            << "#Nc=" << components << std::endl
            << "#MSqr=" << MSqr << std::endl
            << "#lambda=" << lambda << std::endl
            << "#T=" << T << std::endl
            << "#J=" << J << std::endl
            << "#Nx=" << Nx << std::endl
            << "#Ny=" << Ny << std::endl
            << "#Nz=" << Nz << std::endl;
    }

    bool operator==(const paramset &rhs) {
    return 
        (Nx == rhs.Nz) &&
        (Ny == rhs.Ny) &&
        (Nz == rhs.Nz) &&
        (components == rhs.components) &&
        (MSqr == rhs.MSqr) &&
        (lambda == rhs.lambda) &&
        (T == rhs.T) &&
        (J == rhs.J) &&
        (g == rhs.g);
    }

    bool operator!=(const paramset &rhs) { return !(*this == rhs); }
};



/**
 * @Synopsis  Container class for a series of equal-time data.
 *              Has easier access and some utility functions
 */
class DataSeries {
    public:
        /**
         * @Synopsis  operator(datafield, line) is the more practical equivalent of [][], and returns the value of datafield in line no. line.
         *
         * @Param field the requested data field
         * @Param line  the requested line
         * @Param component in case of PHI/PI: component can be specified; defaults to 0
         *
         * @Returns the value of field at line
         */
        Tfloat operator() (dataField field, size_t line, ushort component=0) const { 
            switch (field) {
                case PHI: return data[line].second.PhiAvg[component];
                case PI: return data[line].second.PiAvg[component];

                default: return data[line].second[field];
            }
        };

        float timestep() const { return data[1].first - data[0].first; };
        float time(size_t line) const { return data[line].first; }
        size_t size() const { return data.size(); };

        void setParams(paramset newParams) { parameters = newParams; };
        paramset getParams() const { return parameters; };

        void init(std::string newDesc) { data.clear(); description = newDesc; };
        void append(float time, dataset newData) { data.push_back( std::make_pair(time, newData) ); };

        std::vector<Tfloat> stripData(dataField field, ushort component=0) const {
            std::vector<Tfloat> output(data.size());

            for (size_t i=0; i<data.size(); i++) 
                output[i]=(*this)(field, i, component);

            return output;
        };

    private:
        std::vector< std::pair<float, dataset> > data;
        paramset parameters;
        std::string description;

        friend class boost::serialization::access;
        template<class Archive> void serialize(Archive & ar, const unsigned int version) {
            if (version > 0) ar & description & parameters & data;
        }
};


namespace boost {
    namespace serialization {
        template<class Archive>
            void serialize(Archive &ar, dataset &d, const unsigned int version) {
                if (version>0) {
                    ar & d.PhiAvg & d.PiAvg;
                    ar & d.datamap;
                }
            }

        template<class Archive>
            void serialize(Archive &ar, paramset &p, const unsigned int version) {
                if (version>0) {
                    ar & p.Nx & p.Ny & p.Nz;
                    ar & p.components;

                    ar & p.MSqr & p.lambda;
                    ar & p.T & p.J & p.g;
                }
            }

        template<class Archive> void load(Archive & ar, Tcomplex & c, const unsigned int version) {
            if (version>0) {
                Tfloat re=1, im=1;
                ar & re & im;

                c = Tcomplex(re, im);
            }
        }

        template<class Archive> void save(Archive & ar, Tcomplex c, const unsigned int version) {
            if (version>0) ar & c.real() & c.imag();
        }
    }
}

BOOST_CLASS_VERSION(DataSeries, 1)
BOOST_CLASS_VERSION(dataset, 1)
BOOST_CLASS_VERSION(paramset, 1)
BOOST_CLASS_VERSION(Tcomplex, 1)
BOOST_SERIALIZATION_SPLIT_FREE(Tcomplex)
#endif
