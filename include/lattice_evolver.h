#ifndef LATTICE_EVOLVER_H
#define LATTICE_EVOLVER_H

#include <lattice.h>
#include <tausworthe.h>
#include <observer.h>
#include <functional>

#include <simline.h>

#define TFOFTF std::function< Tfloat(Tfloat) >

class LatticeEvolver {
    private:
        iLattice *lattice;
        Observer obs;
        bool tmi; unsigned int tmifac;

        double time;

        void initObs(std::string postfix, std::string infix="ID");
        unsigned int stepFac(unsigned steps64);

        std::string outputLoc {"OUTPUT/"};

    public:
        LatticeEvolver(iLattice *lat, bool txtout=false, bool pbinout=false)
            : lattice(lat), obs(lat, txtout, pbinout), tmi(false), tmifac(1) {};

        Observer & theObs() { return obs; }

        void settmi(bool newtmi, unsigned int newtmifac=1) { tmi=newtmi; tmifac=newtmifac; };
        void setEP(bool newEP) { obs.setEP(newEP); };
        void setOutputLoc(std::string newOutputLoc) { outputLoc = newOutputLoc; };

        void Thermalize(Tfloat T, Tfloat TMax, unsigned int tSteps, unsigned int sweeps=10, Tfloat dt=0.1, std::string postfix="");

        void HMC(Tfloat T, unsigned NTraj, Tfloat Ltraj=1.0, unsigned NSteps=0, std::string postfix="HMC");
        void RT(Tfloat T, Tfloat TMax, unsigned int tSteps, bool langevin=false, Tfloat dt=0.1, std::string postfix="");

        void RTClassical(Tfloat T, Tfloat TMax, unsigned int tSteps, Tfloat dt=0.1, std::string postfix="Conservative")
        { RT(T, TMax, tSteps, false, dt, postfix); }

        void RTLangevin(Tfloat T, Tfloat TMax, unsigned int tSteps, Tfloat dt=0.1, std::string postfix="Langevin") 
        { RT(T, TMax, tSteps, true, dt, postfix); }

        void RTFunctional(TFOFTF Toft, TFOFTF Joft, Tfloat Time, unsigned int tSteps, Tfloat dt=0.1, std::string postfix="Functional");

        void Simulate(simline sl);

        void resetTime() { time = 0.0; };
        void Leapfrog(unsigned n, Tfloat dt, Tfloat T=0, bool langevin=false);
        void LeapfrogFunctional(unsigned n, Tfloat dt, Tfloat t0, TFOFTF Toft, TFOFTF Joft);
        void Omelyan(unsigned n, Tfloat dt, Tfloat xi=0.1931833);
};

#endif
