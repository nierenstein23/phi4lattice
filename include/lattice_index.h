#ifndef LATTICE_INDEX_H
#define LATTICE_INDEX_H

//#include <stddef.h>
#include <algorithm>
#include <global.h>

/**
 * @Synopsis Base interface class to model lattice index methods, at this level
 * independent of dimensionality.
 */
class latticeIndex {
    public:
        C_HD latticeIndex(unsigned short comp) : components(comp) {};

        C_HD unsigned short getD() const { return d; };
        C_HD unsigned short getComponents() const { return components; };

        /**
         * @Synopsis Elements are the sites of the lattice.
         *
         * @Returns Number of elements/sites on the lattice
         */
        C_HD size_t getElements() const { return doGetElements(); };
        C_HD size_t getPoints() const { return getElements()*getComponents(); };
        C_HD size_t getNeighbour(size_t idx, short dir) const  { return doGetNeighbour(idx, dir); };
        C_HD size_t getIdxNeighbour(size_t idx, short dir) const  { return doGetNeighbour(idx, dir); };

        C_HD size_t getL() const { return doGetL(); };
        C_HD size_t getL(unsigned short i) const { return doGetLi(i); };
        C_HD size_t getKVol() const { return doGetKVol(); };

    protected:
        unsigned short d;
        unsigned short components;

    private:
        C_HD virtual size_t doGetL() const = 0;
        C_HD virtual size_t doGetLi(unsigned short i) const = 0; // no simple overload because of partial hiding
        C_HD virtual size_t doGetKVol() const = 0;
        C_HD virtual size_t doGetElements() const = 0;
        C_HD virtual size_t doGetNeighbour(size_t idx, short dir) const = 0;
};


class lattice2DIndex : public latticeIndex {
    private: 
        C_HD size_t doGetL() const { return lx; };
        C_HD virtual size_t doGetLi(unsigned short i) const
        { 
            size_t res;
            switch(i) {
                case 0: res = lx; break;
                case 1: res = ly; break;
                default: res = 0;
             }
            return res;
        };
        C_HD size_t doGetElements() const { return lx*ly; };
        C_HD size_t doGetKVol() const { return ly*(lx/2+1); };
        C_HD size_t doGetNeighbour(size_t idx, short dir) const { size_t x,y; deindex(idx, x, y); return getNeighbour(x, y, dir); }

    protected:
        size_t lx, ly;
        size_t dx, dy;

    public:
        C_HD lattice2DIndex(size_t x, size_t y, unsigned short comp) : latticeIndex(comp), lx(x), ly(y)
        {
            dx = getComponents(); 
            dy = lx*dx; 
            d=2;
        };

        C_HD lattice2DIndex(size_t l, unsigned short comp) : lattice2DIndex(l, l, comp) {};

        C_HD size_t getNeighbour(size_t x, size_t y, short dir) const
        {
            switch(dir) {
                case 1:		x += 1; if(x >= lx) x = 0; break;
                case -1:	if(x == 0) {x = lx - 1;} else {x -= 1;}; break;

                case 2:		y += 1; if(y >= ly) y = 0; break;
                case -2:	if(y == 0) {y = ly - 1;} else {y -= 1;}; break;

                default: break;
            }

            return x*dx + y*dy;
        };

        C_HD long int getdxf(size_t x) const { if (x == lx-1)       return -(lx-1)*dx;      else return dx; };
        C_HD long int getdxb(size_t x) const { if (x == 0)          return (lx-1)*dx;       else return -dx; };
        C_HD long int getdyf(size_t y) const { if (y == ly-1)       return -(ly-1)*dy;      else return dy; };
        C_HD long int getdyb(size_t y) const { if (y == 0)          return (ly-1)*dy;       else return -dy; };

        C_HD void deindex(size_t idx, size_t &x, size_t &y) const 
        {
            y = idx/dy; idx -= y*dy;
            x = idx/dx; 
        };

        C_HD size_t index(int x, int y) const 
        {
            return x*dx + y*dy;
        }

        C_HD size_t getIdxNeighbour(size_t idx, short dir) const
        { 
            size_t x,y; deindex(idx, x, y);
            return (getNeighbour(x,y,dir));
        };

        C_HD size_t getLx() const { return lx; };
        C_HD size_t getLy() const { return ly; };
        C_HD size_t getDx() const { return dx; };
        C_HD size_t getDy() const { return dy; };
};


class lattice3DIndex : public lattice2DIndex {
    private:
        C_HD size_t doGetLi(unsigned short i) const
        { 
            size_t res;
            switch(i) {
                case 2: res = lz; break;
                default: res = lattice2DIndex::getL(i);
             }
            return res;
        };
        C_HD size_t doGetKVol() const { return lz*ly*(lx/2+1); };
        C_HD size_t doGetElements() const { return lx*ly*lz; };
        C_HD virtual size_t doGetNeighbour(size_t idx, short dir) const { size_t x,y,z; deindex(idx, x, y, z); return getNeighbour(x, y, z, dir); }

    protected:
        size_t lz;
        size_t dz;

    public:
        C_HD lattice3DIndex(size_t x, size_t y, size_t z, unsigned short comp) : lattice2DIndex(x, y, comp), lz(z)
        {
            dz = ly*dy;
            d=3;
        }

        C_HD lattice3DIndex(size_t l, unsigned short comp) : lattice3DIndex(l, l, l, comp) {};

        C_HD size_t getNeighbour(size_t x, size_t y, size_t z, short dir) const
        {
            switch(dir) {
                case 3:		z += 1; if(z >= lz) z = 0; break;
                case -3:	if(z == 0) {z = lz - 1;} else {z -= 1;}; break;
                default:        return lattice2DIndex::getNeighbour(x, y, dir) + z*dz;
            }
            return x*dx + y*dy + z*dz;
        }

        C_HD long int getdzf(size_t z) const { if (z == lz-1)       return -(lz-1)*dz;      else return dz; };
        C_HD long int getdzb(size_t z) const { if (z == 0)          return (lz-1)*dz;       else return -dz; };

        C_HD void deindex(size_t idx, size_t &x, size_t &y, size_t &z) const
        {
            z = idx/dz; idx -= z*dz;
            lattice2DIndex::deindex(idx, x, y);
        };

        C_HD size_t index(int x, int y, int z) const
        {
            return lattice2DIndex::index(x, y) + z*dz;
        }

        C_HD size_t getLz() const { return lz; };
        C_HD size_t getDz() const { return dz; };
};

#endif
