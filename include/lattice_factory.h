#ifndef LATTICE_FACTORY_H
#define LATTICE_FACTORY_H


#ifndef __CPUONLY__
#include <gpu_lattice.hcu>
#endif
#include <cpu_lattice.hpp>

#include <iostream>
#include <exception>

/**
 * @Synopsis  Factory class producing field lattices on various hardware platforms.
 */
class latticeFactory {
    public:
        static iLattice* buildIsoLattice(size_t l, ushort c, hardware hw, ushort dim) {
            switch (dim) {
                case 3:
                    switch (hw.type) {
                        case CPU:
                            return new CPU3DLattice(l, c); break;
                        case GPU:
#ifndef __CPUONLY__
                            return new GPU3DLattice(l, c, hw); break;
#else
                            throw std::invalid_argument("selected arch not available"); break;
#endif
                        default:
                            return (iLattice*)nullptr; break; // return empty pointer
                    } break;
                case 2:
                    switch (hw.type) {
                        case CPU:
                            return new CPU2DLattice(l, c); break;
                        case GPU:
#ifndef __CPUONLY__
                            return new GPU2DLattice(l, c, hw); break;
#else
                            throw std::invalid_argument("selected arch not available"); break;
#endif
                        default:
                            return (iLattice*)nullptr; break; // return empty pointer
                    } break;

                default:
                    throw std::runtime_error("The dimension you desire is temporarily not available");
            }
        }
};


/**
 * @Synopsis  Overload of istream >>, so we can decode hardware enums.
 *
 * @Param in the input stream
 * @Param hw the hardware type
 *
 * @Returns the input stream, with a failbit in case of invalid input
 */
inline std::istream& operator>>(std::istream& in, hwtype& hw) {
    std::string input;
    in >> input;

    if (input == "CPU") hw = CPU;
    else if (input == "GPU") hw = GPU;
    else in.setstate(std::ios_base::failbit);

    return in;
}

/**
 * @Synopsis  Overload of istream >>, so we can decode dynmode enums.
 *
 * @Param in the input stream
 * @Param dm the dynamics mode
 *
 * @Returns the input stream, with a failbit in case of invalid input
 */
inline std::istream& operator>>(std::istream& in, dynmode& dm) {
    std::string input;
    in >> input;

    if (input == "relax") dm = relax;
    else if (input == "diff") dm = diff;
    else in.setstate(std::ios_base::failbit);

    return in;
}

#endif
