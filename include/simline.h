#ifndef SIMLINE_H
#define SIMLINE_H

#include <sstream>
#include <vector>
#include <algorithm>

#include <boost/log/trivial.hpp>

#define restofstr std::atof(prop.substr(1, std::string::npos).c_str())

enum simmode { LANGEVIN, CONSERVATIVE, LINEAR, ASYMPTOTIC, RESET, HMC };

/**
 * @Synopsis should contain what is needed by the lattice evolver to complete a simulation run
 */
struct simline {
    simmode mode {LANGEVIN};

    float time {0.};

    float trate {.05};    // asymptotic rate of change in T
    float jrate {1.};    // asymptotic rate of change in J
    float texp {1.};     // exponent of asymptotic change in T
    float jexp {1.};     // exponent of asymptotic change in J

    float T {1.};       // initial temperature
    float Tfinal {1.0}; // final temperature, only relevant for quench modes

    float J {0.};       // initial source
    float Jfinal {0.};  // final source, only relevant for quench modes

    std::string desc {"SIM"};

    unsigned int tSteps {160};
    float dt {1.0};

    std::string str() 
    { 
        std::stringstream buf;
        switch (mode) {
            case LANGEVIN:
            case CONSERVATIVE:
                buf << "Evolving with mode=" << mode << " for t=" << time << ", at T=" << T << " and J=" << J; break;
            case LINEAR:
                buf << "Evolving linearly (mode=" << mode << ") for t=" << time << ", from Ti=" << T << ", Ji=" << J << " to Tf=" << Tfinal << ", Jf=" << Jfinal; break;
            case HMC:
                buf << "HMC Evolution for t=" << time << ", at T=" << T << " and J=" << J; break;
            case ASYMPTOTIC:
                buf << "Evolving asymptotically (mode=" << mode << ") for t=" << time << ", from Ti=" << T << ", Ji=" << J << " to Tf=" << Tfinal << ", Jf=" << Jfinal << ", with t/j-rate " << trate << ", " << jrate << " and exponents " << texp << ", " << jexp; break;
            case RESET:
                buf << "RESET"; break;

            default:
                buf << "UNHANDLED MODE m=" << mode;
        }

        return buf.str();
    };
};

/**
 * @Synopsis Parser for human-readable strings to the simline format.
 *
 * Simlines contain '|'-(pipe-)separated strings. Each pipe implies a new simulation mode.
 * Between the pipes, one gives the characteristic quantities for the simulation in a comma-separated format.
 * Therein, an initial character indicates what the following substring signifies.
 *
 * Example: "L100,T5.0,J0.1|l10,T5.0,F4.0,J0.1,G0|L100,T4.0,J0"
 *      1. Langevin evolution for t=100 at T=5.0, J=0.1
 *      2. linear quench for t=10 from T=5.0, J=0.1 to T=4.0, J=0
 *      3. Langevin evolution for t=100 at T=4.0, J=0
 *
 */
class simline_parser { 
    public: 
        static std::vector<simline> parse(std::string &line)
        {
            std::vector<simline> result;
            simline sim;

            for (std::string & simstr : split(line, '|')) {

                for (std::string & prop : split(simstr, ',')) {
                    try {
                        const char marker = prop[0];

                        switch(marker) {
                            case 'C':
                                sim.mode = CONSERVATIVE;
                                sim.time = restofstr; break;
                            case 'H':
                                sim.mode = HMC;
                                sim.time = restofstr; break;
                            case 'L':
                                sim.mode = LANGEVIN;
                                sim.time = restofstr; break;
                            case 'l':
                                sim.mode = LINEAR;
                                sim.time = restofstr; break;
                            case 'A':
                                sim.mode = ASYMPTOTIC;
                                sim.time = restofstr; break;
                            case 'R':
                                sim.mode = RESET; break;

                            case 'T':
                                sim.T = restofstr; break;
                            case 'F':
                                sim.Tfinal = restofstr; break;
                            case 'J':
                                sim.J = restofstr; break;
                            case 'G':
                                sim.Jfinal = restofstr; break;

                            case 'D':
                                sim.desc = prop.substr(1, std::string::npos); break;

                            case 'r':
                                sim.trate = restofstr; break;
                            case 'q':
                                sim.jrate = restofstr; break;
                            case 'e':
                                sim.texp = restofstr; break;
                            case 'f':
                                sim.jexp = restofstr; break;

                            case 't':
                                sim.dt = restofstr; break;
                            case 'd':
                                sim.tSteps = std::atoi(prop.substr(1, std::string::npos).c_str()); break;

                            default:
                                BOOST_LOG_TRIVIAL(error) << "Unknown marker: " << marker;
                                throw std::runtime_error("error parsing simline");
                                break;
                        }
                    }

                    catch (const std::exception& e) {
                        BOOST_LOG_TRIVIAL(error) << "Could not decode simline!";
                        BOOST_LOG_TRIVIAL(error) << "Problematic part: " << simstr << ", prop:" << prop;
                        throw std::runtime_error("error parsing simline");
                    }
                }

                result.push_back(sim);
            }

            return result;
        }

    private: 
        static inline std::vector<std::string> split(const std::string& s, char delimiter)
        {
            std::vector<std::string> tokens;
            std::string token;
            std::istringstream tokenStream(s);
            while (std::getline(tokenStream, token, delimiter))
            {
                tokens.push_back(token);
            }
            return tokens;
        }

};
#endif
