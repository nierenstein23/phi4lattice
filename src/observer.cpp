#include <observer.h>
#include <sstream>
#include <numeric>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/map.hpp>
#include <boost/log/trivial.hpp>


/**
 * @Synopsis  Updates the observables and stores them in private variables.
 */
void Observer::update(updateMode mode) {

    // always update p=0 field stuff
    generatePhiPi();

    bool genMomE=true, genCorr=true, genP=true, genEMod=true;

    // exclude observables based on update mode
    switch (mode) {
        default:
            genCorr = false;
            break;

        case FULL:      // record everything
            break;
    }

    generateMoments(!genMomE);
    generateEnergies(!genMomE);
    generateCorrels(!genCorr);
    generateP(!genP);
    if (doEP) generateEP(!genEMod);
}


/**
 * @Synopsis  record field energies
 *
 * @Param nan if true, do not actually measure, but save time and fill with NaN
 */
void Observer::generateEnergies(bool nan) {
    if (nan) {
        theData[E] = NAN;
        theData[EK] = NAN;
        theData[EP] = NAN;
    } else {
        theData[E] = getEnergy();
        theData[EK] = getEKin();
        theData[EP] = getEPot();
    }
}

/**
 * @Synopsis  Record field correlators
 *
 * @Param nan if true, do not actually measure, but save time and fill with NaN
 */
void Observer::generateCorrels(bool nan) {
    if (nan) {
        theData[C1] = NAN;
        theData[C2] = NAN;
        theData[C3] = NAN;
        theData[C4] = NAN;
    } else {
        theData[C1] = lattice->getSliceCorrelation(1); // - OPSq/elems;
        theData[C2] = 0;
        theData[C3] = 0;
        theData[C4] = 0;
    }
}


/**
 * @Synopsis  Record field moments average
 *
 * @Param nan if true, do not actually measure, but save time and fill with NaN
 */
void Observer::generateMoments(bool nan) {
    if (nan) {
        theData[PHISQ] = NAN;
        theData[PISQ] = NAN;
        theData[GPHISQ] = NAN;
        theData[PHIQ] = NAN;
    } else {
        theData[PHISQ] = lattice->getPhiSq();
        theData[PISQ] = lattice->getPiSq();
        theData[GPHISQ] = lattice->getGradPhiSq();
        theData[PHIQ] = lattice->getPhiQuart();
    }
}


/**
 * @Synopsis  Fill vectors with p=0 field observables
 */
void Observer::generatePhiPi() {
    ushort comp = lattice->Dims().getComponents();
    
    // read in average over order parameters
    if (comp == 1) { 
        // special case N=1 can use optimizations
        theData.PhiAvg[0] = lattice->getSumPhi();
        theData.PiAvg[0] = lattice->getSumPi();
    } else {
        for (ushort j=0;j<comp;j++) {
            theData.PhiAvg[j] = lattice->getPhiJ(j);
            theData.PiAvg[j] = lattice->getPiJ(j);
        }
    };
}


/**
 * @Synopsis  Fill vectors with finite momentum observables
 *
 * @Param nan if true, do not actually measure, but save time and fill with NaN
 */
void Observer::generateP(bool nan) {
    // vector struct for lazy access

    ushort comp = lattice->Dims().getComponents();
    size_t L = lattice->Dims().getL();
    std::vector<Tcomplex> buffer(comp);

    for (uint p=1;p<L/2;p++) {
        Tfloat idx = p;

        if (nan) {
            PhiPt[idx] = std::vector<Tcomplex>(comp,NAN);
            PhiPt[idx] = std::vector<Tcomplex>(comp,NAN);
        } else {
            // init
            PhiPt[idx] = lattice->getPhiP(p);
            PiPt[idx] = lattice->getPiP(p);
        }
    }

}

/**
 * @Synopsis  Fill vectors with finite momentum observables
 *
 * @Param nan if true, do not actually measure, but save time and fill with NaN
 */
void Observer::generateEP(bool nan) {
    size_t L = lattice->Dims().getL();

    for (uint p=1;p<L/2+1;p++) {
        Tfloat idx = p;

        if (nan) EPt[idx] = Tcomplex(0);
        else EPt[idx] = lattice->getEP(p);
    }
}


/**
 * @Synopsis sums kinetic and potential energy.
 *
 * @Returns the total lattice energy
 */
Tfloat Observer::getEnergy() const {
    return getEKin() + getEPot();
}


/**
 * @Synopsis  Calculate the field energy, a.k.a. the classical action.
 *
 * @Returns the field energy
 */
Tfloat Observer::getEPot() const {
    Tfloat m2=lattice->getMu0Sq(), g0=lattice->getg0(), J=lattice->getSource();
    ushort N = lattice->Dims().getComponents();

    return 0.5*theData[GPHISQ] + 0.5*m2*theData[PHISQ]+ (g0/(24*N))*theData[PHIQ]+ J*theData.PhiAvg[0]; 
}


/**
 * @Synopsis  Calculate the "kinetic" energy term, i.e. the contribution of conjugate momenta.
 *
 * @Returns  the kinetic energy term 0.5 pi0^2 = pi^2/k (?)
 */
Tfloat Observer::getEKin() const {
    // units should not matter
    return theData[PISQ]/2.0;
}

//======================================================================================

Observer::Observer(iLattice *lat, bool ntxtout, bool npout) : lattice(lat), txtout(ntxtout), pbinout(npout) {
    doEP = false;

    hasbackup = false;
    theData.PhiAvg.resize(lattice->Dims().getComponents());
    theData.PiAvg.resize(lattice->Dims().getComponents());

    maxp = lat->Dims().getL();
}


void Observer::init(std::string outputLoc, std::string newPostfix) {
    outputLocation = outputLoc;
    setPostfix(newPostfix);

    binData.init(newPostfix);
    PhiP.clear();
    PiP.clear();

    // write header to TFile
    size_t L = lattice->Dims().getL();
    TFile.write(reinterpret_cast<const char *>(&L), sizeof(size_t));
}



void Observer::setPostfix(std::string newPostfix) {
    if (OPFile.is_open()) OPFile.close();
    if (EnergyFile.is_open()) EnergyFile.close();
    if (MomFile.is_open()) EnergyFile.close();
    if (PFile.is_open()) PFile.close();
    if (doEP && EPFile.is_open()) EPFile.close();
    if (doEP && TFile.is_open()) TFile.close();
    if (CorrFile.is_open()) CorrFile.close();
    
    postfix = newPostfix;

    if (txtout) {
        OPFile.open(fs::path(outputLocation + "OrderParameter" + postfix), std::ios::app);
        EnergyFile.open(fs::path(outputLocation + "Energy" + postfix), std::ios::app);
        MomFile.open(fs::path(outputLocation + "Moments" + postfix), std::ios::app);
        PFile.open(fs::path(outputLocation + "FiniteP" + postfix), std::ios::app);
        if (doEP) EPFile.open(fs::path(outputLocation + "EModesP" + postfix), std::ios::app);
        if (doEP) TFile.open(fs::path(outputLocation + "Stress" + postfix), std::ios::app|std::ios::binary);
        CorrFile.open(fs::path(outputLocation + "Correlator" + postfix), std::ios::app);
    }
}


Observer::~Observer() {
    OPFile.close();
    EnergyFile.close();
    MomFile.close();
    PFile.close();
    if (doEP) EPFile.close();
    if (doEP) TFile.close();
    CorrFile.close();
}


//======================================================================================

void Observer::output(Tfloat time) {
    // append to binary
    if (pbinout) {
        binData.append(time, theData);
        PhiP.push_back(PhiPt);
        PiP.push_back(PiPt);
    }

    if(txtout) {
        // if for some reason we lost the files, reopen them!
        if (!(OPFile.is_open()) || !(EnergyFile.is_open()) || !(MomFile.is_open()))
            setPostfix(postfix);

        OPFile << time << OPString() << std::endl;
        EnergyFile << time <<  " " << EnergyString() << std::endl;
        MomFile << time <<  " " << MomString() << std::endl;
        PFile << time << " " << PString() << std::endl;
        if (doEP) EPFile << time << " " << EPString() << std::endl;
        CorrFile << time << " " << CorrString() << std::endl;;
    }

    writeToTFile(time);
}


void Observer::writeToTFile(Tfloat time)
{
    if (doEP) {
    TFile.write(reinterpret_cast<const char *>(&time), sizeof(Tfloat));

    size_t csize = lattice->Dims().getL()/2 + 1;
    short stcomp = (lattice->Dims().getD()==2)?8:9;
    for (short c=0;c<stcomp;c++) {
        Tcomplex * data = lattice->getStressTensor(c).data();
        TFile.write(reinterpret_cast<const char *>(data), csize*sizeof(Tcomplex));
    }
    }
}


void Observer::reset() {
    if (hasbackup) {
        // load the backup
        theData = previousData;

        // special treatment for finite p
        PhiPt = bPhiPt;
        PiPt = bPiPt;
        EPt = bEPt;
    }
}


void Observer::backup() {
    // make backup
    previousData = theData;

    bPhiPt = PhiPt;
    bPiPt = PiPt;
    bEPt = EPt;

    hasbackup=true;
}


void Observer::finalize() {
    OPFile.close(); 
    EnergyFile.close(); 
    MomFile.close(); 
    PFile.close(); 
    if (doEP) EPFile.close(); 

    if (pbinout) {
        try {
            BinFile.open(fs::path(outputLocation + "BIN" + postfix), std::ios::binary);
            boost::archive::binary_oarchive boa(BinFile);
            boa << binData;
            BinFile.close();

            PhiP.shrink_to_fit();
            PiP.shrink_to_fit();
            BinPFile.open(fs::path(outputLocation + "BINP" + postfix), std::ios::binary);
            {
                boost::archive::binary_oarchive boap(BinPFile);
                boap << PhiP.size();
                boap << PhiP << PiP;
                boap << binData.getParams() << binData.timestep();
            }
            BinPFile.close();
        } catch (std::exception &e) {
            BOOST_LOG_TRIVIAL(warning) << "writing binary output failed - does output location exist?"; 
        }
    }
}


void Observer::setParams(Tfloat T) {
    paramset params;

    // TODO: think of a better concept
    switch (lattice->getDim()) {
        case 3:
            params.Nx = reinterpret_cast<const lattice3DIndex&>(lattice->Dims()).getLx();
            params.Ny = reinterpret_cast<const lattice3DIndex&>(lattice->Dims()).getLy();
            params.Nz = reinterpret_cast<const lattice3DIndex&>(lattice->Dims()).getLz();
            break;
        case 2:
            params.Nx = reinterpret_cast<const lattice2DIndex&>(lattice->Dims()).getLx();
            params.Ny = reinterpret_cast<const lattice2DIndex&>(lattice->Dims()).getLy();
            params.Nz = 1;
            break;
        default: std::runtime_error("Strange return value from lattice->getDim()"); break;
    }

    params.components = lattice->Dims().getComponents();
    params.MSqr = lattice->getMu0Sq();
    params.lambda = lattice->getg0();
    params.T = T;
    params.J=lattice->getSource();
    params.g = lattice->getGamma();

    binData.setParams(params);
}


std::string Observer::OPString() const {
    std::stringstream sstream;
    size_t elems = lattice->Dims().getElements();

    for (ushort j=0;j<lattice->Dims().getComponents();j++)
        sstream << " " << theData.PhiAvg[j]/elems << " " << theData.PiAvg[j]/elems;

    return sstream.str();
}


std::string Observer::EnergyString() const {
    std::stringstream sstream;

    size_t elems = lattice->Dims().getElements();
    sstream << getEnergy()/elems << " " << getEKin()/elems << " " << getEPot()/elems << " " << lattice->getinvlappisqr()/elems;
    
    return sstream.str();
}

std::string Observer::MomString() const {
    std::stringstream sstream;

    size_t elems = lattice->Dims().getElements();
    sstream << theData[GPHISQ]/elems << " " << theData[PHISQ]/elems << " " << theData[PISQ]/elems << " " << theData[PHIQ]/elems << " " << lattice->getGradSqPhiSq()/elems << " " << lattice->getGradSqPhiQrt()/elems;

    return sstream.str();
}

std::string Observer::PString() const {
    std::stringstream sstream;

    size_t elems = lattice->Dims().getElements();

    uint p=0;
    for (auto pit = PhiPt.begin();pit!=PhiPt.end();pit++) {
        sstream << pit->second[0].real()/elems << " " << pit->second[0].imag()/elems << " ";
        if (++p >= maxp) break;
    }

    p=0;
    for (auto pit = PiPt.begin();pit!=PiPt.end();pit++) {
        sstream << pit->second[0].real()/elems << " " << pit->second[0].imag()/elems << " ";
        if (++p >= maxp) break;
    }

    return sstream.str();
}


std::string Observer::EPString() const {
    std::stringstream sstream;

    size_t elems = lattice->Dims().getElements();

    for (auto eit = EPt.begin();eit!=EPt.end();eit++)
        sstream << eit->second.real()/elems << " " << eit->second.imag()/elems << " ";

    return sstream.str();
}


std::string Observer::CorrString() const {
    std::stringstream sstream;

    size_t elems = lattice->Dims().getElements();

    for (unsigned int z=0; z<(lattice->Dims().getL()+1)/2;z++)
        sstream << lattice->getSliceCorrelation(z)/elems << " ";

    return sstream.str();
}
