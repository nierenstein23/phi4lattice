#include <lattice_evolver.h>
#include <sstream>

#include <boost/log/trivial.hpp>
#include <boost/format.hpp>

#define TIMEDIFF 2

void LatticeEvolver::Thermalize( Tfloat T, Tfloat TMax, unsigned int tSteps, unsigned int sweeps, Tfloat dt, 
        std::string postfix)
{
    initObs(postfix);
    obs.setParams(T);

    BOOST_LOG_TRIVIAL(info) << boost::format("Starting %d thermalization sweeps") % sweeps;

    Tfloat deltaT = TMax/sweeps;
    for (uint i=0;i<sweeps;i++) {

        Tfloat ltime = 0;
        while (ltime < deltaT) {
            ltime += dt; 

            Leapfrog(tSteps, dt/tSteps, T, false);

            obs.update(LANG);
            obs.output(time + ltime);
        }
        time += ltime;
        lattice->randomizePi(T);
        BOOST_LOG_TRIVIAL(debug) << boost::format("Thermalization sweep %d of %d") % i % sweeps;
    }

    obs.finalize();
}


/**
 * @Synopsis  Real-time evolution 
 *
 * @Param T             Target temperature (only relevant for Langevin)
 * @Param TMax          Maximum time
 * @Param tSteps        timesteps per dt
 * @Param langevin      whether to introduce langevin damping or not
 * @Param dt            output interval
 * @Param postfix       postfix for output files
 */
void LatticeEvolver::RT( Tfloat T, Tfloat TMax, unsigned int tSteps, bool langevin, Tfloat dt, 
        std::string postfix) 
{

    // initialize observer object
    initObs(postfix);
    obs.setParams(T);

    int counter = 0;

    // evolve
    BOOST_LOG_TRIVIAL(info) << boost::format("Starting RT evolution from %.1f to %.1f") % time % TMax;

    while(std::abs(time) < std::abs(TMax)) {
        time += dt; counter++;
        Leapfrog(tSteps, dt/tSteps, T, langevin);

        if(tmi && (counter%tmifac == 0)) {
            std::stringstream prettyFname, prettyPname;
            prettyFname << outputLoc << "PrettyPhi" << postfix << "ID" << lattice->getSeed() << "_" << counter/tmifac;
            prettyPname << outputLoc << "PrettyPi" << postfix << "ID" << lattice->getSeed() << "_" << counter/tmifac;
            std::ofstream prettyFout(prettyFname.str().c_str());
            std::ofstream prettyPout(prettyPname.str().c_str());

            lattice->savePhiPretty(prettyFout);
            lattice->savePiPretty(prettyPout);
        }

        if (counter%50 == 0) {
            // if counter is multiple of 50, do full update for consistence checking purpose
            obs.update(FULL);
            BOOST_LOG_TRIVIAL(debug) << boost::format("Evolution progress at %.1f/%.1f (%i%%)") % time % TMax % std::ceil(time*100/TMax);
        } else {
            // if counter is not multiple of 50, just do update according to prescription
            obs.update((langevin?LANG:CLAS));
            BOOST_LOG_TRIVIAL(trace) << boost::format("Evolution progress at %.1f/%.1f (%i%%)") % time % TMax % std::ceil(time*100/TMax);
        }

        obs.output(time);
    }

    // write binaries etc.
    obs.finalize();
}


void LatticeEvolver::RTFunctional(TFOFTF Toft, TFOFTF Joft, Tfloat TMax, unsigned int tSteps, Tfloat dt, 
        std::string postfix) 
{

    // initialize observer object
    initObs(postfix);
    obs.setParams(Toft(0));

    int counter = 0;

    // evolve
    BOOST_LOG_TRIVIAL(info) << boost::format("Starting functional RT evolution from %.1f to %.1f") % time % TMax;
    while (time < TMax) {
        time += dt; counter++;
        LeapfrogFunctional(tSteps, dt/tSteps, time, Toft, Joft);

        if(tmi && (counter%tmifac == 0)) {
            std::stringstream prettyFname;
            prettyFname << outputLoc << "PrettyPhi" << postfix << "ID" << lattice->getSeed() << "_" << counter/tmifac;
            std::ofstream prettyFout(prettyFname.str().c_str());

            lattice->savePhiPretty(prettyFout);
        }

        if (counter%50 == 0) {
            BOOST_LOG_TRIVIAL(debug) << boost::format("Evolution progress at %.1f/%.1f (%i%%) @ T=%.5g, J=%.5g") % time % TMax % std::ceil(time*100/TMax) % Toft(time) % Joft(time);

            // if counter is multiple of 50, do full update for consistence checking purpose
            obs.update(FULL);
        } else {
            // if counter is not multiple of 50, just do update according to prescription
            obs.update(LANG);
        }

        obs.output(time);
    }

    obs.finalize();

}

inline int sign(Tfloat x) {
    return ((x>0) - (x<0));
}

void LatticeEvolver::Simulate(simline sl) {
    // save current time
    const double now = time;

    switch(sl.mode) {
        case CONSERVATIVE:
            lattice->setSource(sl.J);
            RTClassical(sl.T, sl.time + time, sl.tSteps, sl.dt, sl.desc); break;

        case LANGEVIN:
            lattice->setSource(sl.J);
            RTLangevin(sl.T, sl.time + time, sl.tSteps, sl.dt, sl.desc); break;

        case LINEAR:
            RTFunctional(
                    [=](Tfloat t) { return ((t-now)/sl.time)*(sl.Tfinal - sl.T) + sl.T; }, 
                    [=](Tfloat t) { return ((t-now)/sl.time)*(sl.Jfinal - sl.J) + sl.J; }, 
                    sl.time + time, sl.tSteps, sl.dt, sl.desc); 
            break;

        case ASYMPTOTIC:
            RTFunctional(
                    [=](Tfloat t) { return sl.Tfinal + 1./( 1./(sl.T - sl.Tfinal) + std::pow(sl.trate*(t-now), sl.texp)); },
                    [=](Tfloat t) { return sl.Jfinal + 1./( 1./(sl.J - sl.Jfinal) + sign(sl.J-sl.Jfinal)*std::pow(sl.jrate*(t-now), sl.jexp)); },
                    sl.time + time, sl.tSteps, sl.dt, sl.desc); 
            break;


        case RESET:
            resetTime(); break;

        case simmode::HMC:
            lattice->setSource(sl.J);
            this->HMC(sl.T, std::ceil(sl.time), -1.);
            resetTime(); break;

        default:
            break;
    }
}


/**
 * @Synopsis  Hybrid Monte Carlo evolution.
 *
 * @Param T             Target Temperature
 * @Param NTraj         Total number of trajectories
 * @Param NSteps        Steps per trajectory
 * @Param postfix       postfix for output files
 */
void LatticeEvolver::HMC(Tfloat T, unsigned int NTraj, Tfloat Ltraj, unsigned int NSteps, 
        std::string postfix) 
{
    initObs(postfix);
    obs.setParams(T);

    // calculate Ltraj if necessary by heuristic formula (valid near TC), use at least 10
    if (Ltraj < 0.0) Ltraj = std::max(10.0*std::pow((lattice->Dims().getL()/16.0), 1), 10.0);
    BOOST_LOG_TRIVIAL(info) << boost::format("HMC uses trajectories of length %.3f.") % Ltraj;

    // calculate NSteps if necessary by heuristic formula
    if (NSteps == 0) NSteps = std::ceil(Ltraj)*stepFac(23);
    BOOST_LOG_TRIVIAL(info) << boost::format("HMC uses %i steps per trajectory.") % NSteps;

    taus_state metro = taus_seed();
    float acc=0.0, count=0.0;
    time = 0.0;

    Tfloat Hi, Hf, DeltaH;

    // initial lattice backup and its energy
    lattice->backupPhi(); 

    lattice->randomizePi(T);
    obs.update(EUCL);

    obs.output(time);

    for (unsigned n=0;n<NTraj;n++) {
        // do a trajectory and save energies
        lattice->randomizePi(T); obs.update(EUCL);
        Hi = obs.getEnergy();

        Leapfrog(NSteps, Ltraj/NSteps); time += 1.0;

        obs.update(EUCL); Hf = obs.getEnergy(); DeltaH = Hf - Hi;

        if (do_metropolis_check(DeltaH/T, &acc, &count, &metro)) {
            // if the trajectory is accepted, replace the backup with 
            // the new configuration and set the new initial energy
            obs.backup();
            lattice->backupPhi();
        } else {
            // if the trajectory is rejected, restore the field
            // and leave the initial energy in place
            obs.reset();
            lattice->restorePhi();
        }

        obs.output(time);
        BOOST_LOG_TRIVIAL(debug) << boost::format("DeltaH/T = %.3f\tn = %d (%.1f%%)") % double(DeltaH/T) % n % double(acc/count*100.);
    };

    obs.finalize();
}


//====================================================================================================
/**
 * @Synopsis  Standard integrator with error in O(dt^2).
 *
 * @Param n number of steps
 * @Param dt time difference per step
 */
void LatticeEvolver::Leapfrog(unsigned n, Tfloat dt, Tfloat T, bool langevin) {
    lattice->stepPhi(dt/2);

    for (unsigned i=1;i<n;i++) {
        if (langevin) lattice->stepPiLangevin(T, dt);
        lattice->stepPi(dt);
        lattice->stepPhi(dt);
    }

    if (langevin) lattice->stepPiLangevin(T, dt);
    lattice->stepPi(dt);
    lattice->stepPhi(dt/2);
}


/**
 * @Synopsis  Leapfrog integrator which can vary T and J according to some functions
 *
 * @Param n number of steps
 * @Param dt time difference per step
 * @Param t0 starting time of evolution w.r.t. beginning of simulation
 * @Param Toft function returning the temperature T at a given time
 * @Param Joft function returning the source J at a given time
 */
void LatticeEvolver::LeapfrogFunctional(unsigned n, Tfloat dt, Tfloat t0, TFOFTF Toft, TFOFTF Joft) {
    lattice->stepPhi(dt/2);

    Tfloat simTime; // shorthand for the real time at leapfrog step n

    // assume that the system already is at T=Ti, start reducing T by dT already in first step
    for (unsigned i=1;i<n;i++) {

        simTime = t0+(i-0.5)*dt;

        lattice->setSource(Joft(simTime));

        lattice->stepPiLangevin(Toft(simTime), dt);
        lattice->stepPi(dt);
        lattice->stepPhi(dt);
    }

    simTime = t0+(n-0.5)*dt;

    lattice->setSource(Joft(simTime));

    lattice->stepPiLangevin(Toft(simTime), dt);
    lattice->stepPi(dt);
    lattice->stepPhi(dt/2);
}



/**
 * @Synopsis  Higher order integrator, reduces the coefficient of the O(dt^2) error term.
 *
 * @Param n     number of steps
 * @Param dt    time difference per step
 * @Param xi    integrator parameter; xi=0.5 <==> Leapfrog
 */
void LatticeEvolver::Omelyan(unsigned n, Tfloat dt, Tfloat xi) {
    lattice->stepPhi(xi*dt);

    for (unsigned i=1;i<n;i++) {
        lattice->stepPi(0.5*dt);
        lattice->stepPhi((1.0-2*xi)*dt);
        lattice->stepPi(0.5*dt);
        lattice->stepPhi(2*xi*dt);
    }

    lattice->stepPi(0.5*dt);
    lattice->stepPhi((1.0-2*xi)*dt);
    lattice->stepPi(0.5*dt);
    lattice->stepPhi(xi*dt);
}

//====================================================================================================

void LatticeEvolver::initObs(std::string postfix, std::string infix) {
    // generate a unique postfix for data files
    std::stringstream newPostfix;
    newPostfix << postfix << infix << lattice->getSeed();

    // initialize the observer
    obs.init(outputLoc, newPostfix.str());
}

unsigned int LatticeEvolver::stepFac(unsigned steps64) {
    return std::ceil(steps64*std::exp(0.25*std::log(float(lattice->Dims().getElements())/262144) + 0.25*std::log(lattice->Dims().getComponents())));
}
