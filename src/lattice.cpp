#include <lattice.h>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

// =============================== nD ===================================================================

void iLattice::doCopyFrom(const iLattice &src) {
    seedTaus(src.getSeed());

    lambda = src.getLambda();
    kappa = src.getKappa();
    gamma = src.getGamma();
    la = src.getlatticeSpacing();
    lmu0sq = src.getMu0Sq();
    lg0 = src.getg0();
    J = src.getSource();
}


/**
 * @Synopsis  randomizes and damps conjugate momenta according to Langevin equation
 *
 * @Param T	Temperature
 * @Param dt timestep
 */
void iLattice::stepPiRelaxLangevin(const Tfloat T, const Tfloat dt)
{
    Tfloat noiseAmp = SSQRT(std::abs(2*gamma*T*dt));
    Tfloat damping = gamma*std::abs(dt);

    pi->randomizeAndDamp(noiseAmp, damping, rnd_state);
}


void iLattice::stepPiDiffLangevin(const Tfloat T, const Tfloat dt)
{
    dampPi(-gamma*(std::abs(dt)));
    generateNoiseVector(SSQRT(2*gamma*T*std::abs(dt))); 
    addGradNoiseToPi();
}


std::vector<Tcomplex> iLattice::getFieldP(const int p, const bool on_phi)
{
    if (!zMomentumIsCurrent) { 
        doUpdateZMomentum(true); 
        doUpdateZMomentum(false); 
        convertPtoC();
        zMomentumIsCurrent=true; 
    }

    std::vector<Tcomplex> result(index->getComponents());
    for (ushort c=0;c<index->getComponents();c++)
        result[c] = on_phi?PhizMomentum[c][p]:PizMomentum[c][p];

    return result;
}


Tfloat iLattice::doGetSliceCorrelation(const size_t offset)
{
    if (!zMomentumIsCurrent) { 
        doUpdateZMomentum(true); 
        doUpdateZMomentum(false); 
        convertPtoC();
        zMomentumIsCurrent=true; 
    }

    return zCorrelation[0][offset];
}


void iLattice::convertPtoC()
{
    unsigned int lz = index->getL();

    // fill buffer with <phi(p)phi(-p)>
    std::vector<Tcomplex> buffer(lz/2+1);

    for (unsigned int i=0;i<lz/2+1;i++) {
        buffer[i] = SQR(std::abs(PhizMomentum[0][i]));
    }

    // fft
    fft_plan plan = fft_plan_c2r(lz, (ffTcomplex*)(buffer.data()), (ffTfloat*)(zCorrelation[0].data()), FFTW_ESTIMATE);
    fft_execute(plan);
    /*
    for (uint i=0; i<lz/2+1; i++)
        zCorrelation[0][i] = std::abs(buffer[i]);
    */
}


Tcomplex iLattice::getEP(const int p)
{ 
    if (!EzMomentumIsCurrent) { 
        doUpdateStressTensor();
        EzMomentumIsCurrent = true;
    }

    //return EzMomentum[p];
    return stressTensor[0][p];
}


/*
Tcomplex iLattice::getStress(const short component, const int p)
{ 
    if (!EzMomentumIsCurrent) { 
        doUpdateStressTensor();
        EzMomentumIsCurrent = true;
    }

    return stressTensor[component][p];
}
*/
std::vector<Tcomplex>& iLattice::getStressTensor(const short c)
{
    if (!EzMomentumIsCurrent) { 
        doUpdateStressTensor();
        EzMomentumIsCurrent = true;
    }

    return stressTensor[c];
}


void iLattice::randomizePi(const Tfloat T)
{
    switch(dmode) {
        case relax: setGaussPi(0, SSQRT(T)); break;
        case diff:
                    setGaussPi(0,0);                    // reset to 0
                    generateNoiseVector(SSQRT(T));      // generate vectorial noise
                    addGradNoiseToPi();                 // add divergence of vectorial noise to 0
    }
}


void iLattice::stepPi(const Tfloat dt)
{
    switch(dmode) {
        case relax: stepPiRelax(dt); break;
        case diff: 
                    stepPiDiff(dt); 
                    pi->addConst(-pi->sumComponents()/getDims().getPoints());  // eliminate q=0 mode
                    break;
        default: break;
    }
}


void iLattice::stepPiLangevin(const Tfloat T, const Tfloat dt) {
    switch(dmode) {
        case relax:
            stepPiRelaxLangevin(T, dt); break;
        case diff:
            stepPiDiffLangevin(T, dt); break;
    }
}


/**
 * @Synopsis  Generate dimensionless lagrange parameters kappa/lambda from field parameters.
 *
 * @Param a		lattice spacing
 * @Param mu0sq unrenormalized mass square parameter
 * @Param g0	coupling parameter
 */
void iLattice::generateLagrangeParameters(const Tfloat a, const Tfloat mu0sq, const Tfloat g0) {
    /*
       Tfloat am = a*a*mu0sq;

       Tfloat root1 = SSQRT(3)*SSQRT(3*SQR(am) + 48*am + 4*g0 + 192);
       Tfloat term2 = 3*SQR(am) + 48*am + 2*g0 + 192;

       Tfloat kappa1 = -(root1 + 3*am + 24.0)/(2.0 * g0);
       Tfloat kappa2 = (root1 - 3*am - 24.0)/(2.0 * g0);

       Tfloat lambda1 = (root1*am + 8*root1 + term2)/(4.0*g0);
       Tfloat lambda2 = (-1.0*root1*am - 8*root1 + term2)/(4.0*g0);

    // choose the pair with kappa > 0 for ferromagnetic coupling
    if (kappa1 >= 0.0) {
    kappa = kappa1; lambda = lambda1;
    } else {
    kappa = kappa2; lambda = lambda2;
    }
    */

    la = a; lmu0sq = mu0sq; lg0 = g0;
}


void iLattice::saveFields(std::ofstream &outStream) const {
    // create buffers (overhead if not on GPU)
    Tfloat *piOut, *phiOut; size_t nPoint = index->getPoints();
    piOut = new Tfloat[nPoint];
    phiOut = new Tfloat[nPoint];

    // write fields to those buffers
    pi->writeFieldsToBuffer(piOut);
    phi->writeFieldsToBuffer(phiOut);

    // dump buffers to outStream
    outStream.write(reinterpret_cast<char*>(piOut), nPoint*sizeof(Tfloat));
    outStream.write(reinterpret_cast<char*>(phiOut), nPoint*sizeof(Tfloat));

    delete[] piOut;
    delete[] phiOut;
}


void iLattice::loadFields(std::ifstream &inStream) {
    // create buffers (overhead if not on GPU)
    Tfloat *piOut, *phiOut; size_t nPoint = index->getPoints();
    piOut = new Tfloat[nPoint];
    phiOut = new Tfloat[nPoint];

    // fill those buffers from inStream
    inStream.read(reinterpret_cast<char*>(piOut), nPoint*sizeof(Tfloat));
    inStream.read(reinterpret_cast<char*>(phiOut), nPoint*sizeof(Tfloat));

    // read in fields from buffers
    pi->readFieldsFromBuffer(piOut);
    phi->readFieldsFromBuffer(phiOut);

    delete[] piOut;
    delete[] phiOut;
}


void iLattice::init()
{
    generateLagrangeParameters(1.0, -1.0, 1.0);
    setSource(0);
    setGamma(0);

    auto comp = index->getComponents();
    auto l = index->getL();
    auto fft_size = l/2 + 1;

    PhizMomentum = new std::vector<Tcomplex>[comp];
    PizMomentum = new std::vector<Tcomplex>[comp];
    zCorrelation = new std::vector<Tfloat>[comp];

    const short stcomp = (dim==2)?8:9;
    stressTensor = new std::vector<Tcomplex>[stcomp];
    for (int k=0;k<stcomp;k++) stressTensor[k].resize(l/2 + 1);

    for (ushort c=0;c<comp;c++) {
        PhizMomentum[c].resize(fft_size);
        PizMomentum[c].resize(fft_size);
        zCorrelation[c].resize(l);
    }

    EzMomentum.resize(fft_size);
    TkBuffer.resize(fft_size);
    zMomentumIsCurrent = false;
}


iLattice::~iLattice()
{
    delete[] PhizMomentum;
    delete[] PizMomentum;
    delete[] zCorrelation;

    BOOST_LOG_TRIVIAL(trace) << "iLattice destructor";
}


void iLattice::copyFrom(const iLattice &src)
{
    seed = src.seed;
    dim = src.dim;
    lambda = src.lambda;
    kappa = src.kappa;
    gamma = src.gamma;
    la = src.la;
    lmu0sq = src.lmu0sq;
    lg0 = src.lg0;
    J = src.J;

    // derived classes do the rest
    doCopyFrom(src);
}


// =============================== 3D ===================================================================

/**
 * @Synopsis  Copy lattice from src, including fields.
 *
 * @Param src
 */
void iLattice3D::doCopyFrom(const iLattice &src) {
    BOOST_LOG_TRIVIAL(trace) << "doCopyFrom";

    if (src.getDim() == 3) {

        const iLattice3D *csrc = dynamic_cast<const iLattice3D*>(&src);

        phi->copyFrom( *(csrc->phi) );
        pi->copyFrom( *(csrc->pi) );

        indexer = csrc->indexer;
        index = &indexer;

    } else throw std::runtime_error("incompatible lattice dimensions");
}


void iLattice3D::doDumpInfo(const Tfloat T, const std::string outputLocation) const {
    boost::filesystem::ofstream infoStream(boost::filesystem::path(outputLocation + "InfoID" + boost::lexical_cast<std::string>(getSeed()) + ".txt"), std::ios::out);

    infoStream 
        << "#Nc=" << indexer.getComponents() << std::endl
        << "#MSqr=" << getMu0Sq() << std::endl
        << "#lambda=" << getg0() << std::endl
        << "#T=" << T << std::endl
        << "#J=" << getSource() << std::endl
        << "#Nx=" << indexer.getLx() << std::endl
        << "#Ny=" << indexer.getLy() << std::endl
        << "#Nz=" << indexer.getLz() << std::endl;

    infoStream.close();
}


void iLattice3D::doSavePhiPretty(std::ofstream &outStream) const {
    Tfloat *phiOut; size_t nPoint = indexer.getPoints();
    phiOut = new Tfloat[nPoint];
    phi->writeFieldsToBuffer(phiOut);

    for (uint z=0;z<indexer.getLz();z++) {
        for (uint y=0;y<indexer.getLy();y++) {
            for (uint x=0;x<indexer.getLx();x++) {
                outStream << z << "\t" << y << "\t" << x << "\t" << phiOut[indexer.index(x, y, z)] << std::endl;
            }
        }
    }

    delete phiOut;
}


iLattice3D::iLattice3D(const size_t lx, const size_t ly, const size_t lz, const ushort comp) 
    : indexer(lx, ly, lz, comp) 
{ 
    index = &indexer; dim=3;
    init(); 
    BOOST_LOG_TRIVIAL(debug) << "3D Lattice constructor";
}


// =============================== 2D ===================================================================
void iLattice2D::doCopyFrom(const iLattice &src) {
    BOOST_LOG_TRIVIAL(trace) << "doCopyFrom";

    if (src.getDim() == 2) {

        const iLattice2D *csrc = dynamic_cast<const iLattice2D*>(&src);

        phi->copyFrom( *(csrc->phi) );
        pi->copyFrom( *(csrc->pi) );

        indexer = csrc->indexer;
        index = &indexer;

    } else 
        throw std::runtime_error("incompatible lattice dimensions");
}


void iLattice2D::doDumpInfo(const Tfloat T, const std::string outputLocation) const {
    boost::filesystem::ofstream infoStream(boost::filesystem::path(outputLocation + "InfoID" + boost::lexical_cast<std::string>(getSeed()) + ".txt"), std::ios::out);

    infoStream 
        << "#Nc=" << indexer.getComponents() << std::endl
        << "#MSqr=" << getMu0Sq() << std::endl
        << "#lambda=" << getg0() << std::endl
        << "#T=" << T << std::endl
        << "#J=" << getSource() << std::endl
        << "#Nx=" << indexer.getLx() << std::endl
        << "#Ny=" << indexer.getLy() << std::endl;

    infoStream.close();
}


void iLattice2D::doSavePhiPretty( std::ofstream &outStream ) const {
    Tfloat *phiOut; size_t nPoint = indexer.getPoints();
    phiOut = new Tfloat[nPoint];
    phi->writeFieldsToBuffer(phiOut);

    for (uint y=0;y<indexer.getLy();y++) {
        for (uint x=0;x<indexer.getLx();x++) {
            outStream << phiOut[indexer.index(x, y)] << "\t";
        }

        outStream << std::endl;
    }
}

void iLattice2D::doSavePiPretty( std::ofstream &outStream ) const {
    Tfloat *phiOut; size_t nPoint = indexer.getPoints();
    phiOut = new Tfloat[nPoint];
    pi->writeFieldsToBuffer(phiOut);

    for (uint y=0;y<indexer.getLy();y++) {
        for (uint x=0;x<indexer.getLx();x++) {
            outStream << phiOut[indexer.index(x, y)] << "\t";
        }

        outStream << std::endl;
    }
}


iLattice2D::iLattice2D(const size_t lx, const size_t ly, const ushort comp) 
    : indexer(lx, ly, comp) 
{ 
    index = &indexer; dim=2;
    init(); 
    BOOST_LOG_TRIVIAL(debug) << "2D Lattice constructor";
}

