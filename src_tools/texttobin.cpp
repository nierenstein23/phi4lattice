#include <boost/program_options.hpp>
#include <stdio.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <datafile.h>

#include <boost/archive/binary_oarchive.hpp>

// change to double if necessary
typedef float floattype;

namespace po = boost::program_options;

int main(int argc, char const *argv[]) {

    std::string inputDirName;
    std::string outputDirName = "";
    std::string outputFileName = "";
    std::string ID, postfix;

    size_t iStart, iEnd;
    bool verbose=false;

    po::options_description desc("Options for the Bin-O-Conv 2000");
    desc.add_options()
    ("help,h", "produce this help message")
    ("DirIn,d", po::value<std::string>(&inputDirName)->default_value("OUTPUT"), "Directory to look into")
    ("DirOut,o", po::value<std::string>(&outputDirName), "Output directory")
    ("OutputFile,f", po::value<std::string>(&outputFileName), "Output file suffix")
    ("postfix,p", po::value<std::string>(&postfix), "postfix")
    ("ID,i", po::value<std::string>(&ID), "Input ID")
    ("iStart,s", po::value<size_t>(&iStart)->default_value(0), "where to start")
    ("iEnd,e", po::value<size_t>(&iEnd)->default_value(5000), "where to end")
    ("verbose,v", "increased verbosity, useful for very large data sets");
            
    po::variables_map vm;
    po::store(po::parse_command_line( argc, argv, desc), vm);
    po::notify(vm);

    // evaluate parsed stuff
    //check if help is needed
    if( vm.count( "help" ) ) {  
        std::cerr << desc << std::endl; exit(0);
    }

    if (vm.count( "verbose" )) verbose = true;
	if( iEnd < iStart ) { std::cerr << "Please give correct limits for the inverval!" << std::endl; return 1; }

    /**
     * A DataSeries object is prepared; while reading in the files, values are appended to the data series.
     */
    paramset parameters;
    DataSeries data;


    /* ======================= read in Info-File ==============================*/
    {
        // temporary string variable which will hold lines from input files
        std::string inputLine;

        std::stringstream infoname; 
        infoname << inputDirName << "/Info" << ID << ".txt";
        std::ifstream infostream;

        infostream.open(infoname.str().c_str());
        std::cerr << "Processing " << infoname.str().c_str() << std::endl;
        if (!infostream.good()) return -1; //exit on failure

        while(getline(infostream, inputLine)) {
            std::istringstream is_line(inputLine);
            std::string key;
        
            if( getline(is_line, key, '=') ) {
                std::string value;
                if( std::getline(is_line, value) ) getline(is_line, value);

                if( key.find("T") != std::string::npos ) parameters.T = std::atof(value.c_str());
                if( key.find("J") != std::string::npos ) parameters.J = std::atof(value.c_str());
                if( key.find("Nx") != std::string::npos ) parameters.Nx = std::atoi(value.c_str());
                if( key.find("Ny") != std::string::npos ) parameters.Ny = std::atoi(value.c_str());
                if( key.find("Nz") != std::string::npos ) parameters.Nz = std::atoi(value.c_str());
                if( key.find("Nc") != std::string::npos ) parameters.components = std::atoi(value.c_str());
                if( key.find("MSqr") != std::string::npos ) parameters.MSqr = std::atoi(value.c_str());
                if( key.find("lambda") != std::string::npos ) parameters.lambda = std::atoi(value.c_str());
            }
        }

        infostream.close();
    }

    data.init(postfix);
    data.setParams(parameters);

/* ======================= read in the text files ==============================*/
    std::ifstream OPStream, EnergyStream, MomStream;
    std::stringstream OPname, EnergyName, MomName;

    OPname << inputDirName << "/OrderParameter" << postfix << ID;
    EnergyName << inputDirName << "/Energy" << postfix << ID;
    MomName << inputDirName << "/Moments" << postfix << ID;
    if (verbose)
        std::cerr << "Trying to open "<< OPname.str().c_str()
       << ", " << EnergyName.str()
       << ", " << MomName.str()
       << std::endl;

    OPStream.open(OPname.str().c_str());
    EnergyStream.open(EnergyName.str().c_str());
    MomStream.open(MomName.str().c_str());

    std::string OPLine, EnergyLine, MomLine;
    std::stringstream OPValues, EnergyValues, MomValues;

    size_t number=0, vol = parameters.Nx*parameters.Ny*parameters.Nz;
    Tfloat tmp, tmp2, time;

    // complexity due to possible NoC>1; 
    // I need both fields and conjugate momenta as independent 
    // data to construct the spectral function later
    dataset theData;

    while(OPStream.good() && MomStream.good() && EnergyStream.good() && (number <= iEnd)) {
        if (verbose) std::cerr << "Reading line " << number << "\r";

        getline(OPStream, OPLine);
        getline(EnergyStream, EnergyLine);
        getline(MomStream,MomLine);
        
        // skip if uninteresting, prethermalized state
        if(number < iStart) {number++; continue;}

        // data line evaluation
        if(!OPLine.empty() && !EnergyLine.empty() && !MomLine.empty()) {
            OPValues.str(OPLine);
            EnergyValues.str(EnergyLine);
            MomValues.str(MomLine);

            // first value is the timestamp
            OPValues >> time;

            // check if lines are synchronized
            {
                EnergyValues >> tmp;
                MomValues >> tmp2;
                if (tmp2 != tmp || tmp != time) break;
            }

            // read in the order parameter/momentum averages
            theData.PhiAvg.resize(parameters.components);
            theData.PiAvg.resize(parameters.components);

            for (int i=0; i<parameters.components; i++) {
                // first comes the field component
                OPValues >> tmp;
                theData.PhiAvg[i] = tmp * vol;

                // second value is the momentum, which is of much iterest to us
                OPValues >> tmp;
                theData.PiAvg[i] = tmp * vol;
            }

            // read in the energies
            {
                EnergyValues >> tmp;
                theData[E] = tmp * vol;

                EnergyValues >> tmp;
                theData[EK] = tmp * vol;

                EnergyValues >> tmp;
                theData[EP] = tmp * vol;

                EnergyValues >> tmp;
                theData[C1] = tmp * vol;

                EnergyValues >> tmp;
                theData[C2] = tmp * vol;
            }

            // read in the field power averages
            {
                MomValues >> tmp;
                theData[GPHISQ] = tmp * vol;

                MomValues >> tmp;
                theData[PHISQ] = tmp * vol;

                MomValues >> tmp;
                theData[PISQ] = tmp * vol;

                MomValues >> tmp;
                theData[PHIQ] = tmp * vol;
            }

            // append theData to data series
            data.append(time, theData);

            // clear the lines
            OPValues.str(std::string());
            EnergyValues.str(std::string());
            MomValues.str(std::string());
            OPValues.clear();
            EnergyValues.clear();
            MomValues.clear();

        } else {
            std::cerr << std::endl << "empty line at " << number << std::endl;
            break;
        }   

        number++;
    }

    if (verbose) std::cerr << std::endl << "read " << number - 1 << " lines!" << std::endl;
    OPStream.close();
    EnergyStream.close();
    MomStream.close();

/* ======================== everythin read in - now write! =======================*/

    std::stringstream outFileName;
    outFileName << outputDirName << "/BIN" << postfix << ID << outputFileName;
    std::ofstream outstream(outFileName.str().c_str(), std::ios::out | std::ios::binary);

    boost::archive::binary_oarchive boa(outstream);
    boa << data;

    outstream.close();
    return 0; 
}
