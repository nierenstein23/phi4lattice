#include <datafile.h>
#include <classpecfunc.h>
#include <iostream>
#include <observer.h>
#include <logger.h>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/vector.hpp>

namespace po = boost::program_options;
namespace fs = boost::filesystem;
namespace ar = boost::archive;
namespace lo = logger;

typedef lo::log_level_t loglevel;
// global verbosity level
loglevel GLOG_LEVEL;

int main(int argc, char const *argv[]) {
    // Initialize IO, options
    std::string outputFileName;
    std::string outputDir;
    std::string inputFileName;
    std::string ID;

    int timepoints; 
    float maxTime;

    short poffset;

    po::options_description desc("Options for the Evaluator3000 DynamicEdition");
    desc.add_options()
    ("help,h", "produce this help message")
    ("poffset,p", po::value<short>(&poffset)->default_value(0), "Binaries contain multiple momentum magnitudes in a map; use this flag to choose a place in that map.")
    ("infile,i", po::value<std::string>(&inputFileName), "binary file to use")
    ("OutputFile,o", po::value<std::string>(&outputFileName), "Give output file")
    ("OutputDir,O", po::value<std::string>(&outputDir)->default_value("tmp"), "Output directory; location where to save the FT in binary")
    ("maxTime", po::value<float>(&maxTime), "maximum time")
    ("loglevel,l",po::value<loglevel>(&GLOG_LEVEL)->default_value(logger::LOG_ERROR), "specify log level")
    ("verbose,v", "increased verbosity, useful for very large output sets");
            
    po::variables_map vm;
    po::store(po::parse_command_line( argc, argv, desc), vm);
    po::notify(vm);

    // read in parsed stuff
    if(vm.count( "help" )) {std::cerr << desc << std::endl; exit(0);}
    if (vm.count( "verbose" )) GLOG_LEVEL = logger::LOG_INFO; // compatibility to -v flag

    // check if input is regular file
    fs::path inFile(inputFileName);
    if (!fs::is_regular_file(inFile)) {
        log<lo::LOG_CRITICAL>("No valid input file specified. Aborting");
        return -1;
    }


    std::vector< std::map< Tfloat, std::vector<Tcomplex> > > PhiP, PiP;
    paramset params; 
    Tfloat timestep; size_t iInt; ushort NoC;
    /* ======================= read in binary ==============================*/
    {
        fs::ifstream inStream(inFile, std::ios::in | std::ios::binary);
        ar::binary_iarchive bia(inStream);

        // read input archive to data series object
        size_t garbage;
        try {
            bia >> garbage >> PhiP >> PiP >> params >> timestep;
        } catch (std::exception e) {
            log<lo::LOG_CRITICAL>("\nException while trying to read archive: %s") % e.what();
            return -2;
        }
        // now that we have info about the rest, we can allocate memory to arrays!
        iInt = PhiP.size();
        NoC = params.components;
   
        // if no max time was supplied, set it to half the available time interval
        if (!vm.count("maxTime")) maxTime = iInt/2 * timestep;
        timepoints = std::ceil(maxTime/timestep);

        log<lo::LOG_INFO>("Done reading in Data. Now calculating %lu time points...") % timepoints;
        log<lo::LOG_DEBUG>("iInt = %lu") % iInt;;
    }

    // check if specified poffset is valid, abort if not
    if (poffset >= PhiP[0].size()) { log<lo::LOG_CRITICAL>("Specified offset too large. Only %i values for p in range. Aborting.") % PhiP[0].size(); exit(-1); }

    ClassicalSpectralFunction SpecFunc(timepoints, timestep);

    std::vector<Tcomplex> *PhiPV, *PiPV;
    PhiPV = new std::vector<Tcomplex>[NoC];
    PiPV = new std::vector<Tcomplex>[NoC];

    for (ushort k=0;k<NoC;k++) {
        PhiPV[k].resize(iInt);
        PiPV[k].resize(iInt);
    }

    for (size_t i=0;i<iInt;i++) {
        // ugly hack
        auto phielem = PhiP[i].begin(); for (short u=0;u<poffset;u++) phielem++;
        auto pielem = PiP[i].begin(); for (short u=0;u<poffset;u++) pielem++;

        // sorry...
        for (short k=0;k<NoC;k++) {
            PhiPV[k][i] = phielem->second[k];
            PiPV[k][i] = pielem->second[k];
        }
    }

    SpecFunc.readTData(PhiPV, PiPV, params);
    SpecFunc.createFFT();

    delete[] PhiPV;
    delete[] PiPV;

/*=================================OUTPUT=================================*/
    // Write spectral functions of time to output file
    if (vm.count("OutputFile")) {
        std::ofstream outStream(outputFileName.c_str(), std::ios::out);
        log<lo::LOG_INFO>("Writing textfile %s...") % outputFileName;

        for (int w=0;w<SpecFunc.getFSize();w++) {
            outStream << w*SpecFunc.getFreqStep() << " " << SpecFunc.getSFatFreqStep(w) << std::endl;
        }

        // two empty lines s.t. gnuplot can distinguish blocks
        outStream << std::endl;
        outStream << std::endl;

        for (int t=0;t<SpecFunc.getTSize();t++) {
            outStream << t*SpecFunc.getTimestep() << " " << SpecFunc.getSFatTimeStep(t) << std::endl;
        }

        outStream.close();
    }

    // create output file name
    std::string binOutName(inputFileName);

    // cut stuff
    size_t IDpos = binOutName.find("ID");
    if ( IDpos != std::string::npos ) binOutName.erase( 0, IDpos );

    size_t suffixpos = binOutName.find_last_of(".");
    if ( suffixpos != std::string::npos ) binOutName.erase( suffixpos, binOutName.length()-1);

    // open binary output location
    {
        std::stringstream binOutLoc;
        binOutLoc << outputDir << "/" << binOutName;
        std::ofstream outStream(binOutLoc.str().c_str());

        boost::archive::text_oarchive oa(outStream);
        oa << SpecFunc;

        outStream.close();
    }

    log<lo::LOG_INFO>("Finished evaluating %s.") % inputFileName;
}
