#include <classpecfunc.h>
#include <iostream>

/**
 * @Synopsis  read the time data from a data series object, optionally include some damping (superfluous)
 *
 * @Param data  data series object to analyze
 * @Param tau   damping parameters
 */
void ClassicalSpectralFunction::readTData(const DataSeries &data, double tau) {
    if (data.size() < getTSize()) {throw std::out_of_range ("Input data not long enough!");}
    paramset params = data.getParams(); const ushort NoC = params.components;

    std::vector<Tfloat> Phi[NoC], Pi[NoC]; 

    for (int i=0; i<NoC; i++) {
        Phi[i] = data.stripData(PHI, i);
        Pi[i] = data.stripData(PI, i);
    }

    readTData(Phi, Pi, params);
}


/**
 * @Synopsis  read Data from array of Vectors (array entries corresponding to components)
 *
 * @Param phi,pi   arrays of vectors (interchangeable)
 * @Param params   parameters
 */
void ClassicalSpectralFunction::readTData(std::vector<Tfloat> *Phi, std::vector<Tfloat> *Pi, paramset params) {
    ushort NoC = params.components;
    size_t minsize = -1; // uintmax
    // check data sufficiency, extract minimum size
    for (ushort i=0;i<NoC;i++) 
        if( std::min(Phi[i].size(), Pi[i].size()) < minsize) minsize = std::min(Phi[i].size(), Pi[i].size());

    if (minsize < getTSize())
        throw std::out_of_range ("Input data not long enough!");

    Tfloat weight, factor = -1.0/(params.Nx*params.Ny*params.Nz*params.T);
    for (int t=0;t<getTSize();t++) {
	int Theta = minsize - t;
	weight = 0.5/Theta;

	float tmp=0;
        #pragma omp simd collapse(2)
	for (int i=0;i<Theta;i++)
	    for (int k=0;k<params.components;k++)
		tmp += Pi[k][t+i]*Phi[k][i] - Pi[k][i]*Phi[k][t+i];

        SFTime[t] = factor*weight*tmp;
    }

    hasData = true;
}


void ClassicalSpectralFunction::readTData(std::vector<Tcomplex> *Phi, std::vector<Tcomplex> *Pi, paramset params) {
    ushort NoC = params.components;
    size_t minsize = -1; // uintmax
    // check data sufficiency, extract minimum size
    for (ushort i=0;i<NoC;i++) 
        if( std::min(Phi[i].size(), Pi[i].size()) < minsize) minsize = std::min(Phi[i].size(), Pi[i].size());

    if (minsize < getTSize())
        throw std::out_of_range ("Input data not long enough!");

    Tfloat weight, factor = -1.0/(params.Nx*params.Ny*params.Nz*params.T);
    for (int t=0;t<getTSize();t++) {
	int Theta = minsize - t;
	weight = 1.0/Theta;

	Tcomplex tmp=0;
        #pragma omp simd collapse(2)
	for (int i=0;i<Theta;i++)
	    for (int k=0;k<params.components;k++)
		tmp += std::conj(Pi[k][t+i])*Phi[k][i];

        // average over forward and backward time direction (gives 2*real()/2 = real())
        SFTime[t] = (factor*weight*tmp).real();
    }

    hasData = true;
}


/*
void ClassicalSpectralFunction::readTData( std::vector<Tfloat> *field, paramset params) {
    params.components = 1;

    size_t datasize = field->size();
    std::vector<Tfloat> field_dot(datasize);
    Tfloat dt = getTimestep();

    std::transform(field->begin()+2, field->end(), field->begin(), field_dot.begin()+1, [=](Tfloat x, Tfloat y){ return (x-y)/(2*dt); });
    
    // replicate first and last point
    size_t fillpos1 = (std::rand() % (datasize-2)) + 1,
           fillpos2 = (std::rand() % (datasize-2)) + 1;

    field_dot[0] = 0;//field_dot[fillpos1];
    field_dot[datasize-1] = 0;//field_dot[fillpos2];

    // let the old methods do the rest
    readTData(field, &field_dot, params);
}
*/


/**
 * @Synopsis  Create the fourier transform of the time data with a simple plan.
 */
void ClassicalSpectralFunction::createFFT() {

	size_t fullsize = 2*getTSize() -1;

        // simd allocating
	fftw_complex *fullfreq = fftw_alloc_complex(getTSize());
	double *fulltimep = fftw_alloc_real(fullsize);

	std::vector<double> fulltime(fulltimep, (fulltimep+fullsize));

	std::reverse(SFTime.begin(), SFTime.end());
        std::transform(SFTime.begin(), SFTime.end(), SFTime.begin(), [](double x) { return -x;});
	fulltime.insert(fulltime.begin(), SFTime.begin(), SFTime.end());
	std::reverse(SFTime.begin(), SFTime.end());
        std::transform(SFTime.begin(), SFTime.end(), SFTime.begin(), [](double x) { return -x;});
	fulltime.insert(fulltime.begin()+getTSize(), SFTime.begin()+1, SFTime.end());

	fftw_plan plan = fftw_plan_dft_r2c_1d(fullsize, &fulltime[0], &fullfreq[0], FFTW_ESTIMATE);
	fftw_execute(plan); 
	hasFFT = true;

	for (size_t idx=0;idx<getFSize();idx++)
		SFFreq[idx] = std::abs(fullfreq[idx][1]);

	fftw_free(fullfreq);
	fftw_free(fulltimep);
}
