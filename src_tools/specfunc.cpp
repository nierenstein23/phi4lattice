#include <iostream>
#include <specfunc.h>
#include <stdexcept>
#include <cmath>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#define CUBE(x) (x*x*x)

/**
 * @Synopsis  Constructor of a Spectral Function object. Needs the size and time resolution as minimal information.
 *
 * @Param theSize	size of the dataset
 * @Param theTimestep	timestep between datapoints
 */
SpectralFunction::SpectralFunction(const int theSize, const float theTimestep) : size(theSize), timestep(theTimestep) {
	SFTime.resize(size);
	SFFreq.resize(size);

	hasData = false;
	hasFFT = false;
}


float SpectralFunction::getMaxTime() const { return size*timestep; }
float SpectralFunction::getTimestep() const { return timestep; }
float SpectralFunction::getFreqStep() const { return 1.0/getMaxTime(); }
int SpectralFunction::getTSize() const { return size; }
int SpectralFunction::getFSize() const { return size; }

float SpectralFunction::getSFatTimeStep(const int step) const {
	if (step > size) throw std::out_of_range("Desired timestep not in range!");
	return SFTime[step];
}

float SpectralFunction::getSFatFreqStep(const int step) const {
	if (step > getFSize()) throw std::out_of_range("Desired timestep not in range!");
	return SFFreq[step];
}

float SpectralFunction::getSFRealPart(const int step) const {
	if (step > getFSize()) throw std::out_of_range("Desired timestep not in range!");
	return 0.0;
}

float SpectralFunction::getSFImagPart(const int step) const {
	if (step > getFSize()) throw std::out_of_range("Desired timestep not in range!");
	return SFFreq[step];
}
