#include <fstream>
#include <iostream>
#include <boost/program_options.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/archive_exception.hpp>

#include <vector>
#include <classpecfunc.h>
#include <util.h>
#include <logger.h>

namespace po = boost::program_options;
namespace fs = boost::filesystem;
namespace lo = logger;

typedef logger::log_level_t loglevel;
// global verbosity level
loglevel GLOG_LEVEL;

/**
 * @Synopsis  Main routine of the combinator for spectral functions
 *
 * @Param argc,argv[] parsed by boost:program_options
 *
 * @Returns an exit code
 */
int main(int argc, char const *argv[]) {
    // Initialize IO, options
    std::string outputFileName;
    std::string inputDirName;
    std::string ID;
    int nbins, nTbin;

    po::options_description desc("Options for the Combinator2018 DynamicEdition");
    desc.add_options()
    ("help,h", "produce this help message")
    ("InputDir,i", po::value<std::string>(&inputDirName)->default_value("tmp"), "input directory to iterate over")
    ("OutputFile,o", po::value<std::string>(&outputFileName)->default_value("combined_output.dat"), "Output file")
    ("nBins,n", po::value<int>(&nbins)->default_value(0), "Maximum number of bins")
    ("nTbin,t", po::value<int>(&nTbin)->default_value(1), "Consecutive time/freq points to average")
    ("loglevel,l",po::value<loglevel>(&GLOG_LEVEL)->default_value(logger::LOG_ERROR), "specify log level")
    ("verbose,v", "DEPRECATED: increased verbosity, useful for very large data sets");
                        
    po::variables_map vm;
    po::store(po::parse_command_line( argc, argv, desc), vm);
    po::notify(vm);

    // read in parsed stuff
    if(vm.count( "help" )) {std::cerr << desc << std::endl; exit(0);}
    if (vm.count( "verbose" )) GLOG_LEVEL = logger::LOG_INFO; // compatibility to -v flag

    // boost foreach loop over contents of input directory
    fs::path inputDir(inputDirName);
    fs::directory_iterator cit(inputDir), it(inputDir), eod;

    // find number of file nodes in directory:
    int filecount;
    filecount = std::count_if(cit, fs::directory_iterator(), static_cast<bool(*)(const fs::path&)>(fs::is_regular_file));
    log<lo::LOG_INFO>("There appear to be %i files in the specified directory %s") % filecount % inputDirName;

    std::vector<ClassicalSpectralFunction> SF(filecount);

    bool first = true;
    int counter=0;
    BOOST_FOREACH(fs::path const &p, std::make_pair(it, eod)) {
        // check for file integrity (kinda...)
                
        log<lo::LOG_DEBUG>("\t loop no. %i") % (counter+1);
        if (fs::is_regular_file(p)) {
            fs::ifstream inStream(p);
            boost::archive::text_iarchive ia(inStream);

            log<lo::LOG_DEBUG>("Reading in file %s...");
            try {
                ia >> SF[counter];
            } catch (boost::archive::archive_exception &ex) {
                log<lo::LOG_CRITICAL>("Reading archive %s failed: %s\n Aborting.") % p.c_str() % ex.what();
                return -1;
            }

            // if it is the first item, use it as comparator
            log<lo::LOG_DEBUG>("Comparing file %i") % (counter+1);
            if ( SF[0].getTSize() != SF[counter].getTSize() || SF[0].getTimestep() != SF[counter].getTimestep()) {
                log<lo::LOG_CRITICAL>("Size mismatch! Stopping at file %s!") % p.c_str();
                log<lo::LOG_DEBUG>("SFCons[0].getTSize() = %i, SFCons[counter].getTSize() = ") % SF[0].getTSize() % SF[counter].getTSize();
                return -2;
            }

            counter++;
            if (first) first = false;

            inStream.close();
        }
    }

    log<lo::LOG_INFO>("read in %i files!") % counter;

    // ============================Combine=============================== //        
        
    /**
     * To combine the results and get some error estimation, I have to apply 
     * ye olde statistics. Might be sensible to keep it simple though.
     *
     */
    // if no bin parameter was given, make bins with 1 point each (i.e. no binning)
    if (nbins == 0) nbins = counter;
    int timepoints = SF[0].getTSize()/nTbin;
    int freqpoints = SF[0].getFSize()/nTbin;

    log<lo::LOG_INFO>("Starting analysis of %i time points...") % timepoints;

    // use Tfloat2 types to store both value and error
    std::vector<Tfloat2> specFuncT(timepoints), specFuncW(timepoints);

    for (int t=0;t<timepoints;t++) {
        //Tfloat meantc=0.0, meantl=0.0;
        Tfloat tmp[counter];

        for (int k=0;k<counter;k++) {
            tmp[k] = 0.0;

            for (int l=0;l<nTbin;l++) {
                tmp[k] += SF[k].getSFatTimeStep(nTbin*t+l)/nTbin;
            }
        }

        // jackknife
        binning(tmp, specFuncT[t].x, specFuncT[t].y, 1, counter);
    }

    log<lo::LOG_INFO>("Finished time analysis. Starting analysis of %i freq points...") % freqpoints;

    for (int w=0;w<freqpoints;w++) {
        Tfloat tmp[counter];

        for (int k=0;k<counter;k++) {
            tmp[k] = 0.0;

            for (int l=0;l<nTbin;l++) {
                tmp[k] += SF[k].getSFatFreqStep(nTbin*w+l)/nTbin;
            }
        }

        binning(tmp, specFuncW[w].x, specFuncW[w].y, greaterDivisor(Tfloat(counter)/nbins, counter), counter);
    }

    log<lo::LOG_INFO>("Finished freq analysis.");

    // ============================OUTPUT================================ //

    // Write spectral functions of time to output file
    std::ofstream outStream(outputFileName.c_str(), std::ios::out);
    log<lo::LOG_DEBUG>("Opened output file %s...") % outputFileName;

    // frequency stepping
    Tfloat wstep = nTbin*SF[0].getFreqStep();
    Tfloat tstep = nTbin*SF[0].getTimestep();

    log<lo::LOG_DEBUG>("Freq. step: %f, time step: %f") % wstep % tstep;

    outStream << "# combination of " << counter << " files" << std::endl;
    for (int w=0;w<freqpoints;w++) {
        outStream << w*wstep << " " << specFuncW[w].x << " " << specFuncW[w].y << std::endl;
    }
        
    // gnuplot needs two empty lines for indexing
    outStream << std::endl;
    outStream << std::endl;

    for (int t=0;t<timepoints;t++) {
        outStream << t*tstep << " " << specFuncT[t].x << " " << specFuncT[t].y << std::endl;
    }

    outStream.close();
    return 0;
}
