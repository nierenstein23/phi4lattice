#include <mhrw.h>
#include <numeric>
#include <cmath>
#include <logger.h>

namespace lo = logger;

/**
 * @Synopsis The actual reweighting method. 
 * Calling this starts the process. Parameters such as verbosity level and maximum number of iterations etc. either 
 * have been set or are at their (sensible) default values.  
 */
void MultiHistogram::Reweigh() {
    int NumberOfDataFiles = energies.size();

    // special (stupid) case
    if (NumberOfDataFiles == 1) {
        log<lo::LOG_INFO>("only one data file, weight should be sufficiently close to 1");
        weights.clear();
        weights.push_back(1.0);
        return;
    }

    // allocate and initialize containers ==========================================================
    log<lo::LOG_INFO>("Allocating memory...");
    std::vector<Tprec> 
        fWeights(NumberOfDataFiles, -std::log(float(NumberOfDataFiles)));

    weights.assign(NumberOfDataFiles, 1.0/float(NumberOfDataFiles));

    // function which is to be minimized, containing the deviation from self-consistency
    std::vector<Tprec> function(NumberOfEquations());

    // jacobian
    Tprec **Jacobian = new Tprec*[NumberOfEquations()];
    for(int k=0;k<NumberOfEquations();k++)
        Jacobian[k]=new Tprec[NumberOfVariables()];

    // coefficient matrix for the update step
    Tprec **CoefficientMatrix = new Tprec*[NumberOfVariables()];
    for(int k=0;k<NumberOfVariables();k++)
        CoefficientMatrix[k]=new Tprec[NumberOfEquations()];

    log<lo::LOG_INFO>("... done!");
    // allocated and initialize dcontainers ==========================================================

    
    /* DataIncrement = 2^(floor(log2(min_element(lenghts)/512))) 
     * ==> DataInc is power of two, but the smallest dataset still has at least MinElements elements in calculation
     */
    int DataIncrement = std::pow(2, std::floor(std::log2( *(std::min_element(lengths.begin(), lengths.end()))/MinElements)));
    if (DataIncrement < 1) DataIncrement = 1;

    // deviation control 
    Tprec GLOBALDEVIATION = std::numeric_limits<Tprec>::max(), // initialize deviation with maximum possible value, it's safe.
          OLDDEVIATION; // backup deviation for finer increment control

    // output progress to cmdline
    std::stringstream message; 
    log<lo::LOG_INFO>("Starting to compute weights.");

    bool justDecreased=true; // knows if we just decreased the data increment
    int counter=0; 
    while ((GLOBALDEVIATION>maxDeviation && counter<MaxIterations) || DataIncrement>1) {
        counter++;

        // step-wise increase data point density near convergence; full dataset is overkill for rough estimate
        // do not increase in first round, but when deviations are flat or some max count is reached
        if ( (((2.0*GLOBALDEVIATION) > OLDDEVIATION) || GLOBALDEVIATION < maxDeviation) && !justDecreased)
        {
            DataIncrement = std::max(1, DataIncrement/Divisor);
            justDecreased = true;
        }
        else 
            justDecreased = false;

        // update deviation function and its jacobian with current weights

        log<lo::LOG_INFO>("Updating deviation function and jacobian with increment %i...") % DataIncrement;
        bool success=updateDeviation(Jacobian, function, DataIncrement);
        if (!success) exit(0); // exit on failure

        log<lo::LOG_DEBUG>("forcing norm = 1");
        enforceNorm(Jacobian, function);

        // apply some strange factors to Jacobian
        log<lo::LOG_DEBUG>("apply 1/r weighting");
        inverseWeighting(Jacobian, function);

        // update deviation
        OLDDEVIATION = GLOBALDEVIATION;
        GLOBALDEVIATION = std::abs(*std::max_element(function.begin(), function.end()));

        log<lo::LOG_INFO>("Maximum deviation in run no. %i: %e") % counter % GLOBALDEVIATION;

        // SETUP SYSTEM OF LINEAR EQUATIONS TO DETERMINE UPDATE CoefficientMatrix.Update = RHS //
        for(int k=0;k<NumberOfVariables();k++){
            for (int l=0;l<NumberOfVariables();l++) {
                CoefficientMatrix[k][l] = 0;

                for (int m=0;m<NumberOfEquations();m++)
                    CoefficientMatrix[k][l] += Jacobian[m][l] * Jacobian[m][k];
            }

            CoefficientMatrix[k][NumberOfVariables()] = 0.0;
            for (int l=0;l<NumberOfEquations();l++)
                CoefficientMatrix[k][NumberOfVariables()] += Jacobian[l][k]*function[l];

        }

        // SOLVE SET OF LINEAR EQUATIONS //
        std::vector<Tprec> Update(NumberOfVariables());
        Update=SolveLinearEquation(CoefficientMatrix, NumberOfVariables());
            
        // PERFORM UPDATE //
        for(int k=0;k<NumberOfVariables();k++){
            fWeights[k]-=Update[k];
            weights[k]=exp(fWeights[k]);
        }

        std::stringstream buffer("");
        for(int k=0;k<energies.size();k++)
            buffer << weights[k] << "\t"; 

        log<lo::LOG_INFO>("Resulting weights: %s") % buffer.str();

    } // end while

    log<lo::LOG_INFO>("Optimization of weights completed.");


    // free stuff 
    for(int k=0;k<NumberOfEquations();k++)
        delete[] Jacobian[k];

    for(int k=0;k<NumberOfVariables();k++)
        delete[] CoefficientMatrix[k];

    delete[] Jacobian;
    delete[] CoefficientMatrix;
};


/**
 * @Synopsis  Calculate the current deviation vector from self-consistency as well as its jacobian.
 *
 * @Param Jacobian pointer to the jacobian matrix
 * @Param function reference to the deviation vector
 * @Param DataIncrement increment over the data sets (only use a subset of N/DataIncrement values, thereby faster)
 *
 * @Returns false if there was a numerical error, otherwise true
 */
bool MultiHistogram::updateDeviation(Tprec **Jacobian, std::vector<Tprec>& function, int DataIncrement) const {
    bool success = true;

    // prepare 
    for (int i=0;i<weights.size();i++) {
        function[i] = -weights[i];

        for (int j=0;j<weights.size();j++) 
            Jacobian[i][j]=-1.0*(i==j);
    }

    // calculate actual current deviation
    #pragma omp parallel for schedule(static)
    for (int i=0;i<energies.size();i++) {
        auto data_i = energies[i].second;
        for (int a=0;a<lengths[i];a+=DataIncrement) {
            
            // get current energy deviation
            Tprec EnergyArgument = data_i[a] - Emean;

            // update function_k and jacobian df_l / dr_l
            for (int k=0;k<energies.size();k++) {
                Tprec Denominator = 0.0;

                #pragma omp simd
                for (int j=0;j<energies.size();j++) 
                    Denominator += (lengths[j]/DataIncrement) *std::exp(-(1.0/(energies[j].first) - 1.0/(energies[k].first))*EnergyArgument) /weights[j];

                // exit on error
                if (std::isnan(Denominator)) { 
                    log<lo::LOG_CRITICAL>("TOO LARGE DENOMINATOR!");
                    success = false;
                }

                #pragma omp atomic
                function[k] += 1.0/Denominator;
                for (int l=0;l<energies.size();l++) {
                    Jacobian[k][l] += 1.0/SQR(weights[l])
                        *(lengths[l]/float(DataIncrement))
                        *std::exp(-(1.0/(energies[l].first) - 1.0/(energies[k].first))*EnergyArgument)
                        /SQR(Denominator);

                }
            }
        }
    }
    return success;
};


/**
 * @Synopsis  appends the normalization constraint to deviation vector and jacobian
 *
 * @Param Jacobian pointer to the jacobian matrix
 * @Param function reference to the deviation vector
 */
void MultiHistogram::enforceNorm(Tprec **Jacobian, std::vector<Tprec>& function) const {
        // compute norm of weights; without normalization condition, 0 is an attractor
        Tprec Norm = std::accumulate(weights.begin(), weights.end(), 0.0);

        function[NumberOfEquations()-1] = log(Norm); // last entry should enforce Norm = 1
        for (int l=0;l<NumberOfVariables();l++)
            Jacobian[NumberOfEquations()-1][l] = 1.0/Norm; // respective entries in jacobian
};


/**
 * @Synopsis  does something fancy with weights I do not really understand, but it helps.
 *
 * @Param Jacobian pointer to the jacobian matrix
 * @Param function reference to the deviation vector
 */
void MultiHistogram::inverseWeighting(Tprec **Jacobian, std::vector<Tprec>& function) const {
        // "1/r weighting and option to use f-weights"
        for (int k=0;k<NumberOfEquations();k++) {
            for (int l=0;l<NumberOfVariables();l++) {
                if (k<NumberOfVariables())
                    Jacobian[k][l] *= weights[l]/weights[k];
                else
                    Jacobian[k][l] *= weights[l];

                if (l==k) Jacobian[k][l] -= function[k]/(weights[k]*weights[l]);
            }

            if(k<NumberOfVariables()) function[k] /= weights[k];
        }
};


/**
 * @Synopsis  reap the harvest and generate reweighting factors for all data points.
 *
 * @Param T target temperature
 *
 * @Returns a vector of vectors of weights. 
 */
std::vector< std::vector<Tprec> > MultiHistogram::getWeights(Tprec T) const {
    // initialize the result vector
    std::vector< std::vector<Tprec> > TWeights(NumberOfDataFiles());

    #pragma omp parallel for schedule(static)
    for (int i=0;i<TWeights.size();i++) {
        // shorthands, optimized out
        std::vector<Tfloat> Ei = energies[i].second;
        size_t Ni = lengths[i];

        // resize TWeights
        TWeights[i].resize(Ni);

        // iterate over data points
        for (int j=0;j<lengths[i];j++) {
            Tprec Denominator = 0.0, DeltaE = Ei[j] - Emean;

            for (int k=0;k<NumberOfDataFiles();k++) {
                Tprec DeltaBeta = 1.0/energies[k].first - 1.0/T;

                Denominator += (lengths[k]/weights[k])*std::exp( -DeltaBeta*DeltaE ); // the actual reweighting
            }

            if (std::isinf(Denominator))
                TWeights[i][j] = 0.0;
            else 
                TWeights[i][j] = 1.0/Denominator;

        }

        // impose normalization condition
        Tprec normFac = Ni/std::accumulate(TWeights[i].begin(), TWeights[i].end(), 0.0);
        std::transform(TWeights[i].begin(), TWeights[i].end(), TWeights[i].begin(), [normFac](Tprec x){ return normFac*x; });
    }

    return TWeights;
}


/**
 * @Synopsis  read actual data points into the object.
 * Expects a vector of pairs of a float type and a vector of a float type.
 * One pair for each independent simulation (i.e. "data file"), with the first element 
 * containing the temperature at runtime, the second the energies of the collected datapoints.
 * The order of the elements is crucial: the result vector will have the same order.
 *
 * @Param input the input vector 
 */
void MultiHistogram::readData( std::vector<std::pair<Tfloat, std::vector<Tfloat> > >& input ) {
    // clear potentially data holding structures
    energies.clear(); weights.clear(); lengths.clear(); 
    Emean = 0; totalPoints = 0;
    
    // iterate over input vector, extract energies at temperatures
    for (int i=0;i<input.size();i++) {
        std::vector<Tfloat> &input_energies = input[i].second;

        energies.push_back(input[i]);
        lengths.push_back(input_energies.size());

        Emean += std::accumulate(input_energies.begin(), input_energies.end(), 0.0);
        totalPoints += input_energies.size();
    }

    // do not err around if no points were scanned
    Emean = totalPoints>0?Emean/totalPoints:0;
};


/**
 * @Synopsis  Helper for solving linear equations
 *
 * @Param A coefficient matrix
 * @Param nvar size of problem
 *
 * @Returns the solution of the system
 */
std::vector<Tprec> MultiHistogram::SolveLinearEquation(Tprec **A, int nvar) {
    int n = nvar;
    
    for (int i=0; i<n; i++) {
        // Search for maximum in this column
        Tprec maxEl = std::abs(A[i][i]);
        int maxRow = i;
        for (int k=i+1; k<n; k++) {
            if (std::abs(A[k][i]) > maxEl) {
                maxEl = std::abs(A[k][i]);
                maxRow = k;
            }
        }
        
        // Swap maximum row with current row (column by column)
        for (int k=i; k<n+1;k++) {
            Tprec tmp = A[maxRow][k];
            A[maxRow][k] = A[i][k];
            A[i][k] = tmp;
        }
        
        // Make all rows below this one 0 in current column
        for (int k=i+1; k<n; k++) {
            Tprec c = -A[k][i]/A[i][i];
            for (int j=i; j<n+1; j++) {
                if (i==j) {
                    A[k][j] = 0;
                } else {
                    A[k][j] += c * A[i][j];
                }
            }
        }
    }
    
    // Solve equation Ax=b for an upper triangular matrix A
    std::vector<Tprec> x(n);
    for (int i=n-1; i>=0; i--) {
        x[i] = A[i][n]/A[i][i];
        for (int k=i-1;k>=0; k--) {
            A[k][n] -= A[k][i] * x[i];
        }
    }
    return x;
};
