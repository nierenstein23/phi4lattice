#include <datafile.h>
#include <classpecfunc.h>
#include <iostream>
#include <logger.h>
#include <numeric>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

namespace po = boost::program_options;
namespace fs = boost::filesystem;
namespace ar = boost::archive;
namespace lo = logger;

typedef logger::log_level_t loglevel;
// global verbosity level
loglevel GLOG_LEVEL;


int main(int argc, char const *argv[]) {
    // Initialize IO, options
    std::string outputFileName;
    std::string outputDir;
    std::string inputFileName;
    std::string ID;

    int timepoints, Ssample; 
    float maxTime;

    po::options_description desc("Options for the Evaluator3000 DynamicEdition");
    desc.add_options()
    ("help,h", "produce this help message")
    ("infile,i", po::value<std::string>(&inputFileName), "binary file to use")
    ("OutputFile,o", po::value<std::string>(&outputFileName), "Give output file")
    ("OutputDir,O", po::value<std::string>(&outputDir)->default_value("tmp"), "Output directory; location where to save the FT in binary")
    ("maxTime", po::value<float>(&maxTime), "maximum time")
    ("sampling,S", po::value<int>(&Ssample)->default_value(1), "only use every Sth value for the final spec func")
    ("loglevel,l",po::value<loglevel>(&GLOG_LEVEL)->default_value(logger::LOG_ERROR), "specify log level")
    ("verbose,v", "DEPRECATED: increased verbosity, useful for very large data sets");
            
    po::variables_map vm;
    po::store(po::parse_command_line( argc, argv, desc), vm);
    po::notify(vm);

    // read in parsed stuff
    if(vm.count( "help" )) {std::cerr << desc << std::endl; exit(0);}
    if (vm.count( "verbose" )) GLOG_LEVEL = logger::LOG_INFO; // compatibility to -v flag

    // check if input is regular file
    fs::path inFile(inputFileName);
    if (!fs::is_regular_file(inFile)) {
        log<lo::LOG_CRITICAL>("No valid input file specified. Aborting");
        return -1;
    }

    /* ======================= read in binary ==============================*/
    DataSeries data;
    fs::ifstream inStream(inFile, std::ios::in | std::ios::binary);
    ar::binary_iarchive bia(inStream);

    // read input archive to data series object
    try {
        bia >> data;
    } catch (std::exception e) {
        log<lo::LOG_CRITICAL>("\nException while trying to read archive: %s") % e.what();
        return -2;
    }
    // now that we have info about the rest, we can allocate memory to arrays!
    const size_t iInt = data.size();
    const ushort NoC = data.getParams().components;
    const Tfloat dt = data.timestep();
    // const size_t size = iInt*sizeof(float);
   
    // if no max time was supplied, set it to half the available time interval
    if (!vm.count("maxTime")) maxTime = iInt/2 * dt;
    timepoints = std::ceil(maxTime/dt);

    log<lo::LOG_INFO>("Done reading in Data. Now calculating %i time points...") % timepoints;

    std::vector<Tfloat> edata = data.stripData(E),
                        edot(edata.size());
    size_t datasize = edata.size();

    std::vector<Tfloat> sampled_edata(0), sampled_edot(0);

    // normalize e-data
    Tfloat mean = std::accumulate(edata.begin(), edata.end(), 0.0)/datasize;
    std::transform(edata.begin(), edata.end(), edata.begin(), [=](Tfloat x){ return x-mean; });

    // fill edot
    std::transform(edata.begin()+2, edata.end(), edata.begin(), edot.begin()+1, [=](Tfloat x, Tfloat y){ return (x-y)/(2.0*dt); });
    edot[0] = 0;
    edot[datasize-1] = 0;

    // give <edot^2> for scale
    //std::cout << "<edot^2> == " << std::inner_product(edot.begin(), edot.end(), edot.begin(), 0.0)/datasize << std::endl;
    // plot edot for fuck's sake
    //for (Tfloat edott : edot)
        //std::cerr << edott << std::endl;

    // take samples to decorrelate edot from edata
    size_t sampled_size = datasize / Ssample; if ((datasize%Ssample)==0) sampled_size--;
    int offset = Ssample/2;
    for (size_t k=0;k<sampled_size;k++) {
        sampled_edata.push_back(edata[offset + Ssample*k]);
        sampled_edot.push_back(edot[offset + Ssample*k]);
    }

    sampled_edata.shrink_to_fit();
    sampled_edot.shrink_to_fit();

    ClassicalSpectralFunction SpecFunc(timepoints/Ssample, Ssample*dt);
    SpecFunc.readTData(&sampled_edata, &sampled_edot, data.getParams());
    SpecFunc.createFFT();

/*=================================OUTPUT=================================*/
    // Write spectral functions of time to output file
    if (vm.count("OutputFile")) {
        std::ofstream outStream(outputFileName.c_str(), std::ios::out);
        log<lo::LOG_INFO>("Writing text file %s.") % outputFileName;

        outStream << std::scientific;

        for (int w=0;w<SpecFunc.getFSize();w++) {
            outStream << w*SpecFunc.getFreqStep() << " " << SpecFunc.getSFatFreqStep(w) << std::endl;
        }

        // two empty lines s.t. gnuplot can distinguish blocks
        outStream << std::endl;
        outStream << std::endl;

        for (int t=0;t<SpecFunc.getTSize();t++) {
            outStream << t*SpecFunc.getTimestep() << " " << SpecFunc.getSFatTimeStep(t) << std::endl;
        }

        outStream.close();
    }

    // create output file name
    std::string binOutName(inputFileName);

    // cut stuff
    size_t IDpos = binOutName.find("ID");
    if ( IDpos != std::string::npos ) binOutName.erase( 0, IDpos );

    size_t suffixpos = binOutName.find_last_of(".");
    if ( suffixpos != std::string::npos ) binOutName.erase( suffixpos, binOutName.length()-1);

    // open binary output location
    {
        std::stringstream binOutLoc;
        binOutLoc << outputDir << "/" << binOutName;
        std::ofstream outStream(binOutLoc.str().c_str());

        boost::archive::text_oarchive oa(outStream);
        oa << SpecFunc;

        outStream.close();
    }

    log<lo::LOG_QUIET>("Finished evaluating %s!") % inputFileName;

}
