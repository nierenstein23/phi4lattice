#include <datafile.h>
#include <vector>
#include <iostream>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/archive/binary_iarchive.hpp>

namespace po = boost::program_options;
namespace fs = boost::filesystem;
namespace ar = boost::archive;

/**
 * @Synopsis  This short program is meant to convert a binary output file into the classic text files.
 *
 * @Param argc, argv[] handled by boost program options
 *
 * @Returns 0 on success
 */
int main(int argc, char *argv[]) {
    std::string 
        inputFileName, 
        pinputFileName, 
        outputDirName, 
        ID;
    bool verbose=false;

    bool hasi,hasp;

    po::options_description desc("Parameters for the bin-to-text converter");

    desc.add_options()
        ("help,h", "produce this help message")
        ("input,i", po::value<std::string>(&inputFileName), "binary input file")
        ("pinput,p", po::value<std::string>(&pinputFileName), "binary finite-p-mode input file")
        ("DirOut,o", po::value<std::string>(&outputDirName)->default_value("OUTPUT/"), "Directory to write into")
        ("ID,I", po::value<std::string>(&ID), "File postfix + ID to append to file names.")
        ("verbose,v", "increased verbosity, useful for very large data sets");

    po::variables_map vm;
    po::store(po::parse_command_line( argc, argv, desc), vm);
    po::notify(vm);

    // check if help/verbosity needed
    if(vm.count("help")) { std::cerr << desc << std::endl; exit(0); }
    if (vm.count( "verbose" )) verbose = true;

    // check if stuff is there
    hasi=(vm.count("input"));
    hasp=(vm.count("pinput"));

    // I would like my data here
    DataSeries data;

    std::vector< std::map< Tfloat, std::vector<Tcomplex> > > PhiP, PiP; // store finite P
    size_t phipsize; 
    Tfloat ptimestep;
    paramset phipparams;

    fs::path inFile(inputFileName);
    fs::path pinFile(pinputFileName);

    if (hasi) {

        // check if input is regular file
        if (!fs::is_regular_file(inFile)) {
            std::cerr << "No valid input file specified. Aborting." << std::endl;
            return -1;
        }

        // open input to archive
        fs::ifstream inStream(inFile);
        ar::binary_iarchive bia(inStream);

        // read input archive to data series object
        try {
            bia >> data;
        } catch (std::exception e) {
            std::cerr << "Exception while trying to read archive: " << e.what() << std::endl;
            return -2;
        }

        if (verbose) std::cerr << "Sucessfully read archive." << std::endl;

        // declare necessary file streams
        fs::ofstream infoStream, OPStream, EnergyStream, MomStream;

        if (verbose) std::cerr << "Writing Info file...";
        {
            // open info file
            infoStream.open(fs::path(outputDirName + "Info" + ID + ".txt"));
            // print to info file and close
            data.getParams().printToFile(infoStream);
            infoStream.close();
        }
        if (verbose) std::cerr << " done!" << std::endl;

        {
            if (verbose) std::cerr << "starting to write " << data.size() << " lines into data files" << std::endl;
            // open other files
            OPStream.open(fs::path(outputDirName + "OrderParameter" + ID));
            EnergyStream.open(fs::path(outputDirName + "Energy" + ID));
            MomStream.open(fs::path(outputDirName + "Moments" + ID));

            size_t vol = data.getParams().Nx * data.getParams().Ny * data.getParams().Nz;

            // write stuff
            for (size_t line=0;line<data.size();line++) {
                float time = data.time(line);
                
                {
                    OPStream << time;
                    for (ushort c=0;c<data.getParams().components;c++)
                        OPStream << " " << data(PHI, line, c)/vol << " " << data(PI, line, c)/vol;
                    OPStream << std::endl;
                }

                EnergyStream << time 
                    << " " << data(E, line)/vol
                    << " " << data(EK, line)/vol
                    << " " << data(EP, line)/vol
                    << " " << data(C1, line)/vol
                    << " " << data(C2, line)/vol
                    << std::endl;

                MomStream << time
                    << " " << data(GPHISQ, line)/vol
                    << " " << data(PHISQ, line)/vol
                    << " " << data(PISQ, line)/vol
                    << " " << data(PHIQ, line)/vol
                    << std::endl;
            }

            // close streams;
            OPStream.close();
            EnergyStream.close();
            MomStream.close();
        }

            if (verbose) std::cerr << std::endl << "all done." << std::endl;
    }

    if (hasp) {

        if (!fs::is_regular_file(pinFile)) {
            std::cerr << "No valid finite-p file specified. Aborting." << std::endl;
            return -1;
        }

        // open input to archive
        fs::ifstream pinStream(pinFile);
        ar::binary_iarchive pbia(pinStream);

        // read input archive to data series object
        try {
            pbia >> phipsize;
            pbia >> PhiP;
            pbia >> PiP;
            pbia >> phipparams >> ptimestep;
        } catch (std::exception e) {
            std::cerr << "Exception while trying to read finite-p archive: " << e.what() << std::endl;
            return -2;
        }
        
        if (verbose) std::cerr << "Sucessfully read p-archive." << std::endl;

        // declare necessary file stream
        fs::ofstream infoStream, PStream;

        if (hasi) {
            if (data.getParams() != phipparams) {
                std::cerr << "Conflicting simulation parameters found. Something is fishy." << std::endl; 
                return -5;
            }

            if (data.size() != phipsize) {
                std::cerr << "Length of archived data does not match. Something is fishy." << std::endl;
                return -6;
            }

            if (data.timestep() != ptimestep) {
                std::cerr << "Timesteps do not match. Something is fishy." << std::endl;
                return -6;
            }
        } 
        else 
        {
            if (verbose) std::cerr << "Writing Info file...";

            // open info file
            infoStream.open(fs::path(outputDirName + "Info" + ID + ".txt"));
            // print to info file and close
            phipparams.printToFile(infoStream);
            infoStream.close();

            if (verbose) std::cerr << " done!" << std::endl;
        }
       
        if (verbose) std::cerr << "starting to write " << phipsize << " lines into data files" << std::endl;
        // open other files
        PStream.open(fs::path(outputDirName + "FiniteP" + ID));

        size_t vol = data.getParams().Nx * data.getParams().Ny * data.getParams().Nz;
        // write stuff
        for (size_t line=0;line<phipsize;line++) {
            Tfloat time = (line+1)*ptimestep;
            
            PStream << time;
            std::map < Tfloat, std::vector<Tcomplex> > 
                PhiPt = PhiP[line], 
                PiPt = PiP[line];

            for (auto phipit = PhiPt.begin(); phipit != PhiPt.end(); phipit++)
                PStream << " " << phipit->second[0].real()/vol << " " << phipit->second[0].imag()/vol;

            for (auto pipit = PiPt.begin(); pipit != PiPt.end(); pipit++)
                PStream << " " << pipit->second[0].real()/vol << " " << pipit->second[0].imag()/vol;

            PStream << " " << std::endl;
        }

        PStream.close();

        if (verbose) std::cerr << std::endl << "all done." << std::endl;
    }

    return 0;
}
