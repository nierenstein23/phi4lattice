#include <bosspecfunc.h>
#include <numeric>
#include <iostream>

/**
 * @Synopsis  read the time data from a data series object, optionally include some damping (superfluous)
 * TODO: add some parameter which controls the data field that is read?!
 *
 * @Param data  data series object to analyze
 * @Param tau   damping parameters
 */
void BosonicSpectralFunction::readTData(const DataSeries &data, double tau) {
    if (data.size() < getTSize()) {throw std::out_of_range ("Input data not long enough!");}
    paramset params = data.getParams(); const ushort NoC = 1; T=params.T;

    std::vector<Tfloat> Obs = data.stripData(E);

    readTData(&Obs, params);
}


/**
 * @Synopsis  read Data from array of Vectors (array entries corresponding to components)
 *
 * @Param phi,pi   arrays of vectors (interchangeable)
 * @Param params   parameters
 */
void BosonicSpectralFunction::readTData(std::vector<Tfloat> *obs, paramset params) {
    ushort NoC = 1; // TODO: Dynamic!!
    size_t minsize = -1; // uintmax
    // check data sufficiency, extract minimum size
    for (ushort i=0;i<NoC;i++) if( obs[i].size() < minsize) minsize = obs[i].size();
    if (minsize < getTSize()) throw std::out_of_range ("Input data not long enough!");

    Tfloat mean[NoC];
    for (ushort i=0;i<NoC;i++) mean[i] = std::accumulate(obs[i].begin(), obs[i].begin()+getTSize()-1, 0.0)/getTSize();
    std::cout << "mean: " << mean[0] << std::endl;

    Tfloat weight, factor = -1.0/(params.Nx*params.Ny*params.Nz);
    for (int t=0;t<getTSize();t++) {
	int Theta = minsize - t;
	weight = 1.0/Theta;

	float tmp=0;
        #pragma omp simd collapse(2)
	for (int i=0;i<Theta;i++)
	    for (int k=0;k<NoC;k++)
		tmp += (obs[k][t+i] - mean[k])*(obs[k][i] - mean[k]);

        SFTime[t] = factor*weight*tmp;
    }

    T = params.T;
    hasData = true;
}

/*
void BosonicSpectralFunction::readTData(std::vector<Tcomplex> *obs, paramset params) {
    ushort NoC = 1; // TODO: Dynamic!!
    size_t minsize = -1; // uintmax
    // check data sufficiency, extract minimum size
    for (ushort i=0;i<NoC;i++) if( obs[i].size() < minsize) minsize = obs[i].size();
    if (minsize < getTSize()) throw std::out_of_range ("Input data not long enough!");

    Tcomplex mean[NoC];
    for (ushort i=0;i<NoC;i++) mean[i] = std::accumulate(obs[i].begin(), obs[i].end(), 0.0)/obs[i].size();


    Tfloat weight, factor = -1.0/(params.Nx*params.Ny*params.Nz*params.T);
    for (int t=0;t<getTSize();t++) {
	int Theta = minsize - t;
	weight = 1.0/Theta;

	Tcomplex tmp=0;
        #pragma omp simd collapse(2)
	for (int i=0;i<Theta;i++)
	    for (int k=0;k<NoC;k++)
		tmp += std::conj(obs[k][t+i])*obs[k][i] - SQR(std::abs(mean[k]));

        // average over forward and backward time direction (gives 2*real()/2 = real())
        SFTime[t] = (factor*weight*tmp).real();
    }

    T = params.T;
    hasData = true;
}
*/


double boseeinsteindist(double T, double w) { return 1.0/(1+std::exp(w/T)); }
/**
 * @Synopsis  Create the fourier transform of the time data with a simple plan.
 */
void BosonicSpectralFunction::createFFT() {
    if (hasFFT) return; // folding with bose einstein is not idempotent

    size_t fullsize = 2*getTSize() -1;

    // simd allocating
    fftw_complex *fullfreq = fftw_alloc_complex(getTSize());
    double *fulltimep = fftw_alloc_real(fullsize);

    std::vector<double> fulltime(fulltimep, (fulltimep+fullsize));

    std::reverse(SFTime.begin(), SFTime.end());
    std::transform(SFTime.begin(), SFTime.end(), SFTime.begin(), [](double x) { return -x;});
    fulltime.insert(fulltime.begin(), SFTime.begin(), SFTime.end());
    std::reverse(SFTime.begin(), SFTime.end());
    std::transform(SFTime.begin(), SFTime.end(), SFTime.begin(), [](double x) { return -x;});
    fulltime.insert(fulltime.begin()+getTSize(), SFTime.begin()+1, SFTime.end());

    fftw_plan plan = fftw_plan_dft_r2c_1d(fullsize, &fulltime[0], &fullfreq[0], FFTW_ESTIMATE);
    fftw_execute(plan); 

    // multiply by bose-einstein distr
    for (size_t n=0;n<getTSize();n++) {
        Tcomplex tmp = Tcomplex(fullfreq[n][0], fullfreq[n][1]);
        fullfreq[n][0] = tmp.imag()*(0.5+boseeinsteindist(T, n*getFreqStep()));
        fullfreq[n][1] = -tmp.real()*(0.5+boseeinsteindist(T, n*getFreqStep()));
    }

    // copy to SFFreq
    for (size_t idx=0;idx<getFSize();idx++) SFFreq[idx] = std::abs(fullfreq[idx][1]);

    // FFT backwards TODO: check if fullsize argument is correct
    plan = fftw_plan_dft_c2r_1d(fullsize, &fullfreq[0], &fulltime[0], FFTW_ESTIMATE);
    fftw_execute(plan); 

    // copy to SFTime
    for (size_t idx=0;idx<getTSize();idx++) SFTime[idx] = fulltime[idx+getTSize()];

    hasFFT = true;
    fftw_free(fullfreq);
    fftw_free(fulltimep);
}
