#include <datafile.h>
#include <util.h>
#include <mhrw.h>
#include <logger.h>

#include <vector>
#include <numeric>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <iostream>

namespace po = boost::program_options;
namespace fs = boost::filesystem;
namespace ar = boost::archive;
namespace lo = logger;

typedef logger::log_level_t loglevel;
// global verbosity level
loglevel GLOG_LEVEL;

int main(int argc, char const *argv[]) {
    /* ======================= command-line parameters ==============================*/
    std::string inputDirName;
    
    Tfloat thermTime;
    std::vector<Tfloat> temps, deltaJs;
    int minbin, nbin;

    int MaxIterations, MinElements, Divisor;

    po::options_description desc("Options for the Evaluator3000:");
    desc.add_options()
    ("help,h", "produce this help message")
    ("indir,i", po::value<std::string>(&inputDirName), "binary file to use")
    ("thermTime,T", po::value<Tfloat>(&thermTime), "thermalization time that is to be cut of; zero if unspecified.")
    ("temps,t", po::value<std::string>()->default_value("0"), "temps: temperatures for reweightings. Beware: If it does not contain 0, only reweighted values calculated!")
    ("deltaJs,j", po::value<std::string>()->default_value("0"), "deltaJs: field shifts for reweightings, see above.")
    ("minbin", po::value<int>(&minbin)->default_value(1000), "Minimum width of bins, even with short autocorrelation time")
    ("loglevel,l",po::value<loglevel>(&GLOG_LEVEL)->default_value(logger::LOG_ERROR), "specify log level")
    ("MaxIterations,M",po::value<int>(&MaxIterations)->default_value(64), "Maximum number of rw iterations")
    ("MinElements,m",po::value<int>(&MinElements)->default_value(512), "Minimum number data elements in first iteration")
    ("Divisor,d",po::value<int>(&Divisor)->default_value(4), "Divisor by which to decrease data stride")
    ("verbose,v", "increased verbosity, useful for very large data sets");

    po::variables_map vm;
    po::store(po::command_line_parser( argc, argv).options(desc).run(), vm);
    po::notify(vm);

    // read in parsed stuff
    if(vm.count( "help" )) {std::cerr << desc << std::endl; exit(0);}
    if (vm.count( "verbose" )) GLOG_LEVEL = logger::LOG_INFO; // compatibility to -v flag

    // parse reweight strings to vectors
    {
        log<lo::LOG_INFO>("Parsing reweighting parameters...");
        std::istringstream tiss(vm["temps"].as<std::string>());
        std::istringstream jiss(vm["deltaJs"].as<std::string>());

        std::copy(std::istream_iterator<Tfloat>(tiss), std::istream_iterator<Tfloat>(),std::back_inserter(temps));
        std::copy(std::istream_iterator<Tfloat>(jiss), std::istream_iterator<Tfloat>(),std::back_inserter(deltaJs));
    }
    /* ====================== /command-line parameters ==============================*/


    /* ======================= read in binaries ==============================*/
    std::vector<DataSeries> datavec(0); paramset params;

    {
        fs::path indir(inputDirName);
        // break if no dir
        if (!fs::is_directory(indir)) { log<lo::LOG_CRITICAL>("FATAL: No valid input directory provided. Exiting."); exit(-1);}
        
        log<lo::LOG_INFO>("Scanning input directory...");

        fs::directory_iterator indirit(indir), eod;

        BOOST_FOREACH(fs::path const &p, std::make_pair(indirit, eod)) {

            // only continue if there is a file
            if (fs::is_regular_file(p)) {
                log<lo::LOG_INFO>("\tprobing %s") % p.c_str();

                fs::ifstream inStream(p, std::ios::in | std::ios::binary);
                ar::binary_iarchive bia(inStream);
                DataSeries data;

                // read input archive to data series object
                try {
                    bia >> data;
                    datavec.push_back(data);

                } catch (std::exception e) {
                    log<lo::LOG_WARNING>("Exeption while trying to read %s: %s --> skipping!") % p.c_str() % e.what();
                }
            }
        }

        log<lo::LOG_INFO>("Scanning input directory successful. Found %i valid data archives.") % datavec.size();
        if (datavec.size() == 0) { log<lo::LOG_CRITICAL>("Zero datafiles is not enough. Aborting."); exit(-3); }

        params = datavec[0].getParams();
    }


    /* ======================= linearize observable vectors ==============================*/
    log<lo::LOG_INFO>("Linearizing raw observable vectors.");
    std::vector<Tfloat> op_raw(0), op2_raw(0), op4_raw(0);
    std::vector<std::pair<Tfloat, std::vector<Tfloat> > > energyvec;

    {
        // generate linearized observable vectors
        for (int i=0;i<datavec.size();i++){
            log<lo::LOG_INFO>("\tprocessing series no. %i...") % (i+1);

            // start by sqr(op), init with 0
            std::vector<float> tmp_opsqr(datavec[i].size(), 0);
            for (ushort c=0;c<datavec[i].getParams().components;c++) {
                std::vector<float> phic = datavec[i].stripData(PHI, c);

                std::transform(phic.begin(), phic.end(), tmp_opsqr.begin(), tmp_opsqr.begin(), [](Tfloat pc, Tfloat psqr){ return SQR(pc) + psqr;});
            }

            op2_raw.insert(op2_raw.end(), tmp_opsqr.begin()+thermTime, tmp_opsqr.end());

            std::vector<Tfloat> tmp_energy = datavec[i].stripData(E);
            tmp_energy.erase(tmp_energy.begin(), tmp_energy.begin()+thermTime);

            energyvec.push_back(std::make_pair(datavec[i].getParams().T,tmp_energy));
        }

        op_raw.resize(op2_raw.size());
        op4_raw.resize(op2_raw.size());
        // generate op, op4
        std::transform(op2_raw.begin(), op2_raw.end(), op_raw.begin(), [](Tfloat x){ return SQRT(x); });
        std::transform(op2_raw.begin(), op2_raw.end(), op4_raw.begin(), [](Tfloat x){ return SQR(x); });
    }


    /* ======================= generate MH weights ============================== */
    log<lo::LOG_INFO>("Starting multi-histogram reweighting.");
    MultiHistogram MH;
    MH.readData(energyvec);

    if (MaxIterations > 0) MH.MaxIterations = MaxIterations;
    if (MinElements > 0) MH.MinElements = MinElements;
    if (Divisor > 0) MH.Divisor = Divisor;

    MH.Reweigh();

    /* ======================= Reweigh observables ============================== */
    log<lo::LOG_INFO>("Reweighing data vectors.");
    Tfloat vol = float(params.Nx*params.Ny*params.Nz);

    std::vector<Tfloat> tmp(op2_raw.size());
    for (int i=0;i<temps.size();i++) {

        log<lo::LOG_DEBUG>("\ttarget temperature: %f") % temps[i];
        Tfloat rwopm, rwopv, rwphi2m, rwphi2v, rwphi4m, rwphi4v;

        log<lo::LOG_DEBUG>("\tlinearizing weights");
        std::vector<Tprec> weights = linearize(MH.getWeights(temps[i]));

        size_t binsize = greaterDivisor(tmp.size(),minbin);
        log<lo::LOG_DEBUG>("\tbinning with binsize %i") % binsize;

        std::transform(weights.begin(), weights.end(), op_raw.begin(), tmp.begin(), [](Tprec w, Tprec o){ return w*o; });
        binning(tmp, rwopm, rwopv, binsize);

        std::transform(weights.begin(), weights.end(), op2_raw.begin(), tmp.begin(), std::multiplies<Tprec>());
        binning(&tmp[0], rwphi2m, rwphi2v, binsize, tmp.size());

        std::transform(weights.begin(), weights.end(), op4_raw.begin(), tmp.begin(), std::multiplies<Tprec>());
        binning(&tmp[0], rwphi4m, rwphi4v, binsize, tmp.size());

        // compiler optimization likes to ruin the day
        rwphi4v=rwphi4v/SQR(vol*vol);

	printf("%u %f %f %e %e %lu %e %e %e %e \n", \
	   params.Nx, temps[i], params.J, rwopm/vol, rwopv/SQR(vol), binsize, rwphi2m/SQR(vol), rwphi2v/SQR(SQR(vol)), rwphi4m/SQR(SQR(vol)), (rwphi4v/SQR(vol*vol)));
    }

    return 0;
}
