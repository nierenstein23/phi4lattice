#include <datafile.h>
#include <util.h>
#include <logger.h>

#include <vector>
#include <numeric>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <iostream>

namespace po = boost::program_options;
namespace fs = boost::filesystem;
namespace ar = boost::archive;
namespace lo = logger;

typedef logger::log_level_t loglevel;
// global verbosity level
loglevel GLOG_LEVEL;

int main(int argc, char const *argv[]) {
    DataSeries data;
    std::vector< std::map< Tfloat, std::vector<Tcomplex> > > PhiP;//, PiP;

    std::string inputFileName, finitepFileName, outputDirName;
    
    Tfloat thermTime;
    std::vector<Tfloat> deltaTs, deltaJs;
    int minbin, nbin, maxp;

    po::options_description desc("Options for the Evaluator3000:");
    desc.add_options()
    ("help,h", "produce this help message")
    ("infile,i", po::value<std::string>(&inputFileName), "binary file to use")
    ("finite-p-infile,p", po::value<std::string>(&finitepFileName), "binary file to use for finite p data")
    ("outdir,o", po::value<std::string>(&outputDirName), "output location for result archive")
    ("thermTime,T", po::value<Tfloat>(&thermTime), "thermalization time that is to be cut of; if unspecified it is set to one autocorrelation time")
    ("deltaTs,t", po::value<std::string>()->default_value("0"), "deltaTs: temperature shifts for reweightings. Beware: If it does not contain 0, only reweighted values calculated!")
    ("deltaJs,j", po::value<std::string>()->default_value("0"), "deltaJs: field shifts for reweightings, see above.")
    ("minbin", po::value<int>(&minbin)->default_value(10), "Minimum width of bins, even with short autocorrelation time")
    ("maxp,P", po::value<int>(&maxp)->default_value(0), "Maximum for which to evaluate; if (maxp < 1), evaluate all that there are (default).")
    ("loglevel,l",po::value<loglevel>(&GLOG_LEVEL)->default_value(logger::LOG_WARNING), "specify log level")
    ("verbose,v", "DEPRECATED: increased verbosity, useful for very large data sets");

    po::variables_map vm;
    po::store(po::command_line_parser( argc, argv).options(desc).run(), vm);
    po::notify(vm);

    // read in parsed stuff
    if(vm.count( "help" )) {std::cerr << desc << std::endl; exit(0);}
    if (vm.count( "verbose" )) GLOG_LEVEL = logger::LOG_INFO; // compatibility to -v flag
    bool hasp = vm.count("finite-p-infile");

    // check if input is regular file
    fs::path inFile(inputFileName);
    if (!fs::is_regular_file(inFile)) {
        log<lo::LOG_CRITICAL>("No valid input file specified. Aborting");
        return -1;
    }

    fs::path pinFile(finitepFileName);
    if (hasp) {
        if (!fs::is_regular_file(inFile)) {
            log<lo::LOG_CRITICAL>("No valid finite p input file specified. Aborting");
            return -1;
        }

    }

    // parse reweight strings to vectors
    {
        log<lo::LOG_INFO>("Parsing reweighting parameters...");
        std::istringstream tiss(vm["deltaTs"].as<std::string>());
        std::istringstream jiss(vm["deltaJs"].as<std::string>());

        std::copy(std::istream_iterator<Tfloat>(tiss), std::istream_iterator<Tfloat>(),std::back_inserter(deltaTs));
        std::copy(std::istream_iterator<Tfloat>(jiss), std::istream_iterator<Tfloat>(),std::back_inserter(deltaJs));
    }

    /* ======================= read in binary ==============================*/
    {
        log<lo::LOG_INFO>("Reading in data series...");
        fs::ifstream inStream(inFile, std::ios::in | std::ios::binary);
        ar::binary_iarchive bia(inStream);

        fs::ifstream pinStream;//(pinFile, std::ios::in | std::ios::binary);
        if (hasp) pinStream.open(pinFile, std::ios::in | std::ios::binary);

        // read input archive to data series object
        try {
            bia >> data;

            if (hasp) {
                ar::binary_iarchive biap(pinStream);
                size_t datasize; biap >> datasize;
                PhiP.resize(datasize); //PiP.resize(datasize);
                biap >> PhiP; // >> PiP;
            }

        } catch (std::exception e) {
            log<lo::LOG_CRITICAL>("\nException while trying to read archive: %s") % e.what();
            return -2;
        }

        if (maxp < 1) maxp = PhiP[1].size();

        log<lo::LOG_INFO>("Done reading in Data.");
        log<lo::LOG_DEBUG>("E(t=0) = %e") % data(E, 0);
    }


    // generate absolutes of order param from raw data
    std::vector<Tfloat> 
        OrderParameter0(data.size()), 
        OP(data.size()), //OP2(data.size()), OP4(data.size()), 
        Energy(data.size());

    // fill with data
    OrderParameter0 = data.stripData(PHI, 0);
    Energy = data.stripData(E);

    size_t vol = data.getParams().Nx*data.getParams().Ny*data.getParams().Nz,
           N = data.getParams().Nx;

    log<lo::LOG_INFO>("Transforming to |phi|");
    for (size_t i=0;i<data.size();i++) {
        Tfloat tmp=0;
        for (ushort c=0;c<data.getParams().components;c++)
            tmp += SQR(data(PHI, i, c));

        OP[i] = SQRT(tmp)/vol;
    }
    log<lo::LOG_INFO>("done! |phi|(t=0) = %e") % OP[0];

    // ==================== reweight stuff =============================== //
    Tfloat beta = 1.0/(data.getParams().T),
        tACOP = std::max(2*autoCorrTime(OrderParameter0), Tfloat(minbin));

    // if no thermalization time was specified, use the autocorrelation time for that.
    if (!vm.count("thermTime")) thermTime = tACOP;

    // remove thermalization time cutoff from data
    size_t datasize = data.size();
    {
        size_t cutoff = std::ceil(thermTime);
        datasize = data.size() - cutoff;

        OrderParameter0.erase(OrderParameter0.begin(), OrderParameter0.begin() + cutoff);
        OP.erase(OP.begin(), OP.begin() + cutoff);
        Energy.erase(Energy.begin(), Energy.begin() + cutoff);
    }

    log<lo::LOG_INFO>("Removed thermalization cutoff.");
    log<lo::LOG_DEBUG>("E(t'=0) = %e") % Energy[0];
    
    std::vector<double> weights(datasize, 1);
    std::vector<Tfloat> rwvalues(datasize);

    std::vector<Tfloat>::iterator itj, itt;

    for (itt=deltaTs.begin();itt<deltaTs.end();itt++) {
        Tfloat dt = *itt; // delta temperature

        Tfloat deltaBeta = 1.0/(data.getParams().T + dt) - beta;
        
        for (itj=deltaJs.begin();itj<deltaJs.end();itj++) {
            Tfloat dj = *itj; // delta field

            log<lo::LOG_DEBUG>("Enter reweighting with dt = %f, dj= %f...") % dt % dj; 

            // fill weights vector
            std::transform(Energy.begin(), Energy.end(), OrderParameter0.begin(), weights.begin(), 
                    [=](Tfloat e, Tfloat op){ return EXP( -deltaBeta*e - beta*dj*op ); });

            // calculate normalization constant
            double Cnorm = std::accumulate(weights.begin(), weights.end(), 0.0)/datasize;
            log<lo::LOG_DEBUG>("\tCnorm = %e") % Cnorm;

            // normalize weights vector
            std::transform(weights.begin(), weights.end(), weights.begin(), std::bind1st(std::multiplies<double>(), 1.0/Cnorm));

            log<lo::LOG_DEBUG>("... done! weight(t'=0) = %e") % weights[0];

            Tfloat rwopm, rwopv;
            std::vector<Tfloat> rwPhiPSqM(maxp), rwPhiSqV(maxp);
            // solve this via a map?!
            {
                log<lo::LOG_DEBUG>("Transforming 1 ...");
                std::transform(weights.begin(), weights.end(), OP.begin(), rwvalues.begin(), std::multiplies<double>());
                log<lo::LOG_DEBUG>("Binning 1 with tACOP %e...") % tACOP;
                binning(&rwvalues[0], rwopm, rwopv, std::ceil(tACOP), datasize);

                if (hasp) {
                    for (int n=1;n<maxp;n++) {
                        std::transform(weights.begin(), weights.end(), PhiP.begin(), rwvalues.begin(), [=](const double w, std::map<Tfloat, std::vector<Tcomplex> > phipm){ return w*SQR(std::abs(phipm[n*1.0][0])/vol); });
                        binning(&rwvalues[0], rwPhiPSqM[n], rwPhiSqV[n], std::ceil(tACOP), datasize);
                    }

                } else rwPhiPSqM = rwPhiSqV = std::vector<Tfloat>(maxp, 0); // fill with zeros
            }

            for (int n=1;n<maxp;n++) {
//                printf("%lu %f %f %e %e %e %e %e\n", N, data.getParams().T + dt, data.getParams().J + dj, rwopm, rwopv, SQRT(1.0 - COS(2.0*n*M_PI/N)), rwPhiPSqM[n], rwPhiSqV[n]);
                printf("%lu %f %f %e %e %e %e %e\n", N, data.getParams().T + dt, data.getParams().J + dj, rwopm, rwopv, 2.0*SIN(n*M_PI/N), rwPhiPSqM[n], rwPhiSqV[n]);
            }

            // ======================== output to stdout ======================= //
            //printf("%u %f %f %e %e %e %e %e %e %e %e %e %e %e %e %e %e %e\n", \
                data.getParams().Nx, data.getParams().T + dt, data.getParams().J + dj, rwopm, rwopv, tACOP, rwphi2m, rwphi2v, rwphi4m, rwphi4v, C1m/SQR(vol), C1v/SQR(vol), C2m/SQR(vol), C2v/SQR(vol), C3m/SQR(vol), C3v/SQR(vol), C4m/SQR(vol), C4v/SQR(vol));

        }
    }


    return 0;
}
