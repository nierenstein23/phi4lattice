/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#ifndef UTIL_HCU
#define UTIL_HCU

#include <cuda_runtime.h>

/**
 * @Synopsis  Helper function for CUDA initialization
 *
 * @Param device number of desired CUDA device
 */
void setCudaDevice(int device); 

/* Utility function to calculate quotient and remainder of p / q. */

/**
 * @Synopsis  Full division with remainder 
 *
 * @Param p	dividend
 * @Param q	divisor
 * @Param quot	quotient
 * @Param rem	remainder
 */
__host__ __device__ void inline divmod( int p, int q, int& quot, int& rem )
{
	quot = p / q;
	rem = p - ( quot * q );
}

/**
 * @Synopsis  Utility class to report errors in CUDA code. 
 */
class CudaError
{
	public:
		CudaError( cudaError_t err );
		CudaError( const char* warn, cudaError_t err );
		cudaError_t getError( );
		const char* getErrorMessage( );

	private:
		cudaError_t cudaErr;
}; 

/**
 * @Synopsis  Utility method for speedy testing of whether a number is odd 
 *
 * @Param cand the candidate number
 *
 * @Returns true if cand is odd, else false
 */
__device__ __host__ inline bool isOdd( int cand )
{
	return ( cand & 0x1 );
}
#endif /* UTIL_HCU */
