#include <lattice_factory.h>
#include <lattice_evolver.h>

#include <simline.h>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/format.hpp>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#ifndef __CPUONLY__
#include <cuda_runtime.h>
#endif
#include <sstream>

/**
 * @Synopsis  Main execution unit for simulating canonical ensembles.
 *              Uses HMC algorithm to generate a Markov chain of 
 *              thermalized ensembles.
 *
 * @Param argc,argv[]   parsed by boost::program_options library
 *
 * @Returns 0
 */
int main(int argc, char const *argv[]) {
    // lattice properties
    size_t l; ushort c, dim;

    // Lagrange parameters
    Tfloat msq, g0;

    // simulation parameters
    Tfloat T, Ttherm, tltherm, tCons, tLang, tLinear, tRelax, Tfinal, dt, gamma, J, Jtherm, Jfinal;
    unsigned int nTraj, nSteps, tSteps, sweeps, tmifac, maxp;
    Tfloat lTraj;

    // random seed
    long seed = time(0);
    // output parameters
    std::string OutDir, InputConfig, line {""};
    bool txtout=false, therm=false, prettyout=false, tmi=false, doEP=false, pbinout=false;
    int loglevel;

    // hardware params
    hwtype hw_t=CPU; ushort dev; std::string ident; hardware hw;
    dynmode dmode=relax;

    /**
     * @Synopsis  set the possible command line parameters and map them to variables.
     */
    {
        po::options_description desc("Options for my program");
        desc.add_options()
        ("help,h", "produce this help message")
        ("hardware,a", po::value<hwtype>(&hw_t), "choose the desired hardware base (CPU or GPU, defaults to CPU)")
        ("dynmode,y", po::value<dynmode>(&dmode), "choose between relax(ational) and diff(usive) dynamics; default relaxational")
        ("device,d", po::value<ushort>(&dev)->default_value(0), "if wanted, set the CUDA device to use")
        ("ident", po::value<std::string>(&ident)->default_value(""), "identifier string for device selection")
        ("seed,s", po::value<long>(&seed), "set random number seed")
        ("sites,N", po::value<size_t>(&l)->default_value(32), "set number of sites to use (one dimension)")
        ("maxp,m", po::value<unsigned int>(&maxp)->default_value(0), "set max momentum to output; 0 === L/2")
        ("dimension,D", po::value<ushort>(&dim)->default_value(3), "the lattice dimension")
        ("Nc,c", po::value<ushort>(&c)->default_value(1), "set number of components to use")
        ("m2", po::value<Tfloat>(&msq)->default_value(-1.0), "set square of mass")
        ("lambda", po::value<Tfloat>(&g0)->default_value(1.0), "set coupling g0 a.k.a. lambda")
        ("source,J", po::value<Tfloat>(&J)->default_value(0.0), "set (initial) source")
        ("thermJ", po::value<Tfloat>(&Jtherm), "set (initial) source")
        ("final-source,G", po::value<Tfloat>(&Jfinal), "set final source")
        ("temperature,T", po::value<Tfloat>(&T)->default_value(1.0), "set (initial) temperature")
        ("thermT,", po::value<Tfloat>(&Ttherm), "set (initial) temperature")
        ("final-temperature,F", po::value<Tfloat>(&Tfinal), "set final temperature")
        ("therm", po::value<Tfloat>(&tltherm)->implicit_value(100), "thermalize a bit with random pi first")
        ("therm-sweeps", po::value<unsigned int>(&sweeps)->default_value(10), "thermalization sweeps")
        ("Ntraj", po::value<unsigned int>(&nTraj)->default_value(100), "set number of trajectories to follow (100)")
        ("Ltraj", po::value<Tfloat>(&lTraj)->default_value(-1.0), "override length of HMC trajectory; values < 0 imply that the length is heuristically estimated")
        ("Nsteps", po::value<unsigned int>(&nSteps)->default_value(0), "override number of steps per trajectory a.k.a. step size; if unset or 0, estimate ideal step size")
        ("tCons,C", po::value<Tfloat>(&tCons)->default_value(0), "conservative evolution time")
        ("tLang,L", po::value<Tfloat>(&tLang)->default_value(0), "Langevin evolution time")
        ("tLinear,l", po::value<Tfloat>(&tLinear)->default_value(0), "Isobaric evolution time")
        ("tRelax,R", po::value<Tfloat>(&tRelax)->default_value(0), "Langevin relaxation time")
        ("tmi", po::value<unsigned int>(&tmifac)->implicit_value(1), "output plottable fields at every step along the way")
        ("e-modes,e", "output finite-p modes of energy density")
        ("dT,t", po::value<Tfloat>(&dt)->default_value(1.0), "output interval")
        ("tSteps", po::value<unsigned int>(&tSteps)->default_value(160), "steps in one output interval")
        ("gamma,g", po::value<Tfloat>(&gamma)->default_value(0.1), "set damping gamma (for Langevin evolution)")
        ("outdir,o", po::value<std::string>(&OutDir)->default_value("OUTPUT"), "directory of output" )
        ("loglevel",po::value<int>(&loglevel)->default_value(4), "specify log level")
        ("notxt", "suppress output of text files, only binaries")
        ("pbinout", "suppress output of text files, only binaries")
        ("pretty-out", "output plot-ready version of configuration at end")
        ("line", po::value<std::string>(&line), "new descriptive format for simulation runs")
        ("in,i", po::value<std::string>(&InputConfig), "read in lattice configuration to start from" );

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if( vm.count( "help" ) ) { std::cout << desc << std::endl; exit(0); }
        txtout = !vm.count("notxt");
        pbinout = vm.count("pbinout");
        therm = vm.count("therm");
        prettyout = vm.count("pretty-out");
        tmi = vm.count("tmi");
        doEP = vm.count("e-modes");

        // set log level
        {
            namespace tlogging = boost::log::trivial;
            auto level = tlogging::info;

            switch(loglevel) {
                case 1: level = tlogging::fatal; break;
                case 2: level = tlogging::error; break;
                case 3: level = tlogging::warning; break;
                case 4: level = tlogging::info; break;
                case 5: level = tlogging::debug; break;
                case 6: level = tlogging::trace; break;
            }

            boost::log::core::get()->set_filter( tlogging::severity >= level );
        }

        hw = hardware{hw_t, ident, dev};

        if (!vm.count("thermT")) Ttherm = T;
        if (!vm.count("final-temperature")) Tfinal = T;
        if (!vm.count("thermJ")) Jtherm = J;
        if (!vm.count("final-source")) Jfinal = J;

        srand48(seed);

        // add trailing slash
        OutDir += "/";

    }

    /**
     * @Synopsis  The actual program flow.
     */
    {
        // first, generate a lattice on the desired hardware, 
        // give it the desired properties
        iLattice *lattice = latticeFactory::buildIsoLattice(l, c, hw, dim);

        lattice->generateLagrangeParameters(1.0, msq, g0);
        lattice->setSource(J);
        lattice->setGamma(gamma);
        lattice->seedTaus(seed);
        lattice->dumpInfo(T, OutDir);
        BOOST_LOG_TRIVIAL(debug) << "after params";

        // choose some sane initial conditions
        if (InputConfig != "") {
            std::ifstream input(InputConfig.c_str(), std::ios::binary);
            lattice->loadFields(input);
        } else {
            lattice->randomizePi(T);
            lattice->randomizePhi(1.0);
        }

        BOOST_LOG_TRIVIAL(debug) << "after init";

        // make an evolver for said lattice
        LatticeEvolver ev(lattice, txtout, pbinout);
        ev.theObs().setMaxP(maxp);
        ev.resetTime();
        ev.setEP(doEP);
        ev.setOutputLoc(OutDir);

        lattice->setSource(Jtherm);
        if (therm) ev.Thermalize(Ttherm, tltherm, 160, sweeps, 1.0, "Therm");
        lattice->setSource(J);

        BOOST_LOG_TRIVIAL(debug) << "after ltherm";

        if(nTraj > 0) {
            // do HMC evolution
            ev.HMC(T, nTraj, lTraj, nSteps, "HMC");
            ev.resetTime();

            BOOST_LOG_TRIVIAL(debug) << "before copy";
            BOOST_LOG_TRIVIAL(debug) << "after copy";

            // save thermalized configuration in file for possible reuse
            try {
                std::stringstream configOutName;
                configOutName << OutDir << "/" << "ConfigurationID" << seed;
                std::ofstream configOut(configOutName.str().c_str(), std::ios::binary);

                lattice->saveFields(configOut);

                if(prettyout) {
                    std::stringstream pconfigOutName;
                    pconfigOutName << OutDir << "/" << "PrettyConfigurationID" << seed;
                    std::ofstream pconfigOut(pconfigOutName.str().c_str());

                    lattice->savePhiPretty(pconfigOut);
                }

                configOut.close();

            } catch (std::exception &e) { BOOST_LOG_TRIVIAL(warning) << boost::format("Saving the field failed: %s\nDoes the output destination exist?") % e.what(); }
        }

        BOOST_LOG_TRIVIAL(debug) << "reset lattice after thermalization";

        // reset to proper source
        lattice->setDynMode(dmode);

        // assign new momenta, can't do much damage, can it?
        lattice->randomizePi(T);
        BOOST_LOG_TRIVIAL(debug) << "after rand";

        ev.settmi(tmi, tmifac);

        //  evolve RT w Langevin
        if (tLang != 0) {
            ev.RT(T, tLang, tSteps, true, dt, "Langevin");
            ev.resetTime();
        }

        // do conservative RT evolution
        if (tCons != 0) {
            ev.RT(T, tCons, tSteps, false, dt, "Conservative");
            ev.resetTime();
        } 

        if (tLinear > 0)
            ev.RTFunctional(
                    [=](Tfloat t) { return (t/tLinear)*(Tfinal - T) + T; }, 
                    [=](Tfloat t) { return (t/tLinear)*(Jfinal - J) + J; }, 
                    tLinear, tSteps, dt, "Linear");

        if (tRelax > 0)
            ev.RTLangevin(Tfinal, tRelax+tLinear, tSteps, dt, "Linear");

        if (line != "") {
            std::vector<simline> sims;

            try {
                sims = simline_parser::parse(line);

                for (simline &s : sims) BOOST_LOG_TRIVIAL(info) << s.str();

                for (auto&& sim : sims) ev.Simulate(sim);

            } catch (std::exception& e) {
                BOOST_LOG_TRIVIAL(error) << "Parsing simline failed: " << e.what();
            }
        }

        // clean up
        delete lattice;
    }

    return 0;
}
