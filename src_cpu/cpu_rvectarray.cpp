#include <cpu_rvectarray.hpp>
#include <cstring> // defines memcpy
#include <tausworthe.h>
#include <par_funcs.hpp>
#include <numeric>

#include <omp.h>

#include <immintrin.h>

/**
 * @Synopsis  Array reduction parallelized via OMP
 *
 * @Param array input values
 * @Param size number of input values
 *
 * @Returns sum over array[0..size-1]
 */
Tfloat CPUrvectArray::ompReduce(const Tfloat * const array, size_t size) const {
    double result = 0.0;

    /*
    std::vector<Tfloat> tmp(0);
    tmp.assign(array, array+size);
    return std::accumulate(tmp.begin(), tmp.end(), 0.0);
    */

    for (size_t i=0;i<size;i++)
        result += array[i];

    return result;
}


/**
 * @Synopsis  allocates aligned memory for fields and a buffer
 */
void CPUrvectArray::init() {
    fields = (Tfloat*)_mm_malloc(NoFloats()*sizeof(Tfloat), 64);
    buffer = (Tfloat*)_mm_malloc(NoFloats()*sizeof(Tfloat), 64);
}


/**
 * @Synopsis  Essentially the copy constructor without the constructing part.
 *              Copies the contents of the array directly to the 'fields' location.
 *
 * @Param src source array
 */
void CPUrvectArray::doCopyFrom(const rvectArray &src) {
    if ((src.NoElements() != NoElements()) || (src.NoComponents() != NoComponents())) throw std::length_error("Numbers of elements in arrays do not match!");

    src.writeFieldsToBuffer(fields);
}


/**
 * @Synopsis  writes fields to the address at buffer. MUST HAVE ENOUGH SPACE!
 *
 * @Param buffer the address of the buffer
 */
void CPUrvectArray::doWriteFieldsToBuffer(Tfloat * const out_buffer) const {
    std::memcpy(out_buffer, fields, NoFloats()*sizeof(Tfloat));
}


/**
 * @Synopsis  Inverse of above
 *
 * @Param buffer
 */
void CPUrvectArray::doReadFieldsFromBuffer(const Tfloat * const in_buffer) {
    std::memcpy(fields, in_buffer, NoFloats()*sizeof(Tfloat));
}


/**
 * @Synopsis  Sums all floats in the fields. Override of base class inline to reduce access operations.
 *
 * @Returns 
 */
Tfloat CPUrvectArray::doSumComponents() const {
    return ompReduce(fields, NoFloats());
}


/**
 * @Synopsis  Sums all jth components. Not as efficient as summing over all j.
 *
 * @Param j component index
 *
 * @Returns the sum of all jth components
 */


Tfloat CPUrvectArray::doSumJthComponent(const unsigned short j) const {
    double result = 0.0;
    short c = NoComponents();
    size_t size = NoFloats();

    #pragma omp parallel for reduction(+:result)
    for (size_t i=j;i<size;i+=c)
        result += fields[i];

    return result;
}


/**
 * @Synopsis  Sum squares of vectors; amounts to sum of all squares,
 *              so simply map a square kernel and reduce the result.
 *
 * @Returns 
 */
Tfloat CPUrvectArray::doSumSquares() const {
    double result = 0.0;
    size_t size = NoFloats(); 

    #pragma omp parallel for reduction(+:result) schedule(static)
    for (size_t i=0;i<size;i++)
        result += SQR(fields[i]);

    return result;
}


/**
 * @Synopsis  Sum over all x^4.
 *
 * @Returns the sum over all fourth powers
 */
Tfloat CPUrvectArray::doSumQuartes() const {
    double result = 0.0;

    size_t size = NoFloats();
    ushort c = NoComponents();

    if (c==1) {
        // one-component case is straightforward
        #pragma omp parallel for reduction(+:result) schedule(static)
        for (size_t i=0;i<size;i++)
            result += SQR(SQR(fields[i]));

    } else {
        #pragma omp parallel for reduction(+:result) schedule(static)
        for (size_t i=0;i<size;i+=c) {
            Tfloat tmp=0.0;
            #pragma omp reduction(+:tmp)
            for (size_t j=i;j<i+c;j++)
                tmp += SQR(fields[j]);

            result += SQR(tmp);
        }

    }

    return result;
}


void CPUrvectArray::doAddConst(const Tfloat a)
{
    size_t size = NoFloats();

    #pragma omp parallel for schedule(static)
    for (size_t i=0;i<size;i++)
        fields[i] += a;
}


void CPUrvectArray::doSaxpy(const Tfloat a, const rvectArray &x) {
    CPUrvectArray newX(x);
    saxpy(a, newX);
}


void CPUrvectArray::doSetGaussianRandom( const Tfloat average, const Tfloat sigma, taus_state * const rnd_state) {
    size_t size = NoFloats();

    fillArrayWithGaussian(fields, average, sigma, size, rnd_state);
}


void CPUrvectArray::doRandomizeAndDamp(const Tfloat noiseAmp, const Tfloat damping, taus_state * const rnd_state) {
    size_t size = NoFloats();

    #pragma omp parallel for schedule(static) // TODO: check vectorization (same as above)
    for (size_t i=0;i<size;i++) {
        fields[i] += noiseAmp*generateGaussian(0.0, 1.0, &rnd_state[i]) - damping*fields[i];
    }


}


/**
 * @Synopsis  saxpy overload if x is on the same device
 *
 * @Param a
 * @Param x
 */
void CPUrvectArray::saxpy(const Tfloat a, const CPUrvectArray &x) {
    size_t size = NoFloats();

    if (size != x.NoFloats()) { std::cerr << "Array size mismatch. Fatal." << std::endl; return; }

    #pragma omp parallel for schedule(static)
    for (size_t i=0;i<size;i++)
        fields[i] += a*x.fields[i];

}

/**
 * @Synopsis  Copy constructor of CPU array FROM CPU array without host buffer.
 * PROBLEM: COPY CONSTRUCTOR IS NOT CALLED AS EXPECTED!
 * Probably due to return value optimizations and copy elision.
 *
 * @Param src src array
 */
CPUrvectArray::CPUrvectArray(const CPUrvectArray &src) : rvectArray(src.NoElements(), src.NoComponents()) {
    init();
    copyFrom(src);
}


/**
 * @Synopsis  Copy constructor of a CPU array. ALWAYS uses a host memory buffer!
 * PROBLEM: COPY CONSTRUCTOR IS NOT CALLED AS EXPECTED!
 * Probably due to return value optimizations and copy elision.
 *
 * @Param src source array 
 */
CPUrvectArray::CPUrvectArray(const rvectArray &src) 
    : rvectArray(src.NoElements(), src.NoComponents()) 
{
    init();
    copyFrom(src);
}


CPUrvectArray::~CPUrvectArray()  { 
    _mm_free(fields); 
    _mm_free(buffer); 
}

