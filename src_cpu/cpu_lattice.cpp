#include <cpu_lattice.hpp>
#include <par_funcs.hpp>
#include <omp.h>
#include <numeric>

// =============================== nD ===================================================================

void CPULattice::generateNoiseVector(Tfloat amplitude)
{
    size_t size = index->getPoints();
    for (ushort d=0;d<getDim();d++) fillArrayWithGaussian(&vecbuf[d*size], 0.0, amplitude, size, rnd_state);
}


void CPULattice::doSeedTaus(const long newseed)
{
    seed = newseed;
    for (size_t i=0;i<index->getPoints();i++) rnd_state[i] = taus_seed();
}


CPULattice::CPULattice(const size_t size, const ushort components)
    : d_phi(size, components), d_pi(size, components), d_phi_backup(size, components) 
{ 
    rnd_state = new taus_state[index->getPoints()];
    seedTaus(time(0));

    phi = &d_phi;
    pi = &d_pi;

    zBuffer = new Tfloat[index->getComponents()*index->getL()];
    zMomentumIsCurrent = EzMomentumIsCurrent = false;

    buffer_d = new Tfloat[index->getPoints()];
    tmp_d = new Tfloat[index->getPoints()];

    BOOST_LOG_TRIVIAL(trace) << "CPULattice constructor";
}


CPULattice::~CPULattice()
{
    delete[] rnd_state;
    delete[] zBuffer;

    phi = pi = nullptr;

    BOOST_LOG_TRIVIAL(trace) << "CPULattice destructor";
}


void CPULattice::doUpdateStressTensor() {
    const size_t lq = index->getL()/2+1;
    std::vector<Tcomplex> V(lq), T(lq), L(lq); // buffer vectors

    const size_t vol = index->getElements();

    // potential energy only
    //eDensKernel(tmp_d, true);
    //BOOST_LOG_TRIVIAL(trace) << "updateStress - energyDensityKernel";
}


// =============================== 3D ===================================================================

void CPU3DLattice::doUpdateZMomentum(const bool on_phi) {
    const size_t 
        lx = indexer.getLx(),
        ly = indexer.getLy(),
        lz = indexer.getLy();

    const ushort comp = indexer.getComponents();

    const Tfloat * const fields = on_phi?getPhiPointer():getPiPointer();
    std::vector<Tcomplex> * const zMomentum = on_phi?PhizMomentum:PizMomentum;

    #pragma omp parallel for
    for (ushort c=0;c<comp;c++) {

        // data transformation
        for(size_t z=0;z<lz;z++) {
            Tfloat tmp=.0;
            #pragma omp simd reduction(+:tmp) collapse(2)
            for (size_t y=0;y<ly;y++)
                for (size_t x=0;x<lx;x++)
                    tmp += fields[indexer.index(x,y,z)+c];

            zBuffer[c*lz + z] = tmp;
        }
    }

    // fftw_plan is not thread safe...
    for (ushort c=0;c<comp;c++) {
        fft_plan plan = fft_plan_r2c(lz, (ffTfloat*)(zBuffer+c*lz), (ffTcomplex*)zMomentum[c].data(), FFTW_ESTIMATE);
        fft_execute(plan);
    }
}


void CPU3DLattice::doUpdateEMomentum() {
    const size_t 
        lx = indexer.getLx(),
        ly = indexer.getLy(),
        lz = indexer.getLy();

    ushort comp = indexer.getComponents();

    Tfloat *phi_ = getPhiPointer(), *pi_ = getPiPointer();

    #pragma omp parallel for
    for(size_t x=0;x<lx;x++) {
        size_t dxf = indexer.getdxf(x);

        Tfloat tmp=.0;
        for (size_t y=0;y<ly;y++) {
            size_t dyf = indexer.getdyf(y);

            for (size_t z=0;z<lz;z++) {
                int dzf = indexer.getdzf(z);

                Tfloat pisqr = 0., phisqr = 0., gradsq = 0.;

                #pragma omp simd reduction(+:gradsq,phisqr,pisqr)
                for (ushort c=0;c<comp;c++) {
                    Tfloat phi_xyc = phi_[indexer.index(x,y,z)+c],
                    pi_xyc = pi_[indexer.index(x,y,z)+c];

                    gradsq += SQR(phi_[indexer.index(x,y,z)+c +dzf] - phi_xyc)
                        + SQR(phi_[indexer.index(x,y,z)+c +dyf] - phi_xyc)
                        + SQR(phi_[indexer.index(x,y,z)+c +dxf] - phi_xyc);

                    phisqr += SQR(phi_xyc);
                    if (getDynMode()!=diff) pisqr += SQR(pi_xyc);
                }

                tmp += la * (0.5*pisqr + 0.5/SQR(la)*gradsq + 0.5*lmu0sq*phisqr + lg0/(24*comp)*SQR(phisqr) + J*phi_[indexer.index(x,y,z)]);
                //if (x+z+y==0) printf("E[0] = %e\n", tmp);
            }
        }

        zBuffer[x] = tmp;
    }

    // fft
    fft_plan plan = fft_plan_r2c(lz, (ffTfloat*)zBuffer, (ffTcomplex*)EzMomentum.data(), FFTW_ESTIMATE);
    fft_execute(plan);

    if (getDynMode() == diff) {
        size_t N = indexer.getElements();
        // copy pi(x) to buffer
        std::memcpy((void*)pibuf, (void*)getPiPointer(), N*sizeof(Tfloat));

        // dft to pi(k)
        fft_execute(kplan);

        // multiply pi(k) *= "k^-2"
        for (uint kz=0;kz<indexer.getLz();kz++)
            invKsqrPlane(pik, false, &indexer, kz);

        // transform pi(k)/k^2 -> nabla^-2 pi(x)
        fft_execute(ikplan);

        // multiply pi(x) * nabla^-2 pi(x)
        #pragma omp parallel for simd schedule(static)
        for (size_t n=0;n<N;n++)
            pibuf[n] *= getPiPointer()[n]/N;

        // DFT back to k-space
        fft_execute(kplan);

        // take only ky=0 components
        std::memcpy((void*)TkBuffer.data(), (void*)pik, (lx/2+1)*sizeof(Tcomplex));
        std::transform(EzMomentum.begin(), EzMomentum.end(), TkBuffer.begin(), EzMomentum.begin(), [](Tcomplex v, Tcomplex t){ t *= -0.5; return v+t; });
    }
}


/**
 * @Synopsis  Hamiltonian momentum update by timestep dt.
 *              Much, much more complex operation necessary than for an update in phi. 
 *              Efficient parallelization required!
 *
 * @Param dt
 */
void CPU3DLattice::stepPiRelax(Tfloat dt) {

    // shorthands; most probably optimized out
    const size_t 
        lz = indexer.getLz();

    #pragma omp parallel for schedule(static)
    for (uint z=0;z<lz;z++) {
        stepMomentumZplane(getPiPointer(), getPhiPointer(),
                dt, lmu0sq, lg0, la, J,
                &indexer, z);
    }
}


void CPU3DLattice::stepPiDiff(Tfloat dt) {

    // shorthands; most probably optimized out
    const size_t 
        lz = indexer.getLz();

    #pragma omp parallel for schedule(static)
    for (uint z=0;z<lz;z++) {
        stepMomentumConsZplane(getPiPointer(), getPhiPointer(),
                dt, lmu0sq, lg0, la, J,
                &indexer, z);
    }
}


Tfloat CPU3DLattice::doGetGradPhiSq() const {
    // shorthands; most probably optimized out
    const size_t lz = indexer.getLz();

    Tfloat squareGrad = 0.0;

    #pragma omp parallel for reduction(+:squareGrad) schedule(static)
    for (uint z=0;z<lz;z++)
        squareGrad += squareGradientZplane(getPhiPointer(), &indexer, z);


    return squareGrad;
}


/*Tfloat CPU3DLattice::doGetSliceCorrelation(size_t offset) const {
    double result=0;

    const size_t 
        lz = indexer.getLz();

    if (offset > lz/2) offset = lz - offset;

    #pragma omp parallel for reduction(+:result)
    for (uint z=0;z<lz;z++) {
        int theoffset = (z<lz-offset)?offset:-int(offset);
        result += sliceCorrelationPlane(theoffset, getPhiPointer(), &indexer, z);
    }
            
    return result;
}*/


void CPU3DLattice::addGradNoiseToPi()
{
    const size_t lz = indexer.getLz();

#pragma omp parallel for schedule(static)
    for (uint z=0;z<lz;z++) 
        addGradientZplane(getPiPointer(), vecbuf, &indexer, z);
}

CPU3DLattice::CPU3DLattice(size_t lx, size_t ly, size_t lz, unsigned short components) 
    : iLattice3D(lx, ly, lz, components), CPULattice(lx*ly*lz, components)
{ 
    vecbuf = new Tfloat[3*lx*ly*lz*components]; 
    pik = new ffTcomplex[lz*ly*(lx/2+1)];
    pibuf = new ffTfloat[lz*lx*ly];

    kfield = new ffTcomplex[lz*ly*(lx/2+1)];
    vkfield = new ffTcomplex[lz*ly*(lx/2+1)];

    kplan = fft_plan_r2c_3d(lz, ly, lx, pibuf, pik, FFTW_ESTIMATE);
    ikplan = fft_plan_c2r_3d(lz, ly, lx, pik, pibuf, FFTW_ESTIMATE);
}

CPU3DLattice::~CPU3DLattice()
{
    delete[] vecbuf;
    delete[] pik;
    delete[] pibuf;

    BOOST_LOG_TRIVIAL(trace) << "CPU3DLattice destructor";
}


//======================================== 2D ====================================//

void CPU2DLattice::doUpdateZMomentum(const bool on_phi) {
    const size_t 
        lx = indexer.getLx(),
        ly = indexer.getLy();

    const ushort comp = indexer.getComponents();

    const Tfloat * const fields = on_phi?getPhiPointer():getPiPointer();
    std::vector<Tcomplex> * const zMomentum = on_phi?PhizMomentum:PizMomentum;

#pragma omp parallel for
    for (ushort c=0;c<comp;c++) {

        // data transformation
        for(size_t y=0;y<ly;y++) {
            Tfloat tmp=.0;
#pragma omp simd reduction(+:tmp)
            for (size_t x=0;x<lx;x++)
                tmp += fields[indexer.index(x,y)+c];

            zBuffer[c*ly + y] = tmp;
        }

        // fft
        fft_plan plan = fft_plan_r2c(ly, (ffTfloat*)(zBuffer+c*ly), (ffTcomplex*)zMomentum[c].data(), FFTW_ESTIMATE);
        fft_execute(plan);

    }
}

void CPU2DLattice::doUpdateEMomentum() {
    const size_t lx = indexer.getLx(), ly = indexer.getLy();
    const ushort comp = indexer.getComponents();

    const Tfloat * const phi_ = getPhiPointer(), * const pi_ = getPiPointer();

    // NEW: reduce along y-axis
    #pragma omp parallel for
    for(size_t x=0;x<lx;x++) {
        int dxf = indexer.getdxf(x),
            dxb = indexer.getdxb(x);

        Tfloat tmp=.0;

        for (size_t y=0;y<ly;y++) {
            int dyf = indexer.getdyf(y),
                dyb = indexer.getdyb(y);

            Tfloat pisqr = 0., phisqr = 0., gradsq = 0.;

            #pragma omp simd reduction(+:gradsq,phisqr,pisqr)
            for (ushort c=0;c<comp;c++) {
                Tfloat phi_xyc = phi_[indexer.index(x,y)+c],
                       pi_xyc = pi_[indexer.index(x,y)+c];

                gradsq -= phi_xyc*(phi_[indexer.index(x,y)+c +dyf] + phi_[indexer.index(x,y)+c +dyb]
                        + phi_[indexer.index(x,y)+c +dxf] + phi_[indexer.index(x,y)+c +dxb]
                        - 4*phi_xyc);
                /*
                gradsq += SQR(phi_[indexer.index(x,y)+c + dyf] - phi_xyc)
                        + SQR(phi_[indexer.index(x,y)+c + dxf] - phi_xyc);
                */

                phisqr += SQR(phi_xyc);
                if (getDynMode()!=diff) pisqr += SQR(pi_xyc);
            }

            tmp += la * (0.5*pisqr + 0.5/SQR(la)*gradsq + 0.5*lmu0sq*phisqr + lg0/(24*comp)*SQR(phisqr) + J*phi_[indexer.index(x,y)]);
        }

        zBuffer[x] = tmp;
    }

    // fft
    fft_plan plan = fft_plan_r2c(ly, (ffTfloat*)zBuffer, (ffTcomplex*)EzMomentum.data(), FFTW_ESTIMATE);
    fft_execute(plan);

    if (getDynMode() == diff) {
        size_t N = indexer.getElements();
        // copy pi(x) to buffer
        std::memcpy((void*)pibuf, (void*)getPiPointer(), N*sizeof(Tfloat));

        // dft to pi(k)
        fft_execute(kplan);

        // multiply pi(k) *= "k^-2"
        for (uint ky=0;ky<indexer.getLy();ky++)
            invKsqrLine(pik, false, &indexer, ky);

        // transform pi(k)/k^2 -> nabla^-2 pi(x)
        fft_execute(ikplan);

        // multiply pi(x) * nabla^-2 pi(x)
        #pragma omp parallel for simd schedule(static)
        for (size_t n=0;n<N;n++)
            pibuf[n] *= getPiPointer()[n]/N;

        // DFT back to k-space
        fft_execute(kplan);

        // take only ky=0 components
        std::memcpy((void*)TkBuffer.data(), (void*)pik, (lx/2+1)*sizeof(Tcomplex));
        std::transform(EzMomentum.begin(), EzMomentum.end(), TkBuffer.begin(), EzMomentum.begin(), [](Tcomplex v, Tcomplex t){ t *= -0.5; return v+t; });
    }
}


void CPU2DLattice::stepPiRelax(Tfloat dt)
{
    const size_t ly = indexer.getLy();

#pragma omp parallel for schedule(static)
    for (uint y=0;y<ly;y++) 
        stepMomentumYline(getPiPointer(), getPhiPointer(),
                dt, lmu0sq, lg0, la, J,
                &indexer, y);
}

void CPU2DLattice::stepPiDiff(Tfloat dt) {
    const size_t 
        ly = indexer.getLy();

#pragma omp parallel for schedule(static)
    for (uint y=0;y<ly;y++) 
        stepMomentumConsYline(getPiPointer(), getPhiPointer(),
                dt, lmu0sq, lg0, la, J,
                &indexer, y);
}

Tfloat CPU2DLattice::doGetGradPhiSq() const {
    const size_t ly = indexer.getLy();

    Tfloat squareGrad = 0.0;

#pragma omp parallel for reduction (+:squareGrad) schedule(static)
    for (uint y=0;y<ly;y++)
        squareGrad += squareGradientYline(getPhiPointer(), &indexer, y);

    return squareGrad;
}


Tfloat CPU2DLattice::doGetGradSqPhiSq() const {
    const size_t ly = indexer.getLy();

    Tfloat squareGrad = 0.0;

#pragma omp parallel for reduction (+:squareGrad) schedule(static)
    for (uint y=0;y<ly;y++)
        squareGrad += squareGradSqYline(getPhiPointer(), &indexer, y);

    return squareGrad;
}


Tfloat CPU2DLattice::doGetGradSqPhiQrt() const {
    const size_t ly = indexer.getLy();

    Tfloat squareGrad = 0.0;

    #pragma omp parallel for reduction (+:squareGrad) schedule(static)
    for (uint y=0;y<ly;y++)
        squareGrad += squareGradQrtYline(getPhiPointer(), &indexer, y);

    return squareGrad;
}



Tfloat CPU2DLattice::dogetinvlappisq() const {
    size_t N = indexer.getElements();
    std::memcpy((void*)pibuf, (void*)getPiPointer(), N*sizeof(Tfloat));

    fft_execute(kplan);

    for (uint ky=0;ky<indexer.getLy();ky++) {
        invKsqrLine(pik, true, &indexer, ky);
    }


    size_t Nmax = indexer.getLy()*(indexer.getLx()/2 + 1);
    double result = 0.;
    #pragma omp parallel for simd reduction(+:result)
    for (uint n=0;n<Nmax;n++) {
        result += pik[n][0];
    }
    return result/(N);
}

void CPU2DLattice::addGradNoiseToPi()
{
    const size_t ly = indexer.getLy();

#pragma omp parallel for schedule(static)
    for (uint y=0;y<ly;y++) 
        addGradientYline(getPiPointer(), vecbuf, &indexer, y);
}

CPU2DLattice::CPU2DLattice(size_t lx, size_t ly, unsigned short components): iLattice2D(lx, ly, components), CPULattice(lx*ly, components)
{ 
    vecbuf = new Tfloat[2*lx*ly*components]; BOOST_LOG_TRIVIAL(debug) << "CPU2DLattice constructor"; 
    pik = new ffTcomplex[ly*(lx/2+1)];
    pibuf = new ffTfloat[lx*ly];

    kfield = new ffTcomplex[ly*(lx/2+1)];
    vkfield = new ffTcomplex[ly*(lx/2+1)];

    kplan = fft_plan_r2c_2d(ly, lx, pibuf, pik, FFTW_ESTIMATE);
    ikplan = fft_plan_c2r_2d(ly, lx, pik, pibuf, FFTW_ESTIMATE);
}

CPU2DLattice::~CPU2DLattice()
{
    delete[] vecbuf;
    delete[] pik;
    delete[] pibuf;

    BOOST_LOG_TRIVIAL(trace) << "CPU2DLattice destructor";
}
