#include <par_funcs.hpp>
#include <math.h>
#include <boost/log/trivial.hpp>
#include <boost/format.hpp>

/**
 * @Synopsis  fill array with gaussian random numbers from taus state
 *
 * @Param target
 * @Param mu    mean
 * @Param sigma stddev
 * @Param size  
 * @Param rnd_state has to be larger equal size!
 */
void fillArrayWithGaussian(Tfloat * const target, const Tfloat mu, const Tfloat sigma, const size_t size, taus_state * const rnd_state)
{ 
    #pragma omp parallel for schedule(static)
    for (size_t i=0;i<size;i++) target[i] = generateGaussian(mu, sigma, &rnd_state[i]); 
}


/**
 * @Synopsis  function to do one step of the momentum field Pi in xy-plane with coordinate z
 *
 * @Param Pi,Phi    fields
 * @Param dt        timestep
 * @Param mu0sq     bare mass square
 * @Param g0        bare coupling
 * @Param a         lattice spacing
 * @Param J         external field
 * @Param d_indexer lattice site enumerator
 * @Param z         z coordinate
 */
void stepMomentumZplane(Tfloat *Pi, Tfloat *Phi, 
        Tfloat dt, Tfloat mu0sq, Tfloat g0, Tfloat a, Tfloat J,
        const lattice3DIndex * const d_indexer, const int z) 
{

    const size_t 
        lx = d_indexer->getLx(), 
        ly = d_indexer->getLy(), 
        dx = d_indexer->getDx(), 
        dy = d_indexer->getDy(), 
        dz = d_indexer->getDz();

    const short c = d_indexer->getComponents();

    long int dzf = d_indexer->getdzf(z), dzb = d_indexer->getdzb(z);

    if (c>1) {
        for (uint y=0;y<ly;y++) {
            long int dyf = d_indexer->getdyf(y), dyb = d_indexer->getdyb(y);

            size_t zyIdx = z*dz + y*dy;

            Tfloat 
                xline[(lx+2)*c], // xline has halo of 1
                ylinef[lx*c],
                ylineb[lx*c],
                zlinef[lx*c],
                zlineb[lx*c];

                // fill buffer
                //#pragma omp simd collapse(2)
                for (uint i=0;i<lx*c;i++) {
                    xline[i+dx] = Phi[zyIdx +i];
                    ylinef[i] = Phi[zyIdx +i +dyf];
                    ylineb[i] = Phi[zyIdx +i +dyb];
                    zlinef[i] = Phi[zyIdx +i +dzf];
                    zlineb[i] = Phi[zyIdx +i +dzb];
                }

                // fill overhang 
                //#pragma omp simd 
                for (ushort i=0;i<c;i++) {
                    xline[i] = xline[lx*dx + i];
                    xline[(lx+1)*dx +i] = xline[dx +i];
                }

                // iterate over sites TODO: explicitely vectorize?!
                for (uint x=0;x<lx;x++) {
                    Tfloat meffsq = mu0sq + 6.0, diff[c];
                    for (ushort i=0;i<c;i++) {
                        meffsq += g0/(6*c)*SQR(xline[(x+1)*dx +i]);   // effective msqr
                        diff[i] = (xline[x*dx+i] + xline[(x+2)*dx+i])
                            + (ylinef[x*dx+i] + ylineb[x*dx+i])
                            + (zlinef[x*dx+i] + zlineb[x*dx+i]);
                    }

                    diff[0] -= J;

                    for (ushort i=0;i<c;i++) {
                        Pi[zyIdx +dx*x +i] += dt*FMA(-1.0*SQR(SQR(a))*meffsq, xline[(x+1)*dx+i], SQR(a)*diff[i]);
                    }
                }

        }

    } else if (c==1) {
        for (uint y=0;y<ly;y++) {
            long int dyf = d_indexer->getdyf(y), dyb = d_indexer->getdyb(y);

            size_t zyIdx = z*dz + y*dy;

            Tfloat 
                xline[lx+2], // xline has halo of 1
                ylinef[lx],
                ylineb[lx],
                zlinef[lx],
                zlineb[lx];

                // fill buffer TODO: vectorize! ("vector version will never be profitable"?!)
                // #pragma omp simd collapse(2)
                for (uint x=0;x<lx;x++) {
                    xline[x+1] = Phi[zyIdx +dx*x];
                    ylinef[x] = Phi[zyIdx +dx*x +dyf];
                    ylineb[x] = Phi[zyIdx +dx*x +dyb];
                    zlinef[x] = Phi[zyIdx +dx*x +dzf];
                    zlineb[x] = Phi[zyIdx +dx*x +dzb];
                }

                // fill overhang
                xline[0] = xline[lx];
                xline[lx+1] = xline[1];

                // iterate over sites 
                for (uint x=0;x<lx;x++) {
                    Tfloat meffsq = mu0sq + 6.0, diff; 
                    meffsq += g0/(6*c)*SQR(xline[x+1]);   // effective msqr

                    diff = -J;
                    diff += (xline[x] + xline[x+2])
                        + (ylinef[x] + ylineb[x])
                        + (zlinef[x] + zlineb[x]);

                    Pi[zyIdx +dx*x] += dt*FMA(-1.0*SQR(SQR(a))*meffsq, xline[x+1], SQR(a)*diff);
                }
        }

    }
}



void stepMomentumConsZplane(Tfloat *Pi, Tfloat *Phi,
        Tfloat dt, Tfloat mu0sq, Tfloat g0, Tfloat a, Tfloat J,
        const lattice3DIndex * const d_indexer, const int z) 
{
    const size_t
        lx = d_indexer->getLx(), 
        ly = d_indexer->getLy(), 
        dx = d_indexer->getDx(), 
        dy = d_indexer->getDy(),
        dz = d_indexer->getDz();

    const short c = d_indexer->getComponents();

    long int 
        dzf = d_indexer->getdzf(z), 
        d2zf = dzf + d_indexer->getdzf(z+1), 
        dzb = d_indexer->getdzb(z),
        d2zb = dzb + d_indexer->getdzb(z-1);

    if (c>1) {
        std::runtime_error("not implemented yet");
    } else if (c==1) {
        for (uint y=0;y<ly;y++) {

            long int 
                dyf = d_indexer->getdyf(y), 
                d2yf = dyf + d_indexer->getdyf(y+1), 
                dyb = d_indexer->getdyb(y),
                d2yb = dyb + d_indexer->getdyb(y-1);

            size_t zyIdx = y*dy + z*dz;

            Tfloat 
                xline[lx+4], // xline has halo of 2

                ylinef[lx+2], 
                ylineb[lx+2],
                yline2f[lx], 
                yline2b[lx],

                zlineb[lx+2],
                zlinef[lx+2], 
                zline2f[lx], 
                zline2b[lx],

                yfzfline[lx],
                yfzbline[lx],
                ybzfline[lx],
                ybzbline[lx];

            // fill buffer
            for (uint i=0;i<lx;i++) {
                xline[i+2] = Phi[zyIdx +i];

                ylinef[i+1] = Phi[zyIdx +i +dyf];
                ylineb[i+1] = Phi[zyIdx +i +dyb];

                yline2f[i] = Phi[zyIdx +i +d2yf];
                yline2b[i] = Phi[zyIdx +i +d2yb];


                zlinef[i+1] = Phi[zyIdx +i +dzf];
                zlineb[i+1] = Phi[zyIdx +i +dzb];

                zline2f[i] = Phi[zyIdx +i +d2zf];
                zline2b[i] = Phi[zyIdx +i +d2zb];

                yfzfline[i] = Phi[zyIdx +i +dyf +dzf];
                yfzbline[i] = Phi[zyIdx +i +dyf +dzb];
                ybzfline[i] = Phi[zyIdx +i +dyb +dzf];
                ybzbline[i] = Phi[zyIdx +i +dyb +dzb];
            }

            // fill overhang
            // x-center: x+2
            xline[0] = xline[lx];
            xline[1] = xline[lx+1];
            xline[lx+2] = xline[2];
            xline[lx+3] = xline[3];

            // yfb-center: x+1
            ylinef[0] = ylinef[lx];
            ylinef[lx+1] = ylinef[1];
            ylineb[0] = ylineb[lx];
            ylineb[lx+1] = ylineb[1];

            zlinef[0] = zlinef[lx];
            zlinef[lx+1] = zlinef[1];
            zlineb[0] = zlineb[lx];
            zlineb[lx+1] = zlineb[1];

            // iterate over sites
            for (uint x=0;x<lx;x++) {
                // aliasing
                Tfloat 
                    phi0 = xline[x+2],
                    phixf       = xline[x+3],
                    phixb       = xline[x+1],
                    phiyf       = ylinef[x+1],
                    phiyb       = ylineb[x+1],
                    phizf       = zlinef[x+1],
                    phizb       = zlineb[x+1],

                    phix2f      = xline[x+4],
                    phixfyf     = ylinef[x+2],
                    phixfyb     = ylineb[x+2],
                    phixfzf     = zlinef[x+2],
                    phixfzb     = zlineb[x+2],

                    phix2b      = xline[x+0],
                    phixbyf     = ylinef[x+0],
                    phixbyb     = ylineb[x+0],
                    phixbzf     = zlinef[x+0],
                    phixbzb     = zlineb[x+0],

                    phiy2f      = yline2f[x],
                    phiyfzf     = yfzfline[x],
                    phiyfzb     = yfzbline[x],

                    phiy2b      = yline2b[x],
                    phiybzf     = ybzfline[x],
                    phiybzb     = ybzbline[x],

                    phiz2f      = zline2f[x],
                    phiz2b      = zline2b[x];

                // laplace phi_x = phi_x+1 + phi_x-1 - 2*phi_x
                Tfloat lapphi = 
                    xline[x+3] + xline[x+1] + 
                    ylinef[x+1] + ylineb[x+1] + 
                    zlinef[x+1] + zlineb[x+1] 
                    - 6*phi0;

                // laplace phi^3_x = phi^3_x+1 + phi^3_x-1 - 2*phi^3_x
                Tfloat lapphi3 = 
                    CUB(phixf) + CUB(phixb) + 
                    CUB(phiyf) + CUB(phiyb) + 
                    CUB(phizf) + CUB(phizb) + 
                    -6*CUB(phi0);
                /*
                   Tfloat lapphi3 = 
                   CUB(xline[x+3]) + CUB(xline[x+1]) + 
                   CUB(ylinef[x+1]) + CUB(ylineb[x+1]) + 
                   CUB(zlinef[x+1]) + CUB(zlineb[x+1]) 
                   - 6*CUB(phi0);
                   */

                // laplace laplace phi_x = laplace (phi_x+1 + phi_x-1 - 2*phi_x) = ...
                /*
                   Tfloat laplapphi = 
                   xline[x] + xline[x+4] + yline2f[x] + yline2b[x] + zline2f[x] + zline2b[x] +
                   2*(ylinef[x] + ylinef[x+2] + ylineb[x] + ylineb[x+2] + zlinef[x] + zlinef[x+2] + zlineb[x] + zlineb[x+2]) +
                   - 12*(xline[x+1] + xline[x+3] + ylinef[x+1] + ylineb[x+1] + zlinef[x+1] + zlineb[x+1])
                   + 42*phi0;
                   */

                Tfloat laplapphi = 
                    (phix2f + phiy2f + phix2b + phiy2b + phiz2f + phiz2b) 
                    + 2*(phixfyf + phixfyb + phixfzf + phixfzb + phixbyf + phixbyb + phixbzf + phixbzb + phiyfzf + phiyfzb + phiybzf + phiybzb) 
                    - 12*(phixf + phixb + phiyf + phiyb + phizf + phizb) 
                    + 42*phi0;

                /*if (x==1 && y==1 && z==1) {
                  BOOST_LOG_TRIVIAL(trace) << "------------------------------------------------------------";
                  BOOST_LOG_TRIVIAL(trace) << "x=" << x <<", y="<<y<<", z="<< z;
                  BOOST_LOG_TRIVIAL(trace) << "============================================================";
                  BOOST_LOG_TRIVIAL(trace) << "\t\t\t\t" << phiz2f;
                  BOOST_LOG_TRIVIAL(trace) << "";

                  BOOST_LOG_TRIVIAL(trace) << "\t\t\t\t" << phiyfzf;
                  BOOST_LOG_TRIVIAL(trace) << "\t\t" << phixbzf << "\t" << phizf << "\t" << phixfzf;
                  BOOST_LOG_TRIVIAL(trace) << "\t\t\t\t" << phiybzf;
                  BOOST_LOG_TRIVIAL(trace) << "";

                  BOOST_LOG_TRIVIAL(trace) << "\t\t\t\t" << phiy2f;
                  BOOST_LOG_TRIVIAL(trace) << "\t\t" << phixbyf << "\t" << phiyf << "\t" << phixfyf;
                  BOOST_LOG_TRIVIAL(trace) << phix2b << "\t" << phixb << "\t" << phi0 << "\t" << phixf << "\t" << phix2f;
                  BOOST_LOG_TRIVIAL(trace) << "\t\t" << phixbyb << "\t" << phiyb << "\t" << phixfyb;
                  BOOST_LOG_TRIVIAL(trace) << "\t\t\t\t" << phiy2b;
                  BOOST_LOG_TRIVIAL(trace) << "";

                  BOOST_LOG_TRIVIAL(trace) << "\t\t\t\t" << phiyfzb;
                  BOOST_LOG_TRIVIAL(trace) << "\t\t" << phixbzb << "\t" << phizb << "\t" << phixfzb;
                  BOOST_LOG_TRIVIAL(trace) << "\t\t\t\t" << phiybzb;
                  BOOST_LOG_TRIVIAL(trace) << "";

                  BOOST_LOG_TRIVIAL(trace) << "\t\t\t\t" << phiz2b;
                  BOOST_LOG_TRIVIAL(trace) << "";

                  BOOST_LOG_TRIVIAL(trace) << "lp3=" << lapphi3 << ", llp=" << laplapphi << ", lap=" << lapphi;
                  BOOST_LOG_TRIVIAL(trace) << "";
                  }
                  */

                Pi[zyIdx +dx*x] += dt*(-laplapphi + mu0sq*lapphi + g0/6.0*lapphi3);
            }
        }
    }
}


void stepMomentumYline(Tfloat *Pi, Tfloat *Phi,
        Tfloat dt, Tfloat mu0sq, Tfloat g0, Tfloat a, Tfloat J,
        const lattice2DIndex * const d_indexer, const int y) {

    const size_t
        lx = d_indexer->getLx(), 
        dx = d_indexer->getDx(), 
        dy = d_indexer->getDy();

    const short c = d_indexer->getComponents();

    long int dyf = d_indexer->getdyf(y), dyb = d_indexer->getdyb(y);
    size_t yIdx = y*dy;

    Tfloat xline[(lx+2)*c], // xline has halo of 1
           ylinef[lx*c], 
           ylineb[lx*c];

    // fill buffer
    for (uint i=0;i<lx*c;i++) {
        xline[i+dx] = Phi[yIdx +i];
        ylinef[i] = Phi[yIdx +i +dyf];
        ylineb[i] = Phi[yIdx +i +dyb];
    }

    if (c>1) {

        // fill overhang 
        for (ushort i=0;i<c;i++) {
            xline[i] = xline[lx*dx + i];
            xline[(lx+1)*dx +i] = xline[dx +i];
        }

        // iterate over sites
        for (uint x=0;x<lx;x++) {
            Tfloat meffsq = mu0sq + 4.0, diff[c];

            for (ushort i=0;i<c;i++) {
                meffsq += g0/(6*c)*SQR(xline[(x+1)*dx +i]);
                diff[i] = (xline[x*dx + i] + xline[(x+2)*dx +i])
                    + (ylinef[x*dx+i] + ylineb[x*dx+i]);
            }

            diff[0] -= J;

            for (ushort i=0;i<c;i++)
                Pi[yIdx +dx*x +i] += dt*FMA(-1.0*SQR(SQR(a))*meffsq, xline[(x+1)*dx+i], SQR(a)*diff[i]);
        }
    } else if (c==1) {

        // fill overhang
        xline[0] = xline[lx];
        xline[lx+1] = xline[1];

        // iterate over sites
        for (uint x=0;x<lx;x++) {
            Tfloat meffsq = mu0sq + 4.0;
            meffsq += g0/(6*c)*SQR(xline[x+1]);

            Tfloat diff = -J;
            diff += (xline[x] + xline[x+2])
                + (ylinef[x] + ylineb[x]);


            Pi[yIdx +dx*x] += dt*FMA(-1.0*SQR(SQR(a))*meffsq, xline[x+1], SQR(a)*diff);
        }
    }
}

void stepMomentumConsYline(Tfloat *Pi, Tfloat *Phi,
        Tfloat dt, Tfloat mu0sq, Tfloat g0, Tfloat a, Tfloat J,
        const lattice2DIndex * const d_indexer, const int y) 
{
    const size_t
        lx = d_indexer->getLx(), 
        dx = d_indexer->getDx(), 
        dy = d_indexer->getDy();

    const short c = d_indexer->getComponents();

    long int 
        dyf = d_indexer->getdyf(y), 
        d2yf = dyf + d_indexer->getdyf(y+1), 
        dyb = d_indexer->getdyb(y),
        d2yb = dyb + d_indexer->getdyb(y-1);

    size_t yIdx = y*dy;

    Tfloat xline[(lx+4)*c], // xline has halo of 1
           ylinef[(lx+2)*c], 
           yline2f[lx*c], 
           ylineb[(lx+2)*c],
           yline2b[lx*c];

    // fill buffer
    for (uint i=0;i<lx*c;i++) {
        xline[i+2*dx] = Phi[yIdx +i];
        ylinef[i+dx] = Phi[yIdx +i +dyf];
        ylineb[i+dx] = Phi[yIdx +i +dyb];
        yline2f[i] = Phi[yIdx +i +d2yf];
        yline2b[i] = Phi[yIdx +i +d2yb];
    }

    if (c>1) {
        std::runtime_error("not implemented yet");
    } else if (c==1) {

        // fill overhang
        // x-center: x+2
        xline[0] = xline[lx];
        xline[1] = xline[lx+1];
        xline[lx+2] = xline[2];
        xline[lx+3] = xline[3];

        // yfb-center: x+1
        ylinef[0] = ylinef[lx];
        ylinef[lx+1] = ylinef[1];
        ylineb[0] = ylineb[lx];
        ylineb[lx+1] = ylineb[1];

        // iterate over sites
        for (uint x=0;x<lx;x++) {
            Tfloat phi0 = xline[x+2];
            
            // laplace phi_x = phi_x+1 + phi_x-1 - 2*phi_x
            Tfloat lapphi = xline[x+3] + xline[x+1] + ylinef[x+1] + ylineb[x+1] - 4*phi0;

            // laplace laplace phi_x = laplace (phi_x+1 + phi_x-1 - 2*phi_x) = ...
            Tfloat laplapphi = (xline[x] + xline[x+4] + yline2f[x] + yline2b[x]) 
                + 2*(ylinef[x] + ylinef[x+2] + ylineb[x] + ylineb[x+2])
                - 8*(xline[x+1] + xline[x+3] + ylinef[x+1] + ylineb[x+1])
                +20*phi0;

            // laplace phi^3_x = phi^3_x+1 + phi^3_x-1 - 2*phi^3_x
            Tfloat lapphi3 = CUB(xline[x+3]) + CUB(xline[x+1]) + CUB(ylinef[x+1]) + CUB(ylineb[x+1]) - 4*CUB(phi0);


            Pi[yIdx +dx*x] += dt*(-laplapphi + mu0sq*lapphi + g0/6.0*lapphi3);
        }
    }
}

/**
 * @Synopsis maps (grad phi)^2 onto phi and reduces one z-plane
 *
 * @Param phi           the field
 * @Param d_indexer a lattice indexer to mind boundaries
 * @Param z          z coordinate of the plane
 *
 * @Returns square gradient on given z-plane
 */
Tfloat squareGradientZplane(Tfloat *phi, const lattice3DIndex * const d_indexer, const int z) {
    Tfloat planeGrad = 0.0;

    const size_t 
        lx = d_indexer->getLx(), 
           ly = d_indexer->getLy(), 
           dx = d_indexer->getDx(), 
           dy = d_indexer->getDy(), 
           dz = d_indexer->getDz();

    const short c = d_indexer->getComponents();

    long int dzf = d_indexer->getdzf(z);
    for (uint y=0;y<ly;y++) {
        // forward and backward derivative step y
        long int dyf= d_indexer->getdyf(y);

        size_t zyIdx = z*dz + y*dy;

        Tfloat linegrad=0.0;

        for (uint i=0;i<lx*dx;i++) // TODO: expl. vectorize i.e. unroll
            linegrad += SQR(phi[zyIdx +dyf +i] - phi[zyIdx +i]) + SQR(phi[zyIdx +dzf +i] - phi[zyIdx +i]);

        for (uint i=0;i<(lx-1)*dx;i++)
            linegrad += SQR(phi[zyIdx +dx +i] - phi[zyIdx +i]);

        for (ushort ci=0;ci<c;ci++)
            linegrad += SQR(phi[zyIdx +ci] - phi[zyIdx +(lx-1)*dx +ci]);


        planeGrad += linegrad;
    }

    return planeGrad;
}


void addGradientZplane(Tfloat * const target, const Tfloat * const v, const lattice3DIndex * const d_indexer, const int z) {
    const size_t
        lx = d_indexer->getLx(), 
        ly = d_indexer->getLy(), 
        dx = d_indexer->getDx(), 
        dy = d_indexer->getDy(),
        dz = d_indexer->getDz(),
        size = d_indexer->getPoints();

    const short comp = d_indexer->getComponents();
    const long int dzf= d_indexer->getdzf(z);
    const size_t zIdx = z*dz;

    for (size_t y=0;y<ly;y++) {
        // forward derivative step y
        const long int dyf= d_indexer->getdyf(y);
        const size_t zyIdx = zIdx + y*dy;

        for (size_t x=0;x<lx;x++) {
            const int dxf = d_indexer->getdxf(x);

            for (ushort c=0;c<comp;c++)
                target[zyIdx + x + c] += v[zyIdx + x + dxf + c] - v[zyIdx + x + c] + v[size + zyIdx + x + dyf + c] - v[size + zyIdx + x + c] + v[2*size + zyIdx + x + dzf + c] - v[2*size + zyIdx + x + c];
        }
    }
}




Tfloat squareGradientYline(Tfloat *phi, const lattice2DIndex * const d_indexer, const int y) {
    Tfloat lineGrad = 0.0;

    const size_t
        lx = d_indexer->getLx(), 
           dx = d_indexer->getDx(), 
           dy = d_indexer->getDy();

    const short c = d_indexer->getComponents();

    // forward and backward derivative step y
    long int dyf= d_indexer->getdyf(y);//, dyb= d_indexer->getdyb(y);
    size_t yIdx = y*dy;

    for (uint i=0;i<lx*dx;i++)
        lineGrad += SQR(phi[yIdx +dyf +i] - phi[yIdx +i]);

    for (uint i=0;i<(lx-1)*dx;i++)
        lineGrad += SQR(phi[yIdx +dx +i] - phi[yIdx +i]);

    for (ushort ci=0;ci<c;ci++)
        lineGrad += SQR(phi[yIdx +ci] - phi[yIdx +(lx-1)*dx +ci]);

    return lineGrad;
}


Tfloat squareGradSqYline(Tfloat *phi, const lattice2DIndex * const d_indexer, const int y) {
    Tfloat lineGrad = 0.0;

    const size_t
        lx = d_indexer->getLx(), 
           dx = d_indexer->getDx(), 
           dy = d_indexer->getDy();

    const short c = d_indexer->getComponents();

    // forward and backward derivative step y
    long int dyf= d_indexer->getdyf(y), dyb= d_indexer->getdyb(y);
    size_t yIdx = y*dy;

    for (uint i=1;i<(lx-1)*dx;i++)
        lineGrad += SQR(phi[yIdx +dyf +i] + phi[yIdx +dyb +i] + phi[yIdx +dx +i] + phi[yIdx -dx +i]- 4*phi[yIdx +i]);

    // boundary terms
    for (ushort ci=0;ci<c;ci++) {
        lineGrad += SQR(phi[yIdx +dyf +ci] + phi[yIdx +dyb +ci] + phi[yIdx +(lx-1)*dx +ci] + phi[yIdx +dx +ci] -4*phi[yIdx +ci]); // beginning of line
        lineGrad += SQR(phi[yIdx +(lx-1)*dx +dyf +ci] + phi[yIdx +(lx-1)*dx +dyb +ci] + phi[yIdx +ci] + phi[yIdx +(lx-2)*dx +ci] -4*phi[yIdx +(lx-1)*dx +ci]); // end of line
    }

    return lineGrad;
}


/**
 * @Synopsis Compute sum_x phi_x \nabla^2 (phi_x^3) = \sum_x \phi_x (\phi_{x+-mu} - 2d \phi_x)
 *
 * @Paramphi
 * @Paramd_indexer
 * @Paramy
 *
 * @Returns
 */
Tfloat squareGradQrtYline(Tfloat *phi, const lattice2DIndex * const d_indexer, const int y) {
    Tfloat lineSum = 0.0;

    const size_t
        lx = d_indexer->getLx(), 
           dx = d_indexer->getDx(), 
           dy = d_indexer->getDy();

    const short c = d_indexer->getComponents();

    // forward and backward derivative step y
    long int dyf= d_indexer->getdyf(y), dyb= d_indexer->getdyb(y);
    size_t yIdx = y*dy;

    for (uint i=1;i<(lx-1)*dx;i++) {
        const size_t pos = yIdx + i*dx;
        lineSum += phi[pos]*(CUB(phi[pos +dyf]) + CUB(phi[pos +dyb]) + CUB(phi[pos +dx]) + CUB(phi[pos -dx]) - 4*CUB(phi[pos]));
    }

    for (ushort ci=0;ci<c;ci++) {
        const size_t ipos = yIdx + ci;
        const size_t vpos = yIdx + (lx-1)*dx + ci;

        lineSum += phi[ipos]*(CUB(phi[ipos +dyf]) + CUB(phi[ipos +dyb]) + CUB(phi[ipos +dx]) + CUB(phi[vpos]) - 4*CUB(phi[ipos]));
        lineSum += phi[vpos]*(CUB(phi[vpos +dyf]) + CUB(phi[vpos +dyb]) + CUB(phi[ipos]) + CUB(phi[vpos -dx]) - 4*CUB(phi[vpos]));
    }

    return lineSum;
}


void addGradientYline(Tfloat * const target, const Tfloat * const v, const lattice2DIndex * const d_indexer, const int y) {
    const size_t
        lx = d_indexer->getLx(), 
        dx = d_indexer->getDx(), 
        dy = d_indexer->getDy(),
        size = d_indexer->getPoints();

    const short comp = d_indexer->getComponents();

    // forward derivative step y
    const long int dyf= d_indexer->getdyf(y);
    const size_t yIdx = y*dy;

    // line w halo
    for (size_t x=0;x<lx;x++) {
        const int dxf = d_indexer->getdxf(x);
        
        for (ushort c=0;c<comp;c++)
            target[yIdx + x*dx + c] += v[yIdx + x*dx + dxf + c] - v[yIdx + x*dx + c] + v[size + yIdx + x*dx + dyf +c] - v[size + x*dx + yIdx + c];
    }
}



/**
 * @Synopsis  This one needs to be aware of the full lattice to properly wrap around it, so give start and end explicitely!
 *
 * @Param offset between phi(x)phi(y), i.e. offset = y - x
 * @Param array the field
 * @Param startIdx      where to start the loop over the first variable
 * @Param endIdx        where to end === "" ===
 * @Param d_indexer     lattice index helper
 * @Param size  array size
 */
Tfloat sliceCorrelationPlane(size_t offset, Tfloat *phi, const lattice3DIndex* const d_indexer, const int z) {
    Tfloat planeCorrelation = 0.0;
    const size_t 
           ly = d_indexer->getLy(), 
           dy = d_indexer->getDy(), 
           dz = d_indexer->getDz();

    for (size_t k=0;k<(ly*dy);k++)
        planeCorrelation += phi[z*dz + k] * phi[(z+offset)*dz + k];

    return planeCorrelation;
}

Tfloat lineCorrelation(size_t offset, Tfloat *phi, const lattice2DIndex* const d_indexer, const int y) {
    Tfloat lineCorrelation = 0.0;
    const size_t
        lx = d_indexer->getLx(), 
           dx = d_indexer->getDx(), 
           dy = d_indexer->getDy();

    for (size_t k=0;k<(lx*dx);k++)
        lineCorrelation += phi[y*dy + k] * phi[(y+offset)*dy + k];

    return lineCorrelation;
}


void invKsqrLine(ffTcomplex * const field, bool abs, const lattice2DIndex * const d_indexer, const int y)
{
    size_t ky = y;

    size_t lx = d_indexer->getLx(), ly = d_indexer->getLy(), L2 = lx/2+1;

    for (size_t kx=0;kx<L2;kx++){
        size_t idx = ky*L2+kx;
        if (idx==0) {
            field[idx][0] = 0;
            field[idx][1] = 0;
        }
        else {
            Tfloat qsqr = 2*cos(kx*2*M_PI/lx) + 2*cos(ky*2*M_PI/ly) - 4;

            if(abs) {
                Tfloat abssqr = SQR(field[idx][0]) + SQR(field[idx][1]);
                if (kx != 0 && kx != lx-kx) abssqr *= 2.;

                field[idx][0] = abssqr/qsqr;
                field[idx][1] = 0.;
            } else {
                field[idx][0] /= qsqr;
                field[idx][1] /= qsqr;
            }
        }
    }
}

void invKsqrPlane(ffTcomplex * const field, bool abs, const lattice3DIndex * const d_indexer, const int z)
{
    const size_t kz = z;

    size_t lx = d_indexer->getLx(), ly = d_indexer->getLy(), lz = d_indexer->getLz(),
           L2 = lx/2+1;

    for (size_t ky=0;ky<ly;ky++) {
        for (size_t kx=0;kx<L2;kx++) {
            size_t idx = kz*ly*L2 + ky*L2+kx;
            if (idx==0) {
                field[idx][0] = 0;
                field[idx][1] = 0;
            }
            else {
                Tfloat qsqr = 2*cos(kx*2*M_PI/lx) 
                            + 2*cos(ky*2*M_PI/ly)
                            + 2*cos(kz*2*M_PI/lz) - 6;

                if(abs) {
                    Tfloat abssqr = SQR(field[idx][0]) + SQR(field[idx][1]);
                    if (kx != 0 && kx != lx-kx) abssqr *= 2.;

                    field[idx][0] = abssqr/qsqr;
                    field[idx][1] = 0.;
                } else {
                    //printf("idx: %lu\t\tz=%lu\ty=%lu\tx=%lu\tpi=%.4e\tqsqr=%e\n", idx, kz, ky, kx,field[idx][0],qsqr);
                
                    field[idx][0] /= qsqr;
                    field[idx][1] /= qsqr;
                }
            }
        }
    }
}
