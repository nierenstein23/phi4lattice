# Phi4Lattice

A code for simulating phi^4 theory on a lattice.

## Getting Started

Clone this git repository and use cmake to build it.

### Prerequisites

Boost libraries with minimum version 1.65.0 are required for building the
application. 
CUDA is optional, but highly recommended.

```
Give examples
```

### Installing

```
make install
```

... as soon as I integrated it into the CMakeLists.txt

To get some sensible data out, start with a sensible temperature, e.g. no
lower than 7.0:

```
./hybridDynamics -T 10
```

## Running the tests

TODO: Explain how to run the automated tests for this system

### Break down into end to end tests

TODO: Explain what these tests test and why

```
Give an example
```

### And coding style tests

TODO: Explain what these tests test and why

```
Give an example
```

## Deployment

TODO: Add additional notes about how to deploy this on a live system

## Built With

* [Boost](https://www.boost.org/) - For convenience when parsing
  complex program options, dealing with files etc.
* [CUDA](https://developer.nvidia.com/cuda-zone) - Use GPUs when simulating, for maximum
  performance

## Contributing

Please don't. This project is still private anyway...

## Versioning

I should start using git tags.

## Authors

* **Dominik Schweitzer** - *Master/Doctoral Thesis*

## License

This project is licensed under the GNU AGPLv3 License - so don't you dare do
anything closed source with it.
Actually, nevermind - I do not yet know enough about license stuff.

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
