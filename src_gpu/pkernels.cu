#include <pkernels.hcu>
#include <stdio.h>

#ifdef __clang__
#define __CUDACC__
#include <__clang_cuda_builtin_vars.h>
#include <device_functions.h>
#endif

__global__ void createIndexOnGPU(const latticeIndex ** const d_index, size_t x, size_t y, size_t z, unsigned short comp)
{ *d_index = new lattice3DIndex(x, y, z, comp); }
__global__ void createIndexOnGPU(const latticeIndex ** const d_index, size_t x, size_t y, unsigned short comp)
{ *d_index = new lattice2DIndex(x, y, comp); }

__global__ void freeIndexOnGPU(const lattice3DIndex ** const d_index) { delete *d_index; };
__global__ void freeIndexOnGPU(const lattice2DIndex ** const d_index) { delete *d_index; };

/**
 * @Synopsis  maps the function x*x onto a vector
 *
 * @Param src   the vector
 * @Param size  size of the vector
 */
__global__ void mapSquareKernel(Tfloat *src, size_t size) {
    const size_t idx = blockDim.x * blockIdx.x + threadIdx.x; if(idx >= size) return;

    Tfloat tmp = src[idx];
    src[idx] = tmp*tmp;
};

__global__ void mapSquareKernel(Tfloat *dest, Tfloat *src, size_t size) {
    const size_t idx = blockDim.x * blockIdx.x + threadIdx.x; if(idx >= size) return;

    dest[idx] = SQR(src[idx]);
};

/**
 * @Synopsis  maps the function (x*x)*(x*x) onto a vector
 *
 * @Param src   the vector
 * @Param size  size of the vector
 */
__global__ void mapVecQuartKernel(Tfloat *tar, Tfloat *src, ushort components, size_t size) {
    const size_t Kidx = blockDim.x * blockIdx.x + threadIdx.x; if(Kidx >= size) return;

    Tfloat tmp = 0.0;
    for (ushort i=0;i<components;i++)
        tmp += SQR(src[Kidx * components + i]);

    tar[Kidx] = tmp*tmp;
};


/**
 * @Synopsis  Copies every (offset + n*stride)-th element of the src vector into the target vector.
 *
 * @Param src       source vector
 * @Param target    target vector
 * @Param stride    steps between elements
 * @Param offset    number of the starting element
 * @Param size      size of the source vector
 */
__global__ void copyStrideKernel(Tfloat *src, Tfloat *target, size_t stride, size_t offset, size_t size) {
    const int idx = blockDim.x * blockIdx.x + threadIdx.x; 
    if(offset + idx*stride >= size) return;

    target[idx] = src[offset + idx*stride];
}

__global__ void saxpyKernel(Tfloat *result, Tfloat a, Tfloat *x, Tfloat *y, size_t size) {
    const size_t idx = blockDim.x * blockIdx.x + threadIdx.x; if(idx >= size) return;

    result[idx] = FMA(a, x[idx], y[idx]);
}

__global__ void addConstKernel(Tfloat *result, Tfloat a, Tfloat *x, size_t size) {
    const size_t idx = blockDim.x * blockIdx.x + threadIdx.x; if(idx >= size) return;

    result[idx] = a + x[idx];
}

__global__ void scaleKernel(Tfloat * const dest, const Tfloat a, const Tfloat * const x, size_t size)
{
    const size_t idx = blockDim.x * blockIdx.x + threadIdx.x; if(idx >= size) return;
    dest[idx] = a*x[idx];
}


__global__ void addGradKernel(Tfloat *target, Tfloat *v, clIdx2cpt d_index, size_t size)
{
    const size_t Kidx = blockDim.x * blockIdx.x + threadIdx.x; if(Kidx >= size) return;

    const latticeIndex * const d_indexer = *d_index;
    const ushort d = d_indexer->getD(),
          comp = d_indexer->getComponents(); 
    const size_t idx = comp*Kidx;

    for (ushort c=0;c<comp;c++) {
    Tfloat grad = 0.;
        for (short j=0;j<d;j++) {
            // forward difference index
            const long int djf = d_indexer->getNeighbour(idx, j+1) - idx;
            const size_t vIdx = j*comp*size + idx + c;

             grad += v[vIdx + djf] - v[vIdx];
        }

        target[idx+c] +=grad;
    }
}

__global__ void fillGaussianKernel(Tfloat *x, const Tfloat mean, const Tfloat sigma, taus_state * const state, size_t size) {
    const size_t idx = blockDim.x * blockIdx.x + threadIdx.x; if(idx >= size) return;

    x[idx] = generateGaussian(mean, sigma, &state[idx]);
};


__global__ void randomizeAndDampKernel(Tfloat *field, Tfloat noiseAmp, Tfloat damping, taus_state * const state, size_t size) {
    const size_t idx = blockDim.x * blockIdx.x + threadIdx.x; if(idx >= size) return;

    field[idx] += noiseAmp*generateGaussian(0.0, 1.0, &state[idx]) - damping * field[idx];
}


/**
 * @Synopsis  computes (grad phi)^2 in parallel asymmetrically, i.e. fetches only forward derivatives.
 *
 * @Param result
 * @Param phi
 * @Param d_index
 * @Param size
 *
 */
__global__ void squareGradientKernel(Tfloat * const result, const Tfloat * const phi, 
        clIdx2cpt d_index,
        size_t size) 
{
    // Kernel index != field variable index this time
    const size_t Kidx = blockDim.x * blockIdx.x + threadIdx.x; if(Kidx >= size) return;
    const latticeIndex * const d_indexer = *d_index;

    // get number of components from indexer, calculate field variable index.
    ushort d = d_indexer->getD(),
           components = d_indexer->getComponents(); 
    size_t idx = Kidx*components;

    Tfloat gradsq = 0.0;

    for (short j=1;j<=d;j++) {
        size_t fidx = d_indexer->getNeighbour(idx,j);

        for (ushort i=0;i<components;i++) 
            gradsq += SQR(phi[idx + i]-phi[fidx + i]);
    }

    result[Kidx] = gradsq;
};



/**
 * @Synopsis  computes (grad phi)^2 in parallel symmetrically
 *
 * @Param result
 * @Param phi
 * @Param d_index
 * @Param size
 *
 */
__global__ void symSquareGradientKernel(Tfloat * const result, const Tfloat * const phi, 
        clIdx2cpt d_index,
        size_t size) 
{
    // Kernel index != field variable index this time
    const size_t Kidx = blockDim.x * blockIdx.x + threadIdx.x; if(Kidx >= size) return;
    const latticeIndex * const d_indexer = *d_index;

    // get number of components from indexer, calculate field variable index.
    ushort d = d_indexer->getD(),
           components = d_indexer->getComponents(); 
    size_t idx = Kidx*components;

    Tfloat gradsq = 0.0;

    for (short j=1;j<=d;j++) {
        size_t fidx = d_indexer->getNeighbour(idx,j);
        size_t bidx = d_indexer->getNeighbour(idx,-j);

        for (ushort i=0;i<components;i++) 
            gradsq += 0.5*(SQR(phi[fidx + i]-phi[idx + i])
                         + SQR(phi[idx + i]-phi[bidx + i]));
    }

    result[Kidx] = gradsq;
    //if (Kidx==0) printf("T[0] = %.2f\t", 0.5*result[Kidx]);
};


__global__ void squareGradSqKernel(Tfloat *result, Tfloat *phi, 
        clIdx2cpt d_index,
        size_t size) 
{
    // Kernel index != field variable index this time
    const size_t Kidx = blockDim.x * blockIdx.x + threadIdx.x; if(Kidx >= size) return;

    const latticeIndex * const d_indexer = *d_index;

    // get number of components from indexer, calculate field variable index.
    ushort d = d_indexer->getD(),
           components = d_indexer->getComponents(); 
    size_t idx = Kidx*components;

    Tfloat gradsq = 0.0;

    for (short j=1;j<=d;j++) {
        size_t fidx = d_indexer->getNeighbour(idx,j);
        size_t bidx = d_indexer->getNeighbour(idx,-j);

        for (ushort i=0;i<components;i++) 
            gradsq += phi[fidx + i] + phi[bidx + i] -2*phi[idx + i];
    }

    result[Kidx] = SQR(gradsq);
}


__global__ void squareGradQrtKernel(Tfloat *result, Tfloat *phi, 
        clIdx2cpt d_index,
        size_t size) 
{
    // Kernel index != field variable index this time
    const size_t Kidx = blockDim.x * blockIdx.x + threadIdx.x; if(Kidx >= size) return;
    const latticeIndex * const d_indexer = *d_index;

    // get number of components from indexer, calculate field variable index.
    ushort d = d_indexer->getD(),
           components = d_indexer->getComponents(); 
    size_t idx = Kidx*components;

    Tfloat sum = 0.0;

    for (short j=1;j<=d;j++) {
        size_t fidx = d_indexer->getNeighbour(idx,j);
        size_t bidx = d_indexer->getNeighbour(idx,-j);

        for (ushort i=0;i<components;i++) 
            sum += phi[idx + i]*(CUB(phi[fidx + i]) + CUB(phi[bidx + i]) -2*CUB(phi[idx + i]));
    }

    result[Kidx] = sum;
}


/**
 * @Synopsis        transform input field with discrete (co-)sine of (px,py,pz), write to output field
 *
 * @Param result    output field
 * @Param phi       input field
 * @Param cos       real/imag switch
 * @Param px,py,pz  momentum vector
 * @Param d_index lattice indexer
 * @Param size      total size (boundary condition)
 */
__global__ void momentumTrafoKernel(Tfloat *result, Tfloat *phi, bool docos,
    int px, int py, int pz,
    cl3DIdx2cpt d_index, 
    size_t size) {

    // Kernel index != field variable index this time
    const size_t Kidx = blockDim.x * blockIdx.x + threadIdx.x; if(Kidx >= size) return;
    const lattice3DIndex * const d_indexer = *d_index;

    const Tfloat Lx = d_indexer->getLx(),
          Ly = d_indexer->getLy(),
          Lz = d_indexer->getLz();
    const ushort c = d_indexer->getComponents();
    
    if (c==1) {
        // find out x,y,z components of field variable index
        size_t x,y,z; d_indexer->deindex(Kidx, x,y,z);

        if (docos)
            result[Kidx] = phi[Kidx]*COS(2*M_PI*(px*x/Lx+py*y/Ly+pz*z/Lz));
        else
            result[Kidx] = phi[Kidx]*SIN(2*M_PI*(px*x/Lx+py*y/Ly+pz*z/Lz));
    } 
    else // c > 1, more complicated procedure; see comment below
    {
        // get number of components from indexer, calculate field variable index.
        ushort components = d_indexer->getComponents(); size_t idx = Kidx*components;

        // find out x,y,z components of field variable index
        size_t x,y,z; d_indexer->deindex(Kidx, x,y,z);

        // iterate over components
        for (ushort k=0;k<c;k++) {
            if (docos)
                result[k*size + Kidx] = phi[idx+k]*COS(2*M_PI*(px*x/Lx+py*y/Ly+pz*z/Lz));
            else 
                result[k*size + Kidx] = phi[idx+k]*SIN(2*M_PI*(px*x/Lx+py*y/Ly+pz*z/Lz));
        }
        
        /*
         * In case of c>1, the field is simultaneously transformed and reordered.
         * Since memory allocation is expensive, the buffer is recycled:
         * The transformed field is written to it, ordered by field component. 
         * Thereby, one can calculate the components of the observable by blockwise reduction of the buffer.
         */

    }
}

__global__ void momentumTrafoKernel(Tfloat *result, Tfloat *phi, bool docos,
    int px, int py,
    cl2DIdx2cpt d_index, 
    size_t size) {

    // Kernel index != field variable index this time
    const size_t Kidx = blockDim.x * blockIdx.x + threadIdx.x; if(Kidx >= size) return;
    const lattice2DIndex * const d_indexer = *d_index;

    const Tfloat Lx = d_indexer->getLx(), Ly = d_indexer->getLy();

    const ushort c = d_indexer->getComponents();
    
    if (c==1) {
        // find out x,y components of field variable index
        size_t x,y; d_indexer->deindex(Kidx, x,y);

        if (docos)
            result[Kidx] = phi[Kidx]*COS(2*M_PI*(px*x/Lx+py*y/Ly));
        else
            result[Kidx] = phi[Kidx]*SIN(2*M_PI*(px*x/Lx+py*y/Ly));
    } else {
        // get number of components from indexer, calculate field variable index.
        ushort components = d_indexer->getComponents(); size_t idx = Kidx*components;

        // find out x,y components of field variable index
        size_t x,y; d_indexer->deindex(Kidx, x,y);

        // iterate over components
        for (ushort k=0;k<c;k++) {
            if (docos)
                result[k*size + Kidx] = phi[idx+k]*COS(2*M_PI*(px*x/Lx+py*y/Ly));
            else 
                result[k*size + Kidx] = phi[idx+k]*SIN(2*M_PI*(px*x/Lx+py*y/Ly));
        }
    }
}


/* 
 * reduction kernel
 * start Ly blocks of Lx/2 threads (if Lx < 6144)
 * allocate Lx*sizeof(Tfloat) bytes of shared memory
 */
__global__ void ySumTrafoKernel(Tfloat *target, Tfloat *field, ushort stride, ushort offset,
    const ushort rfac, const size_t size) 
{
    // use shared memory for reduction
    extern __shared__ Tfloat buffer[];

    const int tidx = threadIdx.x;
    const uint y = blockIdx.x + gridDim.x*blockIdx.y + gridDim.x*gridDim.y*blockIdx.z ; // hopefully
    const uint lx = rfac*blockDim.x;

    const uint idx = lx * y + tidx;
    //if(rfac*idx >= size) return; TODO: think of safeguard if possible

    buffer[tidx]=0;
    for (unsigned short u=0;u<rfac;u++) {
        const Tfloat val = field[(idx + u*blockDim.x)*stride + offset];
        buffer[tidx] += val;
    }

    __syncthreads();

    for (unsigned int s=1;s<blockDim.x;s*=2) {
        if ((tidx%(2*s) == 0) && (tidx+s < blockDim.x)) {
            buffer[tidx] += buffer[tidx+s];
        }
        __syncthreads();
    }

    if (tidx==0) {
        target[y] = buffer[0];
    }
    __syncthreads();
}



/**
 * transform lattice fields to energy density field, 2D
 */
__global__ void energyDensityKernel(Tfloat * const target, Tfloat *phi, Tfloat *pi, Tfloat mu0sq, Tfloat g0, Tfloat a, Tfloat J,
        clIdx2cpt d_index,
        bool onlyV,
        size_t size)
{
    const int tidx = threadIdx.x;
    const size_t Kidx = blockDim.x * blockIdx.x + tidx; if (Kidx >= size) return;

    const latticeIndex * const d_indexer = *d_index;

    ushort components = d_indexer->getComponents(); size_t idx = Kidx*components;

    Tfloat pisqr = 0., phisqr = 0., gradsq = 0.;

    for (ushort c=0;c<components;c++) {
        for (short j=1;j<=d_indexer->getD();j++) {

            size_t fidx = d_indexer->getNeighbour(idx,j);
            size_t bidx = d_indexer->getNeighbour(idx,-j);

            //gradsq -= phi[idx+c]*(phi[fidx+c] + phi[bidx+c] - 2*phi[idx+c]);
            gradsq += .5*(SQR(phi[fidx+c] - phi[idx+c]) + SQR(phi[idx+c] - phi[bidx+c]));
        }

        phisqr += SQR(phi[idx + c]);
        if (!onlyV) pisqr += SQR(pi[idx + c]);
    }


    target[Kidx] = a*(0.5*pisqr + 0.5/SQR(a)*gradsq + 0.5*mu0sq*phisqr + g0/(24*components) * SQR(phisqr) + J*phi[idx]);
}


/**
 * @Synopsis  Kernel for updating lattice momenta
 *
 * @Param pi,phi fields
 * @Param a lattice spacing
 * @Param mu0sq,g0 Lagrange parameters
 * @Param d_index lattice indexer on the device with precalculated values
 */
__global__ void stepMomentumKernel(Tfloat *pi, Tfloat *phi, Tfloat dt, Tfloat mu0sq, Tfloat g0, Tfloat a, Tfloat J,
        // constant pointer to a constant indexer (on the device)
        cl3DIdx2cpt d_index,
        size_t size) 
{
    // Kernel index != field variable index this time
    const size_t Kidx = blockDim.x * blockIdx.x + threadIdx.x; if(Kidx >= size) return;
    const lattice3DIndex * const d_indexer = *d_index;
    ushort comp = d_indexer->getComponents();

    if (comp==1) {
        const size_t idx = Kidx;

        // find out x,y,z components of field variable index
        size_t x,y,z; d_indexer->deindex(idx, x,y,z);

        // will later be -(g0/(6N) phi_0^2 + m_0^2 + 6)
        Tfloat mmeffsq = -1.0*mu0sq - 6.0 - ((g0/6)*SQR(phi[idx])) , 
               diff = -J;

        for (short j=1;j<=3;j++) {
            // index in forward and backwards direction
            size_t fidx, bidx;
            fidx = d_indexer->getNeighbour(x,y,z,j);
            bidx = d_indexer->getNeighbour(x,y,z,-j);

            // FMA optimization potential..?
            diff += SQR(a)*(phi[fidx] + phi[bidx]);
        }

        // update pi lattice by dt
        pi[idx] += dt*(SQR(SQR(a))*mmeffsq*phi[idx] + diff);

    } else {

        // get number of components from indexer, calculate field variable index.
        size_t idx = Kidx*comp;

        // find out x,y,z components of field variable index
        size_t x,y,z; d_indexer->deindex(idx, x,y,z);

        // will later be -(g0/(6N) phi_0^2 + m_0^2 + 6)
        Tfloat mmeffsq = -1.0*mu0sq - 6.0, diff[NCMAX];

        for (int i=0;i<comp;i++) {
            mmeffsq -= g0/(6*comp) * SQR(phi[idx + i]);
            diff[i] = 0.0;
        }

        for (short j=1;j<=3;j++) {
            // index in forward and backwards direction
            size_t fidx, bidx;
            fidx = d_indexer->getNeighbour(x,y,z,j);
            bidx = d_indexer->getNeighbour(x,y,z,-j);

            for (ushort i=0;i<comp;i++) {
                // FMA optimization potential..?
                diff[i] += SQR(a)*(phi[fidx + i] + phi[bidx + i]);
            }
        }

        // incorporate source term
        diff[0] -= J;

        // update pi lattice by dt
        for (int i=0;i<comp;i++) {
            pi[idx + i] += dt*(SQR(SQR(a))*mmeffsq*phi[idx+i] + diff[i]);
        }
    }
};

__global__ void stepMomentumKernel(Tfloat *pi, Tfloat *phi, Tfloat dt, Tfloat mu0sq, Tfloat g0, Tfloat a, Tfloat J,
        cl2DIdx2cpt d_index,
        size_t size) 
{
    // Kernel index != field variable index this time
    const size_t Kidx = blockDim.x * blockIdx.x + threadIdx.x; if(Kidx >= size) return;
    const lattice2DIndex * const d_indexer = *d_index;

    if (d_indexer->getComponents()==1) {
        const size_t idx = Kidx;

        // find out x,y,z components of field variable index
        size_t x,y; d_indexer->deindex(idx, x,y);

        // will later be -(g0/(6N) phi_0^2 + m_0^2 + 6)
        Tfloat mmeffsq = -1.0*mu0sq - 4.0 - ((g0/6)*SQR(phi[idx])) , 
               diff = -J;

        for (short j=1;j<=2;j++) {
            // index in forward and backwards direction
            size_t fidx, bidx;
            fidx = d_indexer->getNeighbour(x,y,j);
            bidx = d_indexer->getNeighbour(x,y,-j);

            // FMA optimization potential..?
            diff += SQR(a)*(phi[fidx] + phi[bidx]);
        }

        // update pi lattice by dt
        pi[idx] += dt*(SQR(SQR(a))*mmeffsq*phi[idx] + diff);

    } else {

        // get number of components from indexer, calculate field variable index.
        ushort components = d_indexer->getComponents(); size_t idx = Kidx*components;

        // find out x,y,z components of field variable index
        size_t x,y; d_indexer->deindex(idx, x,y);

        // will later be -(g0/(6N) phi_0^2 + m_0^2 + 2*d)
        Tfloat mmeffsq = -1.0*mu0sq - 4.0, diff[NCMAX];

        for (int i=0;i<components;i++) {
            mmeffsq -= g0/(6*components) * SQR(phi[idx + i]);
            diff[i] = 0.0;
        }

        for (short j=1;j<=2;j++) {
            // index in forward and backwards direction
            size_t fidx, bidx;
            fidx = d_indexer->getNeighbour(x,y,j);
            bidx = d_indexer->getNeighbour(x,y,-j);

            for (ushort i=0;i<components;i++) {
                // FMA optimization potential..?
                diff[i] += SQR(a)*(phi[fidx + i] + phi[bidx + i]);
            }
        }

        // incorporate source term
        diff[0] -= J;

        // update pi lattice by dt
        for (int i=0;i<components;i++) {
            pi[idx + i] += dt*(SQR(SQR(a))*mmeffsq*phi[idx+i] + diff[i]);
        }
    }
};

__global__ void stepMomentumConsKernel(Tfloat *pi, Tfloat *phi, Tfloat dt, Tfloat mu0sq, Tfloat g0, Tfloat a, Tfloat J,
        cl2DIdx2cpt d_index,
        size_t size) 
{
    // Kernel index != field variable index this time
    const size_t Kidx = blockDim.x * blockIdx.x + threadIdx.x; if(Kidx >= size) return;
    const lattice2DIndex * const d_indexer = *d_index;

    if (d_indexer->getComponents()==1) {
        const size_t idx = Kidx;

        // find out x,y,z components of field variable index
        size_t x,y; d_indexer->deindex(idx, x,y);
        const size_t ly = d_indexer->getLy();

        Tfloat 
            phi0        = phi[idx],
            phixf       = phi[idx + d_indexer->getdxf(x)],
            phixb       = phi[idx + d_indexer->getdxb(x)],
            phiyf       = phi[idx + d_indexer->getdyf(y)],
            phiyb       = phi[idx + d_indexer->getdyb(y)],
            phix2f      = phi[idx + d_indexer->getdxf(x) + d_indexer->getdxf(x+1)],
            phixfyb     = phi[idx + d_indexer->getdxf(x) + d_indexer->getdyb(y)],
            phiy2b      = phi[idx + d_indexer->getdyb(y) + d_indexer->getdyb(y-1)],
            phixbyb     = phi[idx + d_indexer->getdxb(x) + d_indexer->getdyb(y)],
            phix2b      = phi[idx + d_indexer->getdxb(x) + d_indexer->getdxb(x-1)],
            phixbyf     = phi[idx + d_indexer->getdxb(x) + d_indexer->getdyf(y)],
            phiy2f      = phi[idx + d_indexer->getdyf(y) + d_indexer->getdyf(y+1)],
            phixfyf     = phi[idx + d_indexer->getdxf(x) + d_indexer->getdyf(y)];


            // finite difference coefficients from: https://en.wikipedia.org/wiki/Finite_difference_coefficient
            // (nabla)^2 phi =
            Tfloat lapphi = phixf + phixb + phiyf + phiyb - 4*phi0;

            // (nabla^2)^2 phi =
            Tfloat laplapphi = (phix2f + phiy2f + phix2b + phiy2b) + 2*(phixfyb + phixbyb + phixbyf + phixfyf) - 8*(phixf + phixb + phiyf + phiyb) + 20*phi0;

            // (nabla^2) phi^3 =
            Tfloat lapphi3 = CUB(phixf) + CUB(phixb) + CUB(phiyf) + CUB(phiyb) - 4*CUB(phi0);

            //Tfloat source = J*SIN(2*M_PI/ly * y);

            Tfloat dPi = -laplapphi + mu0sq*lapphi/SQR(a) + g0/6.0*lapphi3;// + source;
            pi[idx] += dt*dPi;
    }
}


__global__ void stepMomentumConsKernel(Tfloat *pi, Tfloat *phi, Tfloat dt, Tfloat mu0sq, Tfloat g0, Tfloat a, Tfloat J,
        cl3DIdx2cpt d_index,
        size_t size) 
{
    // Kernel index != field variable index this time
    const size_t Kidx = blockDim.x * blockIdx.x + threadIdx.x; if(Kidx >= size) return;
    const lattice3DIndex * const d_indexer = *d_index;

    if (d_indexer->getComponents()==1) {
        const size_t idx = Kidx;

        // find out x,y,z components of field variable index
        size_t x,y,z; d_indexer->deindex(idx, x,y,z);
        size_t lz = d_indexer->getLz();

        Tfloat 
            phi0        = phi[idx],

            phixf       = phi[idx + d_indexer->getdxf(x)],
            phixb       = phi[idx + d_indexer->getdxb(x)],
            phiyf       = phi[idx + d_indexer->getdyf(y)],
            phiyb       = phi[idx + d_indexer->getdyb(y)],
            phizf       = phi[idx + d_indexer->getdzf(z)],
            phizb       = phi[idx + d_indexer->getdzb(z)],

            phix2f      = phi[idx + d_indexer->getdxf(x) + d_indexer->getdxf(x+1)],
            phixfyf     = phi[idx + d_indexer->getdxf(x) + d_indexer->getdyf(y)],
            phixfyb     = phi[idx + d_indexer->getdxf(x) + d_indexer->getdyb(y)],
            phixfzf     = phi[idx + d_indexer->getdxf(x) + d_indexer->getdzf(z)],
            phixfzb     = phi[idx + d_indexer->getdxf(x) + d_indexer->getdzb(z)],

            phix2b      = phi[idx + d_indexer->getdxb(x) + d_indexer->getdxb(x-1)],
            phixbyf     = phi[idx + d_indexer->getdxb(x) + d_indexer->getdyf(y)],
            phixbyb     = phi[idx + d_indexer->getdxb(x) + d_indexer->getdyb(y)],
            phixbzf     = phi[idx + d_indexer->getdxb(x) + d_indexer->getdzf(z)],
            phixbzb     = phi[idx + d_indexer->getdxb(x) + d_indexer->getdzb(z)],

            phiy2f      = phi[idx + d_indexer->getdyf(y) + d_indexer->getdyf(y+1)],
            phiyfzf     = phi[idx + d_indexer->getdyf(y) + d_indexer->getdzf(z)],
            phiyfzb     = phi[idx + d_indexer->getdyf(y) + d_indexer->getdzb(z)],

            phiy2b      = phi[idx + d_indexer->getdyb(y) + d_indexer->getdyb(y-1)],
            phiybzf     = phi[idx + d_indexer->getdyb(y) + d_indexer->getdzf(z)],
            phiybzb     = phi[idx + d_indexer->getdyb(y) + d_indexer->getdzb(z)],

            phiz2f      = phi[idx + d_indexer->getdzf(z) + d_indexer->getdzf(z+1)],
            phiz2b      = phi[idx + d_indexer->getdzb(z) + d_indexer->getdzb(z-1)];


            // finite difference coefficients from: https://en.wikipedia.org/wiki/Finite_difference_coefficient
            // (nabla)^2 phi =
            Tfloat lapphi = phixf + phixb + phiyf + phiyb + phizf + phizb - 6*phi0;

            // (nabla^2)^2 phi =
            Tfloat laplapphi = 
                (phix2f + phiy2f + phix2b + phiy2b + phiz2f + phiz2b) 
                + 2*(phixfyf + phixfyb + phixfzf + phixfzb + phixbyf + phixbyb + phixbzf + phixbzb + phiyfzf + phiyfzb + phiybzf + phiybzb) 
                - 12*(phixf + phixb + phiyf + phiyb + phizf + phizb) 
                + 42*phi0;

            // (nabla^2) phi^3 =
            Tfloat lapphi3 = CUB(phixf) + CUB(phixb) + CUB(phiyf) + CUB(phiyb) + CUB(phizf) + CUB(phizb) - 6*CUB(phi0);

            // maybe an idea to incorporate diffusive source?
            //Tfloat source = J*SIN(2*M_PI/lz * z);

            Tfloat dPi = -laplapphi + mu0sq*lapphi/SQR(a) + g0/6.0*lapphi3;// + source;
            pi[idx] += dt*dPi;
    }
}

__global__ void invKsqrKernel(cuffTcomplex * const field, bool abs, cl2DIdx2cpt d_index, size_t size)
{
    const size_t idx = blockDim.x * blockIdx.x + threadIdx.x; 
    if(idx >= size) return;
    cuffTcomplex nil {0., 0.};

    const lattice2DIndex * const d_indexer = *d_index;

    if (idx == 0) field[idx] = nil;
    else {
        size_t lx = d_indexer->getLx(), ly = d_indexer->getLy();
        size_t ky = idx/(lx/2+1),
               kx = idx - ky*(lx/2+1);

        Tfloat qx = 2*cos(kx*2*M_PI/lx) - 2.,
               qy = 2*cos(ky*2*M_PI/ly) - 2.,
               qsqr = (qx + qy);

        if (abs) {
            Tfloat abssqr = (SQR(field[idx].x) + SQR(field[idx].y));
            if (kx != 0 && kx != lx-kx) abssqr *= 2.;

            field[idx].x = abssqr/qsqr;
            field[idx].y = 0.;
        } else {
            field[idx].x /= qsqr;
            field[idx].y /= qsqr;
        }
    } 
}

__global__ void invKsqrKernel(cuffTcomplex * const field, bool abs, cl3DIdx2cpt d_index, size_t size)
{
    const size_t idx = blockDim.x * blockIdx.x + threadIdx.x; 
    if(idx >= size) return;
    const lattice3DIndex * const d_indexer = *d_index;
    cuffTcomplex nil {0., 0.};

    if (idx == 0) field[idx] = nil;
    else {
        const size_t lx = d_indexer->getLx(), ly = d_indexer->getLy(), lz = d_indexer->getLz();
        const size_t dy = lx/2+1, dz = ly*dy;
        const size_t kz = idx/dz,
                     ky = (idx - kz*dz)/dy,
                     kx = idx - kz*dz - ky*dy;

        Tfloat qx = 2*cos(kx*2*M_PI/lx) - 2.,
               qy = 2*cos(ky*2*M_PI/ly) - 2.,
               qz = 2*cos(kz*2*M_PI/lz) - 2.,
               qsqr = (qx + qy + qz);

        if (abs) {
            Tfloat abssqr = (SQR(field[idx].x) + SQR(field[idx].y));
            if (kx != 0 && kx != lx-kx) abssqr *= 2.;

            field[idx].x = abssqr/qsqr;
            field[idx].y = 0.;
        } else {
            field[idx].x /= qsqr;
            field[idx].y /= qsqr;
        }
    } 
}


__global__ void energyFluxBKernel(Tfloat * const dest, const Tfloat * const K, const Tfloat * const pi,  const Tfloat * const phi, 
        const Tfloat mu0sq, const Tfloat g0, const Tfloat a, const Tfloat J, 
        clIdx2cpt d_index, short dir, size_t size)
{
    const size_t idx = blockDim.x * blockIdx.x + threadIdx.x; 
    if(idx >= size) return;
    const latticeIndex * const d_indexer = *d_index;

    size_t fidx = d_indexer->getNeighbour(idx,dir), 
           bidx = d_indexer->getNeighbour(idx,-dir);

    short d = d_indexer->getD();

    Tfloat lapphi = -2*d*phi[idx];
    
    for (short i=1;i<=d;i++) {
        lapphi += phi[d_indexer->getNeighbour(idx,i)]
                + phi[d_indexer->getNeighbour(idx,-i)];
    }

    Tfloat Vprime = (mu0sq + g0/6.*SQR(phi[idx]))*phi[idx] - J;

    // T^{i0} = - pi d_i phi - mu (d_t K) (d_i K)
    //        = - pi d_i phi - mu (lap phi - V'(phi)) (d_i K)

    dest[idx] = -pi[idx]*(phi[fidx]-phi[bidx])/2. - (lapphi - Vprime)*(K[fidx]-K[bidx])/2.;
}


__global__ void momentumDensityKernel(Tfloat * const dest, const Tfloat * const pi, const Tfloat * const phi, 
        clIdx2cpt d_index, short dir, size_t size)
{
    const size_t idx = blockDim.x * blockIdx.x + threadIdx.x; 
    if(idx >= size) return;
    const latticeIndex * const d_indexer = *d_index;

    size_t fidx = d_indexer->getNeighbour(idx,dir), 
           bidx = d_indexer->getNeighbour(idx,-dir);

    dest[idx] = pi[idx]*(phi[fidx]-phi[bidx])/2.;
}

__global__ void shearingKernel(Tfloat * const dest, const Tfloat * const phi, const Tfloat * const K,
        clIdx2cpt d_index, short dir1, short dir2, size_t size)
{
    const size_t idx = blockDim.x * blockIdx.x + threadIdx.x; 
    if(idx >= size) return;

    const latticeIndex * const d_indexer = *d_index;


    Tfloat phi0 = phi[idx],
           phi1f = phi[d_indexer->getNeighbour(idx, dir1)],
           phi1b = phi[d_indexer->getNeighbour(idx, -dir1)];


    // averaged forward^2, backward^2 to have tracelessness for m^2=0
    if (dir1==dir2) {
        if (K) {
            Tfloat K1f = K[d_indexer->getNeighbour(idx, dir1)],
                   K1b = K[d_indexer->getNeighbour(idx, -dir1)];

            dest[idx] = 0.5*(SQR(phi1f-phi0) + SQR(phi0-phi1b));
        } else {
            dest[idx] = 0.5*(SQR(phi1f-phi0) + SQR(phi0-phi1b));
        }
        
    } else {
        Tfloat phi2f = phi[d_indexer->getNeighbour(idx, dir2)],
               phi2b = phi[d_indexer->getNeighbour(idx, -dir2)];

        // centered, error of O(a^2)
        if (K) {
            Tfloat K1f = K[d_indexer->getNeighbour(idx, dir1)],
                   K1b = K[d_indexer->getNeighbour(idx, -dir1)],
                   K2f = K[d_indexer->getNeighbour(idx, dir2)],
                   K2b = K[d_indexer->getNeighbour(idx, -dir2)];

            dest[idx] = (phi1f-phi1b)*(phi2f-phi2b)/4. + (K1f - K1b)*(K2f - K2b)/4.;
        } else {
            dest[idx] = (phi1f-phi1b)*(phi2f-phi2b)/4.;
        }
    }
}
