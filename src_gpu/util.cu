/*
   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2 as published by the Free Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.
*/

#include "util.hcu"
#include <iostream>
#include <cuda_runtime.h>
#include <boost/log/trivial.hpp>
#include <boost/format.hpp>

/**
 * @Synopsis  Print an error message to stderr
 *
 * @Param err	the cuda error
 */
CudaError::CudaError( cudaError_t err ) : cudaErr( err )
{
    BOOST_LOG_TRIVIAL(error) << boost::format("A CUDA error occured: %s") % getErrorMessage();
//	std::cerr << "A CUDA error occured: " << getErrorMessage() << std::endl;
}

/**
 * @Synopsis  Print a warning message with error description to stderr
 *
 * @Param warn	the warning message
 * @Param err	the cuda error
 */
CudaError::CudaError( const char* warn, cudaError_t err ) : cudaErr( err )
{
    BOOST_LOG_TRIVIAL(warning) << boost::format("%s: %s") % warn % getErrorMessage();
//	std::cerr << warn << ": " << getErrorMessage() << std::endl;
}

cudaError_t CudaError::getError( )
{
	return cudaErr;
}

const char* CudaError::getErrorMessage( )
{
	return cudaGetErrorString( getError() );
}

/**
 * @Synopsis  Specify the CUDA device that should be used
 *
 * @Param device number of the desired device
 */
void setCudaDevice(int device)
{
	int devId = 0;
	cudaError_t cudaErr;

	if (device >= 0){
		// select user specified device
		cudaErr = cudaSetDevice(device);
		if (cudaErr){
			throw CudaError("Failed to initialize device", cudaErr);
		}
	}
	else{
		// force CUDA to select a device (just so we can be sure the queried device is the one we run on)
		cudaErr = cudaSetValidDevices(0, 0);
		if (cudaErr){
			throw CudaError("Failed to initialize device", cudaErr);
		}
	}
	// check which device we got
	cudaErr = cudaGetDevice(&devId);
	if(cudaErr){
		throw CudaError("Failed to retrieve used device", cudaErr);
	}
	cudaDeviceProp props;
	cudaErr = cudaGetDeviceProperties(&props, devId);
	if(cudaErr){
		throw CudaError( "Failed to get device properties", cudaErr);
	}
//	std::cerr << "Running on CUDA device " << devId << ": " << props.name << std::endl;
        BOOST_LOG_TRIVIAL(info) << boost::format("Running on CUDA device %i: %s") % devId % props.name;
}
