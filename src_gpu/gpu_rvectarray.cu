#include <gpu_rvectarray.hcu>
#include <cuda_reduce.hcu>

#include<pkernels.hcu>

/**
 * @Synopsis Copies the contents of the array using a host memory buffer.
 *
 * @Param src source array
 */
void GPUrvectArray::doCopyFrom(const rvectArray &src) {

	if ((src.NoElements() != NoElements()) || (src.NoComponents() != NoComponents())) throw std::length_error("Numbers of elements in arrays do not match!");

	Tfloat *buffer = (Tfloat*)malloc(NoFloats()*sizeof(Tfloat));
	src.writeFieldsToBuffer(buffer);

	cudaMemcpy(fields, buffer, NoFloats()*sizeof(Tfloat), cudaMemcpyHostToDevice);
	checkCudaError("copyFrom host");

	delete[] buffer;
};


/**
 * @Synopsis  Copying without the host buffer
 *
 * @Param src
 */
void GPUrvectArray::copyFrom(const GPUrvectArray &src) {
	if ((src.NoElements() != NoElements())
		|| (src.NoComponents() != NoComponents())) throw std::length_error("Numbers of elements in arrays do not match!");

	cudaMemcpy(fields, src.fields, NoFloats()*sizeof(Tfloat), cudaMemcpyDeviceToDevice);
	checkCudaError("copyFrom internal");
}


/**
 * @Synopsis  writes fields to the address at buffer. MUST HAVE ENOUGH SPACE!
 *
 * @Param buffer the address of the buffer
 */
void GPUrvectArray::doWriteFieldsToBuffer(Tfloat * const buffer) const {
	cudaMemcpy(buffer, fields, NoFloats()*sizeof(Tfloat), cudaMemcpyDeviceToHost);
	checkCudaError("writeFieldsToBuffer");
};



/**
 * @Synopsis  inverse of writeFieldsToBuffer
 *
 * @Param buffer address of buffer
 */
void GPUrvectArray::doReadFieldsFromBuffer(const Tfloat * const buffer) {
	cudaMemcpy(fields, buffer, NoFloats()*sizeof(Tfloat), cudaMemcpyHostToDevice);
	checkCudaError("readFieldsFromBuffer");
}


/**
 * @Synopsis  Sum squares of vectors; amounts to sum of all squares,
 *				so simply map a square kernel and reduce the result.
 *
 * @Returns 
 */
Tfloat GPUrvectArray::doSumSquares() const 
{ 
	// potential for stream concurrency !!!
	cudaMemcpy(buffer_d, fields, NoFloats()*sizeof(Tfloat), cudaMemcpyDeviceToDevice);

	const dim3 block = CGX_BLOCKSIZE;
	const dim3 grid = NoFloats()/CGX_BLOCKSIZE + 1;

	mapSquareKernel<<< grid, block >>>(buffer_d, NoFloats());
	checkCudaError("sumSquares - kernel");

	Tfloat result = reduce(buffer_d, tmp_d, NoFloats());
	checkCudaError("sumSquares - reduce");

	return result;
}


/**
 * @Synopsis  Sum over all x^4.
 *
 * @Returns	the sum over all fourth powers
 */
Tfloat GPUrvectArray::doSumQuartes() const 
{
	const dim3 block = CGX_BLOCKSIZE;
	const dim3 grid = NoElements()/CGX_BLOCKSIZE + 1;

	mapVecQuartKernel<<< grid, block >>>(buffer_d, fields, NoComponents(), NoElements());
	checkCudaError("sumQuartes - kernel");

	Tfloat result = reduce(buffer_d, tmp_d, NoElements());
	checkCudaError("sumQuartes - reduce");

	return result;
};


/**
 * @Synopsis  Sums all floats in the fields. Override of base class inline to reduce access operations.
 *				USE WITH CARE: does allocate and free its own buffer, which is inefficient
 *
 * @Returns 
 */
Tfloat GPUrvectArray::doSumComponents() const 
{
	cudaMemcpy(buffer_d, fields, NoFloats()*sizeof(Tfloat), cudaMemcpyDeviceToDevice);
	checkCudaError("sumComponents - memcpy");

	Tfloat result = reduce(buffer_d, tmp_d, NoFloats());
	checkCudaError("sumComponents - reduce");
	return result;
}


/**
 * @Synopsis  Sums all jth components. Not as efficient as summing over all j.
 *
 * @Param j component index
 *
 * @Returns the sum of all jth components
 */
Tfloat GPUrvectArray::doSumJthComponent(const unsigned short j) const
{
	const dim3 block = CGX_BLOCKSIZE;
	const dim3 grid = NoElements()/CGX_BLOCKSIZE + 1;
	copyStrideKernel<<< grid, block >>>(fields, buffer_d, NoComponents(), j, NoFloats());
	checkCudaError("sumJthComponent - kernel");

	Tfloat result = reduce(buffer_d, tmp_d, NoElements());
	checkCudaError("sumJthComponent - reduce");

	return result;
}


/**
 * @Synopsis  Generates a values according to a gaussian distribution with given properties
 *
 * @Param average mean of the gaussian
 * @Param sigma of the gaussian
 * @Param rnd_state random state on device
 */
void GPUrvectArray::doSetGaussianRandom( const Tfloat average, const Tfloat sigma, taus_state * const rnd_state) {
	const dim3 block = CGX_BLOCKSIZE;
	const dim3 grid = NoFloats()/CGX_BLOCKSIZE + 1;

	fillGaussianKernel<<< grid, block >>>( fields, average, sigma, rnd_state, NoFloats());
	checkCudaError("setGaussianRandom");
};

void GPUrvectArray::doRandomizeAndDamp(const Tfloat noiseAmp, const Tfloat damping, taus_state * const rnd_state) {
	const dim3 block = CGX_BLOCKSIZE;
	const dim3 grid = NoFloats()/CGX_BLOCKSIZE + 1;

	randomizeAndDampKernel<<< grid, block >>>(fields, noiseAmp, damping, rnd_state, NoFloats());
	checkCudaError("randomizeAndDamp");
}

void GPUrvectArray::doAddConst(const Tfloat a) {
	const dim3 block = CGX_BLOCKSIZE;
	const dim3 grid = NoFloats()/CGX_BLOCKSIZE + 1;

	addConstKernel<<< grid, block >>>(fields, a, fields, NoFloats());
	checkCudaError("addConst");
};

void GPUrvectArray::doSaxpy(const Tfloat a, const rvectArray &x) {
	GPUrvectArray newX(x);
	saxpy(a, newX);
};

void GPUrvectArray::saxpy(const Tfloat a, const GPUrvectArray &x) {
	const dim3 block = CGX_BLOCKSIZE;
	const dim3 grid = NoFloats()/CGX_BLOCKSIZE + 1;

	saxpyKernel<<< grid, block >>>(fields, a, x.fields, fields, NoFloats());
	checkCudaError("saxpy");
};
