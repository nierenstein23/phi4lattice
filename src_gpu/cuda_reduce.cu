/**
 * Reduction code based on the NVIDIA CUDA SDK example
 */

/* include for clang semantic completion in my vim-based env (DS) */
#ifdef __clang__
#include <__clang_cuda_builtin_vars.h>
#endif

#include "cuda_reduce.hcu"
#include "util.hcu"

__global__ void reduceKernelInt(int *g_idata, int *g_odata, unsigned int blockSize, unsigned int n, bool nIsPow2)
{
	__shared__ int sdata[256];

	// perform first level of reduction,
	// reading from global memory, writing to shared memory
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + threadIdx.x;
	unsigned int gridSize = blockSize*2*gridDim.x;
	sdata[tid] = 0;

	// we reduce multiple elements per thread.  The number is determined by the 
	// number of active thread blocks (via gridDim).  More blocks will result
	// in a larger gridSize and therefore fewer elements per thread
	while (i < n)
	{
	    sdata[tid] += g_idata[i];
	    // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
	    if (nIsPow2 || i + blockSize < n) 
	        sdata[tid] += g_idata[i+blockSize];  
	    i += gridSize;
	}
	__syncthreads();

	// do reduction in shared mem
	if (blockSize >= 512) { if (tid < 256) { sdata[tid] += sdata[tid + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (tid < 128) { sdata[tid] += sdata[tid + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (tid <  64) { sdata[tid] += sdata[tid +  64]; } __syncthreads(); }

	
#if __CUDA_ARCH__ == 130
	if (tid < 32)
	{
	    if (blockSize >=  64) { sdata[tid] += sdata[tid + 32]; __syncthreads(); }
	    if (blockSize >=  32) { sdata[tid] += sdata[tid + 16]; __syncthreads(); }
	    if (blockSize >=  16) { sdata[tid] += sdata[tid +  8]; __syncthreads(); }
	    if (blockSize >=   8) { sdata[tid] += sdata[tid +  4]; __syncthreads(); }
	    if (blockSize >=   4) { sdata[tid] += sdata[tid +  2]; __syncthreads(); }
	    if (blockSize >=   2) { sdata[tid] += sdata[tid +  1]; __syncthreads(); }
	}
#elif __CUDA_ARCH__ >= 200
	volatile int *smem = sdata;
	if(tid < 32)
	{
		if (blockSize >= 64) smem[tid] += smem[tid + 32];
        	if (blockSize >= 32) smem[tid] += smem[tid + 16];
        	if (blockSize >= 16) smem[tid] += smem[tid +  8];
        	if (blockSize >=  8) smem[tid] += smem[tid +  4];
        	if (blockSize >=  4) smem[tid] += smem[tid +  2];
        	if (blockSize >=  2) smem[tid] += smem[tid +  1];
	}
#endif

	// write result for this block to global mem 
	if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}

__global__ void reduceKernelFloat(float *g_idata, float *g_odata, unsigned int blockSize, unsigned int n, bool nIsPow2)
{
	__shared__ float sdata[256];

	// perform first level of reduction,
	// reading from global memory, writing to shared memory
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + threadIdx.x;
	unsigned int gridSize = blockSize*2*gridDim.x;
	sdata[tid] = 0;

	// we reduce multiple elements per thread.  The number is determined by the 
	// number of active thread blocks (via gridDim).  More blocks will result
	// in a larger gridSize and therefore fewer elements per thread
	while (i < n)
	{
	    sdata[tid] += g_idata[i];
	    // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
	    if (nIsPow2 || i + blockSize < n) 
	        sdata[tid] += g_idata[i+blockSize];  
	    i += gridSize;
	}
	__syncthreads();

	// do reduction in shared mem
	if (blockSize >= 512) { if (tid < 256) { sdata[tid] += sdata[tid + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (tid < 128) { sdata[tid] += sdata[tid + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (tid <  64) { sdata[tid] += sdata[tid +  64]; } __syncthreads(); }

#if __CUDA_ARCH__ == 130
	if (tid < 32)
	{
	    if (blockSize >=  64) { sdata[tid] += sdata[tid + 32]; __syncthreads(); }
	    if (blockSize >=  32) { sdata[tid] += sdata[tid + 16]; __syncthreads(); }
	    if (blockSize >=  16) { sdata[tid] += sdata[tid +  8]; __syncthreads(); }
	    if (blockSize >=   8) { sdata[tid] += sdata[tid +  4]; __syncthreads(); }
	    if (blockSize >=   4) { sdata[tid] += sdata[tid +  2]; __syncthreads(); }
	    if (blockSize >=   2) { sdata[tid] += sdata[tid +  1]; __syncthreads(); }
	}
#elif __CUDA_ARCH__ >= 200
	volatile float *smem = sdata;
	if(tid < 32)
	{
		if (blockSize >= 64) smem[tid] += smem[tid + 32];
	 	if (blockSize >= 32) smem[tid] += smem[tid + 16];
 		if (blockSize >= 16) smem[tid] += smem[tid +  8];
 		if (blockSize >=  8) smem[tid] += smem[tid +  4];
 		if (blockSize >=  4) smem[tid] += smem[tid +  2];
 		if (blockSize >=  2) smem[tid] += smem[tid +  1];	
	}
#endif
	// write result for this block to global mem 
	if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}

__global__ void reduceKernelFloat_storeresult(float *g_idata, float *g_odata, 
	   unsigned int blockSize, unsigned int n, bool nIsPow2, float *result, float *root)
{
	__shared__ float sdata[256];

	// perform first level of reduction,
	// reading from global memory, writing to shared memory
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + threadIdx.x;
	unsigned int gridSize = blockSize*2*gridDim.x;
	sdata[tid] = 0;

	// we reduce multiple elements per thread.  The number is determined by the 
	// number of active thread blocks (via gridDim).  More blocks will result
	// in a larger gridSize and therefore fewer elements per thread
	while (i < n)
	{
	    sdata[tid] += g_idata[i];
	    // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
	    if (nIsPow2 || i + blockSize < n) 
	        sdata[tid] += g_idata[i+blockSize];  
	    i += gridSize;
	}
	__syncthreads();

	// do reduction in shared mem
	if (blockSize >= 512) { if (tid < 256) { sdata[tid] += sdata[tid + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (tid < 128) { sdata[tid] += sdata[tid + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (tid <  64) { sdata[tid] += sdata[tid +  64]; } __syncthreads(); }

#if __CUDA_ARCH__ == 130
	if (tid < 32)
	{
	    if (blockSize >=  64) { sdata[tid] += sdata[tid + 32]; __syncthreads(); }
	    if (blockSize >=  32) { sdata[tid] += sdata[tid + 16]; __syncthreads(); }
	    if (blockSize >=  16) { sdata[tid] += sdata[tid +  8]; __syncthreads(); }
	    if (blockSize >=   8) { sdata[tid] += sdata[tid +  4]; __syncthreads(); }
	    if (blockSize >=   4) { sdata[tid] += sdata[tid +  2]; __syncthreads(); }
	    if (blockSize >=   2) { sdata[tid] += sdata[tid +  1]; __syncthreads(); }
	}
#elif __CUDA_ARCH__ >= 200
	volatile float *smem = sdata;
	if(tid < 32)
	{
		if (blockSize >= 64) smem[tid] += smem[tid + 32];
	 	if (blockSize >= 32) smem[tid] += smem[tid + 16];
 		if (blockSize >= 16) smem[tid] += smem[tid +  8];
 		if (blockSize >=  8) smem[tid] += smem[tid +  4];
 		if (blockSize >=  4) smem[tid] += smem[tid +  2];
 		if (blockSize >=  2) smem[tid] += smem[tid +  1];	
	}
#endif
	// write result for this block to global mem 
	if (tid == 0){ 
	   	       g_odata[blockIdx.x] = sdata[0]; 
		       result[0]=sdata[0];
		       root[0]=sqrt( sdata[0] );}
}

__global__ void reduceKernelDouble(double *g_idata, double *g_odata, unsigned int blockSize, unsigned int n, bool nIsPow2)
{
	__shared__ double sdata[256];

	// perform first level of reduction,
	// reading from global memory, writing to shared memory
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + threadIdx.x;
	unsigned int gridSize = blockSize*2*gridDim.x;
	sdata[tid] = 0;

	// we reduce multiple elements per thread.  The number is determined by the 
	// number of active thread blocks (via gridDim).  More blocks will result
	// in a larger gridSize and therefore fewer elements per thread
	while (i < n)
	{
	    sdata[tid] += g_idata[i];
	    // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
	    if (nIsPow2 || i + blockSize < n) 
	        sdata[tid] += g_idata[i+blockSize];  
	    i += gridSize;
	}
	__syncthreads();

	// do reduction in shared mem
	if (blockSize >= 512) { if (tid < 256) { sdata[tid] += sdata[tid + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (tid < 128) { sdata[tid] += sdata[tid + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (tid <  64) { sdata[tid] += sdata[tid +  64]; } __syncthreads(); }

#if __CUDA_ARCH__ == 130
	if (tid < 32)
	{
	    if (blockSize >=  64) { sdata[tid] += sdata[tid + 32]; __syncthreads(); }
	    if (blockSize >=  32) { sdata[tid] += sdata[tid + 16]; __syncthreads(); }
	    if (blockSize >=  16) { sdata[tid] += sdata[tid +  8]; __syncthreads(); }
	    if (blockSize >=   8) { sdata[tid] += sdata[tid +  4]; __syncthreads(); }
	    if (blockSize >=   4) { sdata[tid] += sdata[tid +  2]; __syncthreads(); }
	    if (blockSize >=   2) { sdata[tid] += sdata[tid +  1]; __syncthreads(); }
	}
#elif __CUDA_ARCH__ >= 200
	volatile double *smem = sdata;
	if(tid < 32)
	{
		if (blockSize >= 64) smem[tid] += smem[tid + 32];
	 	if (blockSize >= 32) smem[tid] += smem[tid + 16];
 		if (blockSize >= 16) smem[tid] += smem[tid +  8];
 		if (blockSize >=  8) smem[tid] += smem[tid +  4];
 		if (blockSize >=  4) smem[tid] += smem[tid +  2];
 		if (blockSize >=  2) smem[tid] += smem[tid +  1];	
	}
#endif
	// write result for this block to global mem 
	if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}


__global__ void reduceKernelDouble_storeresult(double *g_idata, double *g_odata,
	    unsigned int blockSize, unsigned int n, bool nIsPow2, double *result, double *root)
{
	__shared__ double sdata[256];

	// perform first level of reduction,
	// reading from global memory, writing to shared memory
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + threadIdx.x;
	unsigned int gridSize = blockSize*2*gridDim.x;
	sdata[tid] = 0;

	// we reduce multiple elements per thread.  The number is determined by the 
	// number of active thread blocks (via gridDim).  More blocks will result
	// in a larger gridSize and therefore fewer elements per thread
	while (i < n)
	{
	    sdata[tid] += g_idata[i];
	    // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
	    if (nIsPow2 || i + blockSize < n) 
	        sdata[tid] += g_idata[i+blockSize];  
	    i += gridSize;
	}
	__syncthreads();

	// do reduction in shared mem
	if (blockSize >= 512) { if (tid < 256) { sdata[tid] += sdata[tid + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (tid < 128) { sdata[tid] += sdata[tid + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (tid <  64) { sdata[tid] += sdata[tid +  64]; } __syncthreads(); }

#if __CUDA_ARCH__ == 130
	if (tid < 32)
	{
	    if (blockSize >=  64) { sdata[tid] += sdata[tid + 32]; __syncthreads(); }
	    if (blockSize >=  32) { sdata[tid] += sdata[tid + 16]; __syncthreads(); }
	    if (blockSize >=  16) { sdata[tid] += sdata[tid +  8]; __syncthreads(); }
	    if (blockSize >=   8) { sdata[tid] += sdata[tid +  4]; __syncthreads(); }
	    if (blockSize >=   4) { sdata[tid] += sdata[tid +  2]; __syncthreads(); }
	    if (blockSize >=   2) { sdata[tid] += sdata[tid +  1]; __syncthreads(); }
	}
#elif __CUDA_ARCH__ >= 200
	volatile double *smem = sdata;
	if(tid < 32)
	{
		if (blockSize >= 64) smem[tid] += smem[tid + 32];
	 	if (blockSize >= 32) smem[tid] += smem[tid + 16];
 		if (blockSize >= 16) smem[tid] += smem[tid +  8];
 		if (blockSize >=  8) smem[tid] += smem[tid +  4];
 		if (blockSize >=  4) smem[tid] += smem[tid +  2];
 		if (blockSize >=  2) smem[tid] += smem[tid +  1];	
	}
#endif
	// write result for this block to global mem 
	if (tid == 0){
	   	 g_odata[blockIdx.x] = sdata[0];
		 result[0]=sdata[0];
		 root[0]=sqrt( sdata[0] ); }
}


int reduce_int(int *alpha_d, int *alpha_o, int sizeh)
{
	int res;

	dim3 block = 256;
	int elems = sizeh;

	dim3 grid = static_cast<int>( ceilf( static_cast<float>( elems ) / static_cast<float>( block.x ) ) );
	if( grid.x > 256 )
		grid = 256;

	reduceKernelInt<<< grid, block >>>( alpha_d, alpha_o, block.x, elems, false );

	elems = grid.x;
	grid = 1;
	reduceKernelInt<<< grid, block >>>( alpha_o, alpha_d, block.x, elems, false );
	
	cudaMemcpy(&res, alpha_d, sizeof(int) * 1, cudaMemcpyDeviceToHost);

	return(res);
}

float reduce_float(float *alpha_d, float *alpha_o, int sizeh)
{
	float res;

	dim3 block = 256;
	int elems = sizeh;

	dim3 grid = static_cast<int>( ceilf( static_cast<float>( elems ) / static_cast<float>( block.x ) ) );
	if( grid.x > 256 )
		grid = 256;

	reduceKernelFloat<<< grid, block >>>( alpha_d, alpha_o, block.x, elems, false );

	elems = grid.x;
	grid = 1;
	reduceKernelFloat<<< grid, block >>>( alpha_o, alpha_d, block.x, elems, false );
	
	cudaMemcpy(&res, alpha_d, sizeof(float) * 1, cudaMemcpyDeviceToHost);

	return(res);
}

double reduce_double(double *alpha_d, double *alpha_o, int sizeh)
{
	double res;

	dim3 block = 256;
	int elems = sizeh;

	dim3 grid = static_cast<int>( ceilf( static_cast<float>( elems ) / static_cast<float>( block.x ) ) );
	if( grid.x > 256 )
		grid = 256;

	reduceKernelDouble<<< grid, block >>>( alpha_d, alpha_o, block.x, elems, false );

	elems = grid.x;
	grid = 1;
	reduceKernelDouble<<< grid, block >>>( alpha_o, alpha_d, block.x, elems, false );
	
	cudaMemcpy(&res, alpha_d, sizeof(double) * 1, cudaMemcpyDeviceToHost);

	return(res);
}

void reduce_float_resultondevice(float *alpha_d, float *alpha_o, int sizeh, float *result, float *root)
{

	dim3 block = 256;
	int elems = sizeh;

	dim3 grid = static_cast<int>( ceilf( static_cast<float>( elems ) / static_cast<float>( block.x ) ) );
	if( grid.x > 256 )
		grid = 256;

	reduceKernelFloat<<< grid, block >>>( alpha_d, alpha_o, block.x, elems, false );

	elems = grid.x;
	grid = 1;
	reduceKernelFloat_storeresult<<< grid, block >>>( alpha_o, alpha_d, block.x, elems, false, result, root );
	
}

void reduce_double_resultondevice(double *alpha_d, double *alpha_o, int sizeh, double *result, double *root)
{

	dim3 block = 256;
	int elems = sizeh;

	dim3 grid = static_cast<int>( ceilf( static_cast<float>( elems ) / static_cast<float>( block.x ) ) );
	if( grid.x > 256 )
		grid = 256;

	reduceKernelDouble<<< grid, block >>>( alpha_d, alpha_o, block.x, elems, false );

	elems = grid.x;
	grid = 1;
	reduceKernelDouble_storeresult<<< grid, block >>>( alpha_o, alpha_d, block.x, elems, false, result, root );
	
}


/**
 * Reduction code based on the NVIDIA CUDA SDK example
 * modified to give maximum of array instead of sum
 */

#include <cfloat>
#include <climits>

// Attention: this macro can have side effects/double evaluation. Use properly.
#define SETMAX(a,b) (a = a > b ? a : b)

__global__ void reducemaxKernelFloat(float *g_idata, float *g_odata, unsigned int blockSize, unsigned int n, bool nIsPow2)
{
	__shared__ float sdata[256];

	// perform first level of reduction,
	// reading from global memory, writing to shared memory
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + threadIdx.x;
	unsigned int gridSize = blockSize*2*gridDim.x;
	sdata[tid] = -FLT_MAX;

	// we reduce multiple elements per thread.  The number is determined by the 
	// number of active thread blocks (via gridDim).  More blocks will result
	// in a larger gridSize and therefore fewer elements per thread
	while (i < n)
	{
	    SETMAX(sdata[tid], g_idata[i]);
	    // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
	    if (nIsPow2 || i + blockSize < n) 
	        SETMAX(sdata[tid], g_idata[i+blockSize]);
	    i += gridSize;
	}
	__syncthreads();

	// do reduction in shared mem
	if (blockSize >= 512) { if (tid < 256) { SETMAX(sdata[tid], sdata[tid + 256]); } __syncthreads(); }
	if (blockSize >= 256) { if (tid < 128) { SETMAX(sdata[tid], sdata[tid + 128]); } __syncthreads(); }
	if (blockSize >= 128) { if (tid <  64) { SETMAX(sdata[tid], sdata[tid +  64]); } __syncthreads(); }

#if __CUDA_ARCH__ == 130
	if (tid < 32)
	{
	    if (blockSize >=  64) { SETMAX(sdata[tid], sdata[tid + 32]); __syncthreads(); }
	    if (blockSize >=  32) { SETMAX(sdata[tid], sdata[tid + 16]); __syncthreads(); }
	    if (blockSize >=  16) { SETMAX(sdata[tid], sdata[tid +  8]); __syncthreads(); }
	    if (blockSize >=   8) { SETMAX(sdata[tid], sdata[tid +  4]); __syncthreads(); }
	    if (blockSize >=   4) { SETMAX(sdata[tid], sdata[tid +  2]); __syncthreads(); }
	    if (blockSize >=   2) { SETMAX(sdata[tid], sdata[tid +  1]); __syncthreads(); }
	}
#elif __CUDA_ARCH__ >= 200
	volatile float *smem = sdata;
	if(tid < 32)
	{
		if (blockSize >= 64) SETMAX(smem[tid], smem[tid + 32]);
	 	if (blockSize >= 32) SETMAX(smem[tid], smem[tid + 16]);
 		if (blockSize >= 16) SETMAX(smem[tid], smem[tid +  8]);
 		if (blockSize >=  8) SETMAX(smem[tid], smem[tid +  4]);
 		if (blockSize >=  4) SETMAX(smem[tid], smem[tid +  2]);
 		if (blockSize >=  2) SETMAX(smem[tid], smem[tid +  1]);
	}
#endif
	// write result for this block to global mem 
	if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}

float reducemax_float(float *alpha_d, float *alpha_o, int size)
{
	float res;

	dim3 block = 256;
	int elems = size;

	dim3 grid = static_cast<int>( ceilf( static_cast<float>( elems ) / static_cast<float>( block.x ) ) );
	if( grid.x > 256 )
		grid = 256;

	reducemaxKernelFloat<<< grid, block >>>( alpha_d, alpha_o, block.x, elems, false );

	elems = grid.x;
	grid = 1;
	reducemaxKernelFloat<<< grid, block >>>( alpha_o, alpha_d, block.x, elems, false );
	
	cudaMemcpy(&res, alpha_d, sizeof(float) * 1, cudaMemcpyDeviceToHost);

	return(res);
}

__global__ void reducemaxKernelDouble(double *g_idata, double *g_odata, unsigned int blockSize, unsigned int n, bool nIsPow2)
{
        __shared__ double sdata[256];

        // perform first level of reduction,
        // reading from global memory, writing to shared memory
        unsigned int tid = threadIdx.x;
        unsigned int i = blockIdx.x*(blockSize*2) + threadIdx.x;
        unsigned int gridSize = blockSize*2*gridDim.x;
        sdata[tid] = -DBL_MAX;

        // we reduce multiple elements per thread.  The number is determined by the
        // number of active thread blocks (via gridDim).  More blocks will result
        // in a larger gridSize and therefore fewer elements per thread
        while (i < n)
        {
            SETMAX(sdata[tid], g_idata[i]);
            // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
            if (nIsPow2 || i + blockSize < n)
                SETMAX(sdata[tid], g_idata[i+blockSize]);
            i += gridSize;
        }
        __syncthreads();

        // do reduction in shared mem
        if (blockSize >= 512) { if (tid < 256) { SETMAX(sdata[tid], sdata[tid + 256]); } __syncthreads(); }
        if (blockSize >= 256) { if (tid < 128) { SETMAX(sdata[tid], sdata[tid + 128]); } __syncthreads(); }
        if (blockSize >= 128) { if (tid <  64) { SETMAX(sdata[tid], sdata[tid +  64]); } __syncthreads(); }

#if __CUDA_ARCH__ == 130
        if (tid < 32)
        {
            if (blockSize >=  64) { SETMAX(sdata[tid], sdata[tid + 32]); __syncthreads(); }
            if (blockSize >=  32) { SETMAX(sdata[tid], sdata[tid + 16]); __syncthreads(); }
            if (blockSize >=  16) { SETMAX(sdata[tid], sdata[tid +  8]); __syncthreads(); }
            if (blockSize >=   8) { SETMAX(sdata[tid], sdata[tid +  4]); __syncthreads(); }
            if (blockSize >=   4) { SETMAX(sdata[tid], sdata[tid +  2]); __syncthreads(); }
            if (blockSize >=   2) { SETMAX(sdata[tid], sdata[tid +  1]); __syncthreads(); }
        }
#elif __CUDA_ARCH__ >= 200
        volatile double *smem = sdata;
        if(tid < 32)
        {
                if (blockSize >= 64) SETMAX(smem[tid], smem[tid + 32]);
                if (blockSize >= 32) SETMAX(smem[tid], smem[tid + 16]);
                if (blockSize >= 16) SETMAX(smem[tid], smem[tid +  8]);
                if (blockSize >=  8) SETMAX(smem[tid], smem[tid +  4]);
                if (blockSize >=  4) SETMAX(smem[tid], smem[tid +  2]);
                if (blockSize >=  2) SETMAX(smem[tid], smem[tid +  1]);
        }
#endif
        // write result for this block to global mem
        if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}

double reducemax_double(double *alpha_d, double *alpha_o, int sizeh)
{
        double res;

        dim3 block = 256;
        int elems = sizeh;

        dim3 grid = static_cast<int>( ceilf( static_cast<float>( elems ) / static_cast<float>( block.x ) ) );
        if( grid.x > 256 )
                grid = 256;

        reducemaxKernelDouble<<< grid, block >>>( alpha_d, alpha_o, block.x, elems, false );

        elems = grid.x;
        grid = 1;
        reducemaxKernelDouble<<< grid, block >>>( alpha_o, alpha_d, block.x, elems, false );

        cudaMemcpy(&res, alpha_d, sizeof(double) * 1, cudaMemcpyDeviceToHost);

        return(res);
}


