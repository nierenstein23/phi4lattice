#include <gpu_lattice.hcu>
#include <cuda_reduce.hcu>
#include <pkernels.hcu>

#define CU_MAX_THREADS 1024

// =============================== nD ===================================================================

void GPULattice::doSeedTaus(const long newseed)
{
    // seed the C RNG and store in protected variable
    //	srand48(newseed);
    seed = newseed;

    taus_state *h_state = new taus_state[index->getPoints()];

    // seed every element of the taus state
    for (size_t i=0;i<index->getPoints();i++)
        h_state[i] = taus_seed();

    cudaMemcpy(rnd_state, h_state, index->getPoints()*sizeof(taus_state), cudaMemcpyHostToDevice);

    delete[] h_state;
};

void GPULattice::ePotKernel(Tfloat * const dest) {
    size_t vol = index->getElements();
    dim3 grid = vol/CGX_BLOCKSIZE + 1;
    energyDensityKernel<<< grid, CGX_BLOCKSIZE >>>
        (dest, getPhiPointer(), getPiPointer(), lmu0sq, lg0, la, J, d_index, true, vol);
    checkCudaError(cudaDeviceSynchronize(), "sync after ePotKernel");
}

void GPULattice::eFluxBKernel(Tfloat * const dest, const Tfloat * const K, const short dir) {
    size_t vol = index->getElements();
    dim3 grid = vol/CGX_BLOCKSIZE + 1;

    energyFluxBKernel<<< grid, CGX_BLOCKSIZE >>>
        (dest, K, getPiPointer(), getPhiPointer(), lmu0sq, lg0, la, J, d_index, dir, vol);

    checkCudaError(cudaDeviceSynchronize(), "sync after eFluxKernel");
}

void GPULattice::mDensKernel(Tfloat * const field, const Tfloat * const phidot, const short dir) {
    size_t vol = index->getElements();
    dim3 grid = vol/CGX_BLOCKSIZE + 1;
    momentumDensityKernel<<< grid, CGX_BLOCKSIZE >>>
        (field, phidot, getPhiPointer(), d_index, dir, vol);
    checkCudaError(cudaDeviceSynchronize(), "sync after mDensKernel");
}

void GPULattice::shearKernel(Tfloat * const field, const Tfloat * const K, const short dir1, const short dir2) {
    size_t vol = index->getElements();
    dim3 grid = vol/CGX_BLOCKSIZE + 1;
    shearingKernel<<< grid, CGX_BLOCKSIZE >>>
        (field, getPhiPointer(), K, d_index, dir1, dir2, vol);
    checkCudaError(cudaDeviceSynchronize(), "sync after shearKernel");
}


void GPULattice::symSquareGradKernel(Tfloat * const dest, const Tfloat * const src)
{
    size_t vol = index->getElements();
    dim3 grid = vol/CGX_BLOCKSIZE + 1;
    symSquareGradientKernel<<< grid, CGX_BLOCKSIZE >>>
        (dest, src, d_index, vol);
    checkCudaError(cudaDeviceSynchronize(), "sync after symSquareGradKernel");
}

Tfloat GPULattice::doGetGradSqPhiSq() const {
    const size_t NoElements=index->getElements();

    const dim3 block = CGX_BLOCKSIZE;
    const dim3 grid = NoElements/CGX_BLOCKSIZE + 1;

    squareGradSqKernel<<< grid, block >>>(buffer_d, getPhiPointer(), d_index, NoElements);

    Tfloat result = reduce(buffer_d, tmp_d, NoElements);

    return result;
}

Tfloat GPULattice::doGetGradSqPhiQrt() const {
    const size_t NoElements=index->getElements();

    const dim3 block = CGX_BLOCKSIZE;
    const dim3 grid = NoElements/CGX_BLOCKSIZE + 1;

    squareGradQrtKernel<<< grid, block >>>(buffer_d, getPhiPointer(), d_index, NoElements);

    Tfloat result = reduce(buffer_d, tmp_d, NoElements);

    return result;
}

Tfloat GPULattice::doGetGradPhiSq() const {
    const size_t NoElements=index->getElements();

    const dim3 block = CGX_BLOCKSIZE;
    const dim3 grid = NoElements/CGX_BLOCKSIZE + 1;

    squareGradientKernel<<< grid, block >>>(buffer_d, getPhiPointer(), d_index, NoElements);

    Tfloat result = reduce(buffer_d, tmp_d, NoElements);

    return result;
}


void GPULattice::doUpdateStressTensor()
{
    const size_t lq = index->getL()/2+1;
    std::vector<Tcomplex> V(lq), T(lq), L(lq); // buffer vectors
    
    const size_t vol = index->getElements();

    dim3 block = CGX_BLOCKSIZE;
    dim3 grid = vol/CGX_BLOCKSIZE +1;

    // V[phi](x)
    ePotKernel(tmp_d);
    BOOST_LOG_TRIVIAL(trace) << "updateStress - energyDensityKernel";

    // DFT to V[phi](q)
    if(cufftR2C(kplan, tmp_d, vkfield) != CUFFT_SUCCESS) throw std::runtime_error("doUpdateStressTensor - 1st cufft execution failed"); checkCudaError(cudaDeviceSynchronize(), "sync after r2c");
    checkCudaError(cudaMemcpy(V.data(), vkfield, lq*sizeof(cuffTcomplex), cudaMemcpyDeviceToHost), "cuda memcpy failed");
    checkCudaError(cudaDeviceSynchronize(), "Device failed to sync");

    // DFT to 2T[pi](x)
    if (getDynMode() == relax) {
        mapSquareKernel<<< grid, block >>>(tmp_d, getPiPointer(), vol);
        checkCudaError("mapSquareKernel");
        BOOST_LOG_TRIVIAL(trace) << "updateStress - mapSquareKernel";

    } else {
        // fill tmp_d with Pi(x)
        cudaMemcpy(tmp_d, getPiPointer(), vol*sizeof(Tfloat), cudaMemcpyDeviceToDevice); checkCudaError("invlap - memcpy");
        // DFT to Pi(k)
        if( cufftR2C(kplan, tmp_d, vkfield) != CUFFT_SUCCESS) throw std::runtime_error("doUpdateStressTensor - 2nd cufft execution failed"); checkCudaError(cudaDeviceSynchronize(), "sync after r2c 2");
        // multiply Pi(k) /= k^2, i.e. inverse laplacian
        timesInvKsqr(vkfield, false);
        checkCudaError(cudaDeviceSynchronize(), "sync after invKsqrKernel");

        // transform Pi(k)/k^2 to nabla^{-2}Pi(x) in tmp_d
        if( cufftC2R(ikplan, vkfield, tmp_d) != CUFFT_SUCCESS) throw std::runtime_error("doUpdateStress - 2nd cufft execution failed"); checkCudaError(cudaDeviceSynchronize(), "sync after c2r");

        // keep \nabla^-2 pi in buffer_d for later
        scaleKernel<<< grid, block >>>(buffer_d, -1./vol, tmp_d, vol);

        // use symmetric gradient (nabla K)^2 to get good d_mu T^mu0 = 0
        symSquareGradKernel(tmp_d, buffer_d);
    }

    // DFT to 2T[pi](q)
    if(cufftR2C(kplan, tmp_d, vkfield) != CUFFT_SUCCESS) throw std::runtime_error("doUpdateStressTensor - 2nd cufft execution failed"); checkCudaError(cudaDeviceSynchronize(), "sync after r2c");

    checkCudaError(cudaMemcpy(T.data(), vkfield, lq*sizeof(cuffTcomplex), cudaMemcpyDeviceToHost), "cuda memcpy failed");
    checkCudaError(cudaDeviceSynchronize(), "Device failed to sync");

    // T = 1/2 pi^2 resp. T = 1/2 (nabla K)^2
    std::transform(T.begin(), T.end(), T.begin(), [](Tcomplex x) { x *= 0.5; return x; }); 

    // copy to T+V to T[0]
    short tcomp = 0;
    std::transform(T.begin(), T.end(), V.begin(), stressTensor[tcomp].begin(), [](Tcomplex t, Tcomplex v) { return t + v; });

    // copy to T-V to L
    std::transform(T.begin(), T.end(), V.begin(), L.begin(), [](Tcomplex t, Tcomplex v) { return t - v; });
    BOOST_LOG_TRIVIAL(trace) << "updateStress - Trafos to L";

    // energy flux
    const Tfloat * const KPointer = (getDynMode() == relax)?nullptr:buffer_d;
    for (short j=1;j<=2;j++) {
        tcomp=j;
        BOOST_LOG_TRIVIAL(trace) << "updateStress - energy flux " << j;

        if (getDynMode() == relax)
            mDensKernel(tmp_d, getPiPointer(), j); // is symmetric for relax dynamics
        else
            eFluxBKernel(tmp_d, KPointer, j);

        if(cufftR2C(kplan, tmp_d, vkfield) != CUFFT_SUCCESS) throw std::runtime_error("doUpdateStressTensor - 3rd cufft execution failed"); checkCudaError(cudaDeviceSynchronize(), "sync after r2c");
        checkCudaError(cudaMemcpy(stressTensor[tcomp].data(), vkfield, lq*sizeof(cuffTcomplex), cudaMemcpyDeviceToHost), "cuda memcpy failed");
        checkCudaError(cudaDeviceSynchronize(), "Device failed to sync");
    }

    // calculate longitudinal and transversal momentum density
    for (short j=1;j<=2;j++) {
        tcomp=2+j;
        BOOST_LOG_TRIVIAL(trace) << "updateStress - momentum " << j;
        if (getDynMode() == relax) {
            // just assign the pointer, no need to copy redundant stuff
            stressTensor[tcomp] = stressTensor[j];
        } else {
            mDensKernel(tmp_d, KPointer, j);

            if(cufftR2C(kplan, tmp_d, vkfield) != CUFFT_SUCCESS) throw std::runtime_error("doUpdateStressTensor - 3rd cufft execution failed"); checkCudaError(cudaDeviceSynchronize(), "sync after r2c");
            checkCudaError(cudaMemcpy(stressTensor[tcomp].data(), vkfield, lq*sizeof(cuffTcomplex), cudaMemcpyDeviceToHost), "cuda memcpy failed");
            checkCudaError(cudaDeviceSynchronize(), "Device failed to sync");
        }
    }

    for (short j=1;j<=2;j++) {
        // calculate pressure using L[q] from before, T[q] as buffer
        tcomp=4+j;
        BOOST_LOG_TRIVIAL(trace) << "updateStress - pressure " << j;

        shearKernel(tmp_d, KPointer, j, j);

        if(cufftR2C(kplan, tmp_d, vkfield) != CUFFT_SUCCESS) throw std::runtime_error("doUpdateStressTensor - 4th cufft execution failed"); checkCudaError(cudaDeviceSynchronize(), "sync after r2c");
        checkCudaError(cudaMemcpy(T.data(), vkfield, lq*sizeof(cuffTcomplex), cudaMemcpyDeviceToHost), "cuda memcpy failed");
        checkCudaError(cudaDeviceSynchronize(), "Device failed to sync");
        // copy to stressTensor[i] 
        std::transform(T.begin(), T.end(), L.begin(), stressTensor[tcomp].begin(), [](Tcomplex t, Tcomplex l) { return t + l; });
    }

    short d = Dims().getD();
    for (short j=2;j<=d;j++) {
        short k=j-1;
        tcomp = 6+k;
        // calculate shear stress
        BOOST_LOG_TRIVIAL(trace) << "updateStress - shear stress " << j << ", " << k;
        shearKernel(tmp_d, KPointer, k, j);
        if(cufftR2C(kplan, tmp_d, vkfield) != CUFFT_SUCCESS) throw std::runtime_error("doUpdateStressTensor - 5th cufft execution failed"); checkCudaError(cudaDeviceSynchronize(), "sync after r2c");
        checkCudaError(cudaMemcpy(stressTensor[tcomp].data(), vkfield, lq*sizeof(cuffTcomplex), cudaMemcpyDeviceToHost), "cuda memcpy failed");
        checkCudaError(cudaDeviceSynchronize(), "Device failed to sync");
        }

    // normalize all
    for (short c=0;c<=tcomp;c++) std::transform(stressTensor[c].begin(), stressTensor[c].end(), stressTensor[c].begin(), [vol](Tcomplex z) { z *= 1./vol; return z; });
}



void GPULattice::generateNoiseVector(const Tfloat amplitude)
{
        const size_t size = index->getPoints();
        const dim3 block = CGX_BLOCKSIZE;
        const dim3 grid = size/CGX_BLOCKSIZE + 1;

        for (ushort d=0;d<getDim();d++)
            fillGaussianKernel<<< grid, block >>>(vecbuf_d+d*size, 0, amplitude, rnd_state, size);
}


GPULattice::GPULattice(const size_t size, const unsigned short components, const hardware hw) :
    d_phi(size, components),
    d_pi(size, components),
    d_phi_backup(size, components)
{ 
    checkCudaError("before entering GPULattice constructor");
    BOOST_LOG_TRIVIAL(trace) << "GPULattice constructor";

    // set cuda device if available
    int count;
    cudaError_t res = cudaGetDeviceCount(&count);

    if ((res == cudaSuccess) && (hw.dev < count)) cudaSetDevice(hw.dev);
    else { BOOST_LOG_TRIVIAL(fatal) << boost::format("Cannot choose CUDA device. Aborting."); throw std::runtime_error("error"); };

    // allocate space for random state
    cudaMalloc(&rnd_state, index->getPoints() * sizeof(taus_state));
    checkCudaError("rnd_state malloc");

    cudaMalloc(&d_index, sizeof(latticeIndex **));
    checkCudaError("index malloc");

    seedTaus(time(0));
    checkCudaError("seedTaus");

    // allocate buffers
    cudaMalloc(&buffer_d, index->getPoints()*sizeof(Tfloat));
    checkCudaError("buffer_d malloc");

    cudaMalloc(&tmp_d, index->getPoints()*sizeof(Tfloat));
    checkCudaError("tmp_d malloc");

    // share buffer space
    d_phi.setBuffers(buffer_d, tmp_d);
    d_phi_backup.setBuffers(buffer_d, tmp_d);
    d_pi.setBuffers(buffer_d, tmp_d);

    // forward arrays to base class pointers
    phi	= &d_phi;
    pi	= &d_pi;

    // zMomentum stuff
    cudaMalloc(&cufft_result, index->getComponents()*(index->getL()/2 + 1)*5*sizeof(cuffTcomplex));
    checkCudaError("cufft_result malloc");

    zMomentumIsCurrent = EzMomentumIsCurrent = false;
}


GPULattice::~GPULattice()
{
    cudaFree(rnd_state);

    cudaFree(buffer_d);
    cudaFree(tmp_d);
    cudaFree(vecbuf_d);

    cudaFree(kfield);
    cudaFree(vkfield);
    cudaFree(cufft_result);

    phi = pi = nullptr;
};

// =============================== 3D ===================================================================

void GPU3DLattice::doUpdateZMomentum(const bool on_phi) {
    const uint 
        lx = indexer.getLx(),
           ly = indexer.getLy(), 
           lz = indexer.getLz();

    int major = lx*ly;
    dim3 grid(lx, ly);

    ushort rfac = (lx-1)/CU_MAX_THREADS+1;
    dim3 block = lx/rfac;

    ushort comp = indexer.getComponents();

    size_t vol = indexer.getPoints();

    Tfloat *fields = on_phi?getPhiPointer():getPiPointer();
    std::vector<Tcomplex> *zMomentum = on_phi?PhizMomentum:PizMomentum;

    for (ushort c=0;c<comp;c++)
        ySumTrafoKernel<<< grid, block, lx*sizeof(Tfloat)>>>(tmp_d + (c*major), fields, comp, c, rfac, vol);

    checkCudaError("first ysum");
    checkCudaError(cudaDeviceSynchronize(), "sync after first ysum");

    grid = dim3(lz);

    rfac = (lx-1)/CU_MAX_THREADS+1;
    block = ly/rfac;

    for (ushort c=0;c<comp;c++)
        ySumTrafoKernel<<< grid, block, ly*sizeof(Tfloat)>>>(buffer_d + c*lz, tmp_d + (c*major), 1, 0, rfac, lx*ly);

    checkCudaError("second ysum");
    checkCudaError(cudaDeviceSynchronize(), "sync after second ysum");

    for (ushort c=0;c<comp;c++) {
        if(cufftR2C(plan, buffer_d + c*lz, cufft_result + c*(lz/2+1)) != CUFFT_SUCCESS) throw std::runtime_error("cufft execution failed");
        checkCudaError(cudaDeviceSynchronize(), "device sync after cufft");

        checkCudaError(cudaMemcpy(zMomentum[c].data(), cufft_result+c*(lz/2+1), (lz/2+1)*sizeof(cuffTcomplex), cudaMemcpyDeviceToHost), "memcpy");
        checkCudaError(cudaDeviceSynchronize(), "device failed to sync");
    }
}


void GPU3DLattice::timesInvKsqr(cuffTcomplex * const field, bool abs)
{ 
    size_t hsize = indexer.getKVol(); 
    invKsqrKernel<<< hsize/CGX_BLOCKSIZE + 1, CGX_BLOCKSIZE >>>(field, abs, d3_index, hsize);
    checkCudaError(cudaDeviceSynchronize(), "sync after timesInvKsqr");
}

/**
 * @Synopsis Hamiltonian momentum update by timestep dt.
 *				Much, much more complex operation necessary than for an update in phi. 
 *				GPU Kernel operates not per vector component, only per lattice node, so "less parallel."
 *				Of course that should not be noticeable for NoC=1.
 *
 * @Param dt the timestep.
 */
void GPU3DLattice::stepPiRelax(const Tfloat dt) {
    if (indexer.getComponents() > NCMAX) throw std::out_of_range("WARNING: Momentum update kernel cannot handle this!!");

    const dim3 block = CGX_BLOCKSIZE;
    const dim3 grid = indexer.getElements()/CGX_BLOCKSIZE + 1;

    stepMomentumKernel<<< grid, block >>>(getPiPointer(), getPhiPointer(), dt, lmu0sq, lg0, la, J, d3_index, indexer.getElements());
    checkCudaError("stepMomentumKernel");
}


void GPU3DLattice::stepPiDiff(const Tfloat dt) {
    if (indexer.getComponents() > NCMAX) throw std::out_of_range("WARNING: Momentum update kernel cannot handle this!!");

    const dim3 block = CGX_BLOCKSIZE;
    const dim3 grid = indexer.getElements()/CGX_BLOCKSIZE + 1;

    stepMomentumConsKernel<<< grid, block >>>(getPiPointer(), getPhiPointer(), dt, lmu0sq, lg0, la, J, d3_index, indexer.getElements());
    checkCudaError("stepMomentumKernel");
}


/**
 * @Synopsis applies inverse laplace to pi^2(x), integrates over space and returns the result
 *
 *      The current version operates in k-space; i.e. cufft-transform pi^2(x) to pi(-k)pi(k),
 *      multiply with k^(-2), cufft-inverse-transform back to lap^-1 pi^2(x), integrate
 */
Tfloat GPU3DLattice::dogetinvlappisq() const {
    const size_t size = indexer.getElements();
    const dim3 block = CGX_BLOCKSIZE;

    cufftResult cr = CUFFT_NOT_SUPPORTED;
    Tfloat result = 0.;

    // 1st, copy pi(x) to buffer_d
    cudaMemcpy(buffer_d, getPiPointer(), size*sizeof(Tfloat), cudaMemcpyDeviceToDevice);
    checkCudaError("invlap - memcpy");

    // 2nd, cufft to k-space
    cr = cufftR2C(kplan, buffer_d, kfield);
    if( cr != CUFFT_SUCCESS) throw std::runtime_error("invlap - 1st cufft execution failed");
    checkCudaError(cudaDeviceSynchronize(), "sync after r2c");

    // 3rd, multiply with 1/k^2
    const size_t hsize = indexer.getLy()*indexer.getLy()*(indexer.getLx()/2+1);
    invKsqrKernel<<< hsize/CGX_BLOCKSIZE + 1, block >>>(kfield, true, d3_index, hsize);
    checkCudaError("invKsqrKernel");
    checkCudaError(cudaDeviceSynchronize(), "sync after invKsqrKernel");

    // 4th, integrate
    //result = reduce(buffer_d, tmp_d, size)/size;
    result = reduce((Tfloat*)kfield, tmp_d, 2*hsize)/size;
    checkCudaError("invlap - reduce");

    return result;
}


void GPULattice::addGradNoiseToPi()
{
    const size_t size = index->getElements();
    const dim3 block = CGX_BLOCKSIZE;
    const dim3 grid = size/CGX_BLOCKSIZE + 1;

    addGradKernel<<< grid, block >>>(getPiPointer(), vecbuf_d, d_index, size);
}

GPU3DLattice::GPU3DLattice(const size_t lx, const size_t ly, const size_t lz, const unsigned short components, const hardware hw) :
    iLattice3D(lx, ly, lz, components), GPULattice(lx*ly*lz, components, hw)
{
    BOOST_LOG_TRIVIAL(trace) << "GPU3DLattice constructor";

    cudaMalloc(&d_index, sizeof(lattice3DIndex));
    checkCudaError("index malloc");
    createIndexOnGPU<<< 1, 1 >>>(d_index, lx, ly, lz, components);
    checkCudaError("index creation");
    d3_index = (const lattice3DIndex **)d_index;

    cudaMalloc(&vecbuf_d, 3*lx*ly*lz*components*sizeof(Tfloat));
    checkCudaError("vecbuf malloc");

    cudaMalloc(&kfield, lz*ly*(lx/2+1)*sizeof(cuffTcomplex));
    checkCudaError("kfield malloc");
    cudaMalloc(&vkfield, lz*ly*(lx/2+1)*sizeof(cuffTcomplex));
    checkCudaError("vkfield malloc");

    planFFT();
    cufftResult cr;
    cr = cufftPlan3d(&kplan, indexer.getLz(), indexer.getLy(), indexer.getLx(), cuR2C);
    if (cr != CUFFT_SUCCESS) throw std::runtime_error("r2c plan failed; Error No. "+std::to_string(cr));
    cr = cufftPlan3d(&ikplan, indexer.getLz(), indexer.getLy(), indexer.getLx(), cuC2R);
    if (cr != CUFFT_SUCCESS) throw std::runtime_error("c2r plan failed; Error No. "+std::to_string(cr));
}


GPU3DLattice::~GPU3DLattice()
{
    freeIndexOnGPU<<< 1, 1 >>>(d3_index);
    cudaFree(d3_index);
    checkCudaError("GPU3DLattice destructor");
}


//============================================= 2D =========================================//

void GPU2DLattice::doUpdateZMomentum(const bool on_phi) {
    const size_t 
        lx = indexer.getLx(), ly = indexer.getLy();

    ushort rfac = (lx-1)/CU_MAX_THREADS+1; // max 1024 threads per block

    dim3 grid = ly;
    dim3 block = lx/rfac;
    ushort comp = indexer.getComponents();
    size_t vol = lx*ly*comp;

    Tfloat *fields = on_phi?getPhiPointer():getPiPointer();
    std::vector<Tcomplex> *zMomentum = on_phi?PhizMomentum:PizMomentum;

    for (ushort c=0;c<comp;c++)
        ySumTrafoKernel<<< grid, block, lx*sizeof(Tfloat)>>>(buffer_d + (c*ly), fields, comp, c, rfac, vol);

    checkCudaError("ysum");
    checkCudaError(cudaDeviceSynchronize(), "sync after ysum");

    for (ushort c=0;c<comp;c++) {
        if(cufftR2C(plan, buffer_d + c*ly, cufft_result + c*(ly/2+1)) != CUFFT_SUCCESS) throw std::runtime_error("cufft execution failed");
        checkCudaError(cudaDeviceSynchronize(), "Device failed to sync after FFT");

        checkCudaError(cudaMemcpy(zMomentum[c].data(), cufft_result+c*(ly/2+1), (ly/2+1)*sizeof(cuffTcomplex), cudaMemcpyDeviceToHost), "memcpy");
        checkCudaError(cudaDeviceSynchronize(), "Device failed to sync");
    }
}


void GPU2DLattice::timesInvKsqr(cuffTcomplex * const field, bool abs)
{ 
    size_t hsize = indexer.getKVol(); 
    invKsqrKernel<<< hsize/CGX_BLOCKSIZE+1, CGX_BLOCKSIZE >>>(field, abs, d2_index, hsize);
    checkCudaError(cudaDeviceSynchronize(), "sync after timesInvKsqr");
}


void GPU2DLattice::stepPiRelax(const Tfloat dt) {
    if (indexer.getComponents() > NCMAX) throw std::out_of_range("WARNING: Momentum update kernel cannot handle this!!");

    const dim3 block = CGX_BLOCKSIZE;
    const dim3 grid = indexer.getElements()/CGX_BLOCKSIZE + 1;


    stepMomentumKernel<<< grid, block >>>(getPiPointer(), getPhiPointer(), dt, lmu0sq, lg0, la, J, d2_index, indexer.getElements());
    checkCudaError("stepMomentumKernel");
};


void GPU2DLattice::stepPiDiff(const Tfloat dt) {
    if (indexer.getComponents() > NCMAX) throw std::out_of_range("WARNING: Momentum update kernel cannot handle this!!");

    const dim3 block = CGX_BLOCKSIZE;
    const dim3 grid = indexer.getElements()/CGX_BLOCKSIZE + 1;

    stepMomentumConsKernel<<< grid, block >>>(getPiPointer(), getPhiPointer(), dt, lmu0sq, lg0, la, J, d2_index, indexer.getElements());
    checkCudaError("stepMomentumKernel");
};


/**
 * @Synopsis applies inverse laplace to pi^2(x), integrates over space and returns the result
 *
 *      The current version operates in k-space; i.e. cufft-transform pi^2(x) to pi(-k)pi(k),
 *      multiply with k^(-2), cufft-inverse-transform back to lap^-1 pi^2(x), integrate
 */
Tfloat GPU2DLattice::dogetinvlappisq() const {
    const size_t size = indexer.getElements();
    const dim3 block = CGX_BLOCKSIZE;

    cufftResult cr = CUFFT_NOT_SUPPORTED;
    Tfloat result = 0.;

    // 1st, copy pi(x) to buffer_d
    cudaMemcpy(buffer_d, getPiPointer(), size*sizeof(Tfloat), cudaMemcpyDeviceToDevice);
    checkCudaError("invlap - memcpy");

    // 2nd, cufft to k-space
    cr = cufftR2C(kplan, buffer_d, kfield);
    if( cr != CUFFT_SUCCESS) throw std::runtime_error("invlap - 1st cufft execution failed");
    checkCudaError(cudaDeviceSynchronize(), "sync after r2c");

    // 3rd, multiply with 1/k^2
    const size_t hsize = indexer.getLy()*(indexer.getLx()/2+1);
    invKsqrKernel<<< hsize/CGX_BLOCKSIZE + 1, block >>>(kfield, true, d2_index, hsize);
    checkCudaError("invKsqrKernel");
    checkCudaError(cudaDeviceSynchronize(), "sync after invKsqrKernel");

    // 4th, integrate
    //result = reduce(buffer_d, tmp_d, size)/size;
    result = reduce((Tfloat*)kfield, tmp_d, 2*hsize)/size;
    checkCudaError("invlap - reduce");

    return result;
}


GPU2DLattice::GPU2DLattice(const size_t lx, const size_t ly, const unsigned short components, const hardware hw) :
    iLattice2D(lx, ly, components), GPULattice(lx*ly, components, hw)
{
    BOOST_LOG_TRIVIAL(trace) << "GPU2DLattice constructor";

    cudaMalloc(&d_index, sizeof(lattice2DIndex **));
    checkCudaError("index malloc");
    createIndexOnGPU<<< 1, 1 >>>(d_index, lx, ly, components);
    checkCudaError("index creation");
    d2_index = (const lattice2DIndex **)d_index;

    cudaMalloc(&vecbuf_d, 2*lx*ly*components*sizeof(Tfloat));
    checkCudaError("vecbuf malloc");

    cudaMalloc(&kfield, ly*(lx/2+1)*sizeof(cuffTcomplex));
    checkCudaError("kfield malloc");
    cudaMalloc(&vkfield, ly*(lx/2+1)*sizeof(cuffTcomplex));
    checkCudaError("vkfield malloc");

    planFFT();
    cufftResult cr;
    cr = cufftPlan2d(&kplan, indexer.getLy(), indexer.getLx(), cuR2C);
    if (cr != CUFFT_SUCCESS) throw std::runtime_error("r2c plan failed; Error No. "+std::to_string(cr));
    cr = cufftPlan2d(&ikplan, indexer.getLy(), indexer.getLx(), cuC2R);
    if (cr != CUFFT_SUCCESS) throw std::runtime_error("c2r plan failed; Error No. "+std::to_string(cr));
}

GPU2DLattice::~GPU2DLattice()
{
    freeIndexOnGPU<<< 1, 1 >>>(d2_index);
    cudaFree(d2_index);
    checkCudaError("GPU2DLattice destructor");
}
