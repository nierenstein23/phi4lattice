#define BOOST_TEST_MODULE Array Test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <gpu_rvectarray.hcu>
#include <cpu_rvectarray.hpp>
#include <iostream>
#include <stdexcept>

#include <tausworthe.h>
#include <cuda_runtime.h>
#include <util.hcu>

#define TESTSIZE 64*64*64
#define TESTCOMP 4

struct Fix {
    Fix() : arraySize(TESTSIZE*TESTCOMP), 
    ga1(TESTSIZE, TESTCOMP),
    ga2(TESTSIZE, TESTCOMP),
    ca1(TESTSIZE, TESTCOMP),
    ca2(TESTSIZE, TESTCOMP)
    {
        // seed rng
        srand48(time(0));

        rnd_state = new taus_state[arraySize];
        for (size_t i=0;i<arraySize;i++)
            rnd_state[i] = taus_seed();

        cudaMalloc((void**) &d_rnd_state, arraySize*sizeof(taus_state));
        cudaMemcpy(d_rnd_state, rnd_state, arraySize*sizeof(taus_state), cudaMemcpyHostToDevice);
    };

    ~Fix() {
        delete[] rnd_state;
        cudaFree(d_rnd_state);
    };

    taus_state *rnd_state;
    taus_state *d_rnd_state;
    size_t arraySize;

    GPUrvectArray ga1, ga2;
    CPUrvectArray ca1, ca2;
};

BOOST_FIXTURE_TEST_SUITE(array_test_suite, Fix)
    BOOST_AUTO_TEST_CASE(simpleMembers) {
        BOOST_CHECK_EQUAL(ga1.NoFloats(), arraySize);
        BOOST_CHECK_EQUAL(ga1.NoElements(), TESTSIZE);
        BOOST_CHECK_EQUAL(ga1.NoComponents(), TESTCOMP);
        BOOST_CHECK_EQUAL(ca1.NoFloats(), arraySize);
        BOOST_CHECK_EQUAL(ca1.NoElements(), TESTSIZE);
        BOOST_CHECK_EQUAL(ca1.NoComponents(), TESTCOMP);
    }

BOOST_AUTO_TEST_CASE(setGaussianRandom) {
    ga1.setGaussianRandom(0.0, 1.0, d_rnd_state);
    ca1.setGaussianRandom(0.0, 1.0, rnd_state);

    BOOST_TEST((-1.0 * arraySize) < ga1.sumComponents());
    BOOST_TEST((1.0 * arraySize) > ga1.sumComponents());
    BOOST_TEST((-1.0 * arraySize) < ca1.sumComponents());
    BOOST_TEST((1.0 * arraySize) > ca1.sumComponents());
}

BOOST_AUTO_TEST_CASE(power_functions) {
    Tfloat cval = -1.0;
    ga1.setGaussianRandom(cval, 0.0, d_rnd_state);
    ca1.setGaussianRandom(cval, 0.0, rnd_state);

    BOOST_CHECK_EQUAL(ga1.sumComponents(), cval*arraySize);
    BOOST_CHECK_EQUAL(ga1.sumJthComponent(0), cval*TESTSIZE);
    BOOST_CHECK_EQUAL(ga1.sumSquares(), SQR(cval)*arraySize);
    BOOST_CHECK_EQUAL(ga1.sumQuartes(), SQR(SQR(cval)*TESTCOMP)*TESTSIZE);

    BOOST_CHECK_EQUAL(ca1.sumComponents(), cval*arraySize);
    BOOST_CHECK_EQUAL(ca1.sumJthComponent(0), cval*TESTSIZE);
    BOOST_CHECK_EQUAL(ca1.sumSquares(), SQR(cval)*arraySize);
    BOOST_CHECK_EQUAL(ca1.sumQuartes(), SQR(SQR(cval)*TESTCOMP)*TESTSIZE);
}

BOOST_AUTO_TEST_CASE(saxpy) {
    ga1.setGaussianRandom(-2.0, 0.0, d_rnd_state);
    ga2.setGaussianRandom(2.0, 0.0, d_rnd_state);
    ca1.setGaussianRandom(-2.0, 0.0, rnd_state);
    ca2.setGaussianRandom(2.0, 0.0, rnd_state);

    ga1.saxpy(0.5, ga2); // -2.0 + 0.5*2.0 = -1.0 (?)
    ca1.saxpy(0.5, ca2); 
    BOOST_CHECK_EQUAL(ga1.sumComponents(), -1.0*arraySize);
    BOOST_CHECK_EQUAL(ga1.sumSquares(), 1.0*arraySize);
    BOOST_CHECK_EQUAL(ga1.sumQuartes(), TESTCOMP*arraySize);
    BOOST_CHECK_EQUAL(ca1.sumComponents(), -1.0*arraySize);
    BOOST_CHECK_EQUAL(ca1.sumSquares(), 1.0*arraySize);
    BOOST_CHECK_EQUAL(ca1.sumQuartes(), TESTCOMP*arraySize);
}

BOOST_AUTO_TEST_CASE(copyFrom) {
    ga1.setGaussianRandom(0.0, 1.0, d_rnd_state);
    ga2.copyFrom(ga1);

    ca1.setGaussianRandom(0.0, 1.0, rnd_state);
    ca2.copyFrom(ca1);

    BOOST_CHECK_EQUAL(ga1.sumComponents(), ga2.sumComponents());
    BOOST_CHECK_EQUAL(ga1.sumJthComponent(0), ga2.sumJthComponent(0));
    BOOST_CHECK_EQUAL(ga1.sumSquares(), ga2.sumSquares());
    BOOST_CHECK_EQUAL(ga1.sumQuartes(), ga2.sumQuartes());

    BOOST_CHECK_EQUAL(ca1.sumComponents(), ca2.sumComponents());
    BOOST_CHECK_EQUAL(ca1.sumJthComponent(0), ca2.sumJthComponent(0));
    BOOST_CHECK_EQUAL(ca1.sumSquares(), ca2.sumSquares());
    BOOST_CHECK_EQUAL(ca1.sumQuartes(), ca2.sumQuartes());
}

BOOST_AUTO_TEST_CASE(interop) {
    ga1.setGaussianRandom(0.0, 1.0, d_rnd_state);
    ca1.copyFrom(ga1);

    ca2.setGaussianRandom(0.0, 1.0, rnd_state);
    ga2.copyFrom(ca2);

    BOOST_CHECK_EQUAL(ga1.sumComponents(), ca1.sumComponents());
    BOOST_CHECK_EQUAL(ga2.sumComponents(), ca2.sumComponents());
}

BOOST_AUTO_TEST_CASE(randomDamp) {
    // null the arrays
    ga1.setGaussianRandom(0.0, 0.0, d_rnd_state);
    ca1.setGaussianRandom(0.0, 0.0, rnd_state);

    // randomize a bit
    ga1.randomizeAndDamp(1.0, 0.0, d_rnd_state);
    ca1.randomizeAndDamp(1.0, 0.0, rnd_state);

    // check expectation values
    BOOST_TEST(ga1.sumSquares() > 0.0);
    BOOST_TEST(ca1.sumSquares() > 0.0);

    // set arrays to twos
    ga1.setGaussianRandom(2.0, 0.0, d_rnd_state);
    ca1.setGaussianRandom(2.0, 0.0, rnd_state);
    BOOST_CHECK_EQUAL(ga1.sumComponents(), 2*arraySize);
    BOOST_CHECK_EQUAL(ca1.sumComponents(), 2*arraySize);

    // enforce damping
    ga1.randomizeAndDamp(0.0, 0.5, d_rnd_state);
    ca1.randomizeAndDamp(0.0, 0.5, rnd_state);

    // check expectation values
    BOOST_CHECK_EQUAL(ga1.sumComponents(), arraySize);
    BOOST_CHECK_EQUAL(ca1.sumComponents(), arraySize);

};

BOOST_AUTO_TEST_SUITE_END()
