#include <lattice_factory.h>
#include <lattice_evolver.h>

#include <boost/timer/timer.hpp>
#include <omp.h>
#include <sstream>
#include <logger.h>
logger::log_level_t GLOG_LEVEL = logger::LOG_INFO;

#define STEPS 1000

namespace bt = boost::timer;

void readObs(const iLattice3D *lat) {
    lat->getPhiJ(0);
    lat->getPiJ(0);
    lat->getPhiSq();
    lat->getPiSq();
    lat->getPhiQuart();
    lat->getGradPhiSq();
}

int main(int argc, char *argv[]) {
    
    int L=32, c=1;
    float Temp = 9.37;
    if (argc > 1) L = atoi(argv[1]);
    if (argc > 2) c = atoi(argv[2]);
    if (argc > 3) Temp = atof(argv[3]);
    
    iLattice3D *lat = latticeFactory::buildIsoLattice(L, c, CPU);
    LatticeEvolver ev(lat);

    std::stringstream infostream;
    infostream << " " << omp_get_max_threads() << " " << L << " " << c << "\t";

    // start measuring
    /*
    {   // real-case HMC
        ev.resetTime();
        bt::auto_cpu_timer t("%w %p\n");
        ev.HMC(Temp, STEPS, 0);
        std::cout << "HMC" << infostream.str();
    }

    {   // real-case RTL
        ev.resetTime();
        bt::auto_cpu_timer t("%w %p\n");
        ev.RTLangevin(Temp, STEPS/10, 160);
        std::cout << "RTL" << infostream.str();
    }

    {   // real-case RTC
        ev.resetTime();
        bt::auto_cpu_timer t("%w %p\n");
        ev.RTClassical(Temp, STEPS/10, 160);
        std::cout << "RTC" << infostream.str();
    }
    */

    {   // synthetic step
        bt::auto_cpu_timer t("%w %p\n");
        for (int i=0;i<STEPS;i++)
            lat->stepPi(0.001);
        std::cout << "DPi" << infostream.str();
    }

    /*
    {   // synthetic randomization
        bt::auto_cpu_timer t("%w %p\n");
        for (int i=0;i<STEPS;i++)
            lat->stepPiLangevin(Temp, 0.001);
        std::cout << "RPi" << infostream.str();
    }
    */

    bt::nanosecond_type phitime;
    float convconst = 1e-9;

    {   // synthetic step
        lat->randomizePi(Temp);
        bt::auto_cpu_timer t("%w %p\n");
        bt::cpu_timer phitimer;
        phitimer.start();
        for (int i=0;i<STEPS;i++)
            lat->stepPhi(0.0005);
        phitimer.stop(); phitime = phitimer.elapsed().wall;
        std::cout << "DPh" << infostream.str();
    }

    {
        Tfloat result;
        lat->randomizePi(Temp);
        lat->randomizePhi(0);
        bt::auto_cpu_timer t("%w %p\n");
        bt::cpu_timer mphitimer; mphitimer.start();
        for (int i=0;i<STEPS;i++) {
            lat->stepPhi(0.0005);
            result=lat->getPhiSq();
        }
        mphitimer.stop(); bt::nanosecond_type parttime = mphitimer.elapsed().wall - phitime;
        std::cout << "OSq" << infostream.str() << " " << parttime*convconst << "\t";
    }

    {
        Tfloat result;
        lat->randomizePi(Temp);
        lat->randomizePhi(0);
        bt::auto_cpu_timer t("%w %p\n");
        bt::cpu_timer mphitimer; mphitimer.start();
        for (int i=0;i<STEPS;i++) {
            lat->stepPhi(0.0005);
            result=lat->getPhiQuart();
        }
        mphitimer.stop(); bt::nanosecond_type parttime = mphitimer.elapsed().wall - phitime;
        std::cout << "OQu" << infostream.str() << " " << parttime*convconst << "\t";// << "result: " << result << std::endl;
    }

    {
        lat->randomizePi(Temp);
        lat->randomizePhi(0);
        bt::auto_cpu_timer t("%w %p\n");
        bt::cpu_timer mphitimer; mphitimer.start();
        for (int i=0;i<STEPS;i++) {
            lat->stepPhi(0.0005);
            lat->getSliceCorrelation(1);
            }
        mphitimer.stop(); bt::nanosecond_type parttime = mphitimer.elapsed().wall - phitime;
        std::cout << "OCo" << infostream.str() << " " << parttime*convconst << "\t";
    }

    {
            Tfloat result;
        lat->randomizePi(Temp);
        lat->randomizePhi(0);
        bt::auto_cpu_timer t("%w %p\n");
        bt::cpu_timer mphitimer; mphitimer.start();
        for (int i=0;i<STEPS;i++) {
            lat->stepPhi(0.0005);
            result=lat->getGradPhiSq();
        }
        mphitimer.stop(); bt::nanosecond_type parttime = mphitimer.elapsed().wall - phitime;
        std::cout << "OGr" << infostream.str() << " " << parttime*convconst << "\t";// << "result: " << result << std::endl;
    }


    {
        lat->randomizePi(Temp);
        lat->randomizePhi(0);
        bt::auto_cpu_timer t("%w %p\n");
        bt::cpu_timer mphitimer; mphitimer.start();
        for (int i=0;i<STEPS;i++) {
            lat->stepPhi(0.0005);
            lat->getPhiJ(0);
        }
        mphitimer.stop(); bt::nanosecond_type parttime = mphitimer.elapsed().wall - phitime;
        std::cout << "PhiJ" << infostream.str() << " " << parttime*convconst << "\t";
    }
}
