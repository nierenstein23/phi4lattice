#define BOOST_TEST_MODULE Lattice Test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <lattice.h>
#include <lattice_factory.h>

#include <cuda_runtime.h>
#include <util.hcu>

#include <fstream>

#define L 128
#define C 4
#define V L*L

struct Fix {
    Fix() {
        //setCudaDevice(1);
        glat = latticeFactory::buildIsoLattice(L,C,{GPU, NULL, 0},2);
        clat = latticeFactory::buildIsoLattice(L,C,{CPU, NULL, 0},2);

        glat->generateLagrangeParameters(1.0, -1.0, 1.0);
        clat->generateLagrangeParameters(1.0, -1.0, 1.0);

        auto seed = time(0);
        srand48(seed); glat->seedTaus(seed); clat->seedTaus(seed);
    };
    ~Fix() {
        delete glat;
        delete clat;
    };

    iLattice *glat, *clat;
};

BOOST_FIXTURE_TEST_SUITE(glattice, Fix)
    BOOST_AUTO_TEST_CASE(basic_functions) {
        Tfloat asqmusq = (1-2*glat->getLambda())/(glat->getKappa()) - 8.0;

        BOOST_CHECK_EQUAL(asqmusq, -1.0);
    }

BOOST_AUTO_TEST_CASE(copy) {
    glat->randomizePi(1.0);
    clat->randomizePi(0.0);
    clat->copyFrom(*glat);

    BOOST_CHECK_EQUAL(glat->getSumPi(), clat->getSumPi());
};

BOOST_AUTO_TEST_CASE(grad_funcs) {
    glat->randomizePhi(0.0);
    clat->randomizePhi(0.0);
    BOOST_CHECK_EQUAL(glat->getGradPhiSq(), 0.0);
    BOOST_CHECK_EQUAL(clat->getGradPhiSq(), 0.0);

    Tfloat var=1.0;
    glat->randomizePhi(var);
    clat->copyFrom(*glat);

    BOOST_CHECK_EQUAL(clat->getGradPhiSq(), C*V*4.0*var);
    //BOOST_CHECK_EQUAL(glat->getGradPhiSq(), C*V*2.0*var);
    BOOST_CHECK_EQUAL(glat->getGradPhiSq(), clat->getGradPhiSq());
}

BOOST_AUTO_TEST_CASE(corr_funcs) {
    //glat->randomizePhi(1.0);
    //clat->copyFrom( *glat );

    //		BOOST_CHECK_EQUAL(glat->getSliceCorrelation(1), clat->getSliceCorrelation(1));

    glat->setGaussPhi(1.0, 0.0);
    clat->setGaussPhi(1.0, 0.0);
    BOOST_CHECK_EQUAL(glat->getSliceCorrelation(1), C*V);
    BOOST_CHECK_EQUAL(clat->getSliceCorrelation(1), C*V);
}

/*
   BOOST_AUTO_TEST_CASE(fin_moms) {
   clat->randomizePhi(1.0);
//glat->copyFrom(*clat);
glat->randomizePhi(1.0);

std::vector<Tcomplex> result = clat->getPhiP(1);
for (short i=0;i<C;i++)
std::cout << result[i] << " ";
std::cout << std::endl;
result = glat->getPhiP(1);
for (short i=0;i<C;i++)
std::cout << result[i] << " ";
std::cout << std::endl;
BOOST_CHECK_EQUAL(glat->getPhiP(1), clat->getPhiP(1));
BOOST_CHECK_EQUAL(glat->getPhiP(0), clat->getPhiP(0));
BOOST_CHECK_EQUAL(glat->getPhiP(0), clat->getPhiP(0));
}

BOOST_AUTO_TEST_CASE(gpu_stepper) {
glat->setGaussPhi(0.0, 0.0);
glat->setGaussPi(1.0, 0.0);
BOOST_CHECK_EQUAL(glat->getSumPhi(), 0.0);
BOOST_CHECK_EQUAL(glat->getSumPi(), V*C);

glat->stepPi(1.0);
BOOST_CHECK_EQUAL(glat->getSumPi(), V*C);

glat->stepPhi(1.0);
BOOST_CHECK_EQUAL(glat->getSumPhi(), V*C);

glat->setGaussPhi(1.0, 0.0);
glat->setGaussPi(0.0, 0.0);
glat->stepPi(1.0);
BOOST_CHECK_EQUAL(glat->getSumPi(), V*C*(5.0/6.0));
}
*/

BOOST_AUTO_TEST_CASE(cpu_stepper) {
    clat->setGaussPhi(0.0, 0.0);
    clat->setGaussPi(1.0, 0.0);
    BOOST_CHECK_EQUAL(clat->getSumPhi(), 0.0);
    BOOST_CHECK_EQUAL(clat->getSumPi(), V*C);

    clat->stepPi(1.0);
    BOOST_CHECK_EQUAL(clat->getSumPi(), V*C);

    clat->stepPhi(1.0);
    BOOST_CHECK_EQUAL(clat->getSumPhi(), V*C);

    clat->setGaussPhi(1.0, 0.0);
    BOOST_CHECK_EQUAL(clat->getSumPhi(), V*C);
    clat->setGaussPi(0.0, 0.0);
    BOOST_CHECK_EQUAL(clat->getSumPi(), 0.0);
    clat->stepPi(1.0);
    BOOST_CHECK_EQUAL(clat->getSumPi(), V*C*(5.0/6.0));
}

/*
   BOOST_AUTO_TEST_CASE(reversibility) {
   clat->setGaussPi(1.0, 0.0);
   clat->setGaussPhi(1.0, 0.0);

   glat->setGaussPi(1.0, 0.0);
   glat->setGaussPhi(1.0, 0.0);

   clat->stepPhi(0.5);
   clat->stepPi(1.0);
   clat->stepPhi(0.5);

   clat->stepPhi(-0.5);
   clat->stepPi(-1.0);
   clat->stepPhi(-0.5);

   BOOST_CHECK_EQUAL(clat->getSumPhi(), V*C);
   BOOST_CHECK_EQUAL(clat->getSumPi(), V*C);
   BOOST_CHECK_EQUAL(clat->getPhiJ(0), V);
   BOOST_CHECK_EQUAL(clat->getPiJ(0), V);

   glat->stepPhi(0.5);
   glat->stepPi(1.0);
   glat->stepPhi(0.5);

   glat->stepPhi(-0.5);
   glat->stepPi(-1.0);
   glat->stepPhi(-0.5);

   BOOST_CHECK_EQUAL(glat->getSumPhi(), V*C);
   BOOST_CHECK_EQUAL(glat->getSumPi(), V*C);
   BOOST_CHECK_EQUAL(glat->getPhiJ(0), V);
   BOOST_CHECK_EQUAL(glat->getPiJ(0), V);
   }

   BOOST_AUTO_TEST_CASE(backup) {
   glat->randomizePi(1.0);
   glat->randomizePhi(1.0);

   clat->randomizePi(1.0);
   clat->randomizePhi(1.0);

   Tfloat g1=glat->getSumPhi(), c1=clat->getSumPhi();

   glat->backupPhi();
   clat->backupPhi();

   glat->stepPhi(1.0);
   clat->stepPhi(1.0);

   Tfloat g2=glat->getSumPhi(), c2=clat->getSumPhi();
   BOOST_TEST(g1 != g2);
   BOOST_TEST(c1 != c2);

   glat->restorePhi();
   clat->restorePhi();

   BOOST_CHECK_EQUAL(g1, glat->getSumPhi());
   BOOST_CHECK_EQUAL(c1, clat->getSumPhi());

   }

   BOOST_AUTO_TEST_CASE(fieldio) {
   {

   glat->randomizePi(1.0); glat->randomizePhi(1.0);

   std::ofstream oStream("glat.ar", std::ios::binary);
   glat->saveFields(oStream);
   oStream.close();

std::ifstream inStream("glat.ar", std::ios::binary);
clat->loadFields(inStream);
inStream.close();

BOOST_CHECK_EQUAL(glat->getSumPi(), clat->getSumPi());
BOOST_CHECK_EQUAL(glat->getSumPhi(), clat->getSumPhi());
}
}
*/

BOOST_AUTO_TEST_SUITE_END()
