#define BOOST_TEST_MODULE Basic Test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

struct F {
	F() { i=1; };
	~F() {};

	int i;
};

BOOST_AUTO_TEST_SUITE(basic_suite)
	BOOST_AUTO_TEST_CASE(basic_case) {
		int i=2;
		BOOST_TEST(i);
		BOOST_TEST(i==2);
	}

BOOST_AUTO_TEST_SUITE_END()

BOOST_FIXTURE_TEST_SUITE(s, F)
	BOOST_AUTO_TEST_CASE(fixture_case) {
		BOOST_CHECK_EQUAL(i, 1);
	}

BOOST_AUTO_TEST_SUITE_END()
