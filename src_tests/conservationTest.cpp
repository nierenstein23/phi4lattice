#define BOOST_TEST_MODULE Basic Test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <lattice_factory.h>
#include <lattice_evolver.h>
#include <logger.h>
logger::log_level_t GLOG_LEVEL = logger::LOG_INFO;

#define L 64
#define C 4
#define V L*L*L

struct F {
	F() 
	{ 
		cudaSetDevice(1);
		glat = latticeFactory::buildIsoLattice(L, C, GPU);
		clat = latticeFactory::buildIsoLattice(L, C, CPU);

		glat->generateLagrangeParameters(1.0, -1.0, 1.0);
		clat->generateLagrangeParameters(1.0, -1.0, 1.0);

		gev = new LatticeEvolver(glat);
		cev = new LatticeEvolver(clat);
		gobs = new Observer(glat);
		cobs = new Observer(clat);

		glat->randomizePi(1.0);
		glat->randomizePhi(1.0);
		clat->randomizePi(1.0);
		clat->randomizePhi(1.0);
	};

	~F() {
		delete glat;
		delete clat;
		delete gev;
		delete cev;
		delete gobs;
		delete cobs;
	};

	iLattice3D *glat;
	iLattice3D *clat;

	LatticeEvolver *gev, *cev;
	Observer *gobs, *cobs;
};

BOOST_FIXTURE_TEST_SUITE(s, F)
	BOOST_AUTO_TEST_CASE(benchmark) {
		Tfloat tmp;
		for (int i=0;i<100;i++) {
			gev->Leapfrog(23, 1.0/23);

			tmp = glat->getSumPi();
			tmp = glat->getSumPhi();
			tmp = glat->getPiSq();
			tmp = glat->getPhiSq();
			for (int j=0;j<C;j++) {
				tmp = glat->getPiJ(j);
				tmp = glat->getPhiJ(j);
			}
			tmp = glat->getGradPhiSq();

			tmp = glat->getSumPi();
			tmp = glat->getSumPhi();
			tmp = glat->getPiSq();
			tmp = glat->getPhiSq();
			for (int j=0;j<C;j++) {
				tmp = glat->getPiJ(j);
				tmp = glat->getPhiJ(j);
			}
			tmp = glat->getGradPhiSq();
			
		}
	}

	/*
	BOOST_AUTO_TEST_CASE(observer) {
		glat->setGaussPi(1.0, 0.0);
		glat->setGaussPhi(1.0, 0.0);

		BOOST_CHECK_EQUAL(glat->getPiSq(), V*C);
		BOOST_CHECK_EQUAL(glat->getPhiSq(), V*C);
		BOOST_CHECK_EQUAL(glat->getPhiQuart(), V*C*C);
		BOOST_CHECK_EQUAL(glat->getGradPhiSq(), 0.0);

		gobs->update();
		BOOST_CHECK_EQUAL(gobs->getEKin(), V*C/2.0);
		BOOST_CHECK_EQUAL(gobs->getEPot(), V*C*(1.0/24 - 0.5));
		BOOST_CHECK_EQUAL(gobs->getEnergy(), V*C/24.0);
	}

	BOOST_AUTO_TEST_CASE(leapfrog) {
		glat->randomizePi(1.0);
		glat->randomizePhi(1.0);

		gobs->update(); Tfloat Hg1 = gobs->getEnergy();

		Tfloat dt = 0.1;

	}

	BOOST_AUTO_TEST_CASE(econservation) {
		glat->randomizePi(1.0);
		clat->randomizePi(1.0);

		gev->HMC(10.0, 100,0, "OUTPUT/","HMCGPU");
		cev->HMC(10.0, 100,0, "OUTPUT/","HMCCPU");

		gobs->update(); Tfloat Hg1 = gobs->getEnergy();
		cobs->update(); Tfloat Hc1 = cobs->getEnergy();

		gev->resetTime();
		cev->resetTime();

		gev->RTClassical(100.0,0.2,"OUTPUT/","RTGPU");
		cev->RTClassical(100.0,0.2,"OUTPUT/","RTCPU");

		gobs->update(); 
		cobs->update();
		BOOST_CHECK_EQUAL(Hg1, gobs->getEnergy());
		BOOST_CHECK_EQUAL(Hc1, cobs->getEnergy());
	}
	*/

BOOST_AUTO_TEST_SUITE_END()
