#define BOOST_TEST_MODULE Lattice Test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <lattice.h>
#include <lattice_factory.h>

#include <cuda_runtime.h>
#include <util.hcu>

#include <fstream>

#define L 128
#define C 2
#define V L*L

struct Fix {
    Fix() {
        //setCudaDevice(1);
        glat = latticeFactory::buildIsoLattice(L,C,{GPU, NULL, 0},2);
        clat = latticeFactory::buildIsoLattice(L,C,{CPU, NULL, 0},2);

        glat->generateLagrangeParameters(1.0, -1.0, 1.0);
        clat->generateLagrangeParameters(1.0, -1.0, 1.0);

        auto seed = time(0);
        srand48(seed); glat->seedTaus(seed); clat->seedTaus(seed);
    };
    ~Fix() {
        delete glat;
        delete clat;
    };

    iLattice *glat, *clat;
};

BOOST_FIXTURE_TEST_SUITE(glattice, Fix)
    BOOST_AUTO_TEST_CASE(zMomentumStuff) {
        glat->setGaussPhi(10.0,.001);
        clat->copyFrom(*glat);

        //for (int i=1;1<L/4;i++) {
         //   std::vector<Tcomplex> result = glat->getPhiP(i);
            std::vector<Tcomplex> result = glat->getPhiP(1);
//            std::vector<Tcomplex> cresult = clat->getPhiP(0);

            for (unsigned short c=0;c<C;c++) {
                BOOST_CHECK_EQUAL(std::real(result[c])/(L*L), 0.0);
                BOOST_CHECK_EQUAL(std::imag(result[c])/(L*L), 0.0);
 //               BOOST_CHECK_EQUAL(std::real(cresult[c])/(L*L), 0.0);
 //               BOOST_CHECK_EQUAL(std::imag(cresult[c])/(L*L), 0.0);
            }
        //}

    }

BOOST_AUTO_TEST_SUITE_END()
