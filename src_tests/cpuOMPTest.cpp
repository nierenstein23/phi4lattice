#define BOOST_TEST_MODULE Basic Test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <omp.h>
#include <par_funcs.hpp>
#include <algorithm>

struct F {
	F() {};
	~F() {};

};

BOOST_FIXTURE_TEST_SUITE(s, F)
	BOOST_AUTO_TEST_CASE(fixture_case) {
	
		double result = 0.0;
		Tfloat array[11] = {1.0, 2.0, 3.0, 4.0, 5.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0};
		size_t size = 11;

		omp_set_num_threads(std::min(size, (size_t)omp_get_num_procs()));
		#pragma omp parallel for reduction(+:result)
		for (int i=0;i<omp_get_num_threads();i++) {
			int nthreads = omp_get_num_threads();
			size_t lowerIndex = i*(size/nthreads);
			size_t diff = size/nthreads;

			// if it is the last thread, adjust the end index
			if (i==nthreads-1)  {
				diff = size - lowerIndex;
			}

			result += reductionFunc(&array[lowerIndex], diff);
		}

		BOOST_CHECK_EQUAL(result, 9);
		BOOST_CHECK_EQUAL(reductionFunc(array, size), 9);

	}

BOOST_AUTO_TEST_SUITE_END()
