#define BOOST_TEST_MODULE Lattice Test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <lattice.h>
#include <lattice_factory.h>
#include <cpu_rvectarray.hpp>
#include <logger.h>
logger::log_level_t GLOG_LEVEL = logger::LOG_INFO;

#include <cuda_runtime.h>
#include <util.hcu>

#define L 64
#define C 4
#define V L*L*L

struct Fix {
	Fix() : carr(V,C)
	{
		clat = latticeFactory::buildIsoLattice(L,C,CPU);
		clat->generateLagrangeParameters(1.0, -1.0, 1.0);
		clat->setSource(0);

		rnd_state = new taus_state[V*C];
		for (size_t i=0;i<V*C;i++)
			rnd_state[i] = taus_seed();
	};

	~Fix() {
		delete[] rnd_state;
		delete clat;
	};

	taus_state *rnd_state;
	iLattice3D *clat;
	CPUrvectArray carr;
};

BOOST_FIXTURE_TEST_SUITE(cpushit, Fix)
	BOOST_AUTO_TEST_CASE(cpuarray) {
		carr.setGaussianRandom(0.0, 0.0, rnd_state);

		BOOST_CHECK_EQUAL(carr.sumComponents(), 0);
		BOOST_CHECK_EQUAL(carr.sumJthComponent(0), 0);
		BOOST_CHECK_EQUAL(carr.sumSquares(), 0);
		BOOST_CHECK_EQUAL(carr.sumQuartes(), 0);
	}

	BOOST_AUTO_TEST_CASE(grad_funcs) {
		clat->randomizePhi(0.0);
		BOOST_CHECK_EQUAL(clat->getGradPhiSq(), 0.0);

		clat->randomizePhi(1.0);
		BOOST_TEST(clat->getGradPhiSq() != 0.0);
	}

	BOOST_AUTO_TEST_CASE(cpu_stepper) {
		clat->setGaussPhi(0.0, 0.0);
		clat->setGaussPi(1.0, 0.0);
		BOOST_CHECK_EQUAL(clat->getSumPhi(), 0.0);
		BOOST_CHECK_EQUAL(clat->getSumPi(), V*C);

		clat->stepPi(1.0);
		BOOST_CHECK_EQUAL(clat->getSumPi(), V*C);

		clat->stepPhi(1.0);
		BOOST_CHECK_EQUAL(clat->getSumPhi(), V*C);

		clat->setGaussPhi(1.0, 0.0);
		BOOST_CHECK_EQUAL(clat->getSumPhi(), V*C);
		clat->setGaussPi(0.0, 0.0);
		BOOST_CHECK_EQUAL(clat->getSumPi(), 0.0);
		clat->stepPi(1.0);
		BOOST_CHECK_EQUAL(clat->getSumPi(), V*C*(5.0/6.0));
	}

	BOOST_AUTO_TEST_CASE(reversibility) {
		clat->setGaussPi(1.0, 0.0);
		clat->setGaussPhi(1.0, 0.0);

		clat->stepPhi(0.5);
		clat->stepPi(1.0);
		clat->stepPhi(0.5);

		clat->stepPhi(-0.5);
		clat->stepPi(-1.0);
		clat->stepPhi(-0.5);

		BOOST_CHECK_EQUAL(clat->getSumPhi(), V*C);
		BOOST_CHECK_EQUAL(clat->getSumPi(), V*C);
		BOOST_CHECK_EQUAL(clat->getPhiJ(0), V);
		BOOST_CHECK_EQUAL(clat->getPiJ(0), V);
	}

BOOST_AUTO_TEST_SUITE_END()
