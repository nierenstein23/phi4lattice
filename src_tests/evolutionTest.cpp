#define BOOST_TEST_MODULE Basic Test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <lattice_factory.h>
#include <lattice_evolver.h>
#include <logger.h>
logger::log_level_t GLOG_LEVEL = logger::LOG_INFO;

#define L 16
#define C 1
#define V L*L*L

struct F {
	F() 
	{ 
//		cudaSetDevice(1);
		glat = latticeFactory::buildIsoLattice(L, C, CPU);
		clat = latticeFactory::buildIsoLattice(L, C, CPU);

		glat->generateLagrangeParameters(1.0, -1.0, 1.0);
		clat->generateLagrangeParameters(1.0, -1.0, 1.0);
		glat->setSource(+0.05);
		clat->setSource(+0.05);

		gev = new LatticeEvolver(glat);
		cev = new LatticeEvolver(clat);

		glat->randomizePi(1.0);
		glat->randomizePhi(1.0);
		clat->randomizePi(1.0);
		clat->randomizePhi(1.0);
	};

	~F() {
		delete glat;
		delete clat;
		delete gev;
		delete cev;
	};

	iLattice3D *glat;
	iLattice3D *clat;

	LatticeEvolver *gev, *cev;
};

BOOST_FIXTURE_TEST_SUITE(s, F)
	BOOST_AUTO_TEST_CASE(forceshit) {
		glat->setGaussPi(1.0, 0.0);
		glat->setGaussPhi(0.0, 0.0);

		BOOST_CHECK_EQUAL(glat->getSumPhi(), 0.0);
		BOOST_CHECK_EQUAL(glat->getSumPi(), V*C);

		glat->stepPhi(1.0);
		BOOST_CHECK_EQUAL(glat->getSumPhi(), V*C);
		BOOST_CHECK_EQUAL(glat->getSumPi(), V*C);

		glat->stepPhi(-1.0);
		BOOST_CHECK_EQUAL(glat->getSumPhi(), 0.0);
		BOOST_CHECK_EQUAL(glat->getSumPi(), V*C);
		
		glat->stepPi(1.0);
		BOOST_CHECK_EQUAL(glat->getSumPhi(), 0.0);
		BOOST_CHECK_EQUAL(glat->getSumPi(), V*C);

		glat->setGaussPhi(1.0, 0.0);
		glat->setGaussPi(1.0, 0.0);

		glat->stepPi(1.0);
		BOOST_CHECK_EQUAL(glat->getSumPhi(), V*C);
		BOOST_CHECK_EQUAL(glat->getSumPi(), 11.0*V*C/6.0);

		glat->setGaussPhi(-1.0, 0.0);
		glat->setGaussPi(1.0, 0.0);
		glat->stepPi(1.0);
		BOOST_CHECK_EQUAL(glat->getSumPhi(), -V*C);
		BOOST_CHECK_EQUAL(glat->getSumPi(), 1.0*V*C/6.0);

	}

/*
	BOOST_AUTO_TEST_CASE(econservation) {
		gev->RTClassical(20.0,.01,"OUTPUT/","RTGPU");
		cev->RTClassical(20.0,.01,"OUTPUT/","RTCPU");
	}

	BOOST_AUTO_TEST_CASE(hmc) {
		glat->dumpInfo(8.0, "OUTPUT/");
		gev->HMC(8.0, 1000, 0, "OUTPUT/", "HMCGPU");
		cev->HMC(8.0, 1000, 0, "OUTPUT/", "HMCCPU");
	}
        */

BOOST_AUTO_TEST_SUITE_END()
