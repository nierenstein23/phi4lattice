#define BOOST_TEST_MODULE Index-Test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
//#include <boost/log/trivial.hpp>

#include <lattice_index.h>

BOOST_AUTO_TEST_SUITE(lattice_indexer)

BOOST_AUTO_TEST_CASE(simpleLattice) {
	lattice3DIndex lsym(4,1);
	size_t idx, x, y, z, nidx, xn, yn, zn;

	idx = 33;
	lsym.deindex(idx, x,y,z);

	BOOST_CHECK_EQUAL(x, 1);
	BOOST_CHECK_EQUAL(y, 0);
	BOOST_CHECK_EQUAL(z, 2);

	for (int dir=-3;dir<4;dir++) {
		nidx = lsym.getNeighbour(x,y,z,dir);
		lsym.deindex(nidx, xn,yn,zn);

		switch (dir) {
			case 1: {
						BOOST_CHECK_EQUAL(nidx, 34);
						BOOST_CHECK_EQUAL(xn, 2);
						BOOST_CHECK_EQUAL(yn, 0);
						BOOST_CHECK_EQUAL(zn, 2);
						break;
					}
			case -1: {
						BOOST_CHECK_EQUAL(nidx, 32);
						BOOST_CHECK_EQUAL(xn, 0);
						BOOST_CHECK_EQUAL(yn, 0);
						BOOST_CHECK_EQUAL(zn, 2);
						break;
					}
			case 2: {
						BOOST_CHECK_EQUAL(nidx, 37);
						BOOST_CHECK_EQUAL(xn, 1);
						BOOST_CHECK_EQUAL(yn, 1);
						BOOST_CHECK_EQUAL(zn, 2);
						break;
					}
			case -2: {
						BOOST_CHECK_EQUAL(nidx, 45);
						BOOST_CHECK_EQUAL(xn, 1);
						BOOST_CHECK_EQUAL(yn, 3);
						BOOST_CHECK_EQUAL(zn, 2);
						break;
					}
			case 3: {
						BOOST_CHECK_EQUAL(nidx, 49);
						BOOST_CHECK_EQUAL(xn, 1);
						BOOST_CHECK_EQUAL(yn, 0);
						BOOST_CHECK_EQUAL(zn, 3);
						break;
					}
			case -3: {
					BOOST_CHECK_EQUAL(nidx, 17);
						BOOST_CHECK_EQUAL(xn, 1);
						BOOST_CHECK_EQUAL(yn, 0);
						BOOST_CHECK_EQUAL(zn, 1);
						break;
					}
			case 0: {
						BOOST_CHECK_EQUAL(nidx, 33);
						BOOST_CHECK_EQUAL(xn, 1);
						BOOST_CHECK_EQUAL(yn, 0);
						BOOST_CHECK_EQUAL(zn, 2);
						break;
					}
		}
	}
}

BOOST_AUTO_TEST_CASE(huge_complex_lattice) {
	size_t lx = 256, ly = 128, lz = 64;
	unsigned short comp = 4;

	lattice3DIndex lasym(lx, ly, lz, comp);
	size_t x,y,z,idx;
	size_t xn,yn,zn,nidx;

	x = 0;
	y = 0;
	z = 0;

	idx = lasym.getNeighbour(x,y,z,0);
	BOOST_CHECK_EQUAL(idx, lx*ly*comp*z + lx*comp*y + comp*x);

	lasym.deindex(idx, xn,yn,zn);
	BOOST_CHECK_EQUAL(xn, x);
	BOOST_CHECK_EQUAL(yn, y);
	BOOST_CHECK_EQUAL(zn, z);

	nidx = lasym.getNeighbour(x,y,z,-1);
	BOOST_CHECK_EQUAL(nidx, (lx - 1)*comp);

	lasym.deindex(nidx, xn,yn,zn);
	BOOST_CHECK_EQUAL(xn, 255);
	BOOST_CHECK_EQUAL(yn, y);
	BOOST_CHECK_EQUAL(zn, z);

}

BOOST_AUTO_TEST_CASE(dumb_test) {
	lattice3DIndex lnil(1,1);

	for (int i=-3;i<=3;i++) {
		BOOST_CHECK_EQUAL(lnil.getNeighbour(0,0,0,i), 0);
	}
}

BOOST_AUTO_TEST_CASE(component_shift) {
	lattice3DIndex liso(4,4);

	size_t x,y,z;

	liso.deindex(1, x,y,z);
	BOOST_CHECK_EQUAL(x, 0);
	BOOST_CHECK_EQUAL(y, 0);
	BOOST_CHECK_EQUAL(z, 0);

	liso.deindex(3, x,y,z);
	BOOST_CHECK_EQUAL(x, 0);
	BOOST_CHECK_EQUAL(y, 0);
	BOOST_CHECK_EQUAL(z, 0);

	liso.deindex(5, x,y,z);
	BOOST_CHECK_EQUAL(x, 1);
	BOOST_CHECK_EQUAL(y, 0);
	BOOST_CHECK_EQUAL(z, 0);

	liso.deindex(254, x,y,z);
	BOOST_CHECK_EQUAL(x, 3);
	BOOST_CHECK_EQUAL(y, 3);
	BOOST_CHECK_EQUAL(z, 3);
}

BOOST_AUTO_TEST_SUITE_END()
