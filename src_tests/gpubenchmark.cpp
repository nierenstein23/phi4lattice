#include <lattice_factory.h>
#include <lattice_evolver.h>

#include <boost/timer/timer.hpp>
#include <omp.h>
#include <sstream>
#include <logger.h>
logger::log_level_t GLOG_LEVEL = logger::LOG_INFO;

namespace bt = boost::timer;

int main(int argc, char *argv[]) {
	
	int L=32, c=1;
	float Temp = 9.366;
	if (argc > 1) L = atoi(argv[1]);
	if (argc > 2) c = atoi(argv[2]);
	if (argc > 3) Temp = atof(argv[3]);
	
	cudaSetDevice(1);
	iLattice3D *lat = latticeFactory::buildIsoLattice(L, c, GPU);
	lat->seedTaus(time(0));
	LatticeEvolver ev(lat);

	std::stringstream infostream;
	infostream << " " << 0 << " " << L << " " << c << " ";

	// start measuring
	{	// real-case HMC
		ev.resetTime();
		bt::auto_cpu_timer t("%w %p\n");
		ev.HMC(Temp, 1000, 0);
		std::cout << "HMC" << infostream.str();
	}

	{	// real-case RTL
		ev.resetTime();
		bt::auto_cpu_timer t("%w %p\n");
		ev.RTLangevin(Temp, 100, .1);
		std::cout << "RTL" << infostream.str();
	}

	{	// real-case RTC
		ev.resetTime();
		bt::auto_cpu_timer t("%w %p\n");
		ev.RTClassical(Temp, 100, .1);
		std::cout << "RTC" << infostream.str();
	}

	{	// synthetic step
		bt::auto_cpu_timer t("%w %p\n");
		for (int i=0;i<1000;i++)
			lat->stepPi(0.001);
		std::cout << "DPi" << infostream.str();
	}

	{	// synthetic randomization
		bt::auto_cpu_timer t("%w %p\n");
		for (int i=0;i<1000;i++)
			lat->stepPiLangevin(Temp, 0.001);
		std::cout << "RPi" << infostream.str();
	}
	
	{	// synthetic step
		lat->randomizePi(Temp);
		bt::auto_cpu_timer t("%w %p\n");
		for (int i=0;i<2000;i++)
			lat->stepPhi(0.0005);
		std::cout << "DPh" << infostream.str();
	}

}
