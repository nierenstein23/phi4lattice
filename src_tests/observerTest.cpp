#define BOOST_TEST_MODULE Basic Test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <lattice_factory.h>
#include <lattice_evolver.h>

#define L 128
#define C 4
#define V L*L*L

struct F {
	F() 
	{ 
		glat = latticeFactory::buildIsoLattice(L, C, {GPU, NULL, 0}, 3);
		clat = latticeFactory::buildIsoLattice(L, C, {CPU, NULL, 0}, 3);

		gobs = new Observer(glat);
		cobs = new Observer(clat);

	};

	~F() {
		delete gobs;
		delete cobs;
		delete glat;
		delete clat;
	};

	iLattice *glat;
	iLattice *clat;

	Observer *gobs, *cobs;
};

BOOST_FIXTURE_TEST_SUITE(s, F)
	BOOST_AUTO_TEST_CASE(classical) {
		glat->randomizePi(1.0);
		glat->randomizePhi(1.0);
		clat->randomizePi(1.0);
		clat->randomizePhi(1.0);
		
		gobs->init("OUTPUT/", "GPU");
		cobs->init("OUTPUT/", "CPU");

		gobs->update();
		cobs->update();

		BOOST_CHECK_EQUAL(gobs->getEKin(), cobs->getEKin());
		BOOST_CHECK_EQUAL(gobs->getEPot(), cobs->getEPot());
		BOOST_CHECK_EQUAL(gobs->getEnergy(), cobs->getEnergy());

		gobs->output(0.0); gobs->finalize();
		cobs->output(0.0); cobs->finalize();

	}

BOOST_AUTO_TEST_SUITE_END()
