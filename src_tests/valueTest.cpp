#define BOOST_TEST_MODULE Basic Test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <lattice_factory.h>
#include <lattice_evolver.h>
#include <logger.h>
logger::log_level_t GLOG_LEVEL = logger::LOG_INFO;

#define L 16
#define C 1
#define V L*L*L

struct F {
	F() 
	{ 
		glat = latticeFactory::buildIsoLattice(L, C, GPU);

		glat->generateLagrangeParameters(1.0, -1.0, 1.0);

		gev = new LatticeEvolver(glat);

		glat->randomizePi(0.1);
		glat->randomizePhi(0.1);
	};

	~F() {
		delete glat;
		delete gev;
	};

	iLattice3D *glat;

	LatticeEvolver *gev;
};

BOOST_FIXTURE_TEST_SUITE(s, F)
	BOOST_AUTO_TEST_CASE(classical) {
		gev->RTClassical(0.0, 100.0,.1,"OUTPUT/","RTGPU");
	}

	BOOST_AUTO_TEST_CASE(hmc) {
		gev->HMC(10.0, 1000, 0, "OUTPUT/", "HMCGPU");
	}

BOOST_AUTO_TEST_SUITE_END()
