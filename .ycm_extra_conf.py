import os
from os.path import splitext
import ycm_core

CUDA_EXT = ['.hcu', '.cu']

custom_includes = [
        '-Iinclude', 
        '-Iinclude_gpu', 
        '-Iinclude_cpu', 
        '-Iinclude_tools'
        ]

common = [
        '-std=c++14',
        #'-I/usr/local/include',
        #'-I/usr/include/x86_64-linux-gnu',
        #'-I/usr/bin/../lib/gcc/x86_64-linux-gnu/9.2.0/include',
        #'-I/usr/include',
        #'-I/usr/include/c++/9.2.0',
        '-I/opt/cuda/include'
        ]

wflags = [
        '-Wall',
        '-Wextra',
        '-Werror'
        ]

cpp_flags = [
        '-x', 'c++'
        ]

# http://llvm.org/docs/CompileCudaWithLLVM.html
cuda_flags = [
        '-x', 'cuda' 
        '--cuda-gpu-arch=sm_75'
        ]

def Settings( **kwargs ):
    baseflags = common + custom_includes + wflags

    if splitext( kwargs['filename'] )[1] in CUDA_EXT:
        return { 'flags': baseflags + cuda_flags }
    else:
        return { 'flags': baseflags + cpp_flags }
